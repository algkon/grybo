<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-08
 * Time: 02:24
 */

return [
    'admin.index' => 'Home',
    'admin.users.index' => 'Users',
    'admin.users.create' => 'Add User',
    'admin.users.edit' => 'Edit User',
    'admin.roles.index' => 'Roles',
    'admin.roles.create' => 'Add Role',
    'admin.roles.edit' => 'Edit Role',
    'admin.permissions.index' => 'Permissions',
];