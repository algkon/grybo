<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-11
 * Time: 18:22
 */

return [
    'created' => 'Row successfully created!',
    'updated' => 'Row successfully updated!',
    'deleted' => 'Row successfully deleted!',
];