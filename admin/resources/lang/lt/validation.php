<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-11
 * Time: 20:23
 */

return [
    'has_errors' => 'Neteisingai užpildyti privalomi laukai!',
    'token_mismatch' => 'Ops! Bandykit dar kartą.',
];