<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-13
 * Time: 10:27
 */

return [
    'admin.index' => 'Pagrindinis',
    'admin.users.index' => 'Vartotojai',
    'admin.users.create' => 'Pridėti naują vartotoją',
    'admin.users.edit' => 'Redaguoti vartotoją',
    'admin.roles.index' => 'Vartotojų tipai',
    'admin.roles.create' => 'Kurti naują vartotojo tipą',
    'admin.roles.edit' => 'Redaguoti vartotojo tipą',
    'admin.permissions.index' => 'Leidimai',
    'admin.pages.index' => 'Puslapiai',
    'admin.pages.create' => 'Kurti naują puslapį',
    'admin.pages.edit' => 'Redaguoti puslapį',
    'admin.pages.store' => 'Išsaugoti naują puslapį',
    'admin.pages.update' => 'Išsaugoti redaguojamą puslapį',
    'admin.pages.destroy' => 'Ištrinti puslapį',
    'admin.posts.index' => 'Įrašai',
    'admin.posts.create' => 'Kurti naują įrašą',
    'admin.posts.edit' => 'Redaguoti įrašą',
    'admin.posts.store' => 'Išsaugoti naują įrašą',
    'admin.posts.update' => 'Išsaugoti redaguojamą įrašą',
    'admin.posts.destroy' => 'Ištrinti įrašą',
    'admin.employments.index' => 'Užsiėmimai',
    'admin.employments.create' => 'Kurti naują užsiėmimą',
    'admin.employments.edit' => 'Redaguoti užsiėmimą',
    'admin.employments.store' => 'Išsaugoti naują užsiėmimą',
    'admin.employments.update' => 'Išsaugoti redaguojamą užsiėmimą',
    'admin.employments.destroy' => 'Ištrinti užsiėmimą',
    'admin.employments.calendar' => 'Užsiėmimų kalendorius',
    'admin.galleries.index' => 'Galerijos',
    'admin.galleries.create' => 'Kurti naują galeriją',
    'admin.galleries.edit' => 'Redaguoti galeriją',
    'admin.galleries.store' => 'Išsaugoti naują galeriją',
    'admin.galleries.update' => 'Išsaugoti redaguojamą galeriją',
    'admin.galleries.destroy' => 'Ištrinti galeriją',
    'admin.menu.index' => 'Navigacijos',
    'admin.menu.create' => 'Kurti naują navigaciją',
    'admin.menu.edit' => 'Redaguoti navigaciją',
    'admin.menu.store' => 'Išsaugoti naują navigaciją',
    'admin.menu.update' => 'Išsaugoti redaguojamą navigaciją',
    'admin.menu.destroy' => 'Ištrinti navigaciją',
    'admin.categories.index' => 'Kategorijos',
    'admin.categories.create' => 'Kurti naują kategoriją',
    'admin.categories.edit' => 'Redaguoti kategoriją',
    'admin.categories.store' => 'Išsaugoti naują kategoriją',
    'admin.categories.update' => 'Išsaugoti redaguojamą kategoriją',
    'admin.categories.destroy' => 'Ištrinti kategoriją',
    'admin.settings.index' => 'Nustatymai'
];