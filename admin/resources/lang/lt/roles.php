<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevi�ius
 * Date: 2016-09-13
 * Time: 12:17
 */

return [
    'admin' => 'Administratorius',
    'moderator' => 'Moderatorius',
    'customer' => 'Klientas',
    'coach' => 'Treneris',
];