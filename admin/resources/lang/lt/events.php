<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-11
 * Time: 18:21
 */

return [
    'created' => 'Įrašas sėkmingai sukurtas!',
    'updated' => 'Įrašas sėkmingai atnaujintas!',
    'deleted' => 'Įrašas sėkmingai ištrintas!',
    'saved'   => 'Pakeitimai sėkmingai išsaugoti!',
    'list.updated' => 'Sąrašas sėkmingai atnaujintas!',
    'send.successfully' => 'Laiškas sėkmingai išsiųstas!',
];