<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-05
 * Time: 12:32
 */

if (!function_exists('is_current_url')) {
    function is_current_route($route)
    {
        return $route == app('request')->route()->getName();
    }
}