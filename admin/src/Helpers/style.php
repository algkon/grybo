<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-07
 * Time: 17:39
 */

if (!function_exists('active_class')) {
    function active_class($return)
    {
        return $return ? 'active' : '' ;
    }
}

if (!function_exists('has_error')) {
    function has_error($errors, $name)
    {
        return $errors->has($name) ? 'has-error' : '' ;
    }
}