<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-12
 * Time: 09:36
 */

use Illuminate\Support\Collection;

if (!function_exists('dropdown_list')) {
    function dropdown_list($list, $prepend = [])
    {
        if (! is_array($list) && !($list instanceof Collection)) {
            return [];
        }

        $collection = is_array($list) ? new Collection($list) : $list ;

        foreach ($prepend as $prepend_key => $prepend_item) {
            $collection->prepend($prepend_item, $prepend_key);
        }

        return $collection;
    }
}