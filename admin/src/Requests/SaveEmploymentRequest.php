<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-23
 * Time: 13:56
 */

namespace Admin\Requests;


use Illuminate\Foundation\Http\FormRequest;

class SaveEmploymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth('admin')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:255',
            'user_id' => 'required|integer',
            'active' => 'required|integer',
//            'dates.*.starting_date' => 'required',
//            'dates.*.starting_time' => 'required',
//            'dates.*.duration' => 'required',
//            'dates.*.people_max' => 'required',
        ];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return [
            'user_id' => 'Treneris'
        ];
    }
}