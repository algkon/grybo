<?php

namespace Admin\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class CreatePageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth('admin')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'slug' => 'alpha_dash|unique:pages',
            'user_id' => 'required|integer',
        ];
    }

    /**
     * @return array
     */
    public function all()
    {
        $attributes = parent::all();

        // you can add fields
        $attributes['slug'] = Str::slug($attributes['title']);

        $this->replace($attributes);
        return parent::all();
    }
}
