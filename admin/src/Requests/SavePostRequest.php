<?php

namespace Admin\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class SavePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth('admin')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'slug' => 'alpha_dash',
            'user_id' => 'required|integer',
            'category_id' => 'required_with:categories|integer',
            'active' => 'required|integer',
        ];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return [
            'category_id' => 'Pagrindinė kategorija'
        ];
    }

    /**
     * @return array
     */
    public function all()
    {
        $attributes = parent::all();

        // you can add fields
        $attributes['slug'] = Str::slug($attributes['title']);

        // detect categories
        $attributes['category_id'] = isset($attributes['category_id']) ? $attributes['category_id'] : "" ;

        $this->replace($attributes);
        return parent::all();
    }
}
