<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-10-18
 * Time: 18:23
 */

namespace Admin\Requests;


use Illuminate\Foundation\Http\FormRequest;

class SubscriberMessageSendRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth('admin')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subject' => 'required',
            'content' => 'required',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'content.required' => ':attribute negali būti tuščias'
        ];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return [
            'content' => 'Pranešimas'
        ];
    }
}