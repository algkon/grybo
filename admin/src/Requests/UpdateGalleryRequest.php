<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevi�ius
 * Date: 2016-09-22
 * Time: 16:27
 */

namespace Admin\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class UpdateGalleryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth('admin')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:255',
            'slug' => 'alpha_dash|unique:galleries,slug,' . $this->route('gallery'),
            'user_id' => 'required|integer',
        ];
    }

    /**
     * @return array
     */
    public function all()
    {
        $attributes = parent::all();

        // you can add fields
        $attributes['slug'] = Str::slug($attributes['title']);

        $this->replace($attributes);
        return parent::all();
    }
}
