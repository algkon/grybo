<?php

namespace Admin\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth('admin')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required_without:api_user|max:255',
            'email' => 'required_without:api_user|email|max:255|unique:users,email,' . $this->route('user'),
            'password' => 'confirmed|min:6',

            'api.first_name' => 'required_with:api_user|max:255',
            'api.last_name' => 'required_with:api_user|max:255',
            'api.phone' => 'numeric',
            'api.addr_house' => 'numeric',
            'api.addr_flat' => 'numeric',

            'roles_ids' => 'required',
            'active' => 'required|integer',
        ];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return [
            'name' => 'Vardas, Pavardė'
        ];
    }

    /**
     * @return array
     */
    public function all()
    {
        $attributes = parent::all();

        // remove password if empty
        if (isset($attributes['password']) && strlen(trim($attributes['password'])) == 0) {
            unset($attributes['password']);
        }

        $this->replace($attributes);

        return parent::all();
    }
}
