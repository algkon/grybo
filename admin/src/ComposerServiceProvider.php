<?php

namespace Admin;

use Admin\Repositories\CategoryRepositoy;
use Admin\Repositories\EmploymentUserRepository;
use Admin\Repositories\OrderRepository;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(EmploymentUserRepository $employment_user, CategoryRepositoy $category, OrderRepository $order)
    {
        View::share('admin_menu_categories', $category->inAdminMenu());
        View::share('employments_users_requests_count', $employment_user->new_registrations());
        View::share('new_orders_count', $order->countNewOrders());
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
