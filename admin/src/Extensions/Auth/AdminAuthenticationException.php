<?php

namespace Admin\Extensions\Auth;

use Illuminate\Auth\AuthenticationException;

class AdminAuthenticationException extends AuthenticationException
{
    public $guard = 'admin';
}
