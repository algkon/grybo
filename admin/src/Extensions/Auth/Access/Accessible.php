<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-10
 * Time: 15:46
 */

namespace Admin\Extensions\Auth\Access;

use Admin\Models\Role;

trait Accessible
{
    /**
     * @param $value
     * @return mixed
     */
    public function getRolesIdsAttribute()
    {
        return $this->roles->pluck('id')->toArray();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * @param Role $role
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function giveRole(Role $role)
    {
        return $this->roles()->save($role);
    }

    /**
     * @param $role
     * @return bool
     */
    public function hasRole($role)
    {
        if (is_string($role)) {
            return $this->roles->contains('name', $role);
        }

        return !! $role->intersect($this->roles)->count();
    }
}