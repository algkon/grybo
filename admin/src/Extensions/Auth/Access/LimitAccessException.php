<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-10
 * Time: 16:41
 */

namespace Admin\Extensions\Auth\Access;


use Illuminate\Auth\AuthenticationException;

class LimitAccessException extends AuthenticationException
{
}