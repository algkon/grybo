<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-10
 * Time: 15:49
 */

namespace Admin\Extensions\Auth\Access;

use Admin\Models\Permission;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AccessServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->definePermissions();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Define user permissions
     */
    protected function definePermissions()
    {
        foreach ($this->getPermissions() as $permission) {
            $this->definePermission($permission);
        }
    }

    /**
     * @param $permission
     * @return bool
     */
    protected function definePermission($permission)
    {
        $this->app['Illuminate\Contracts\Auth\Access\Gate']->define($permission->name, function ($user) use ($permission) {

            if (!method_exists($user, 'hasRole')) {
                return false;
            }

            return $user->hasRole($permission->roles);
        });
    }

    /**
     * @return array|\Illuminate\Database\Eloquent\Collection|static[]
     */
    protected function getPermissions()
    {
        if (!Schema::hasTable('roles')
            || !Schema::hasTable('permissions')
            || !Schema::hasTable('permission_role')
            || !Schema::hasTable('role_user')
        ) {
            return [];
        }

        return Permission::with('roles')->get();
    }
}