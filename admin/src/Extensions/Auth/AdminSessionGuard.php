<?php
/**
 * Created by PhpStorm.
 * User: polki
 * Date: 2016-09-02
 * Time: 15:14
 */

namespace Admin\Extensions\Auth;


use Illuminate\Auth\SessionGuard;

class AdminSessionGuard extends SessionGuard
{
    /**
     * Determine if the current user is authenticated.
     *
     * @return \Illuminate\Contracts\Auth\Authenticatable
     *
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function authenticate()
    {
        if (! is_null($user = $this->user())) {
            return $user;
        }

        throw new AdminAuthenticationException;
    }

    /**
     * @return bool
     */
    public function check()
    {
        return ! is_null($this->user()) && $this->user()->isActive();
    }
}