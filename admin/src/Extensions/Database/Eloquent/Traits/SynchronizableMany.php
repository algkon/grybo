<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-25
 * Time: 01:29
 */

namespace Admin\Extensions\Database\Eloquent\Traits;


use Admin\Extensions\Database\Eloquent\Relations\HasManyExtended;

trait SynchronizableMany
{
    /**
     * Define a one-to-many relationship.
     *
     * @param  string  $related
     * @param  string  $foreignKey
     * @param  string  $localKey
     * @return \Admin\Extensions\Database\Eloquent\Relations\HasManyExtended
     */
    public function hasMany($related, $foreignKey = null, $localKey = null)
    {
        $foreignKey = $foreignKey ?: $this->getForeignKey();

        $instance = new $related;

        $localKey = $localKey ?: $this->getKeyName();

        return new HasManyExtended($instance->newQuery(), $this, $instance->getTable().'.'.$foreignKey, $localKey);
    }
}