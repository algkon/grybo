<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-25
 * Time: 01:26
 */

namespace Admin\Extensions\Database\Eloquent\Relations;


use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class HasManyExtended extends HasMany
{
    /**
     * Sync table records.
     *
     * @param  \Illuminate\Database\Eloquent\Collection|array $ids
     * @param  bool $deleting
     * @return array
     */
    public function sync($ids, $deleting = true)
    {
        $changes = [
            'inserted' => [], 'deleted' => [], 'updated' => [],
        ];

        if ($ids instanceof Collection) {
            $ids = $ids->modelKeys();
        }

        // First we need to attach any of the associated models that are not currently
        // in this joining table. We'll spin through the given IDs, checking to see
        // if they exist in the array of current ones, and if not we will insert.
        $current = $this->related
            ->where($this->getPlainForeignKey(), $this->parent->getKey())
            ->pluck($this->localKey)->all();

        $records = $this->formatRecordsList($ids);

        $delete = array_diff($current, array_keys($records));

        // Next, we will take the differences of the currents and given IDs and detach
        // all of the entities that exist in the "current" array but are not in the
        // array of the new IDs given to the method which will complete the sync.
        if ($deleting && count($delete) > 0) {
            $this->delete($delete);

            $changes['deleted'] = $this->castKeys($delete);
        }

        // Now we are finally ready to attach the new records. Note that we'll disable
        // touching until after the entire operation is complete so we don't fire a
        // ton of touch operations until we are totally done syncing the records.
        $changes = array_merge(
            $changes, $this->insertNew($records, $current)
        );

        return $changes;
    }


    /**
     * Format the sync/toggle list so that it is keyed by ID.
     *
     * @param  array $records
     * @return array
     */
    protected function formatRecordsList(array $records)
    {
        $results = [];

        foreach ($records as $id => $attributes) {
            if (!is_array($attributes)) {
                list($id, $attributes) = [$attributes, []];
            }

            $results[$id] = $attributes;
        }

        return $results;
    }

    /**
     * Delete models from the relationship.
     *
     * @param  mixed $ids
     * @param  bool $touch
     * @return int
     */
    protected function delete($ids = [])
    {
        if ($ids instanceof Model) {
            $ids = $ids->getKey();
        }

        if ($ids instanceof Collection) {
            $ids = $ids->modelKeys();
        }

        $model = $this->related;

        // If associated IDs were passed to the method we will only delete those
        // associations, otherwise all of the association ties will be broken.
        // We'll return the numbers of affected rows when we do the deletes.
        $ids = (array)$ids;

        if (count($ids) == 0) {
            return null;
        }

        // Once we have all of the conditions set on the statement, we are ready
        // to run the delete on the table.
        $results = $model->whereIn($this->localKey, $ids)->delete();

        return $results;
    }

    /**
     * Insert all of the IDs that aren't in the current array.
     *
     * @param  array $records
     * @param  array $current
     * @param  bool $touch
     * @return array
     */
    protected function insertNew(array $records, array $current)
    {
        $changes = ['inserted' => [], 'updated' => []];

        foreach ($records as $id => $attributes) {
            // If the ID is not in the list of existing IDs, we will insert a new record,
            // otherwise, we will just update this existing record on this joining
            // table, so that the developers will easily update these records pain free.
            if (!in_array($id, $current)) {
                $new_record = $this->create($attributes);

                $changes['inserted'][] = $new_record->id;
            }

            // Now we'll try to update an existing record.
            // If the model is actually updated we will add it to the
            // list of updated records so we return them back out to the consumer.
            elseif (count($attributes) > 0 &&
                $this->updateExisting($id, $attributes)
            ) { //$this->prepareUpdate($id, $attributes)
                $changes['updated'][] = is_numeric($id) ? (int)$id : (string)$id;
            }
        }

        return $changes;
    }

    /**
     * @param $id
     * @param $attributes
     * @return mixed
     */
    protected function updateExisting($id, $attributes)
    {
        $attributes[$this->getPlainForeignKey()] = $this->parent->getKey();

        return $this->related->where($this->related->getKeyName(), $id)->update($attributes);
    }

    /**
     * Cast the given keys to integers if they are numeric and string otherwise.
     *
     * @param  array $keys
     * @return array
     */
    protected function castKeys(array $keys)
    {
        return (array)array_map(function ($v) {
            return is_numeric($v) ? (int)$v : (string)$v;
        }, $keys);
    }
}