<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-05
 * Time: 14:19
 */

namespace Admin\Extensions\Search;


trait Searchable
{
    /**
     * @param int $perPage
     * @param array $columns
     * @return mixed
     */
    public function paginate($perPage = null, $columns = array('*'))
    {
        return $this->search()->paginate($perPage, $columns);
    }

    /**
     * @param $query
     * @param $request
     * @return mixed
     */
    public function scopeSearch($query)
    {
        $search_query = request('q');

        // if searching
        if ($search_query && property_exists($this, 'searchable')) {
            foreach ($this->searchable as $column) {
                $query->orWhere($column, 'like', '%' . $search_query . '%');
            }
        }

        return $query;
    }
}