<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-22
 * Time: 16:16
 */

namespace Admin\Extensions\Images;


use Admin\Models\Image;
use Illuminate\Support\Collection;

trait Imageable
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function image()
    {
        return $this->morphOne(\Admin\Models\Image::class, 'imageable')->orderBy('ordering');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function images()
    {
        return $this->morphMany(\Admin\Models\Image::class, 'imageable')->orderBy('ordering');
    }

    /**
     * @return bool
     */
    public function hasThumbnail()
    {
        if (is_null($this->images->first())) {
            return false;
        }

        return true;
    }

    /**
     * @return mixed
     */
    public function thumbnail($size = 'small')
    {
        if (is_null($this->images->first())) {
            return '';
        }

        return $this->images->first()->path($size);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function attachment()
    {
        return $this->image();
    }

    /**
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Model|int
     */
    public function setAttachment(array $data)
    {
        return $this->saveImage($data);
    }

    /**
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Model|int
     */
    public function saveImage(array $data)
    {
        if (empty($data)) {
            return $this->image()->delete();
        }

        if ($this->image && !empty($data)) {
            return $this->image->fill($data)->save();
        }

        if (is_null($this->image) && !empty($data)) {
            return $this->image()->create($data);
        }

        return false;
    }

    /**
     * @param array $images
     * @return null
     */
    public function syncImages(array $images)
    {
        if (empty($images)) {
            return null;
        }

        $new_collection = new Collection($images);
        $db_collection = $this->images->pluck('name', 'id');

        $delete_images = $db_collection->diffKeys($new_collection);
//        $insert_images = $new_collection->diffKeys($db_collection);
        $update_or_create_images = $new_collection->diffKeys($delete_images);

        $this->images()->whereIn('id', $delete_images->keys())->delete();

        foreach ($update_or_create_images->all() as $id => $image) {
            $this->images()->updateOrCreate(['id' => $image['id']], $image);
        }
    }
}