<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevi�ius
 * Date: 2016-09-13
 * Time: 15:06
 */

namespace Admin\Extensions\Metadata;

trait Metadataable
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function metadata()
    {
        return $this->morphOne(\Admin\Models\Metadata::class, 'metadataable');
    }

    /**
     * @param array $data
     */
    public function saveMetadata(array $data)
    {
        if ($this->metadata) {
            $this->metadata()->update($data);
        } else {
            $this->metadata()->create($data);
        }
    }

    /**
     * @return mixed
     */
    public function getMetaTitleAttribute()
    {
        return $this->isMetaCreated('title') ? $this->metadata->title : $this->title ;
    }

    /**
     * @return string
     */
    public function getMetaDescriptionAttribute()
    {
        return $this->isMetaCreated('description') ? $this->metadata->description : '' ;
    }

    /**
     * @return string
     */
    public function getMetaKeywordsAttribute()
    {
        return $this->isMetaCreated('keywords') ? $this->metadata->keywords : '' ;
    }

    /**
     * @param $item
     * @return bool
     */
    public function isMetaCreated($item)
    {
        return !is_null($this->metadata) && !is_null($this->metadata->{$item}) && strlen($this->metadata->{$item}) > 0;
    }
}