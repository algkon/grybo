<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-23
 * Time: 04:03
 */

namespace Admin\Extensions\Galleries;


trait Galleryable
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function gallery()
    {
        return $this->belongsTo(\Admin\Models\Gallery::class);
    }

    /**
     * @return string
     */
    public function getGalleryTitleAttribute()
    {
        return $this->gallery ? $this->gallery->title : "" ;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function galleries()
    {
        return $this->morphToMany(\Admin\Models\Gallery::class, 'galleryable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function publishedGalleries()
    {
        return $this->morphToMany(\Admin\Models\Gallery::class, 'galleryable')->where('active', '1');
    }
}