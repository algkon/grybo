<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-11
 * Time: 13:53
 */

namespace Admin\Extensions\Categories;


trait Categoryable
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(\Admin\Models\Category::class);
    }

    /**
     * @return string
     */
    public function getCategoryTitleAttribute()
    {
        return $this->category ? $this->category->title : "" ;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function categories()
    {
        return $this->morphToMany(\Admin\Models\Category::class, 'categoryable');
    }
}