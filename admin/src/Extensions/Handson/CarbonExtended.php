<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-10-21
 * Time: 19:02
 */

namespace Admin\Extensions\Handson;


use Carbon\Carbon;

class CarbonExtended extends Carbon
{
    /**
     * @var array
     */
    protected static $weekdays = [];

    /**
     * @param null $year
     * @param null $month
     * @param null $day
     * @param null $hour
     * @param null $minute
     * @param null $second
     * @param null $tz
     * @return array
     */
    public function weekDays($year = null, $month = null, $day = null)
    {
        static::$weekdays = [];

        for ($i = 0; $i <= 6; $i++) {
            $weekday = $this->create($year, $month, $day)
                ->startOfWeek()
                ->addDay($i);

            array_push(static::$weekdays, $weekday);
        }

        return static::$weekdays;
    }

    /**
     * @param null $year
     * @param null $month
     * @param null $day
     * @return array
     */
    public function weekDaysAsDateString($year = null, $month = null, $day = null)
    {
        return array_map(function ($value) {
            return $value->toDateString();
        }, $this->weekDays($year, $month, $day));
    }

    /**
     * @param null $year
     * @param null $month
     * @param null $day
     * @return static
     */
    public function startOfNextWeek($year = null, $month = null, $day = null)
    {
        return $this->create($year, $month, $day)->startOfWeek()->addWeek();
    }

    /**
     * @param null $year
     * @param null $month
     * @param null $day
     * @return static
     */
    public function endOfNextWeek($year = null, $month = null, $day = null)
    {
        return $this->create($year, $month, $day)->endOfWeek()->addWeek();
    }

    /**
     * @param null $year
     * @param null $month
     * @param null $day
     * @return static
     */
    public function startOfPreviousWeek($year = null, $month = null, $day = null)
    {
        return $this->create($year, $month, $day)->startOfWeek()->subWeek();
    }

    /**
     * @param null $year
     * @param null $month
     * @param null $day
     * @return static
     */
    public function endOfPreviousWeek($year = null, $month = null, $day = null)
    {
        return $this->create($year, $month, $day)->endOfWeek()->subWeek();
    }
}