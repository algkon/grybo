<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-10-22
 * Time: 00:35
 */

namespace Admin\Extensions\Handson;


use Admin\Models\TimetablesCalendar;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Debug\Dumper;

class WeekTable
{
    /**
     * @var CarbonExtended
     */
    protected $carbon;

    /**
     * @var array
     */
    protected $header_columns = [];

    /**
     * @var array
     */
    protected $data = [];

    /**
     * @var array
     */
    protected $columns_definitions = [];

    /**
     * @var array
     */
    protected $weekdays_names = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];

    /**
     * @var array
     */
    protected $weekdays_dates = [];

    /**
     * @var array
     */
    protected $fillable_attributes;

    /**
     * @var array
     */
    protected $attributes;

    /**
     * @var
     */
    public $week_start_at;

    /**
     * @var
     */
    public $week_end_at;

    /**
     * Table constructor.
     * @param Model $model
     * @param CarbonExtended $carbon
     * @param null $week_start_at
     * @param null $week_end_at
     */
    public function __construct(Model $model, CarbonExtended $carbon, $week_start_at = null, $week_end_at = null)
    {
        $this->carbon = $carbon;
        $this->fillable_attributes = array_merge((array)$model->getKeyName(), $model->getFillable());
        $this->attributes = array_merge($this->fillable_attributes, $model->getAppends());

        $this->setCurrentWeek($week_start_at, $week_end_at);

        $this->weekdays_dates = $this->carbon->weekDaysAsDateString(
            $this->week_start_at->year,
            $this->week_start_at->month,
            $this->week_start_at->day
        );

        $this->columns_definitions = array_combine($this->weekdays_names, $this->weekdays_dates);
    }

    /**
     * @return CarbonExtended
     */
    public function timing()
    {
        return $this->carbon;
    }

    /**
     * @param $week_start_at
     * @param $week_end_at
     */
    protected function setCurrentWeek($week_start_at, $week_end_at)
    {
        $this->week_start_at = is_null($week_start_at)
            ? $this->carbon->now()->startOfWeek()
            : $this->carbon->createFromTimestamp(strtotime($week_start_at));

        $this->week_end_at = is_null($week_start_at)
            ? $this->carbon->now()->endOfWeek()
            : $this->carbon->createFromTimestamp(strtotime($week_end_at));
    }

    /**
     * @return array
     */
    public function getFillableAttributes()
    {
        return $this->fillable_attributes;
    }

    /**
     * @return array
     */
    public function getDatesOfWeekDays()
    {
        return $this->weekdays_dates;
    }

    /**
     * @return Collection
     */
    public function getNamedDatesOfWeekDays()
    {
        return new Collection($this->columns_definitions);
    }

    /**
     * @return Collection
     */
    public function getHeaderColumns()
    {
        foreach ($this->columns_definitions as $weekday => $weekday_date) {
            $this->header_columns[$weekday] = $this->formatHeaderColumn($weekday, $weekday_date);
        }

        return new Collection($this->header_columns);
    }

    /**
     * @param $weekday
     * @param $weekday_date
     * @return string
     */
    protected function formatHeaderColumn($weekday, $weekday_date)
    {
        return sprintf("<strong class=\'day-name\'>%s</strong> <br> <small>%s</small>",
            trans('date.weekdays.' . $weekday),
            $weekday_date
        );
    }

    /**
     * @param Collection $collection
     * @return $this
     */
    public function make(Collection $collection)
    {
//        $default = new Collection(settings('timetable_calendar_default', []));
//        $collection = $default->merge($collection);

        if (!$collection->count()) {
            return $this;
        }

        $total_rows = $collection->max('row') + 1;

        $this->makeData($collection, $total_rows);

        return $this;
    }

    /**
     * @param Collection $collection
     * @param $total_rows
     */
    protected function makeData(Collection $collection, $total_rows)
    {
        for ($row = 0; $row < $total_rows; $row++) {
            foreach ($this->weekdays_names as $col => $week_day) {
                $this->data[$row][$week_day] = $this->makeTimeData($collection, $row, $col);
            }
        }
    }

    /**
     * @param Collection $collection
     * @param $row
     * @param $col
     * @return Collection|mixed
     */
    protected function makeTimeData(Collection $collection, $row, $col)
    {
        $empty = $this->getEmptyAttributesObject();

        $record = $collection->where('row', $row)->where('col', $col)->first();

        return !is_null($record) ? $record : $empty ;
    }

    /**
     * @return Collection
     */
    public function getEmptyAttributesObject()
    {
        return new Collection(array_fill_keys($this->attributes, null));
    }

    /**
     * @return array
     */
    public function getEmptyRow()
    {
        $row = [];

        foreach ($this->weekdays_names as $week_day_name) {
            $row[$week_day_name] = $this->getEmptyAttributesObject() ;
        }

        return [$row,$row,$row,$row];
    }

    /**
     * @param $data
     * @return Collection|static
     */
    public function setDefaultData($data)
    {
        $dates = new Collection($data);
        $dates = $dates->map(function ($value, $key) {

            $empty = $this->getEmptyAttributesObject();
            $empty->put('starting_time', $value['starting_time']);
            $empty->put('ending_time', $value['ending_time']);
            $empty->put('status', null);
            $empty->put('row', (int)$value['row']);
            $empty->put('col', (int)$value['col']);
            $empty->put('reserved_from', null);
            $empty->put('reserved_to', null);

            return $empty;
        });

        return $dates;
    }

    /**
     * @return Collection
     */
    public function getData()
    {
        if (empty($this->data)) {
            return new Collection($this->getEmptyRow());
        }

        return new Collection($this->data);
    }

    /**
     * @return string
     */
    public function showTitle()
    {
        return $this->week_start_at->toDateString() . '/' . $this->week_end_at->toDateString();
    }

    /**
     * @return bool
     */
    public function showTitlePrevious()
    {
        return $this->week_start_at  > $this->carbon->now()->endOfWeek();
    }

    /**
     * @return string
     */
    public function titlePrevious()
    {
        return sprintf('%s - %s', $this->startOfPreviousWeek(), $this->endOfPreviousWeek());
    }

    /**
     * @return string
     */
    public function titleNext()
    {
        return sprintf('%s - %s', $this->startOfNextWeek(), $this->endOfNextWeek());
    }

    /**
     * @return string
     */
    public function startOfNextWeek()
    {
        return $this->carbon->startOfNextWeek(
            $this->week_start_at->year,
            $this->week_start_at->month,
            $this->week_start_at->day
        )->toDateString();
    }

    /**
     * @return string
     */
    public function endOfNextWeek()
    {
        return $this->carbon->endOfNextWeek(
            $this->week_start_at->year,
            $this->week_start_at->month,
            $this->week_start_at->day
        )->toDateString();
    }

    /**
     * @return string
     */
    public function startOfPreviousWeek()
    {
        return $this->carbon->startOfPreviousWeek(
            $this->week_start_at->year,
            $this->week_start_at->month,
            $this->week_start_at->day
        )->toDateString();
    }

    /**
     * @return string
     */
    public function endOfPreviousWeek()
    {
        return $this->carbon->endOfPreviousWeek(
            $this->week_start_at->year,
            $this->week_start_at->month,
            $this->week_start_at->day
        )->toDateString();
    }
}