<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-10-11
 * Time: 03:05
 */

namespace Admin\Extensions\Layout;


trait Layoutable
{
    /**
     * @return string
     */
    public function getLayoutNameAttribute()
    {
        return $this->layout ? $this->layout->name : null ;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function layout()
    {
        return $this->morphOne(\Admin\Models\Layout::class, 'layoutable');
    }

    /**
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Model|int
     */
    public function setLayout(array $data)
    {
        if ($this->layout && $data['name'] == "") {
            return $this->layout()->delete();
        }

        if ($this->layout && $data['name'] != "") {
            return $this->layout()->update($data);
        }

        if (is_null($this->layout) && $data['name'] != "") {
            return $this->layout()->create($data);
        }

        return false;
    }
}