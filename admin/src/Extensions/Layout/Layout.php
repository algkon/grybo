<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-10-11
 * Time: 03:37
 */

namespace Admin\Extensions\Layout;


use Illuminate\Support\Collection;

class Layout
{
    /**
     * @var mixed
     */
    private $layouts;

    /**
     * Layout constructor.
     */
    public function __construct()
    {
        $this->layouts = new Collection(config('view.layouts'));
    }

    /**
     * @return Collection
     */
    public function getList()
    {
        $collection = new Collection();

        foreach($this->layouts->all() as $view => $label) {
            if (view()->exists($view)) {
                $collection->put($view, $label);
            }
        }

        return $collection;
    }
}