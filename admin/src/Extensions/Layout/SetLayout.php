<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-10-11
 * Time: 04:16
 */

namespace Admin\Extensions\Layout;


trait SetLayout
{
    /**
     * @param $view
     * @param null $layout
     * @return null
     */
    protected function getLayout($view, $layout = null)
    {
        if (is_null($layout) || !view()->exists($layout)) {
            return $view;
        }

        return $layout;
    }
}