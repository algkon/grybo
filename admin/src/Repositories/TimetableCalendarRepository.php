<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-10-21
 * Time: 18:36
 */

namespace Admin\Repositories;


use Admin\Extensions\Handson\CarbonExtended;
use Admin\Extensions\Handson\WeekTable;
use Illuminate\Support\Collection;

class TimetableCalendarRepository extends Repository
{
    /**
     * @return mixed
     */
    function model()
    {
        return \Admin\Models\TimetablesCalendar::class;
    }

    /**
     * @param $id
     * @param $start_date
     * @param $end_date
     * @return WeekTable
     */
    function handsonTableData($id, $start_date, $end_date)
    {
        $week_table = new WeekTable($this->model, new CarbonExtended(), $start_date, $end_date);
        $dates = $this->model
            ->where('timetable_id', $id)
            ->where('start_date', $start_date)
            ->where('end_date', $end_date)
            ->orderBy('row')
            ->get($week_table->getFillableAttributes())->keyBy('unique_custom_key');

//        $default = new Collection(settings('timetable_calendar_default_' . $id, []));
        $dates = $dates->count() ? $dates : $this->getHandsonTableDataDefault($id);
//        $dates   = $default->merge($dates);

        $week_table->make($dates);

        return $week_table;
    }

    /**
     * @param $id
     * @return mixed
     */
    function getHandsonTableDataDefault($id, $start_date = null, $end_date = null)
    {
        $dates = $this->model
            ->where('timetable_id', $id)
            ->where('start_date', $start_date)
            ->where('end_date', $end_date)
            ->orderBy('row')
            ->get()->keyBy('unique_custom_key');

        return $dates;
    }

    /**
     * @param $data
     * @param $start_date
     * @param $end_date
     */
    function setHandsonTableDefaultData($dates, $id, $start_date = null, $end_date = null)
    {
//        $week_table = new WeekTable($this->model, new CarbonExtended(), $start_date, $end_date);
//        $dates = $week_table->setDefaultData($data);

//        dd($dates);
        $this->model
            ->where('timetable_id', $id)
            ->where('start_date', $start_date)
            ->where('end_date', $end_date)
            ->delete();

        foreach ($dates as $date) {
            $this->create(array_merge($date, [
                'id' => $id,
                'start_date' => $start_date,
                'end_date' => $end_date,
            ]));
        }

//        settings()->set('timetable_calendar_default_' . $id, $dates->all());
    }

    /**
     * @param $id
     * @param $data
     * @param $start_date
     * @param $end_date
     */
    function calendarUpdate($id, $data, $start_date, $end_date)
    {
        $week_table = new WeekTable($this->model, new CarbonExtended(), $start_date, $end_date);

        $this->model->where('timetable_id', $id)
            ->whereIn('date', $week_table->getDatesOfWeekDays())
            ->delete();

        foreach ($data as $key => $item) {
            $this->create($item);
        }
    }

}