<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-10
 * Time: 22:40
 */

namespace Admin\Repositories;


use Illuminate\Database\Eloquent\Model;

class GalleryRepository extends Repository
{
    /**
     * @return mixed
     */
    function model()
    {
        return \Admin\Models\Gallery::class;
    }

    /**
     * @param array $columns
     * @return mixed
     */
    public function allWithImages($columns = array('*'))
    {
        return $this->model->with('images')->get($columns);
    }

    /**
     * @param $id
     * @return mixed
     */
    function deleteWithRelationships($id)
    {
        $model = $this->find($id);
        $model->images()->delete();

        return $model->delete();
    }
}