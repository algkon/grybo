<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-13
 * Time: 08:45
 */

namespace Admin\Repositories;


use Admin\Models\Permission;
use Faker\Provider\DateTime;

class PermissionRepository extends Repository
{
    /**
     * @return mixed
     */
    function model()
    {
        return \Admin\Models\Permission::class;
    }

    /**
     * @param $routes
     * @return bool|mixed
     */
    function createIfNotExist($routes)
    {
        $new_routes = [];

        foreach ($routes as $route) {

            $name = $route->getName();

            if (is_null($name)) {
                continue;
            }

            $permission = $this->findBy('name', $name);

            if (is_null($permission)) {
                $new_routes[] = [
                    'name' => $name,
                    'label' => str_replace('admin::permissions.', '', trans('admin::permissions.' . $name)),
                    'created_at' => new \DateTime(),
                    'updated_at' => new \DateTime(),
                ];
            }
        }

        if (empty($new_routes)) {
            return false;
        }

        return $this->model->insert($new_routes);
    }
}