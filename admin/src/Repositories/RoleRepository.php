<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevi�ius
 * Date: 2016-09-08
 * Time: 16:26
 */

namespace Admin\Repositories;


use Illuminate\Support\Collection;

class RoleRepository extends Repository
{
    /**
     * @return mixed
     */
    function model()
    {
        return \Admin\Models\Role::class;
    }

    /**
     * @return mixed
     */
    function withPermissions()
    {
        return $this->model->with('permissions');
    }

    /**
     * @param $id
     * @param $permissions
     */
    function givePermissions($id, $permissions)
    {
        // prepare array
        $permissions = !is_array($permissions) ? [$permissions] : $permissions ;

        // filter array. Only permission ids need
        $permissions = array_where($permissions, function ($value, $key) {
            return is_numeric($value) ;
        });

        $role = $this->find($id);
        $role->permissions()->sync($permissions);
    }

    /**
     * @param array $prepend
     * @param array $put
     * @param array $pull
     * @return Collection
     */
    function getRolesDropdownList($prepend = [], $put = [], $pull = [], $forget = [])
    {
        $roles = $this->all();

        $collection = new Collection();
        foreach($roles as $role) {
            $collection->put($role->id, $role->getStyledLabel());
        }

        foreach ($pull as $pull_item) {
            $collection->pull($pull_item);
        }

        foreach ($prepend as $prepend_key => $prepend_item) {
            $collection->prepend($prepend_item, $prepend_key);
        }

        foreach ($put as $put_key => $put_item) {
            $collection->put($put_key, $put_item);
        }

        $collection->forget($forget);

        return $collection;
    }

    /**
     * @param $role
     * @return mixed
     */
    function users($role_name)
    {
        $role = $this->findBy('name', $role_name);

        if (is_null($role)) {
            return null;
        }

        return $role->users;
    }

    /**
     * @param $role_name
     * @return array|mixed
     */
    function getUsersDropdownList($role_name)
    {
        $users = $this->users($role_name);

        if (is_null($users)) {
            return ["" => str_repeat('-', 20)];
        }

        $users = $users->pluck('name', 'id');
        $users->prepend(str_repeat('-', 20), "");

        return $users;
    }
}