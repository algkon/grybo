<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-03
 * Time: 10:54
 */

namespace Admin\Repositories\Exceptions;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RepositoryException extends NotFoundHttpException
{

}