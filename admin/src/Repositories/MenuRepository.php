<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevi�ius
 * Date: 2016-09-20
 * Time: 09:39
 */

namespace Admin\Repositories;


class MenuRepository extends Repository
{
    /**
     * @return mixed
     */
    function model()
    {
        return \Admin\Models\Menu::class;
    }

    /**
     * @param array $links
     * @param null $id
     */
    function saveLinks(array $links, $id)
    {
        $menu = $this->find($id);
        $menu->links()->delete();

        foreach ($links as $link) {
            $menu->giveLink([
                'parent_id' => 0,
                'title' => $link->title,
                'slug' => $link->slug,
                'rel' => $link->rel,
                'target' => $link->target,
                'sort' => $link->sort,
            ]);
        }
    }
}