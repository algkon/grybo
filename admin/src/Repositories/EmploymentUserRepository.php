<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-26
 * Time: 15:58
 */

namespace Admin\Repositories;


class EmploymentUserRepository extends Repository
{
    /**
     * @return mixed
     */
    function model()
    {
        return \Admin\Models\EmploymentUser::class;
    }

    /**
     * @return mixed
     */
    function new_registrations()
    {
        return $this->model->where('status', '2')->count();
    }
}