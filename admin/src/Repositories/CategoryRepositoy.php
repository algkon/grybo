<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevi�ius
 * Date: 2016-09-11
 * Time: 15:39
 */

namespace Admin\Repositories;

use Admin\Models\Post;

class CategoryRepositoy extends Repository
{
    /**
     * @return mixed
     */
    function model()
    {
        return \Admin\Models\Category::class;
    }

    /**
     * @return mixed
     */
    function inAdminMenu()
    {
        return $this->model->where('admin_menu', "1")->get();
    }

    /**
     * @return mixed
     */
    function withParent()
    {
        return $this->model->with('parent');
    }

    /**
     * @param null $active
     * @return mixed
     */
    function rootCategoriesDropdownList($prepend = [], $put = [], $pull = [])
    {
        $builder = $this->model->where(['parent_id' => 0]);
        $collection = $builder->pluck('title', 'id');

        foreach ($pull as $pull_item) {
            $collection->pull($pull_item);
        }

        foreach ($prepend as $prepend_key => $prepend_item) {
            $collection->prepend($prepend_item, $prepend_key);
        }

        foreach ($put as $put_key => $put_item) {
            $collection->put($put_key, $put_item);
        }

        return $collection;
    }

    /**
     * @return mixed
     */
    function getTree()
    {
        return $this->model->with('child')->where(['parent_id' => 0, 'active' => '1'])->get();
    }

    /**
     * @param $id
     * @return mixed
     */
    function deleteWithRelationships($id)
    {
        $model = $this->find($id);
        $model->metadata()->delete();
        $model->images()->delete();
        $model->layout()->delete();

        $this->update(['parent_id' => 0], $id, 'parent_id');

        return $model->delete();
    }
}