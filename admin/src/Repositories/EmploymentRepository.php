<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-23
 * Time: 13:51
 */

namespace Admin\Repositories;


class EmploymentRepository extends Repository
{
    /**
     * @return mixed
     */
    function model()
    {
        return \Admin\Models\Employment::class;
    }

}