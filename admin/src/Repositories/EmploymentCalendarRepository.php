<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-26
 * Time: 10:41
 */

namespace Admin\Repositories;


use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class EmploymentCalendarRepository extends Repository
{
    /**
     * @return mixed
     */
    function model()
    {
        return \Admin\Models\EmploymentCalendar::class;
    }

    /**
     * @return $this
     */
    function dateDesc()
    {
        $this->model = $this->model->orderBy('starting_date')->orderBy('starting_time');

        return $this;
    }

    /**
     * @param $year
     * @param $month
     * @param null $day
     * @return mixed
     */
    function eventsByDate($year, $month, $day = null)
    {
        $query = $this->model->with('employment', 'users')->whereHas('employment', function ($query) {
            $query->where('active', "1");
        })->where('active', "1");

        $query->whereYear('starting_date', $year);
        $query->whereMonth('starting_date', $month);
        $query->whereRaw("concat(date(starting_date), ' ', time(starting_time)) >= ?", [Carbon::now()]);

        return $query->get();
    }
}