<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevi�ius
 * Date: 2016-09-03
 * Time: 11:30
 */

namespace Admin\Repositories;


class UserRepository extends Repository
{
    /**
     * @return mixed
     */
    function model()
    {
        return \Admin\Models\User::class;
    }

    /**
     * @param $id
     * @return mixed
     */
    function findWithRoles($id)
    {
        return $this->model->with('roles')->find($id);
    }

    /**
     * @return mixed
     */
    function allWithRoles($columns = array('*'))
    {
        return $this->model->with('roles')->get($columns);
    }
}