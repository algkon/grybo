<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-10-03
 * Time: 15:25
 */

namespace Admin\Repositories;


use Carbon\Carbon;

class OrderRepository extends Repository
{
    /**
     * @return mixed
     */
    function model()
    {
        return \Admin\Models\Order::class;
    }

    /**
     * @return mixed
     */
    function countNewOrders()
    {
        return $this->model->where('created_at', '>', settings('admin.orders.index.last_activity', ''))->count();
    }

    /**
     * @param $order_id
     * @param $cart
     * @param $price
     * @param $payment_method
     * @param array $customer
     * @param null $user_id
     */
    function make($order_id, $cart, $price, $discount_code = '', $payment_method, array $customer, $user_id = null)
    {
        return $this->create([
            'user_id' => $user_id,
            'order_id' => $order_id,
            'payment_method' => $payment_method,
            'cart' => $cart,
            'price' => $price,
            'discount_code' => $discount_code,
            'first_name' => $customer['first_name'],
            'last_name' => $customer['last_name'],
            'email' => $customer['email'],
            'phone' => $customer['phone'],
            'ip' => $customer['ip'],
            'status' => "0",
        ]);
    }

    /**
     * @param $id
     * @return mixed|null
     */
    function getByOrderId($id)
    {
        if (is_null($id)) {
            return null;
        }

        return $this->findBy('order_id', $id);
    }

    /**
     * @param $order_id
     * @return mixed
     */
    function setAsPayed($order_id)
    {
        return $this->update([
            'status' => "1",
            'payed_at' => Carbon::now()
        ], $order_id, 'order_id');
    }
}