<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevi�ius
 * Date: 2016-09-10
 * Time: 21:55
 */

namespace Admin\Repositories;


class PostRepository extends Repository
{
    /**
     * @return mixed
     */
    function model()
    {
        return \Admin\Models\Post::class;
    }

    /**
     * @param $id
     * @param array $columns
     * @return mixed
     */
    function findPostWithCategories($id, $columns = array('*'))
    {
        return $this->model->with('categories')->find($id, $columns);
    }

    /**
     * @param $id
     * @param int $limit
     * @return mixed
     */
    function getLastsPostsByCategoryId($id, $limit = 10)
    {
        return $this->model->with('category')
            ->where('category_id', $id)
            ->where('active', '1')
            ->orderBy('created_at', 'desc')
            ->limit($limit)
            ->get();
    }

    /**
     * @param array $ids
     * @return mixed
     */
    function getPostsByIds(array $ids)
    {
        return $this->model->with('category')
            ->whereIn('id', $ids)
            ->where('active', '1')
            ->get();
    }

    /**
     * @param $id
     * @return mixed
     */
    function deleteWithRelationships($id)
    {
        $model = $this->find($id);
        $model->categories()->detach();
        $model->galleries()->detach();
        $model->images()->delete();
        $model->metadata()->delete();
        $model->layout()->delete();

        return $model->delete();
    }
}