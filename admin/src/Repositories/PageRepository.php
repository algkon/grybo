<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevi�ius
 * Date: 2016-09-10
 * Time: 19:44
 */

namespace Admin\Repositories;


class PageRepository extends Repository
{
    function model()
    {
        return \Admin\Models\Page::class;
    }

    /**
     * @param $id
     * @return mixed
     */
    function deleteWithRelationships($id)
    {
        $model = $this->find($id);
        $model->galleries()->detach();
        $model->images()->delete();
        $model->metadata()->delete();
        $model->layout()->delete();

        return $model->delete();
    }
}