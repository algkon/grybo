<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevi�ius
 * Date: 2016-09-20
 * Time: 08:48
 */

namespace Admin\Repositories;


class SubscriberRepository extends Repository
{
    /**
     * @return mixed
     */
    function model()
    {
        return \Admin\Models\Subscriber::class;
    }

    /**
     * @param $email
     * @return mixed
     */
    function subscribe($email)
    {
        return $this->create([
            'email' => $email,
            'unsubscribe_token' => str_random(60)
        ]);
    }

    /**
     * @param $unsubscribe_token
     * @return null
     */
    function unsubscribe($unsubscribe_token)
    {
        $subscriber = $this->findBy('unsubscribe_token', $unsubscribe_token);

        if (is_null($subscriber)) {
            return null;
        }

        return $subscriber->delete();
    }
}