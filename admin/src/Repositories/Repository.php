<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevi�ius
 * Date: 2016-09-03
 * Time: 10:48
 */

namespace Admin\Repositories;


use Admin\Repositories\Contracts\RepositoryInterface;
use Admin\Repositories\Exceptions\RepositoryException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Container\Container as App;
use Illuminate\Http\Request;

abstract class Repository implements RepositoryInterface
{

    /**
     * @var App
     */
    private $app;

    /**
     * @var
     */
    protected $model;

    /**
     * @param App $app
     * @throws \Admin\Repositories\Exceptions\RepositoryException
     */
    public function __construct(App $app)
    {
        $this->app = $app;
        $this->makeModel();
    }

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    abstract function model();

    /**
     * @param array $columns
     * @return mixed
     */
    public function all($columns = array('*'))
    {
        return $this->model->get($columns);
    }

    /**
     * @param int $perPage
     * @param array $columns
     * @return mixed
     */
    public function paginate($perPage = null, $columns = array('*'))
    {
        return $this->model->paginate($perPage, $columns);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        return $this->model->create($data);
    }

    /**
     * @param array $data
     * @param $id
     * @param string $attribute
     * @return mixed
     */
    public function update(array $data, $id, $attribute = "id")
    {
        return $this->model->where($attribute, '=', $id)->update($data);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    /**
     * @param $id
     * @param array $columns
     * @return mixed
     */
    public function find($id, $columns = array('*'))
    {
        return $this->model->find($id, $columns);
    }

    /**
     * @param $attribute
     * @param $value
     * @param array $columns
     * @return mixed
     */
    public function findBy($attribute, $value, $columns = array('*'))
    {
        return $this->model->where($attribute, '=', $value)->first($columns);
    }

    /**
     * @param ...$relationships
     * @return mixed
     */
    function allWithRelationships($relations)
    {
        if (is_string($relations)) {
            $relations = func_get_args();
        }

        return $this->model->with($relations)->get();
    }

    /**
     * @param null $perPage
     * @param array $columns
     * @param $relations
     * @return mixed
     */
    function paginateWithRelationships($perPage = null, $columns = array('*'), $relations)
    {
        if (is_string($relations)) {
            $relations = func_get_args();
            $relations = array_slice($relations, 2);
        }

        return $this->model->with($relations)->paginate($perPage, $columns);
    }

    /**
     * @return mixed
     */
    function active()
    {
        $this->model = $this->model->where('active', '1');

        return $this;
    }

    /**
     * @param Request $request
     * @return $this
     */
    function filter(Request $request)
    {
        if ($request->has('filter')) {
            $this->model = $this->model->where($request->get('filter', []));
        }

        return $this;
    }

    /**
     * @return Model
     * @throws RepositoryException
     */
    public function makeModel()
    {
        $model = $this->app->make($this->model());

        if (!$model instanceof Model)
            throw new RepositoryException("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");

        return $this->model = $model;
    }
}