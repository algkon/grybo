<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevi�ius
 * Date: 2016-09-03
 * Time: 01:14
 */

namespace Admin;

use Admin\Extensions\Auth\AdminSessionGuard;
use Admin\Extensions\Bags\BreadcrumbBag;
use Admin\Models\User;
use Illuminate\Support\ServiceProvider;

class AdminServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // publish resources
        $this->publishResources();

        // Load views
        $this->loadViewsFrom(dirname(__FILE__) . '/../resources/views', 'admin');

        // Load translation resource
        $this->loadTranslationsFrom(dirname(__FILE__) . '/../resources/lang', 'admin');

        // Load migrations
        $this->loadMigrationsFrom(dirname(__FILE__) . '/../migrations');

        // init 'admin' guard
        $this->loadGuard();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register('Admin\AdminRouteServiceProvider');
        $this->app->register('Admin\Extensions\Auth\Access\AccessServiceProvider');
        $this->app->register('Admin\ComposerServiceProvider');
        $this->app->register('Simplepack\Bag\BagServiceProvider');

        // create breadcrumb bag
        bag()->make(new BreadcrumbBag());
    }

    /**
     * Publish package resources
     */
    public function publishResources()
    {
        if (is_dir(resource_path('views/vendor/admin'))) {
            return;
        }

        if ($this->app->runningInConsole()) {
            $this->publishes([
                dirname(__FILE__) . '/../resources/views' => resource_path('views/vendor/admin'),
            ], 'views');
        }
    }

    /**
     * Admin authentication guard
     */
    private function loadGuard()
    {
        if ($this->app['request']->segment(1) != 'admin') {
            return;
        }

        // add new guard
        if ($this->app['config']->has("auth.guards") && !$this->app['config']->has("auth.guards.admin")) {
            $this->app['config']->set('auth.guards.admin', [
                'driver' => 'admin.session',
                'provider' => 'users_admin',
            ]);
        }

        // add new provider
        if ($this->app['config']->has("auth.providers") && !$this->app['config']->has("auth.providers.users_admin")) {
            $this->app['config']->set('auth.providers.users_admin', [
                'driver' => 'eloquent',
                'model' => User::class,
            ]);
        }

        // Return an instance of Illuminate\Contracts\Auth\Guard...
        $this->app['auth']->extend('admin.session', function ($app, $name, array $config) {
            return new AdminSessionGuard($name,
                $app['auth']->createUserProvider($config['provider']),
                $app['session.store'],
                $app['request']);
        });
    }
}
