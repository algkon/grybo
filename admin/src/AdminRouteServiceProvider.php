<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-03
 * Time: 01:14
 */

namespace Admin;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class AdminRouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'Admin\Controllers';

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app['router']->middleware('admin.auth', \Admin\Middleware\Authenticate::class);
        $this->app['router']->middleware('admin.guest', \Admin\Middleware\RedirectIfAuthenticated::class);
        $this->app['router']->middleware('admin.access', \Admin\Middleware\AccessRoute::class);

        parent::boot();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Admin routes
     */
    public function map()
    {
        Route::group([
            'middleware' => ['web'],
            'prefix' => 'admin',
            'as' => 'admin.',
            'namespace' => $this->namespace,
        ], function ($router) {

            // For not authorized users
            Route::get('/login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
            Route::post('/login', ['as' => 'login.submit', 'uses' => 'Auth\LoginController@login']);
            Route::get('/logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout'])->middleware('admin.auth');

            // For authorized users
            Route::group([
                'middleware' => ['admin.auth', 'admin.access:route'],
            ], function ($router) {
                require dirname(__FILE__) . '/routes.php';
            });
        });
    }
}
