<?php

namespace Admin\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Admin\Controllers\AdminController;
use Krucas\Settings\Settings;

class SettingsController extends AdminController
{
    /**
     * @var Settings
     */
    private $settings;

    /**
     * Settings Controller constructor.
     * @param Settings $settings
     */
    public function __construct(Settings $settings)
    {
        $this->settings = $settings;

        bag('breadcrumb')->push('admin.settings.edit');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editSettings()
    {
        $schema = config('settings.schema');
        $settings = $this->settings->get('custom_settings');

        return view('admin::content.settings.form', compact('schema', 'settings'));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function saveSettings(Request $request)
    {
        $this->settings->set('custom_settings', $request->get('settings', []));

        return redirect()->route('admin.settings.edit')->withSuccess(trans('admin::events.updated'));
    }
}
