<?php

namespace Admin\Controllers;
use Illuminate\Http\Request;

use App\Http\Requests;
use Admin\Controllers\AdminController;

class IndexController extends AdminController
{
    public function index()
    {
//        dd(Auth::guard('admin'), Auth::guard('admin')->check());
        return view('admin::content.index');
    }
}
