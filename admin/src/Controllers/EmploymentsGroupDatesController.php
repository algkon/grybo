<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-10-19
 * Time: 21:50
 */

namespace Admin\Controllers;


use Admin\Models\EmploymentCalendar;
use Admin\Repositories\EmploymentCalendarRepository;
use Admin\Repositories\EmploymentRepository;
use App\Mail\Employments\Canceled;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class EmploymentsGroupDatesController extends AdminController
{
    /**
     * @var EmploymentCalendarRepository
     */
    private $calendar;

    private $employment;

    /**
     * EmploymentsGroupDatesController constructor.
     * @param $calendar
     */
    public function __construct(EmploymentCalendarRepository $calendar, EmploymentRepository $employment)
    {
        $this->calendar = $calendar;
        $this->employment = $employment;

        bag('breadcrumb')->push('admin.employments.group.dates.index');
    }

    /**
     * @param $group
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($group)
    {
        $employment = $this->employment->find($group);

        return view('admin::content.employments.group.dates.index', compact('employment'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create($group)
    {
        return view('admin::content.employments.group.dates.form', [
            'model' => $this->calendar->makeModel(),
            'method' => "POST",
            'url' => route('admin.employments.group.dates.store', [0]),
            'employment' => $employment = $this->employment->find($group),
            'employments' => is_null($employment) ? $this->employment->allWithRelationships('dates', 'coach')->pluck('combined_title_coach', 'id') : []
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $calendar = $this->calendar->makeModel();
        $calendar->fill($request->all());
        $calendar->save();

        if ($request->expectsJson()) {
            return response()->json([
                'message' => trans('admin::events.created'),
                'row' => $this->getRow($calendar, $request->header('referer'))
            ]);
        }

        return redirect()
            ->route('admin.employments.group.dates.index', [$request->get('employmemnt_id')])
            ->withSuccess(trans('admin::events.created'));
    }

    /**
     * @param $group
     * @param $date
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($group, $date)
    {
        $model = $this->calendar->find($date);

        return view('admin::content.employments.group.dates.form', [
            'model' => $model,
            'method' => "PUT",
            'url' => route('admin.employments.group.dates.update', [$model->employment_id, $model->id]),
            'employment' => $model->employment
        ]);
    }

    /**
     * @param Request $request
     * @param $group
     * @param $date
     * @return string
     */
    public function update(Request $request, $group, $date)
    {
        $calendar = ($date instanceof EmploymentCalendar) ? $date : $this->calendar->find($date);
        $calendar->fill($request->all());
        $calendar->save();

        if ($request->expectsJson()) {
            return response()->json([
                'message' => trans('admin::events.updated'),
                'row' => $this->getRow($calendar, $request->header('referer'))
            ]);
        }

        return redirect()->route('admin.employments.group.dates.index', [$group])->withSuccess(trans('admin::events.updated'));
    }

    /**
     * @param Request $request
     * @param $group
     * @param $date
     * @return string
     */
    public function cancel(Request $request, $group, $date)
    {
        $request->merge(['active' => '0']);

        $date = $this->calendar->find($date);
        $response = $this->update($request, $group, $date);

        if (!is_null($message = $request->get('message'))) {
            foreach ($date->users as $user) {
                Mail::to($user->email)->queue(new Canceled($user, $message));
            }
        }

        return $response;
    }

    /**
     * @param Request $request
     * @param $group
     * @param $date
     * @return string
     */
    public function destroy(Request $request, $group, $date)
    {
        $this->calendar->delete($date);

        if ($request->ajax()) {
            return 'OK';
        }

        return redirect()->route('admin.employments.group.dates.index', [$group])->withSuccess(trans('admin::events.deleted'));
    }

    /**
     * @param $date
     * @param $previous_url
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function getRow($date, $previous_url)
    {
        if (str_contains($previous_url, 'calendar')) {
            return view('admin::content.employments.group.dates.row', ['calendar' => true, 'date' => $date])->render();
        }

        return view('admin::content.employments.group.dates.row', ['date' => $date])->render();
    }
}