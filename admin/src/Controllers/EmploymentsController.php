<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-23
 * Time: 13:50
 */

namespace Admin\Controllers;


use Admin\Repositories\EmploymentCalendarRepository;
use Admin\Repositories\EmploymentRepository;
use Admin\Repositories\RoleRepository;
use Admin\Repositories\UserRepository;
use Admin\Requests\SaveEmploymentRequest;
use Illuminate\Http\Request;

class EmploymentsController extends AdminController
{

}