<?php

namespace Admin\Controllers;

use Admin\Repositories\PermissionRepository;
use Admin\Repositories\RoleRepository;
use Illuminate\Http\Request;

use App\Http\Requests;
use Admin\Controllers\AdminController;

class RolesController extends AdminController
{
    /**
     * @var RoleRepository
     */
    private $role;

    /**
     * @var PermissionRepository
     */
    private $permission;

    /**
     * RolesController constructor.
     *
     * @param RoleRepository $role
     * @param PermissionRepository $permission
     */
    public function __construct(RoleRepository $role, PermissionRepository $permission)
    {
        $this->role       = $role;
        $this->permission = $permission;

        // create breadcrumb bag and set first breadcrumb trail
        bag('breadcrumb')->push('admin.roles.index');
    }

//    /**
//     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
//     */
//    public function index()
//    {
//        $roles = $this->role->withPermissions()->get();
//        $permissions = $this->permission->all();
//
//        return view('admin::content.roles.index', compact('roles', 'permissions'));
//    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        bag('breadcrumb')->push('admin.roles.create');

        return view('admin::content.roles.form', [
            'model' => $this->role->makeModel(),
            'method' => "POST"
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        bag('breadcrumb')->push('admin.roles.edit');

        return view('admin::content.roles.form', [
            'model' => $this->role->find($id),
            'method' => "PUT"
        ]);
    }

    public function destroy($id)
    {
        $this->role->makeModel()->findOrFail(4545);
        var_dump($id);
    }
}
