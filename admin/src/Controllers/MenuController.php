<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevi�ius
 * Date: 2016-09-10
 * Time: 22:54
 */

namespace Admin\Controllers;


use Admin\Repositories\MenuRepository;
use Admin\Requests\CreateMenuRequest;
use Admin\Requests\UpdateMenuRequest;
use Illuminate\Http\Request;

class MenuController extends AdminController
{
    /**
     * @var MenuRepository
     */
    private $menu;

    /**
     * @var array
     */
    private $_anchor_relationships = ['nofollow' => 'nofollow'];

    /**
     * @var array
     */
    private $_anchor_target = ['_blank' => 'Naujas langas']; //, '_self', '_parent', '_top'

    /**
     * MenuController constructor.
     * @param MenuRepository $menu
     */
    public function __construct(MenuRepository $menu)
    {
        $this->menu = $menu;

        bag('breadcrumb')->push('admin.menu.index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $menus = $this->menu->all();

        return view('admin::content.menu.index', compact('menus'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        bag('breadcrumb')->push('admin.menu.create');

        return view('admin::content.menu.form', [
            'model' => $this->menu->makeModel(),
            'repository' => $this->menu,
            'anchor_relationships' => $this->_anchor_relationships,
            'anchor_target' => $this->_anchor_target,
            'method' => "POST"
        ]);
    }

    /**
     * @param CreateMenuRequest $request
     * @return mixed
     */
    public function store(CreateMenuRequest $request)
    {
        $menu = $this->menu->create($request->all());

        $this->menu->saveLinks(json_decode($request->get('links')), $menu->id);

        return redirect()->route('admin.menu.index')->withSuccess(trans('admin::events.created'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        bag('breadcrumb')->push('admin.menu.edit');

        return view('admin::content.menu.form', [
            'model' => $this->menu->find($id),
            'repository' => $this->menu,
            'anchor_relationships' => $this->_anchor_relationships,
            'anchor_target' => $this->_anchor_target,
            'method' => "PUT"
        ]);
    }

    public function update(UpdateMenuRequest $request, $id)
    {
        $menu = $this->menu->find($id);
        $menu->fill($request->all());
        $menu->save();

        $this->menu->saveLinks(json_decode($request->get('links')), $id);

        return redirect()->route('admin.menu.index')->withSuccess(trans('admin::events.updated'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return string
     */
    public function destroy(Request $request, $id)
    {
        $this->menu->delete($id);

        if ($request->ajax()) {
            return 'OK';
        }

        return redirect()->route('admin.menu.index')->withSuccess(trans('admin::events.deleted'));
    }
}