<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-26
 * Time: 12:03
 */

namespace Admin\Controllers;


use Admin\Repositories\EmploymentUserRepository;
use App\Mail\Employments\UserApproved;
use App\Mail\Employments\UserCanceled;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class EmploymentsUsersController extends AdminController
{

    /**
     * @var EmploymentUserRepository
     */
    private $employment_user;

    /**
     * EmploymentsUsersController constructor.
     * @param EmploymentUserRepository $calendar
     */
    public function __construct(EmploymentUserRepository $employment_user)
    {
        $this->employment_user = $employment_user;

        bag('breadcrumb')->push('admin.employments.users.index');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $users = $this->employment_user->filter($request)->all();

        return view('admin::content.employments.users.index', compact('users'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
//        bag('breadcrumb')->push('admin.employments.users.edit');

        return view('admin::content.employments.users.form', [
            'model' => $this->employment_user->find($id),
            'repository' => $this->employment_user,
            'method' => "PUT"
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function update(Request $request, $id)
    {
        $employment_user = $this->employment_user->find($id);
        $employment_user->fill($request->only('status'));
        $employment_user->save();

        if ($employment_user->status == 0) {
            Mail::to($employment_user->email)->queue(new UserCanceled($employment_user));
        } elseif($employment_user->status == 1) {
            Mail::to($employment_user->email)->queue(new UserApproved($employment_user));
        }

        if ($request->expectsJson()) {
            return response()->json(['message' => trans('admin::events.updated')]);
        }

        return redirect()->route('admin.employments.users.index')->withSuccess(trans('admin::events.updated'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return string
     */
    public function destroy(Request $request, $id)
    {
        $this->employment_user->delete($id);

        if ($request->ajax()) {
            return 'OK';
        }

        return redirect()->route('admin.employments.users.index')->withSuccess(trans('admin::events.deleted'));
    }

}