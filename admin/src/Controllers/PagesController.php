<?php

namespace Admin\Controllers;

use Admin\Models\Metadata;
use Admin\Repositories\PageRepository;
use Admin\Requests\CreatePageRequest;
use Admin\Requests\UpdatePageRequest;
use Illuminate\Http\Request;

use App\Http\Requests;
use Admin\Controllers\AdminController;

class PagesController extends AdminController
{
    /**
     * @var PageRepository
     */
    private $page;

    /**
     * PagesController constructor.
     * @param $page
     */
    public function __construct(PageRepository $page)
    {
        $this->page = $page;

        bag('breadcrumb')->push('admin.pages.index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $pages = $this->page->all();

        return view('admin::content.pages.index', compact('pages'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        bag('breadcrumb')->push('admin.pages.create');

        return view('admin::content.pages.form', [
            'model' => $this->page->makeModel(),
            'method' => "POST"
        ]);
    }

    /**
     * @param CreatePageRequest $request
     * @return mixed
     */
    public function store(CreatePageRequest $request)
    {
        $page = $this->page->makeModel();
        $page->fill($request->all());
        $page->save();

        // set attachment
        $page->setAttachment($request->get('attachment', []));

        // update metadata
        $page->saveMetadata($request->get('metadata', []));

        // set galleries
        $page->galleries()->sync($request->input('galleries.*.id', []));

        // set layout
        $page->setLayout($request->get('layout'));

        return redirect()->route('admin.pages.index')->withSuccess(trans('admin::events.created'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        bag('breadcrumb')->push('admin.pages.edit');

        return view('admin::content.pages.form', [
            'model' => $this->page->find($id),
            'method' => "PUT"
        ]);
    }

    /**
     * @param UpdatePageRequest $request
     * @param $id
     * @return mixed
     */
    public function update(UpdatePageRequest $request, $id)
    {
        $page = $this->page->find($id);
        $page->fill($request->all());
        $page->save();

        // set attachment
        $page->setAttachment($request->get('attachment', []));

        // update metadata
        $page->saveMetadata($request->get('metadata', []));

        // set galleries
        $page->galleries()->sync($request->input('galleries.*.id', []));

        // set layout
        $page->setLayout($request->get('layout'));

        return redirect()->route('admin.pages.index')->withSuccess(trans('admin::events.updated'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return string
     */
    public function destroy(Request $request, $id)
    {
        $this->page->deleteWithRelationships($id);

        if ($request->ajax()) {
            return 'OK';
        }

        return redirect()->route('admin.posts.index')->withSuccess(trans('admin::events.deleted'));
    }
}
