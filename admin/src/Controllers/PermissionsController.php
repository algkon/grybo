<?php

namespace Admin\Controllers;

use Admin\Repositories\PermissionRepository;
use Admin\Repositories\RoleRepository;
use Admin\Requests\CreatePermissionRequest;
use Admin\Requests\UpdatePermissionRequest;
use Illuminate\Http\Request;

use App\Http\Requests;
use Admin\Controllers\AdminController;
use Illuminate\Routing\Route;

class PermissionsController extends AdminController
{
    /**
     * @var PermissionRepository
     */
    private $permission;

    /**
     * @var RoleRepository
     */
    private $role;

    /**
     * PermissionsController constructor.
     *
     * @param RoleRepository $role
     * @param PermissionRepository $permission
     */
    public function __construct(PermissionRepository $permission, RoleRepository $role)
    {
        $this->permission = $permission;
        $this->role       = $role;

        bag('breadcrumb')->push('admin.permissions.index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $permissions = $this->permission->all();
        $roles = $this->role->withPermissions()->get();

        return view('admin::content.permissions.index', compact('permissions', 'roles'));
    }

    /**
     * @param CreatePermissionRequest $request
     * @return mixed
     */
    public function store(CreatePermissionRequest $request)
    {
//        $category = $this->permission->makeModel();
//        $category->fill($request->all());
//        $category->save();

        return redirect()->route('admin.permissions.index')->withSuccess(trans('admin::events.created'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        bag('breadcrumb')->push('admin.categories.edit');

        return view('admin::content.permissions.form', [
            'model' => $this->permission->find($id),
            'repository' => $this->permission,
            'method' => "PUT"
        ]);
    }

    /**
     * @param UpdatePermissionRequest $request
     * @param $id
     * @return mixed
     */
    public function update(UpdatePermissionRequest $request, $id)
    {
        $category = $this->permission->find($id);
        $category->fill($request->all());
        $category->save();

        return redirect()->route('admin.permissions.index')->withSuccess(trans('admin::events.updated'));
    }

    /**
     * @param Request $request
     * @param Route $route
     * @return mixed
     */
    public function refresh(Request $request, Route $route)
    {
        $routes = app('router')->getRoutes()->getRoutes();

        $this->permission->createIfNotExist($routes);

        return redirect()->back()->withSuccess(trans('admin::events.list.updated'));
    }
}
