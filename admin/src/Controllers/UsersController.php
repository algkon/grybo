<?php

namespace Admin\Controllers;

use Admin\Repositories\RoleRepository;
use Admin\Repositories\UserRepository;
use Admin\Requests\CreateUserRequest;
use Admin\Requests\UpdateUserRequest;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use SimplePack\Nsoft\v2\User;

//use App\Http\Requests;

class UsersController extends AdminController
{
    /**
     * @var UserRepository
     */
    private $user;

    /**
     * @var RoleRepository
     */
    private $role;

    /**
     * UsersController constructor.
     * @param UserRepository $user
     */
    public function __construct(UserRepository $user, RoleRepository $role)
    {
        $this->user = $user;
        $this->role = $role;

        // create breadcrumb bag and set first breadcrumb trail
        bag('breadcrumb')->push('admin.users.index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $users = $this->user->allWithRoles();

        return view('admin::content.users.index', compact('users'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        bag('breadcrumb')->push('admin.users.create');

        return view('admin::content.users.form', [
            'model' => $this->user->makeModel(),
            'role' => $this->role,
            'method' => "POST"
        ]);
    }

    /**
     * @param CreateUserRequest $request
     * @return mixed
     */
    public function store(CreateUserRequest $request)
    {
        $user = $this->user->makeModel();

        if ($request->has('api_user')) {
            return $this->storeApi($user, $request);
        } else {
            $user->fill($request->all());
            $user->save();
        }

        $user->roles()->sync($request->get('roles_ids', []));

        return redirect()->route('admin.users.index')->withSuccess(trans('admin::events.created'));
    }

    /**
     * @param $user
     * @param $request
     * @return $this
     */
    public function storeApi($user, $request)
    {
        /// generate tmp password
        $request->merge(['password' => str_random(10)]);

        // Nsoft registration
        $response = (new User())->register([
            'vardas' => $request->input('api.first_name'),
            'pavarde' => $request->input('api.last_name'),
            'kodas' => $request->input('api.national_id'),
            'el_pastas' => $request->input('api.email'),
            'tel_num' => $request->input('api.phone'),
            'salis' => $request->input('api.country'),
            'miestas' => $request->input('api.addr_town'),
            'gatve' => $request->input('api.addr_street'),
            'namas' => $request->input('api.addr_house'),
            'butas' => $request->input('api.addr_flat'),
            'pasto_kodas' => $request->input('api.addr_post_code'),
            'password' => $request->get('password'),
        ]);

        // Nsoft fails check
        if ($response->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($response->errors());
        }

        $user_api_data = $response->getData()->first();

        $user->fill([
            'name' => $request->input('api.first_name') . ' ' . $request->input('api.last_name'),
            'email' => $request->input('api.email'),
            'password' => bcrypt($request->input('password')),
            'active' => $request->get('active'),
        ]);
        $user->save();
        $user->roles()->sync($request->get('roles_ids', []));

        // Create UserApi
        $user->api()->create($request->get('api'));

        // update activation_code
        $user->api->activation_code = $user_api_data['registracijos_kodas'];
        $user->api->save();

        event(new Registered($user));

        return redirect()->route('admin.users.index')->withSuccess(trans('admin::events.created'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        bag('breadcrumb')->push('admin.users.edit');

        return view('admin::content.users.form', [
            'model' => $this->user->findWithRoles($id),
            'role' => $this->role,
            'method' => "PUT"
        ]);
    }

    /**
     * @param UpdateUserRequest $request
     * @param $id
     * @return mixed
     */
    public function update(UpdateUserRequest $request, $id)
    {
        $user = $this->user->find($id);

        if ($request->has('api_user')) {
            return $this->updateApi($user, $request);
        } else {
            $user->fill($request->all());
            $user->save();
        }

        $user->roles()->sync($request->get('roles_ids', []));

        return redirect()->route('admin.users.index')->withSuccess(trans('admin::events.updated'));
    }

    /**
     * @param $user
     * @param $request
     * @return $this
     */
    public function updateApi($user, $request)
    {
        $login_response = (new \SimplePack\Nsoft\v2\User())->login(
            config('npoint.admin.username'),
            config('npoint.admin.password')
        );

        // if not nsoft user
        if ($login_response->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($login_response->errors());
        }

        $npoint_admin_data = $login_response->getData()->first();

        // start updating
        $user_data = $request->except('api.addr_country_code', 'api.email', 'api.national_id');

        $response = (new \SimplePack\Nsoft\v1\User)->setDetails(
            $npoint_admin_data['session_token'], // ws_client_user, g7wsclient
            $user->api->user_id_api,
            $user->api->email,
            $user_data['api']
        );

        if ($response->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($response->errors());
        }

        $user->name = $user_data['api']['first_name'] . ' ' . $user_data['api']['last_name'];
        $user->api->fill($user_data['api']);
        $user->push();

        // redirect back to users list and show success message
        return redirect()->route('admin.users.index')->withSuccess(trans('admin::events.updated'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return string
     */
    public function destroy(Request $request, $id)
    {
        $this->user->delete($id);

        if ($request->ajax()) {
            return 'OK';
        }

        return redirect()->route('admin.posts.index')->withSuccess(trans('admin::events.deleted'));
    }
}
