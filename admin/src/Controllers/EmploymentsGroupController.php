<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-10-19
 * Time: 14:13
 */

namespace Admin\Controllers;

use Admin\Repositories\EmploymentCalendarRepository;
use Admin\Repositories\EmploymentRepository;
use Admin\Repositories\RoleRepository;
use Admin\Requests\SaveEmploymentRequest;
use Illuminate\Http\Request;

class EmploymentsGroupController extends AdminController
{
    /**
     * @var EmploymentRepository
     */
    private $employment;

    /**
     * @var RoleRepository
     */
    private $role;

    /**
     * EmploymentsController constructor.
     * @param EmploymentRepository $employment
     * @param RoleRepository $role
     */
    public function __construct(EmploymentRepository $employment, RoleRepository $role)
    {
        $this->employment = $employment;
        $this->role = $role;

        bag('breadcrumb')->push('admin.employments.group.index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $employments = $this->employment->allWithRelationships('dates', 'coach');

        return view('admin::content.employments.group.index', compact('employments'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        bag('breadcrumb')->push('admin.employments.group.create');

        return view('admin::content.employments.group.form', [
            'model' => $this->employment->makeModel(),
            'repository' => $this->employment,
            'coaches' => $this->role->getUsersDropdownList('coach'),
            'method' => "POST"
        ]);
    }

    /**
     * @param SaveEmploymentRequest $request
     * @return mixed
     */
    public function store(SaveEmploymentRequest $request)
    {
        $employment = $this->employment->makeModel();
        $employment->fill($request->all());
        $employment->save();

//        $employment->dates()->sync($request->get('dates', []));

        return redirect()->route('admin.employments.group.index')->withSuccess(trans('admin::events.created'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        bag('breadcrumb')->push('admin.employments.group.edit');

        return view('admin::content.employments.group.form', [
            'model' => $this->employment->find($id),
            'repository' => $this->employment,
            'coaches' => $this->role->getUsersDropdownList('coach'),
            'method' => "PUT"
        ]);
    }

    /**
     * @param SaveEmploymentRequest $request
     * @param $id
     * @return mixed
     */
    public function update(SaveEmploymentRequest $request, $id)
    {
        $employment = $this->employment->find($id);
        $employment->fill($request->all());
        $employment->save();

//        $employment->dates()->sync($request->get('dates', []));

        return redirect()->route('admin.employments.group.index')->withSuccess(trans('admin::events.updated'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return string
     */
    public function destroy(Request $request, $id)
    {
        $this->employment->delete($id);

        if ($request->ajax()) {
            return 'OK';
        }

        return redirect()->route('admin.employments.group.index')->withSuccess(trans('admin::events.deleted'));
    }

    /**
     * @param EmploymentCalendarRepository $calendar
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function calendar(EmploymentCalendarRepository $calendar)
    {
        bag('breadcrumb')->push('admin.employments.group.calendar');

        return view('admin::content.employments.group.calendar', [
            'dates' => $calendar->allWithRelationships(['employment.coach', 'users']),
        ]);
    }
}