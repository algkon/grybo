<?php

namespace Admin\Controllers;

use Admin\Repositories\CategoryRepositoy;
use Admin\Repositories\PostRepository;
use Admin\Requests\SavePostRequest;
use Illuminate\Http\Request;

use App\Http\Requests;
use Admin\Controllers\AdminController;

class PostsController extends AdminController
{
    /**
     * @var PostRepository
     */
    private $post;

    /**
     * @var CategoryRepositoy
     */
    private $category;

    /**
     * PostsController constructor.
     * @param $post
     */
    public function __construct(PostRepository $post, CategoryRepositoy $category)
    {
        $this->post = $post;
        $this->category = $category;

        bag('breadcrumb')->push('admin.posts.index');
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $posts = $this->post->filter($request)->allWithRelationships('author');

        return view('admin::content.posts.index', compact('posts'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        bag('breadcrumb')->push('admin.post.create');

        return view('admin::content.posts.form', [
            'model' => $this->post->makeModel(),
            'categories' => $this->category->getTree(),
            'method' => "POST"
        ]);
    }

    /**
     * @param SavePostRequest $request
     * @return mixed
     */
    public function store(SavePostRequest $request)
    {
        $post = $this->post->makeModel();
        $post->fill($request->all());
        $post->save();

        // set attachment
        $post->setAttachment($request->get('attachment', []));

        // update metadata
        $post->saveMetadata($request->get('metadata', []));

        // set categories
        $post->categories()->sync($request->get('categories', []));

        // set galleries
        $post->galleries()->sync($request->input('galleries.*.id', []));

        return redirect()->route('admin.posts.index')->withSuccess(trans('admin::events.created'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        bag('breadcrumb')->push('admin.posts.edit');

        return view('admin::content.posts.form', [
            'model' => $this->post->findPostWithCategories($id),
            'categories' => $this->category->getTree(),
            'method' => "PUT"
        ]);
    }

    /**
     * @param SavePostRequest $request
     * @param $id
     * @return mixed
     */
    public function update(SavePostRequest $request, $id)
    {
        // update post
        $post = $this->post->find($id);
        $post->fill($request->all());
        $post->save();

        // set attachment
        $post->setAttachment($request->get('attachment', []));

        // update metadata
        $post->saveMetadata($request->get('metadata', []));

        // set categories
        $post->categories()->sync($request->get('categories', []));

        // set galleries
        $post->galleries()->sync($request->input('galleries.*.id', []));

        // redirect after submit
        return redirect()->route('admin.posts.index')->withSuccess(trans('admin::events.updated'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return string
     */
    public function destroy(Request $request, $id)
    {
        $this->post->deleteWithRelationships($id);

        if ($request->ajax()) {
            return 'OK';
        }

        return redirect()->route('admin.posts.index')->withSuccess(trans('admin::events.deleted'));
    }
}
