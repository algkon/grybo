<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevi�ius
 * Date: 2016-09-20
 * Time: 00:15
 */

namespace Admin\Controllers;


use Admin\Repositories\SubscriberRepository;
use Admin\Requests\SubscriberMessageSendRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class SubscribersController extends AdminController
{
    /**
     * @var SubscriberRepository
     */
    private $subscriber;

    /**
     * RolesController constructor.
     *
     * @param SubscriberRepository $subscriber
     */
    public function __construct(SubscriberRepository $subscriber)
    {
        $this->subscriber = $subscriber;

        // create breadcrumb bag and set first breadcrumb trail
        bag('breadcrumb')->push('admin.subscribers.index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $subscribers = $this->subscriber->all();

        return view('admin::content.subscribers.index', compact('subscribers'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function mailForm()
    {
        bag('breadcrumb')->push('admin.subscribers.mail');

        return view('admin::content.subscribers.form', [
            'model' => $this->subscriber->makeModel(),
            'method' => "POST"
        ]);
    }

    /**
     * @param SubscriberMessageSendRequest $request
     * @return mixed
     */
    public function mailSend(SubscriberMessageSendRequest $request)
    {
        $subscribers = $this->subscriber->all(['email']);
        $input = $request->only('subject', 'content');

        foreach ($subscribers as $subscriber) {
            Mail::queue('mail.subscribers', ['email' => $input], function ($message) use ($subscriber, $input) {
                $message->to($subscriber->email)->subject($input['subject']);
            });
        }

        return redirect()->route('admin.subscribers.mail')->withSuccess(trans('admin::events.send.successfully'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return string
     */
    public function destroy(Request $request, $id)
    {
        $this->subscriber->delete($id);

        if ($request->ajax()) {
            return 'OK';
        }

        return redirect()->route('admin.subscribers.index')->withSuccess(trans('admin::events.deleted'));
    }
}