<?php

namespace Admin\Controllers;

use Admin\Repositories\CategoryRepositoy;
use Admin\Requests\CreateCategoryRequest;
use Admin\Requests\UpdateCategoryRequest;
use Illuminate\Http\Request;

use App\Http\Requests;
use Admin\Controllers\AdminController;

class CategoriesController extends AdminController
{
    /**
     * @var CategoryRepositoy
     */
    private $category;

    /**
     * CategoriesController constructor.
     * @param $category
     */
    public function __construct(CategoryRepositoy $category)
    {
        $this->category = $category;

        bag('breadcrumb')->push('admin.categories.index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $categories = $this->category->withParent()->get();

        return view('admin::content.categories.index', compact('categories'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        bag('breadcrumb')->push('admin.categories.create');

        return view('admin::content.categories.form', [
            'model' => $this->category->makeModel(),
            'repository' => $this->category,
            'method' => "POST"
        ]);
    }

    /**
     * @param CreateCategoryRequest $request
     * @return mixed
     */
    public function store(CreateCategoryRequest $request)
    {
        $category = $this->category->makeModel();
        $category->fill($request->all());
        $category->save();

        // set attachment
        $category->setAttachment($request->get('attachment', []));

        // update metadata
        $category->saveMetadata($request->get('metadata', []));

        return redirect()->route('admin.categories.index')->withSuccess(trans('admin::events.created'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        bag('breadcrumb')->push('admin.categories.edit');

        return view('admin::content.categories.form', [
            'model' => $this->category->find($id),
            'repository' => $this->category,
            'method' => "PUT"
        ]);
    }

    /**
     * @param UpdateCategoryRequest $request
     * @param $id
     * @return mixed
     */
    public function update(UpdateCategoryRequest $request, $id)
    {
        $category = $this->category->find($id);
        $category->fill($request->all());
        $category->save();

        // set attachment
        $category->setAttachment($request->get('attachment', []));

        // update metadata
        $category->saveMetadata($request->get('metadata', []));

        return redirect()->route('admin.categories.index')->withSuccess(trans('admin::events.updated'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return string
     */
    public function destroy(Request $request, $id)
    {
        $this->category->deleteWithRelationships($id);

        if ($request->ajax()) {
            return 'OK';
        }

        return redirect()->route('admin.posts.index')->withSuccess(trans('admin::events.deleted'));
    }
}
