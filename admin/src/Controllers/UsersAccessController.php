<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-13
 * Time: 11:39
 */

namespace Admin\Controllers;


use Admin\Repositories\PermissionRepository;
use Admin\Repositories\RoleRepository;
use Illuminate\Http\Request;

class UsersAccessController extends AdminController
{
    /**
     * @var RoleRepository
     */
    private $role;

    /**
     * @var PermissionRepository
     */
    private $permission;

    /**
     * UserAccessController constructor.
     * @param RoleRepository $roles
     * @param PermissionRepository $permissions
     */
    public function __construct(RoleRepository $role, PermissionRepository $permission)
    {
        $this->role = $role;
        $this->permission = $permission;

        bag('breadcrumb')->push('admin.users.access.index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $roles = $this->role->withPermissions()->get();
        $permissions = $this->permission->all();

        return view('admin::content.users.access.index', compact('roles', 'permissions'));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function save(Request $request)
    {
        foreach ($request->get('roles', []) as $role => $permissions) {
            $this->role->givePermissions($role, $permissions);
        }

        return redirect()->route('admin.users.access.index')->withSuccess(trans('admin::events.saved'));
    }
}