<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-10-21
 * Time: 15:26
 */

namespace Admin\Controllers;


use Admin\Repositories\TimetableCalendarRepository;
use Admin\Repositories\TimetableRepository;
use Admin\Requests\CreateTimetableRequest;
use Admin\Requests\UpdateTimetableRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class TimetablesController extends AdminController
{
    /**
     * @var TimetableRepository
     */
    private $timetable;

    /**
     * TimetablesController constructor.
     * @param TimetableRepository $timetable
     */
    public function __construct(TimetableRepository $timetable)
    {
        $this->timetable = $timetable;

        bag('breadcrumb')->push('admin.timetables.index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $timetables = $this->timetable->all();

        return view('admin::content.timetables.index', compact('timetables'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        bag('breadcrumb')->push('admin.timetables.create');

        return view('admin::content.timetables.form', [
            'model' => $this->timetable->makeModel(),
            'method' => "POST"
        ]);
    }

    /**
     * @param CreateTimetableRequest $request
     * @return mixed
     */
    public function store(CreateTimetableRequest $request)
    {
        $timetable = $this->timetable->makeModel();
        $timetable->fill($request->all());
        $timetable->save();

        // set attachment
        $timetable->setAttachment($request->get('attachment', []));

        // update metadata
        $timetable->saveMetadata($request->get('metadata', []));
//
//        // set galleries
//        $timetable->galleries()->sync($request->input('galleries.*.id', []));
//
//        // set layout
//        $timetable->setLayout($request->get('layout'));

        return redirect()->route('admin.timetables.index')->withSuccess(trans('admin::events.created'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        bag('breadcrumb')->push('admin.timetables.edit');

        return view('admin::content.timetables.form', [
            'model' => $this->timetable->find($id),
            'method' => "PUT"
        ]);
    }

    /**
     * @param UpdateTimetableRequest $request
     * @param $id
     * @return mixed
     */
    public function update(UpdateTimetableRequest $request, $id)
    {
        $timetable = $this->timetable->find($id);
        $timetable->fill($request->all());
        $timetable->save();

        // set attachment
        $timetable->setAttachment($request->get('attachment', []));

        // update metadata
        $timetable->saveMetadata($request->get('metadata', []));
//
//        // set galleries
//        $timetable->galleries()->sync($request->input('galleries.*.id', []));
//
//        // set layout
//        $timetable->setLayout($request->get('layout'));

        return redirect()->route('admin.timetables.index')->withSuccess(trans('admin::events.updated'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return string
     */
    public function destroy(Request $request, $id)
    {
//        $this->timetable->deleteWithRelationships($id);

        if ($request->ajax()) {
            return 'OK';
        }

        return redirect()->route('admin.posts.index')->withSuccess(trans('admin::events.deleted'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function calendarForm(TimetableCalendarRepository $calendar, $timetable_id, $start = null, $end = null)
    {
        bag('breadcrumb')->push('admin.timetables.calendar.form');

//        dd($calendar->handsonTableData($timetable_id, $start, $end)->getData()->toArray());

        return view('admin::content.timetables.calendar.form', [
            'model' => $this->timetable->find($timetable_id),
            'table' => $calendar->handsonTableData($timetable_id, $start, $end),
            'method' => "PUT"
        ]);
    }

    /**
     * @param Request $request
     * @param TimetableCalendarRepository $calendar
     * @param $timetable_id
     * @param null $start
     * @param null $end
     * @return mixed
     */
    public function calendarSubmit(Request $request, TimetableCalendarRepository $calendar, $timetable_id, $start = null, $end = null)
    {
        if ($request->has('store_as_default_all')) {
            $calendar->setHandsonTableDefaultData($request->get('default', []), $timetable_id);
        } else {
            $calendar->setHandsonTableDefaultData($request->get('default', []), $timetable_id, $start, $end);
        }

//        $calendar->calendarUpdate($timetable_id, $request->get('dates', []),$start, $end);

        return redirect()->back()->withSuccess(trans('admin::events.updated'));
    }
}