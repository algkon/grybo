<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-26
 * Time: 12:06
 */

namespace Admin\Controllers;


use Admin\Repositories\EmploymentCalendarRepository;
use Admin\Repositories\OrderRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OrdersController extends AdminController
{
    /**
     * @var OrderRepository
     */
    private $order;

    /**
     * EmploymentsUsersController constructor.
     * @param OrderRepository $order
     */
    public function __construct(OrderRepository $order)
    {
        $this->order = $order;

        bag('breadcrumb')->push('admin.orders.index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $orders = $this->order->filter($request)->all();

        settings()->set('admin.orders.index.last_activity', Carbon::now());

        return view('admin::content.orders.index', compact('orders'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        bag('breadcrumb')->push('admin.orders.edit');

        return view('admin::content.orders.show', [
            'model' => $this->order->find($id),
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return string
     */
    public function destroy(Request $request, $id)
    {
        $this->order->delete($id);

        if ($request->ajax()) {
            return 'OK';
        }

        return redirect()->route('admin.orders.index')->withSuccess(trans('admin::events.deleted'));
    }
}