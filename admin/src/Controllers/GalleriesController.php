<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevi�ius
 * Date: 2016-09-10
 * Time: 22:40
 */

namespace Admin\Controllers;

use Admin\Repositories\GalleryRepository;
use Admin\Requests\InsertGalleryRequest;
use Admin\Requests\UpdateGalleryRequest;
use Illuminate\Http\Request;

class GalleriesController extends AdminController
{
    /**
     * @var GalleryRepository
     */
    private $gallery;

    /**
     * GalleriesController constructor.
     * @param $gallery
     */
    public function __construct(GalleryRepository $gallery)
    {
        $this->gallery = $gallery;

        bag('breadcrumb')->push('admin.galleries.index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $galleries = $this->gallery->allWithImages();

        if ($request->ajax()) {
            return view('admin::content.galleries.list', compact('galleries'));
        }

        return view('admin::content.galleries.index', compact('galleries'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        bag('breadcrumb')->push('admin.galleries.create');

        return view('admin::content.galleries.form', [
            'model' => $this->gallery->makeModel(),
            'method' => "POST"
        ]);
    }

    /**
     * @param InsertGalleryRequest $request
     * @return mixed
     */
    public function store(InsertGalleryRequest $request)
    {
        $gallery = $this->gallery->makeModel();
        $gallery->fill($request->all());
        $gallery->save();

        $gallery->syncImages($request->get('images', []));

        return redirect()->route('admin.galleries.index')->withSuccess(trans('admin::events.created'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        bag('breadcrumb')->push('admin.galleries.edit');

        return view('admin::content.galleries.form', [
            'model' => $this->gallery->find($id),
            'method' => "PUT"
        ]);
    }

    /**
     * @param UpdateGalleryRequest $request
     * @param $id
     * @return mixed
     */
    public function update(UpdateGalleryRequest $request, $id)
    {
        $gallery = $this->gallery->find($id);
        $gallery->fill($request->all());
        $gallery->save();

        $gallery->syncImages($request->get('images', []));

        return redirect()->route('admin.galleries.index')->withSuccess(trans('admin::events.updated'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return string
     */
    public function destroy(Request $request, $id)
    {
        $this->gallery->deleteWithRelationships($id);

        if ($request->ajax()) {
            return 'OK';
        }

        return redirect()->route('admin.posts.index')->withSuccess(trans('admin::events.deleted'));
    }
}