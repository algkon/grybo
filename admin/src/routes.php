<?php
/**
 * Created by PhpStorm.
 * User: polki
 * Date: 2016-09-01
 * Time: 21:56
 */

// Admin home page(currently as page index)
Route::get('/', ['as' => 'index', 'uses' => 'PagesController@index']);

// User permissions table
Route::get('users/access', ['as' => 'users.access.index', 'uses' => 'UsersAccessController@index']);
Route::put('users/access', ['as' => 'users.access.save', 'uses' => 'UsersAccessController@save']);

// Permissions update list
Route::get('permissions/refresh', ['as' => 'permissions.refresh', 'uses' => 'PermissionsController@refresh']);

// Subscribers (currently off)
Route::get('subscribers', ['as' => 'subscribers.index', 'uses' => 'SubscribersController@index']);
Route::delete('subscribers/{id}', ['as' => 'subscribers.destroy', 'uses' => 'SubscribersController@destroy']);
Route::get('subscribers/mail', ['as' => 'subscribers.mail', 'uses' => 'SubscribersController@mailForm']);
Route::post('subscribers/mail', ['as' => 'subscribers.mail.send', 'uses' => 'SubscribersController@mailSend']);

// All resources
Route::resource('users', 'UsersController', ['except' => ['show']]);
Route::resource('roles', 'RolesController', ['except' => ['show', 'index']]);
Route::resource('permissions', 'PermissionsController', ['except' => ['show', 'create']]);
Route::resource('pages', 'PagesController', ['except' => ['show']]);
Route::resource('posts', 'PostsController', ['except' => ['show']]);
Route::resource('categories', 'CategoriesController', ['except' => ['show']]);
Route::resource('galleries', 'GalleriesController', ['except' => ['show']]);
Route::resource('menu', 'MenuController', ['except' => ['show']]);
Route::resource('orders', 'OrdersController', ['only' => ['index', 'show', 'destroy']]);
Route::resource('employments', 'EmploymentsController', ['except' => ['show']]);
Route::get('employments/group/calendar', 'EmploymentsGroupController@calendar')->name('employments.group.calendar');
Route::resource('employments/group', 'EmploymentsGroupController', [
    'except' => ['show'],
    'names'  => [
        'index'   => 'employments.group.index',
        'create'  => 'employments.group.create',
        'store'   => 'employments.group.store',
        'edit'    => 'employments.group.edit',
        'update'  => 'employments.group.update',
        'destroy' => 'employments.group.destroy',
    ],
]);
Route::put('employments/group/{group}/dates/{date}/cancel', 'EmploymentsGroupDatesController@cancel')->name('employments.group.dates.cancel');
Route::resource('employments/group.dates', 'EmploymentsGroupDatesController', [
    'except' => ['show'],
    'names'  => [
        'index'   => 'employments.group.dates.index',
        'create'  => 'employments.group.dates.create',
        'store'   => 'employments.group.dates.store',
        'edit'    => 'employments.group.dates.edit',
        'update'  => 'employments.group.dates.update',
        'destroy' => 'employments.group.dates.destroy',
    ],
]);
Route::resource('employments/users', 'EmploymentsUsersController', [
    'except' => ['show', 'create', 'store'],
    'names'  => [
        'index'   => 'employments.users.index',
        'edit'    => 'employments.users.edit',
        'update'  => 'employments.users.update',
        'destroy' => 'employments.users.destroy',
    ],
]);
Route::get('timetables/{timetable}/calendar/{start?}/{end?}', 'TimetablesController@calendarForm')->name('timetables.calendar.form');
Route::put('timetables/{timetable}/calendar/{start?}/{end?}', 'TimetablesController@calendarSubmit')->name('timetables.calendar.submit');
Route::resource('timetables', 'TimetablesController');
Route::get('settings', ['as' => 'settings.edit', 'uses' => 'SettingsController@editSettings']);
Route::post('settings', ['as' => 'settings.save', 'uses' => 'SettingsController@saveSettings']);