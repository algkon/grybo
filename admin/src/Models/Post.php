<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevi�ius
 * Date: 2016-09-10
 * Time: 21:55
 */

namespace Admin\Models;

use Admin\Extensions\Categories\Categoryable;
use Admin\Extensions\Galleries\Galleryable;
use Admin\Extensions\Images\Imageable;
use Admin\Extensions\Layout\Layoutable;
use Admin\Extensions\Metadata\Metadataable;

class Post extends AdminModel
{
    use Categoryable, Metadataable, Galleryable, Layoutable, Imageable;

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'category_id', 'title', 'description', 'content', 'slug', 'active'];

    /**
     * @return string
     */
    public function getShortDescriptionAttribute()
    {
        return str_limit($this->description, 230);
    }

    /**
     * @param $value
     */
    public function setCategoryIdAttribute($value)
    {
        $this->attributes['category_id'] = strlen(trim($value)) == 0 ? 0 : $value ;
    }

    /**
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function url()
    {
        return $this->category ? url($this->category->slug . '/' . $this->slug) : url($this->slug) ;
    }
}