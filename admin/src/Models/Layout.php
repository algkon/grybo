<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-10-11
 * Time: 03:05
 */

namespace Admin\Models;


class Layout extends AdminModel
{
    /**
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function layoutable()
    {
        return $this->morphTo();
    }
}