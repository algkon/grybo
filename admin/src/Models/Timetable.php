<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevi�ius
 * Date: 2016-09-10
 * Time: 22:40
 */

namespace Admin\Models;

use Admin\Extensions\Images\Imageable;
use Admin\Extensions\Metadata\Metadataable;

class Timetable extends AdminModel
{
    use Metadataable, Imageable;

    /**
     * @var array
     */
    protected $fillable = ['title', 'slug', 'description', 'content', 'active'];


    /**
     * @return string
     */
    public function getShortDescriptionAttribute()
    {
        return str_limit($this->description, 230);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function calendar()
    {
        return $this->hasMany(TimetablesCalendar::class)->orderBy('starting_time');
    }
}