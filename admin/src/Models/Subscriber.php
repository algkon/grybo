<?php

namespace Admin\Models;

use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['email', 'unsubscribe_token'];

    /**
     * @var bool
     */
    public $timestamps = false;
}
