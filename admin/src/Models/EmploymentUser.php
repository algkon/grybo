<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-26
 * Time: 15:49
 */

namespace Admin\Models;


use Carbon\Carbon;

class EmploymentUser extends AdminModel
{
    /**
     * @var string
     */
    protected $table = 'employment_user';

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'employment_id',
        'employment_calendar_id',
        'employment_name',
        'employment_date',
        'employment_duration',
        'coach',
        'first_name',
        'last_name',
        'phone',
        'email',
        'status',
    ];

    /**
     * @return string
     */
    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * @return Carbon
     */
    public function getEmploymentDateAttribute($value)
    {
        return (new Carbon($value))->toDateString();
    }

    /**
     * @return Carbon
     */
    public function getEmploymentTimeAttribute($value)
    {
        return (new Carbon($this->getAttributeFromArray('employment_date')))->toTimeString();
    }

    /**
     * @param $value
     * @return string
     */
    public function getEmploymentDurationAttribute($value)
    {
        return (new Carbon($value))->format('H:i');
    }

    /**
     * @return string
     */
    public function getTimeRangeAttribute()
    {
        $duration      = new Carbon($this->getAttributeFromArray('employment_duration'));
        $starting_time = new Carbon($this->employment_time);
        $ending_time   = (new Carbon($this->employment_time))
            ->addHours($duration->hour)
            ->addMinutes($duration->minute)
            ->addSeconds($duration->second);

        return $starting_time->format('H:i') . '-' . $ending_time->format('H:i');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function employment()
    {
        return $this->belongsTo(Employment::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function calendar()
    {
        return $this->belongsTo(EmploymentCalendar::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}