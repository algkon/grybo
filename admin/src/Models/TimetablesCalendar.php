<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-10-21
 * Time: 17:16
 */

namespace Admin\Models;


use Carbon\Carbon;

class TimetablesCalendar extends AdminModel
{
    protected $table = 'timetables_calendar';

    /**
     * @var array
     */
    protected $fillable = ['timetable_id', 'starting_time', 'ending_time', 'status', 'row', 'col', 'start_date', 'end_date'];

    /**
     * @var array
     */
    protected $appends = ['reserved_from', 'reserved_to'];

    /**
     * @param $value
     * @return string
     */
    public function getStartingTimeAttribute($value)
    {
        return $this->exists && !is_null($value) ? (new Carbon($value))->format('H:i') : $value ;
    }

    /**
     * @param $value
     * @return string
     */
    public function getEndingTimeAttribute($value)
    {
        return $this->exists && !is_null($value) ? (new Carbon($value))->format('H:i') : $value ;
    }

//    public function getStatusAttribute($value)
//    {
//        return 1;
//    }

    /**
     * @return string
     */
    public function getUniqueCustomKeyAttribute()
    {
        return $this->row . '-' . $this->col ;
    }

    public function getReservedFromAttribute()
    {
        return 'ok';
    }

    public function getReservedToAttribute()
    {
        return 'ok2';
    }

    public function getAppends()
    {
        return $this->appends;
    }
}