<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-13
 * Time: 15:08
 */

namespace Admin\Models;


class Metadata extends AdminModel
{
    /**
     * @var string
     */
    protected $table = 'metadata';

    /**
     * @var array
     */
    protected $fillable = ['title', 'description', 'keywords'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function metadataable()
    {
        return $this->morphTo();
    }
}