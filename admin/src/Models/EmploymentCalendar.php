<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-23
 * Time: 14:02
 */

namespace Admin\Models;


use Carbon\Carbon;

class EmploymentCalendar extends AdminModel
{
    /**
     * @var string
     */
    protected $table = 'employments_calendar';

    /**
     * @var array
     */
    protected $fillable = [
        'employment_id',
        'starting_date',
        'starting_time',
        'duration',
        'people_min',
        'people_max',
        'people_registered',
        'active'
    ];

    /**
     * @var array
     */
    protected $appends = ['is_active', 'is_now', 'is_ended', 'users_registered'];

    /**
     * @param $value
     * @return int
     */
    public function getActiveAttribute($value)
    {
        return is_null($value) ? 1 : $value ;
    }

    /**
     * @return int
     */
    public function getYearAttribute()
    {
        return (new Carbon($this->getAttributeFromArray('starting_date')))->year;
    }

    /**
     * @return int
     */
    public function getMonthAttribute()
    {
        return (new Carbon($this->getAttributeFromArray('starting_date')))->month;
    }


    /**
     * @return int
     */
    public function getDayAttribute()
    {
        return (new Carbon($this->getAttributeFromArray('starting_date')))->day;
    }

    /**
     * @return string
     */
    public function getStartingDateTimeAttribute()
    {
        return $this->starting_date . ' ' . $this->getAttributeFromArray('starting_time') ;
    }

    /**
     * @return string
     */
    public function getEndingDateTimeAttribute()
    {
        $duration    = new Carbon($this->getAttributeFromArray('duration'));
        $ending_time = (new Carbon($this->getStartingDateTimeAttribute()))
            ->addHours($duration->hour)
            ->addMinutes($duration->minute)
            ->addSeconds($duration->second);

        return $ending_time->format('Y-m-d H:i');
    }

    /**
     * @return Carbon
     */
    public function getStartingDateAttribute($value)
    {
        return $this->exists ? (new Carbon($value))->toDateString() : null ;
    }

    /**
     * @param $value
     * @return string
     */
    public function getStartingTimeAttribute($value)
    {
        return $this->exists ? (new Carbon($value))->format('H:i') : null ;
    }

    /**
     * @param $value
     * @return string
     */
    public function getDurationAttribute($value)
    {
        return $this->exists ? (new Carbon($value))->format('H:i') : null ;
    }

    /**
     * @return string
     */
    public function getTimeRangeAttribute()
    {
        return $this->starting_time . '-' . (new Carbon($this->ending_date_time))->format('H:i');
    }

    /**
     * @return string
     */
    public function getRouteAttribute()
    {
        return route('employments', [
            $this->getYearAttribute(),
            str_pad($this->getMonthAttribute(), 2, 0, STR_PAD_LEFT),
            str_pad($this->getDayAttribute(), 2, 0, STR_PAD_LEFT)
        ]);
    }

    public function getRouteWithScrollAttribute()
    {
        return $this->getRouteAttribute() . '#day-events';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function employment()
    {
        return $this->belongsTo(Employment::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany(EmploymentUser::class)->whereIn('status', ["1", "2"]);
    }

    /**
     * @return mixed
     */
    public function getTotalFreeSpaceAttribute()
    {
        return $this->people_max - $this->users->count();
    }

    /**
     * @return bool
     */
    public function getIsNowAttribute()
    {
        return $this->isNow();
    }

    /**
     * @return bool
     */
    public function getIsEndedAttribute()
    {
        return $this->isEnded();
    }

    /**
     * @return bool
     */
    public function getIsActiveAttribute()
    {
        return $this->isActive();
    }

    /**
     * @return mixed
     */
    public function getUsersRegisteredAttribute()
    {
        return $this->users->count();
    }

    /**
     * @return bool
     */
    public function available()
    {
        return $this->total_free_space && $this->starting_date_time > Carbon::now();
    }

    /**
     * @return bool
     */
    public function isNow()
    {
        $now = Carbon::now();

        return ($this->active == 1 && $this->starting_date_time <= $now && $this->ending_date_time >= $now) ;
    }

    /**
     * @return bool
     */
    public function isEnded()
    {
        $now = Carbon::now();

        return $this->ending_date_time < $now ;
    }
}