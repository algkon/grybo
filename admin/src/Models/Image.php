<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-22
 * Time: 16:22
 */

namespace Admin\Models;


class Image extends AdminModel
{
    /**
     * @var array
     */
    protected $fillable = ['ordering', 'alt', 'name', 'path'];

    /**
     * @var array
     */
    protected $appends = ['full_path_medium', 'full_path_original'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function imageable()
    {
        return $this->morphTo();
    }

    /**
     * @return string
     */
    public function getFullPathMediumAttribute()
    {
        return $this->path('medium');
    }

    /**
     * @return string
     */
    public function getFullPathOriginalAttribute()
    {
        return $this->path();
    }

    /**
     * @param string $size
     * @return string
     */
    public function path($size = 'original')
    {
        return asset('image/' . $size . '/' . $this->path);
    }
}