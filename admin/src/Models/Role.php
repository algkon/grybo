<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevi�ius
 * Date: 2016-09-08
 * Time: 16:26
 */

namespace Admin\Models;


use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'label', 'name', 'style'
    ];

    /**
     * @return mixed|string|\Symfony\Component\Translation\TranslatorInterface
     */
    public function getLabel()
    {
        $translated = str_replace('admin::roles.', '', trans('admin::roles.' . $this->name));

        return $translated != $this->name ? $translated : $this->label;
    }

    /**
     * @return string
     */
    public function getStyledLabel()
    {
        return '<span class="label label-' . $this->style . '">' . $this->getLabel() . '</span>';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }

    /**
     * @param Permission $permission
     * @return Model
     */
    public function givePermissionTo(Permission $permission)
    {
        return $this->permissions()->save($permission);
    }

    /**
     * @param $permission
     * @return bool
     */
    public function hasPermission($permission)
    {
        if (!($permission instanceof Permission)) {
            return false;
        }

        return $this->permissions->contains('id', $permission->id);
    }
}