<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-08
 * Time: 16:41
 */

namespace Admin\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'label', 'name'
    ];

    /**
     * @return mixed|string|\Symfony\Component\Translation\TranslatorInterface
     */
    public function getLabel()
    {
        $translated = str_replace('admin::permissions.', '', trans('admin::permissions.' . $this->name));

        return $translated != $this->name ? $translated : $this->label ;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }
}