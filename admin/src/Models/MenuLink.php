<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevi�ius
 * Date: 2016-09-20
 * Time: 10:18
 */

namespace Admin\Models;


class MenuLink extends AdminModel
{
    /**
     * @var string
     */
    protected $table = 'menu_links';

    /**
     * @var array
     */
    protected $fillable = ['menu_id', 'parent_id', 'title', 'slug', 'rel', 'target', 'active', 'sort'];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function child()
    {
        return $this->hasMany(MenuLink::class, 'parent_id');
    }

    /**
     * @param $value
     * @return string
     */
    public function getTargetAttribute($value)
    {
        return is_null($value) || $value == '' ? '' : $value ;
    }
}