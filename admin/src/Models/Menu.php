<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-20
 * Time: 09:39
 */

namespace Admin\Models;


class Menu extends AdminModel
{
    /**
     * @var string
     */
    protected $table = 'menu';

    /**
     * @var array
     */
    protected $fillable = ['title', 'slug', 'active'];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function links()
    {
        return $this->hasMany(MenuLink::class);
    }

    /**
     * @param $link
     */
    public function giveLink($link)
    {
        $this->links()->create($link);
    }
}