<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevi�ius
 * Date: 2016-09-07
 * Time: 21:03
 */

namespace Admin\Models;

use Admin\Extensions\Galleries\Galleryable;
use Admin\Extensions\Images\Imageable;
use Admin\Extensions\Layout\Layoutable;
use Admin\Extensions\Metadata\Metadataable;

class Page extends AdminModel
{
    use Metadataable, Galleryable, Layoutable, Imageable;

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'title', 'content', 'slug', 'active'];
}