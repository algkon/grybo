<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevi�ius
 * Date: 2016-09-05
 * Time: 14:36
 */

namespace Admin\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class AdminModel extends Model
{
    /**
     * @var int
     */
    protected $perPage = 20;

    /**
     * @return array|\Illuminate\Http\Request|string
     */
    public function getPerPage()
    {
        $per_page = request('per_page', $this->perPage);

        if (! in_array($per_page, [20, 50, 100])) {
            return $this->perPage;
        }

        return $per_page;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active == 1;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return string
     */
    public function getAuthorName()
    {
        return $this->author ? $this->author->name : "---------" ;
    }

    /**
     * @return string
     */
    public function createdDateAsString()
    {
        $dt = new Carbon($this->created_at);

        return sprintf('%s %s %s d.',
            $dt->formatLocalized('%Y'),
            trans('time.of_month.' . str_slug($dt->formatLocalized('%B'), '_')),
            $dt->formatLocalized('%d')
        );
    }
}