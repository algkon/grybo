<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-10-03
 * Time: 15:24
 */

namespace Admin\Models;

use Carbon\Carbon;
use Gloudemans\Shoppingcart\Facades\Cart as ShoppingCart;
use SimplePack\Nsoft\v2\Cart;

class Order extends AdminModel
{
    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'order_id',
        'payment_method',
        'cart',
        'price',
        'discount_code',
        'first_name',
        'last_name',
        'email',
        'phone',
        'ip',
        'status',
        'payed_at'
    ];

    /**
     * @param $value
     */
    public function setCartAttribute($value)
    {
        $this->attributes['cart'] = serialize($value);
    }

    /**
     * @param $value
     * @return string
     */
    public function getPriceFormattedAttribute()
    {
        return number_format($this->price, 2, ',', '');
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getCartAttribute($value)
    {
        return unserialize($value);
    }

    /**
     * @return bool
     */
    public function getAvailableAttribute()
    {
        $response = (new Cart())->carts_state($this->order_id);

        if ($response->fails()) {
            return false;
        }

        if (in_array($this->status, [1,2])) {
            return false;
        }

        $cart = $response->getData()->all();

        return ($cart['status']['status'] == 1 && $cart['status']['expiration'] > Carbon::now()) ? true : false ;
    }

    /**
     * @return string
     */
    public function createdDay()
    {
        return (new Carbon($this->created_at))->toDateString();
    }

    /**
     * @return mixed
     */
    public function cart()
    {
        ShoppingCart::instance('admin')->add(json_decode(json_encode($this->cart), true));
        $cart['products'] = ShoppingCart::instance('admin')->content();
        $cart['count'] = ShoppingCart::instance('admin')->content()->count();
        $cart['subtotal'] = ShoppingCart::instance('admin')->subtotal();
        $cart['total'] = ShoppingCart::instance('admin')->total();
        $cart['tax'] = ShoppingCart::instance('admin')->tax();
        ShoppingCart::instance('admin')->destroy();

        return json_decode(json_encode($cart));
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return bool
     */
    public function hasDiscount()
    {
        return !is_null($this->discount_code);
    }
}