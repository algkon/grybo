<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevi�ius
 * Date: 2016-09-11
 * Time: 13:54
 */

namespace Admin\Models;

use Admin\Extensions\Images\Imageable;
use Admin\Extensions\Metadata\Metadataable;

class Category extends AdminModel
{
    use Metadataable, Imageable;
    /**
     * @var array
     */
    protected $fillable = ['user_id', 'parent_id', 'title', 'slug', 'description', 'admin_menu', 'active'];

    /**
     * @return mixed|string
     */
    public function getTitleTreeAttribute()
    {
        return $this->parent ? implode(' > ', [$this->parent->title, $this->title]) : $this->title ;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function child()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function posts()
    {
        return $this->morphedByMany(Post::class, 'categoryable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function publishedPosts()
    {
        return $this->morphedByMany(Post::class, 'categoryable')->orderBy('created_at', 'desc')->where('active', '1');
    }
}