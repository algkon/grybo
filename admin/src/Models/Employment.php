<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-23
 * Time: 13:51
 */

namespace Admin\Models;


use Admin\Extensions\Database\Eloquent\Traits\SynchronizableMany;

class Employment extends AdminModel
{
    use SynchronizableMany;

    /**
     * @var array
     */
    protected $fillable = ['title', 'description', 'user_id', 'active'];

    /**
     * @return string
     */
    public function getCombinedTitleCoachAttribute()
    {
        return sprintf('%s (%s)', $this->title, $this->coach->name);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function dates()
    {
        return $this->hasMany(EmploymentCalendar::class)->orderBy('starting_date')->orderBy('starting_time');
    }

    /**
     * @return \Admin\Extensions\Database\Eloquent\Relations\HasManyExtended
     */
    public function users()
    {
        return $this->hasMany(EmploymentUser::class)->whereIn('status', ["1", "2"]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function coach()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}