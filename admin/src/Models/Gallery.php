<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevi�ius
 * Date: 2016-09-10
 * Time: 22:40
 */

namespace Admin\Models;

use Admin\Extensions\Images\Imageable;
use Admin\Extensions\Metadata\Metadataable;
use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    use Imageable, Metadataable;

    /**
     * @var array
     */
    protected $fillable = ['title', 'slug', 'user_id', 'active'];

    /**
     * @var array
     */
    protected $appends = ['thumbnail_medium'];

    /**
     * @return mixed
     */
    public function getThumbnailMediumAttribute()
    {
        return $this->thumbnail('medium');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function galleryable()
    {
        return $this->morphTo();
    }
}