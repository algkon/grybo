<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-10
 * Time: 15:43
 */

namespace Admin\Middleware;

use Admin\Extensions\Auth\Access\LimitAccessException;
use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class AccessRoute
{
    /**
     * The authentication factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory $auth
     * @return void
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth->guard('admin');
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string[] ...$guards
     * @return mixed
     *
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function handle($request, Closure $next, ...$params)
    {
        //tmp
//        return $next($request);


        if ($this->auth->guest() || ! in_array('route', $params)) {
            return $next($request);
        }

        if ($this->auth->user()->can($request->route()->getName())) {
            return $next($request);
        }

        throw new LimitAccessException;
    }
}