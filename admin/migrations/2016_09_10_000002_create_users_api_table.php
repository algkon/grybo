<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevi�ius
 * Date: 2016-09-10
 * Time: 15:56
 */

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateUsersApiTable extends Migration
{
    /**
     * UpdateUsersTable constructor.
     *
     * @throws Exception
     */
    public function __construct()
    {
        if (! Schema::hasTable('users')) {
            throw new Exception("Table 'users' doesn't exist.");
        }
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_api', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('user_id_api')->unsigned()->nullable();
            $table->string('email')->unique();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('member_card_number')->nullable();
            $table->string('leisure_card_number')->nullable();
            $table->bigInteger('national_id');
            $table->integer('gender')->nullable();
            $table->string('phone')->nullable();
            $table->string('mobile_phone')->nullable();
            $table->string('date_of_birth')->nullable();
            $table->string('addr_country_code')->nullable();
            $table->string('addr_town')->nullable();
            $table->string('addr_street')->nullable();
            $table->string('addr_house')->nullable();
            $table->string('addr_flat')->nullable();
            $table->string('addr_post_code')->nullable();
            $table->float('balance')->nullable();
            $table->string('activation_code')->nullable();
            $table->string('session_token')->nullable();
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users_api');
    }
}