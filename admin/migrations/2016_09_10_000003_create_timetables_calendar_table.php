<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevi�ius
 * Date: 2016-09-10
 * Time: 15:56
 */

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateTimetablesCalendarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timetables_calendar', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('timetable_id')->unsigned();
            $table->time('starting_time');
            $table->time('ending_time');
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->integer('row');
            $table->integer('col');
            $table->enum('status', [0, 1]);
            $table->timestamps();

            $table->foreign('timetable_id')
                ->references('id')
                ->on('timetables')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('timetables_calendar', function (Blueprint $table) {
            $table->dropForeign(['timetable_id']);
        });
        Schema::drop('timetables_calendar');
    }
}