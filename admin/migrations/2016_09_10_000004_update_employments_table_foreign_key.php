<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-23
 * Time: 04:46
 */

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class UpdateEmploymentsTableForeignKey extends Migration
{
    /**
     * UpdateUsersTable constructor.
     *
     * @throws Exception
     */
    public function __construct()
    {
        if (! Schema::hasTable('users')) {
            throw new Exception("Table 'users' doesn't exist.");
        }
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employments', function (Blueprint $table) {

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employments', function ($table) {
            $table->dropForeign('employments_user_id_foreign');
        });
    }
}