<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->enum('active', [0, 1]);
        });

        Schema::create('menu_links', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('menu_id')->unsigned();
            $table->integer('parent_id')->unsigned();
            $table->string('title');
            $table->string('slug');
            $table->string('rel')->nullable();
            $table->string('target')->nullable();
            $table->integer('sort')->unsigned();
            $table->enum('active', [0, 1]);

            $table->foreign('menu_id')
                ->references('id')
                ->on('menu')
                ->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('menu');
        Schema::drop('menu_links');
    }
}
