<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-23
 * Time: 04:46
 */

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateEmploymentsCalendarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employments_calendar', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employment_id')->unsigned();
            $table->timestamp('starting_date');
            $table->time('starting_time');
            $table->time('duration');
            $table->integer('people_min')->default(4);
            $table->integer('people_max')->default(12);
            $table->integer('people_registered')->default(0);
            $table->enum('active', [1, 0]);
            $table->timestamps();

            $table->foreign('employment_id')
                ->references('id')
                ->on('employments')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employments_calendar');
    }
}