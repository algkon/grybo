<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-23
 * Time: 04:46
 */

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class UpdateEmploymentUserTableForeignKey extends Migration
{
    /**
     * UpdateUsersTable constructor.
     *
     * @throws Exception
     */
    public function __construct()
    {
        if (! Schema::hasTable('employment_user')) {
            throw new Exception("Table 'employment_user' doesn't exist.");
        }

        if (! Schema::hasTable('employments_calendar')) {
            throw new Exception("Table 'employments_calendar' doesn't exist.");
        }
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employment_user', function (Blueprint $table) {

            $table->foreign('employment_calendar_id')
                ->references('id')
                ->on('employments_calendar')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employment_user', function ($table) {
            $table->dropForeign('employment_user_employment_calendar_id_foreign');
        });
    }
}