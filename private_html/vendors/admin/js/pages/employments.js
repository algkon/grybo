/**
 * Created by Povilas Stankevičius on 2016-09-25.
 */

function init_pickers() {
    $('.pickadate').pickadate({
        today: '',
        close: '',
        clear: '',
        format: 'yyyy-mm-dd',
        formatSubmit: 'yyyy-mm-dd'
    });

    $('.pickatime').pickatime({
        clear: '',
        format: 'HH:i',
        formatSubmit: 'HH:i',
        formatLabel: 'HH:i',
        min: [8, 0],
        max: [22, 0]
    });
    $('.pickatime-limit').pickatime({
        clear: '',
        format: 'HH:i',
        formatSubmit: 'HH:i',
        formatLabel: 'HH:i',
        min: [0, 30],
        max: [4, 0]
    });

    $(".touchspin-set-value-min").TouchSpin({
        initval: 4
    });

    $(".touchspin-set-value-max").TouchSpin({
        initval: 12
    });

}

//function get_status(data)
//{
//    if (data.is_active == 0) {
//        return '<span class="label label-default">Įsigalios išsaugojus</span>';
//    }
//
//    if (data.is_active == 1 && data.is_now == 1) {
//        return '<span class="label label-primary">Vyksta dabar</span>';
//    } else if(data.is_active == 1 && data.is_ended == 0) {
//        return '<span class="label label-success">Galiojanti</span>';
//    } else {
//        return '<span class="label label-danger">Nebegalioja</span>';
//    }
//}

function employment_row(data) {
    return '<tr class="date" data-starting_date="' + data.starting_date + '" ' +
        'data-starting_time="' + data.starting_time + '" data-duration="' + data.duration + '" ' +
        'data-people_min="' + data.people_min + '" data-people_max="' + data.people_max + '" ' +
        'data-id="' + data.id + '">' +
        '<td>' + data.starting_date +
        '<input type="hidden" name="dates[' + data.id + '][starting_date]" value="' + data.starting_date + '">' +
        '<input type="hidden" name="dates[' + data.id + '][id]" value="' + data.id + '">' +
        '</td>' +
        '<td class="text-center">' + data.starting_time +
        '<input type="hidden" name="dates[' + data.id + '][starting_time]" value="' + data.starting_time + '">' +
        '</td>' +
        '<td class="text-center">' + data.duration +
        '<input type="hidden" name="dates[' + data.id + '][duration]" value="' + data.duration + '">' +
        '</td>' +
        '<td class="text-center">' + data.people_min +
        '<input type="hidden" name="dates[' + data.id + '][people_min]" value="' + data.people_min + '">' +
        '</td>' +
        '<td class="text-center">' + data.people_max +
        '<input type="hidden" name="dates[' + data.id + '][people_max]" value="' + data.people_max + '">' +
        '</td>' +
        '<td class="text-center">' +
        '<ul class="icons-list">' +
        '<li class="dropdown">' +
        '<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="icon-menu9"></i></a>' +
        '<ul class="dropdown-menu dropdown-menu-right">' +
        '<li>' +
        '<a href="javascript:void(0)" onclick="employment_date_edit(this);"><i class="icon-pencil"></i> Redaguoti</a>' +
        '</li>' +
        '<li class="divider"></li>' +
        '<li>' +
        '<a href="javascript:void(0)" onclick="return $.fn.currentDatatable.sweetDelete(this);"><i class="icon-trash"></i> Ištrinti</a>' +
        '</li>' +
        '</ul>' +
        '</li>' +
        '</ul>' +
        '</td>' +
        '</tr>';
}

function employment_calendar_modal(item, callback) {
    bootbox.dialog({
            size: "small",
            title: "Pridėti/Redaguoti laiką.",
            message: '<div class="row">  ' +
            '<div class="col-md-12">' +
            '<form class="form-vertical">' +
            '<div class="form-group">' +
            '<label class="control-label">Data</label>' +
            '<div class="input-group">' +
            '<span class="input-group-addon"><i class="icon-calendar3"></i></span>' +
            '<input type="text" class="form-control pickadate" name="starting_date" value="' + item.starting_date + '">' +
            '<input type="hidden" name="id" value="' + item.id + '">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="control-label">Laikas</label>' +
            '<div class="input-group">' +
            '<span class="input-group-addon"><i class="icon-alarm"></i></span>' +
            '<input type="text" class="form-control pickatime" name="starting_time" value="' + item.starting_time + '">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="control-label">Trukmė</label>' +
            '<div class="input-group">' +
            '<span class="input-group-addon"><i class="icon-alarm"></i></span>' +
            '<input type="text" class="form-control pickatime-limit" name="duration" value="' + item.duration + '">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="control-label">Minimalus reikalingas narių skaičius</label>' +
            '<input type="text" class="form-control touchspin-set-value-min" name="people_min" value="' + item.people_min + '">' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="control-label">Vietų skaičius</label>' +
            '<input type="text" class="form-control touchspin-set-value-max" name="people_max" value="' + item.people_max + '">' +
            '</div>' +
            '</form>' +
            '</div>' +
            '</div>',
            buttons: {
                success: {
                    label: '<b><i class="icon-floppy-disk"></i></b> Išsaugoti',
                    className: "btn-primary btn-labeled",
                    callback: function () {
                        var form = $(this).find('form'),
                            formInput = form.find('input'),
                            formData = {};

                        formInput.each(function () {
                            var input = $(this);

                            formData[input.attr('name')] = input.val();
                        });

                        if (typeof callback == "function") {
                            callback(formData);
                        }
                    }
                }
            }
        }
    ).addClass('employment-date-modal');

    init_pickers();
}

function employment_date_edit(element) {
    var item = $(element).closest('tr');

    employment_calendar_modal(item.data(), function (data) {
        $.fn.currentDatatable.updateRow(item, employment_row(data));
    });
}

function employment_date_add(element) {
    var button = $(element),
        data = button.data(),
        items = $('#employment_calendar').find('tbody tr.date'),
        idsArray = [];

    items.each(function () {
        idsArray.push($(this).data('id'));
    });

    var maxId = idsArray.length != 0 ? Math.max.apply(Math, idsArray) : 0;
    data.id = maxId + 1;

    employment_calendar_modal(data, function (data) {
        //if (items.length == 0) {
        //    $('#employment_calendar').find('tbody').html(employment_row(data));
        //} else {
        //    $('#employment_calendar').find('tbody').append(employment_row(data));
        //}
        $.fn.currentDatatable.addRow(employment_row(data));
    });
}

$(function () {
    $.fn.currentDatatable = $('.datatable-basic').DataTable({
        autoWidth: false,
        paging: false,
        info: false,
        ordering: false,
        dom: '<"datatable-scroll"t><"datatable-footer">',
        language: {
            search: '<span>Paieška:</span> _INPUT_'
        },
        columnDefs: [
            {
                width: '30px',
                targets: "no-sort"
            }
        ]
    });

    // Extend datatable
    $.extend($.fn.currentDatatable, {
        sweetDelete: function (element) {
            var table = this,
                row = $(element).closest('tr');

            swal({
                title: "Ar tikrai norite ištrinti?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#EF5350",
                confirmButtonText: "Taip, ištrinti!",
                cancelButtonText: "Ne, atšaukti!"
            },
            function (isConfirm) {
                if (isConfirm) {
                    table
                        .row(row)
                        .remove()
                        .draw();
                }
            });

            return false;
        },
        addRow: function (newRow) {
            var table = this;

            table.row.add($(newRow)).draw();
        },
        updateRow: function (row, newRow) {
            var table = this;

            row.outerHTML = newRow;
            //row.html($(newRow).html());
            //console.log(row);

            table
                .row( row )
                .invalidate()
                .draw();
        }
    });
});
