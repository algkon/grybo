/**
 * Created by Povilas Stankevičius on 2016-09-25.
 */

function init_pickers() {
    $('.pickadate').pickadate({
        today: '',
        close: '',
        clear: '',
        format: 'yyyy-mm-dd',
        formatSubmit: 'yyyy-mm-dd'
    });

    $('.pickatime').pickatime({
        clear: '',
        format: 'HH:i',
        formatSubmit: 'HH:i',
        formatLabel: 'HH:i',
        min: [8, 0],
        max: [22, 0]
    });
    $('.pickatime-limit').pickatime({
        clear: '',
        format: 'HH:i',
        formatSubmit: 'HH:i',
        formatLabel: 'HH:i',
        min: [0, 30],
        max: [4, 0]
    });

    $(".touchspin-set-value-min").TouchSpin({
        initval: 4
    });

    $(".touchspin-set-value-max").TouchSpin({
        initval: 12
    });

}

function employment_submit_form(form, callback) {
    var formData = $(form).serialize(),
        method   = $(form).find('input[name="_method"]');
        requestMethod = method.length ? method.val() : $(form).attr('method') ;

    $.ajax({
        url: $(form).attr('action'),
        type: requestMethod,
        data: formData,
        dataType: 'json',
        success: function (response) {

            swal({
                title: response.message,
                confirmButtonColor: "#66BB6A",
                type: "success"
            }, function () {
                if (typeof callback == "function") {
                    callback(response.row);
                }
            });
        },
        error: function () {
            swal({
                title: "Neteisingai užpildyta forma!",
                confirmButtonColor: "#EF5350",
                type: "error"
            });
        }
    });
}

function employment_calendar_modal(button, callback) {
    $.ajax({
        url: $(button).attr('href'),
        type: 'GET',
        success: function (response) {
            bootbox.dialog({
                    size: "small",
                    title: "Pridėti/Redaguoti laiką.",
                    message: response,
                    buttons: {
                        success: {
                            label: '<b><i class="icon-floppy-disk"></i></b> Išsaugoti',
                            className: "btn-primary btn-labeled",
                            callback: function () {
                                var form = $(this).find('form');

                                employment_submit_form(form, callback);
                            }
                        }
                    }
                }
            ).addClass('employment-date-modal');

            init_pickers();
        }
    });
}

function employment_date_cancel(e, button) {
    e.preventDefault();

    var form = $(button).parents('tr').find('form.cancel');

    sweet_cancel(form, function (row) {
        $.fn.currentDatatable.updateRow($(button).parents('tr'), row);
    });
}

function employment_date_edit(e, button) {
    e.preventDefault();

    employment_calendar_modal(button, function (row) {
        $.fn.currentDatatable.updateRow($(button).closest('tr'), row);
    });
}

function employment_date_add(e, button) {
    e.preventDefault();

    employment_calendar_modal(button, function (row) {
        $.fn.currentDatatable.addRow(row);
    });
}

$(function () {
    //function extend_datatable_defaults() {
    //
    //}
});
