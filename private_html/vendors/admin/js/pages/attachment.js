/**
 * Created by Povilas Stankevičius on 2016-10-11.
 */
function delete_attachment() {
    var html = '<a href="javascript:void(0)" id="attach_image" class="btn btn-default" type="button">' +
        'Pasirinkti paveikslėlį' +
        '</a>';

    $('#widget-featured-image').html(html);
}

$(function () {

    var alphamanager_path = window.Laravel.base_url + '/vendors/admin/js/plugins/alphamanager';

    $(document).on('click', '#attach_image', function () {
        var browser = AlphaManager.init({
            returnUrlPrefix: window.Laravel.base_url + '//',
            skin: 'business',
            skinMod: '',
            hideCopyright: true,
            showRootDir: true,
            fileToolbar: ["upload", "|", "preview", "rename", "del", "view", "order", "search"], //"preview", "download",
            dirToolbar: ["create", "rename", "del"],
            fileMenu: ["select", "-", "preview", "-", "paste", "cut", "copy", "-", "rename", "del"],
            dirMenu: ["paste", "cut", "copy", "create", "rename", "del"],
            multiSelect: false,
            onFileSet: function (files) {

                if (files.length > 0) {
                    var html = '';

                    for (var i in files) {
                        //thumbnail = alphamanager_path + '/php/thumb.php?f=' + encodeURIComponent(filePath) + '&width=180&height=180';//window.Laravel.base_url + '/uploads/' + file.name;
                        var file = files[i],
                            path = file.path.replace(window.Laravel.base_url, '').replace(/(\/?\.\.\/)+/, ''),
                            thumbnail = file.path.replace('../../../', 'image/medium/'),
                            fullPath = file.path.replace('../../../', 'image/original/');

                        if (file.image === true) {

                            html += '<div class="thumbnail">' +
                                '<input type="hidden" name="attachment[name]" value="' + file.name + '" />' +
                                '<input type="hidden" name="attachment[path]" value="' + path + '" />' +
                                '<input type="hidden" name="attachment[full_path_medium]" value="' + thumbnail + '" />' +
                                '<input type="hidden" name="attachment[full_path_original]" value="' + fullPath + '" />' +
                                '<div class="thumb">' +
                                '<img src="' + thumbnail + '" alt="">' +
                                '</div>' +
                                '</div>' +
                                '<a href="javascript:void(0)" onclick="delete_attachment();" class="btn btn-danger" type="button">' +
                                'Pašalinti paveikslėlį' +
                                '</a>';
                        }
                    }
                }
                $('#widget-featured-image').html(html);
            }
        }, 'attach_image');

        if (!$('.alphamanager_dlg.alphamanager_mod_').hasClass('alphamanager_show')) {
            browser.show();
        }
    });
});