/**
 * Created by Povilas Stankevičius on 2016-10-22.
 */

var random_id = function () {
    return (Date.now() || function () {
            return +new Date;
        }) + Math.random().toString(16).substr(2, 8);
};

(function (Handsontable) {
    var ModalEditor = Handsontable.editors.BaseEditor.prototype.extend();

    //ModalEditor.prototype.init = function () {};
    //ModalEditor.prototype.prepare = function () {};
    ModalEditor.prototype.getValue = function () {
    };
    ModalEditor.prototype.setValue = function (value) {
    };
    ModalEditor.prototype.saveValue = function (val, ctrlDown) {
    };
    ModalEditor.prototype.focus = function () {
    };
    ModalEditor.prototype.close = function () {
    };

    ModalEditor.prototype.open = function () {
        var data = this,
            newData = $.extend({}, data.originalValue);

        bootbox.dialog({
                size: "small",
                title: "Pridėti/Redaguoti laiką.",
                message: '<div class="row">  ' +
                '<div class="col-md-12">' +
                '<form class="form-vertical">' +
                '<div class="form-group">' +
                '<label class="control-label">Laikas nuo</label>' +
                '<div class="input-group">' +
                '<span class="input-group-addon"><i class="icon-alarm"></i></span>' +
                '<input type="text" class="form-control pickatime" name="starting_time" value="' +
                (data.originalValue.starting_time === null ? '' : data.originalValue.starting_time) +
                '">' +
                '</div>' +
                '</div>' +
                '<div class="form-group">' +
                '<label class="control-label">Laikas iki</label>' +
                '<div class="input-group">' +
                '<span class="input-group-addon"><i class="icon-alarm"></i></span>' +
                '<input type="text" class="form-control pickatime" name="ending_time" value="' +
                (data.originalValue.ending_time === null ? '' : data.originalValue.ending_time) +
                '">' +
                '</div>' +
                '</div>' +
                '</form>' +
                '</div>' +
                '</div>',
                buttons: {
                    success: {
                        label: '<b><i class="icon-floppy-disk"></i></b> Išsaugoti',
                        className: "btn-primary btn-labeled",
                        callback: function () {
                            var form = $(this).find('form'),
                                starting_time = form.find('input[name="starting_time"]').val(),
                                ending_time = form.find('input[name="ending_time"]').val();

                            if (starting_time != '' && ending_time != '') {
                                newData.starting_time = starting_time;
                                newData.ending_time = ending_time;
                            } else {
                                newData.starting_time = null;
                                newData.ending_time = null;
                                newData.id = null;
                                newData.col = null;
                                //newData.date = null;
                                //ending_time = null
                                //id = null
                                newData.row = null;
                                //starting_time = null
                                newData.status = null;
                                newData.timetable_id = null;
                            }

                            data.instance.setDataAtCell(data.row, data.col, newData);
                        }
                    }
                }
            }
        ).addClass('handsontable-form-modal');

        $('.pickatime').pickatime({
            clear: 'Išvalyti',
            format: 'HH:i',
            formatSubmit: 'HH:i',
            formatLabel: 'HH:i',
            min: [7, 0],
            max: [22, 0]
        });
    };

    // Put editor in dedicated namespace
    Handsontable.editors.ModalEditor = ModalEditor;

    // Register alias
    Handsontable.editors.registerEditor('modal', ModalEditor);

})(Handsontable);

$(function () {

    // Define element
    var hot_checks_labels = document.getElementById('hot_checks_labels');

    // Initialize with options
    var hot_checks_labels_init = new Handsontable(hot_checks_labels, {
        data: hot_checks_labels_data,
        stretchH: 'all',
        rowHeights: 100,
        colHeaders: hot_headers,
        editor: "modal",
        //minSpareRows: 1,
        columns: [
            {data: "monday", renderer: customRenderer},
            {data: "tuesday", renderer: customRenderer},
            {data: "wednesday", renderer: customRenderer},
            {data: "thursday", renderer: customRenderer},
            {data: "friday", renderer: customRenderer},
            {data: "saturday", renderer: customRenderer},
            {data: "sunday", renderer: customRenderer}
        ],
        beforeChange: function (changes) {
            var total_changes = changes.length;

            for (var i = 0; i < total_changes; i++) {

                if (changes[i][3].starting_time !== null && changes[i][3].ending_time !== null) {
                    if (changes[i][2].id === null) {
                        changes[i][3].id = 'new-' + random_id();
                        changes[i][3].timetable_id = timetable_id;

                        if (changes[i][2].row != changes[i][3].row || changes[i][2].col != changes[i][3].col) {
                            changes[i][3].status = 1;
                        }

                    } else {
                        changes[i][3].id = changes[i][2].id;

                        if (changes[i][2].row != changes[i][3].row || changes[i][2].col != changes[i][3].col) {
                            changes[i][3].status = changes[i][2].status;
                        }
                    }

                    changes[i][3].row = changes[i][2].row;
                    changes[i][3].col = changes[i][2].col;
                    //changes[i][3].date = weekdays_dates[changes[i][1]];
                }

            }
        }
    });

    function checkboxCheck(event) {

        if (event.target.type == 'checkbox') {

            var tr = $(this).closest('tr'),
                td = $(this).closest('td'),
                checkbox = $(this),
                newValue = $.extend({}, hot_checks_labels_init.getDataAtCell(tr[0].rowIndex - 1, td[0].cellIndex));

            if (event.target.checked) {
                newValue.status = 0;
            } else {
                newValue.status = 1;
            }

            hot_checks_labels_init.setDataAtCell(tr[0].rowIndex - 1, td[0].cellIndex, newValue);
        }

        newValue = null;
    }

    function customRenderer(instance, td, row, col, prop, value, cellProperties) {
        var id = row + '-' + col,
            html = '<i class="icon-pencil"></i>';

        if (typeof value == 'object') {

            if (value.status == 0) {
                $(td).addClass('htInvalid');
            } else {
                $(td).removeClass('htInvalid');
            }

            if (value.starting_time !== null && value.ending_time !== null) {
                html += '<p class="text-center">' + value.starting_time + ' - ' + value.ending_time + '</p>';

                //html += '<input type="hidden" name="default[' + id + '][date]" value="' + weekdays_dates[prop] + '">';
                html += '<input type="hidden" name="default[' + id + '][starting_time]" value="' + value.starting_time + '">';
                html += '<input type="hidden" name="default[' + id + '][ending_time]" value="' + value.ending_time + '">';
                //html += '<input type="hidden" name="default[' + id + '][timetable_id]" value="' + timetable_id + '">';
                //html += '<input type="hidden" name="default[' + id + '][row]" value="' + row + '">';
                //html += '<input type="hidden" name="default[' + id + '][col]" value="' + col + '">';

                var checkbox = '<input type="hidden" name="default[' + id + '][status]" value="1">';
                checkbox += '<input type="checkbox" class="status" name="default[' + id + '][status]" value="0"';
                checkbox += value.status == 0 ? ' checked="checked">' : '>';
                //checkbox += value.status == 0 ? ' checked="checked"><small class="reserved_note">užimta iki 2016-12-14</small>' : '>';

                html += checkbox;
            } else {
                html = '';
            }

            //if (value.id !== null) {
            //
            //    $(td).addClass('has-content');
            //
            //    //html += '<i class="icon-cross3"></i>';
            //
            //    var checkbox = '<input type="hidden" name="dates[' + id + '][status]" value="1">';
            //    checkbox += '<input type="checkbox" class="status" name="dates[' + id + '][status]" value="0"';
            //    checkbox += value.status == 0 ? ' checked="checked"><small class="reserved_note">užimta iki 2016-12-14</small>' : '>';
            //
            //    html += checkbox;
            //    //html += '<input type="hidden" name="dates[' + id + '][date]" value="' + weekdays_dates[prop] + '">';
            //    //html += '<input type="hidden" name="dates[' + id + '][starting_time]" value="' + value.starting_time + '">';
            //    //html += '<input type="hidden" name="dates[' + id + '][ending_time]" value="' + value.ending_time + '">';
            //    //html += '<input type="hidden" name="dates[' + id + '][timetable_id]" value="' + timetable_id + '">';
            //    //html += '<input type="hidden" name="dates[' + id + '][row]" value="' + row + '">';
            //    //html += '<input type="hidden" name="dates[' + id + '][col]" value="' + col + '">';
            //
            //} else
            console.log(id, value);
            //if (value.starting_time !== null && value.ending_time !== null) {
            //    var checkbox = '<input type="hidden" name="default[' + id + '][status]" value="1">';
            //    checkbox += '<input type="checkbox" class="status" name="default[' + id + '][status]" value="0"';
            //    checkbox += value.status == 0 ? ' checked="checked"><small class="reserved_note">užimta iki 2016-12-14</small>' : '>';
            //
            //    html += checkbox;
            //}

            html += '<input type="hidden" name="default[' + id + '][timetable_id]" value="' + timetable_id + '">';
            html += '<input type="hidden" name="default[' + id + '][row]" value="' + row + '">';
            html += '<input type="hidden" name="default[' + id + '][col]" value="' + col + '">';
        }

        Handsontable.Dom.addEvent(td, 'click', checkboxCheck);

        td.innerHTML = html;

        return td;
    }
});