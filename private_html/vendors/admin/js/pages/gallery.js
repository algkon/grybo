/* ------------------------------------------------------------------------------
 *
 * Created by Povilas Stankevičius on 2016-09-23.
 *
 * ---------------------------------------------------------------------------- */

function gallery_images_resort(gallery)
{
    gallery = gallery || '#gallery';

    var images = $(gallery).find('.thumbnail');

    if (images.length) {
        images.each(function (index, value) {
            $(this).find('input.image_order').val(index);
        });
    }
}

function gallery_item_delete(element) {
    var item = $(element).closest('.thumbnail');

    return swal({
            title: "Are you sure?",
            text: "You will not be able to recover this imaginary file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel pls!"
        },
        function (isConfirm) {
            if (isConfirm) {
                item.parent().remove();
                gallery_images_resort();
            }
        });
}

function gallery_image_delete(element) {
    gallery_item_delete(element);
}

function gallery_delete(element) {
    gallery_item_delete(element);
}

function gallery_add(callback) {
    $.ajax({
        url: window.Laravel.base_url + '/admin/galleries',
        method: 'GET',
        success: function (response) {
            bootbox.dialog({
                    size: "large",
                    title: "Galerijos",
                    message: response,
                    buttons: {
                        success: {
                            label: '<b><i class="icon-floppy-disk"></i></b> Išsaugoti',
                            className: "btn-primary btn-labeled",
                            callback: function () {
                                var galleries = $(this).find('table tr.selected');

                                if (galleries.length > 0) {
                                    var html = '';
                                    galleries.each(function () {
                                        var gallery = $(this).data();

                                        html += '<div class="col-lg-3 col-sm-6">' +
                                            '<div class="thumbnail">' +
                                            '<input type="hidden" name="galleries[' + gallery.id + '][id]" value="' + gallery.id + '" class="gallery_id"/>' +
                                            '<input type="hidden" name="galleries[' + gallery.id + '][title]" value="' + gallery.title + '"/>' +
                                            '<input type="hidden" name="galleries[' + gallery.id + '][thumbnail_medium]" value="' + gallery.thumbnail + '"/>' +
                                            '<div class="thumb">' +
                                            '<img src="' + gallery.thumbnail + '" alt="">' +
                                            '<div class="caption-overflow">' +
                                            '<span>' +
                                            '<a href="' + gallery.edit_url + '" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5" target="_blank">' +
                                            '<i class="icon-pencil"></i>' +
                                            '</a>' +
                                            '<a href="javascript:void(0)" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5" onclick="gallery_delete(this);">' +
                                            '<i class="icon-trash"></i>' +
                                            '</a>' +
                                            '</span>' +
                                            '</div>' +
                                            '</div>' +

                                            '<div class="caption" data-popup="tooltip" data-placement="bottom" data-original-title="' + gallery.title + '">' +
                                            gallery.title +
                                            '</div>' +
                                            '</div>' +
                                            '</div>';
                                    });
                                    $('#galleries').append(html);
                                    $('[data-popup=tooltip]').tooltip();
                                }

                                if (typeof callback == "function") {
                                    callback(form);
                                }
                            }
                        }
                    }
                }
            ).addClass('gallery-modal');
            //$('#galleries').append(response);
        }
    });
}

$(function () {

    // Initialize lightbox
    $('[data-popup="lightbox"]').fancybox({
        padding: 3
    });

    $('#gallery_add').click(function (e) {
        e.preventDefault();

        gallery_add();
    });

});
