function getError(data) {
	for (var prop in data)
		return data[prop].isArray ? data[prop][0] : data[prop];
}

$(function(){

	/* RWD menu */

	$('#header-block .menu-toggle, .app-login a').on('click', function(){

		if($('#header-block #navigation .col-md-12 > .user-block').length == 0) {
			var userArea = $('#header-block .user-area').html();

			if($('#header-block .user-area').hasClass('logged')) {
				$('#header-block #navigation .col-md-12').prepend('<div class="user-block logged">'+ userArea + '</div>');
			} else {
				$('#header-block #navigation .col-md-12').prepend('<div class="user-block">'+ userArea + '</div>');
			}
			
		}

		if($(document).width() <= 768 && $('#header-block #navigation .col-md-12 ul li.divider').length == 0) {
			var headerLinks = $('#header-block .links').html();
			$('#header-block #navigation .col-md-12 > ul').append('<li role="separator" class="divider"></li>' + headerLinks);
		}

		$('#header-block #navigation, #nav-overlay').toggleClass('active');
		
		return false;
	});

	$('#header-block #navigation #close-menu, #nav-overlay').on('click', function() {
		$("#header-block #navigation, #nav-overlay").removeClass('active');

		return false;
	});


	/* Radio check */

	$('.radio-list label').on('click', function(){
		$('.radio-list label').removeClass('active');
		$(this).addClass('active');
	});

	/* Ajax request */
	$(document).on('submit', 'form.ajax-form', function (e) {
		e.preventDefault();

		var form = $(this);

		form.find('.errors').text('').hide();
		form.find('.message').text('').hide();

		$.ajax({
			type: "POST",
			url: form.attr('action'),
			dataType: "JSON",
			data: form.serialize(),
			success: function(response) {
				if (response.reload === true) {
					window.location.reload();
				} else {
					form.find('.message').text(response.message).show();

					setTimeout(function () {
						form.find('.message').fadeOut(400, function () {
							$(this).text('');
						});
					}, 5000);
				}
			},
			error: function(xhr, error) {
				form.find('.errors').text(getError(xhr.responseJSON)).show();
			}
		});
	});

	/* dropdown customize */
	$('nav#navigation li.dropdown').on('mouseenter', function(event) {
		if (!$('.menu-toggle').is(':visible')) {
			$(this).find('.dropdown-toggle').dropdown('toggle');
		}
	}).on('mouseleave', function(event) {
		if (!$('.menu-toggle').is(':visible')) {
			if ($(this).hasClass('open')) {
				$(this).find('.dropdown-toggle').dropdown('toggle');
			}
		}
	});

	$('li.dropdown > a').on('mouseleave', function(event) {
		$(this).blur();
	});
});