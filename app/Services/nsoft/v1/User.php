<?php
/**
 * Created by PhpStorm.
 * User: Povilas
 * Date: 2016-07-14
 * Time: 05:57
 */

namespace SimplePack\Nsoft\v1;


class User extends Api
{
    /**
     * REQUIRED: (string) session_token, (integer) id
     *
     * @param $session_token
     * @param $id
     * @return mixed|null
     */
    public function getDetails($session_token, $id)
    {
        $response = $this->call("subject_info_get", "GET", [
            "session_token" => $session_token,
            'id' => (int)$id,
        ]);

        return !$response->fails() ? $response->getData()->first() : null;
    }

    /**
     * REQUIRED: (string) session_token, (int) action, (int) id
     * OPTIONAL: (string) first_name, (string) last_name, (int) gender, (int) national_id, (string) phone,
     *           (string) mobile_phone, (email) email, (date) date_of_birth, (string) addr_country_code,
     *           (string) addr_town, (string) addr_street, (string) addr_house, (string) addr_flat,
     *           (string) addr_post_code
     *
     * @param $session_token
     * @param $id
     * @param array $data
     * @return $this
     */
    public function setDetails($session_token, $id, $email, array $data)
    {
        // Optional data
        $optional = ['tel_num', 'addr_country_code', 'addr_town', 'address', 'gender', 'addr_house', 'addr_flat', 'addr_post_code'];

        // Update optional data
        foreach ($optional as $item) {
            if (isset($data[$item]) && strlen(trim($data[$item])) == 0) {
                unset($data[$item]);
            }
        }

//        unset($data['addr_country_code'], $data['email']);

        return $this->call("subject_info_set", "POST", array_merge([
            'action' => 2,
            'session_token' => $session_token,
            'id' => (int)$id
        ], $data));
    }

    /**
     * @param $session_token
     * @param array $data
     * @return $this
     */
    public function changePassword($session_token, array $data)
    {
        return $this->call("changePassword", "POST", array_merge([
            'session_token' => $session_token
        ], $data));
    }

    /**
     * @param $email
     * @return $this
     */
    public function forgotPassword($email)
    {
        return $this->call("forgotPassword", "POST", ['email' => $email]);
    }

    /**
     * @param $session_token
     * @return $this
     */
    public function checkToken($session_token)
    {
        return $this->call("checkToken ", "POST", ['session_token' => $session_token]);
    }

    /**
     * @param $session_token
     * @return $this
     */
    public function logout($session_token)
    {
        return $this->call("logout", "POST", ['session_token' => $session_token]);
    }
}