<?php
/**
 * Created by PhpStorm.
 * User: Povilas
 * Date: 2016-07-14
 * Time: 04:27
 */

namespace SimplePack\Nsoft\v1;

use SimplePack\Nsoft\Api as ApiBuilder;

class Api extends ApiBuilder
{
//    protected $api_base_url = "http://web.test.nsoft.eu/g7/api/v1/";
    protected $api_base_url = "https://ws.grybo7.lt/api/v1/";
}