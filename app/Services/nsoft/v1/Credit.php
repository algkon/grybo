<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-14
 * Time: 14:27
 */

namespace SimplePack\Nsoft\v1;


class Credit extends Api
{
    /**
     * @param $session_token
     * @return null
     */
    public function get($session_token)
    {
        $response = $this->call('credit', 'GET', [
            'sessionToken' => $session_token
        ]);

        return !$response->fails() ? $response->getData()->all() : null;
    }

    public function add()
    {

    }
}