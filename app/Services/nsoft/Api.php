<?php
/**
 * Created by PhpStorm.
 * User: Povilas
 * Date: 2016-07-13
 * Time: 16:20
 */

namespace SimplePack\Nsoft;

use Illuminate\Support\MessageBag;


abstract class Api
{
    /**
     * @var Api full url
     */
    protected $url;

    /**
     * @var Api base url
     */
    protected $api_base_url;

    /**
     * @var array
     */
    protected $response;

    /**
     * @var string
     */
    private $request_type = "POST";

    /**
     * Valid request methods
     *
     * @var string[]
     */
    private $validRequestMethods = [
        'CONNECT' => 1,
        'DELETE' => 1,
        'GET' => 1,
        'HEAD' => 1,
        'OPTIONS' => 1,
        'PATCH' => 1,
        'POST' => 1,
        'PUT' => 1,
        'TRACE' => 1,
    ];

    /**
     * @var query string
     */
    private $request_data = "";

    /**
     * @var string
     */
    private $action;

    /**
     * @return string
     */
    public function getRequestType()
    {
        return $this->request_type;
    }

    /**
     * @return array
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @return array|null
     */
    public function getData()
    {
        return $this;
    }

    public function first()
    {
        return isset($this->response['data'][0]) ? $this->response['data'][0] : null ;
    }

    public function all()
    {
        return isset($this->response['data']) ? $this->response['data'] : null ;
    }

    /**
     * @return mixed
     */
    public function getRequestData()
    {
        if ($this->request_type !== "POST") {
            return null;
        }

        return $this->request_data;
    }

    /**
     * @param string $request_type
     */
    public function setRequestType($request_type)
    {
        if ($this->validRequestMethod($request_type)) {
            $this->request_type = $request_type;
        }
    }

    /**
     * @param mixed $request_data
     */
    public function setRequestData(array $request_data)
    {
        $this->request_data = (string)http_build_query($request_data);
    }

    /**
     * @param mixed $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }

    /**
     * @param Api $url
     */
    public function setUrl()
    {
        if ($this->request_type == "POST") {
            $this->url = implode("", [$this->api_base_url, $this->action]);
        } else {
            $this->url = implode("?", [$this->api_base_url . $this->action, $this->request_data]);
        }
    }

    /**
     * @param $request_type
     * @param array $request_data
     * @return $this
     * @throws \Exception
     */
    public function call($action, $request_type, array $request_data = [])
    {
        $this->setRequestType($request_type);
        $this->setRequestData($request_data);
        $this->setAction($action);
        $this->setUrl();

        $this->response = json_decode($this->connect(), true);

        return $this;
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    private function connect()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $this->request_type,
            CURLOPT_POSTFIELDS => $this->request_data,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            throw new \Exception("cURL Error #:" . $err);
        }

        return $response;
    }

    /**
     * @return bool
     */
    public function fails()
    {
        return $this->response['status'] === "fail";
    }

    /**
     * @return MessageBag
     */
    public function errors()
    {
        $messages = new MessageBag();

        if (isset($this->response['data']['code'])) {
            if ($this->response['data']['name'] == "Database Exception") {
                $messages->add('api_error', app('translator')->trans('validation.nsoft.response.sqlstate.' . $this->response['data']['error-info'][0]));
            } else {
                $messages->add('api_error', app('translator')->trans('validation.nsoft.response.code.' . $this->response['data']['code']));
            }
        }

        return $messages;
    }

    /**
     * @param $request_type
     * @return bool
     */
    public function validRequestMethod($request_type)
    {
        return array_key_exists($request_type, $this->validRequestMethods) && $this->validRequestMethods[$request_type];
    }
}