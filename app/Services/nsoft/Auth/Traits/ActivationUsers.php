<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-28
 * Time: 15:16
 */

namespace SimplePack\Nsoft\Auth\Traits;

use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;

trait ActivationUsers
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showActivationForm()
    {
        return view('auth.activate');
    }

    /**
     * @param Request $request
     * @param User $user
     * @return $this
     */
    public function activate(Request $request, User $user_model)
    {
        $this->validator($request->all())->validate();

        $response = (new \SimplePack\Nsoft\v2\User())->activate($request->all());

        if ($response->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($response->errors());
        }

        $activation_data = $response->getData()->first();

        if (is_null($user_model->where('email', '=', $request->get('email'))->first())) {
            // get user data by email and password
            $user_api = $this->getDetailsByEmailAndPassword($request->get('email'), $request->get('new_password'));

            if (is_null($user_api)) {
                return redirect()->route('auth.activate.form')->withErrors([
                    'errors' => trans('validation.nsoft.response.code.1')
                ]);
            }

            // Create User with role customer
            $user = $this->create([
                'name' => $user_api['first_name'] . ' ' . $user_api['last_name'],
                'email' => $request->get('email'),
                'password' => $request->get('new_password')
            ]);
            $user->roles()->attach(3);

            // Create UserApi
            $user->api()->create($user_api);

            // update activation_code
            $user->api->activation_code = $request->get('confirmation_code');

            event(new Registered($user));
        } else {
            $user = $user_model->where('confirmation_code', $request->get('confirmation_code'))->first();
        }

        $user->api->user_id_api = $activation_data['new_id'];
        $user->api->save();

        return redirect()->route('auth.login')->withSuccess("Vartotojas sėkmingai aktyvuotas! Dabar galite prisijungti.");
    }

    /**
     * @param $email
     * @param $password
     * @return mixed|null
     */
    private function getDetailsByEmailAndPassword($email, $password)
    {
        $user_api = new \SimplePack\Nsoft\v2\User();
        $response = $user_api->login($email, $password);

        // if not nsoft user
        if ($response->fails()) {
            return null;
        }

        $api_user_session = $response->getData()->first();
        $api_user_data = (new \SimplePack\Nsoft\v1\User())->getDetails(
            $api_user_session['session_token'],
            $api_user_session['sub_id']
        );

        return $api_user_data;
    }
}