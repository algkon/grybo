<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-27
 * Time: 15:35
 */

namespace SimplePack\Nsoft\Auth\Traits;

use Illuminate\Foundation\Auth\RedirectsUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Registered;
use SimplePack\Nsoft\v2\User;

trait RegistersUsers
{
    use RedirectsUsers;

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('auth.register');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request, User $user)
    {
        $this->validator($request->all())->validate();

        // generate tmp password
        $request->merge(['password' => str_random(10)]);

        // Nsoft registration
        $response = $user->register([
            'vardas' => $request->get('first_name'),
            'pavarde' => $request->get('last_name'),
            'kodas' => $request->get('national_id'),
            'el_pastas' => $request->get('email'),
            'tel_num' => $request->get('phone'),
            'salis' => $request->get('country'),
            'miestas' => $request->get('addr_town'),
            'gatve' => $request->get('addr_street'),
            'namas' => $request->get('addr_house'),
            'butas' => $request->get('addr_flat'),
            'pasto_kodas' => $request->get('addr_post_code'),
            'password' => $request->get('password'),
        ]);

        // Nsoft fails check
        if ($response->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($response->errors());
        }

        $user_api_data = $response->getData()->first();

        // Create User with role customer
        $user = $this->create($request->all());
        $user->roles()->attach(3);

        // Create UserApi
        $user->api()->create($request->all());

        // update activation_code
        $user->api->activation_code = $user_api_data['registracijos_kodas'];
        $user->api->save();

        event(new Registered($user));

//        $this->guard()->login($user);

        return redirect()
            ->route('auth.login')
            ->withSuccess("Registracija sėkminga! Norėdami prisijungti turite patvirtinti registraciją paspausdami nuorodą, kurią išsiuntėme nurodytu el. paštu.");
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }
}
