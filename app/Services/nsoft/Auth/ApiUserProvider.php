<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-27
 * Time: 19:02
 */

namespace SimplePack\Nsoft\Auth;


use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Support\Str;
use SimplePack\Nsoft\v1\Credit;

class ApiUserProvider extends EloquentUserProvider
{

    /**
     * Retrieve a user by the given credentials.
     *
     * @param  array $credentials
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByCredentials(array $credentials)
    {
        if (empty($credentials)) {
            return;
        }

//        dd($credentials);

        // First we will add each credential element to the query as a where clause.
        // Then we can execute the query and, if we found a user, return it in a
        // Eloquent User "model" that will be utilized by the Guard instances.
        $query = $this->createModel()->newQuery();

        foreach ($credentials as $key => $value) {
            if (!Str::contains($key, 'password')) {
                $query->where($key, $value);
            }
        }

        return $this->retrieveByCredentialsApi($query->first(), $credentials);
    }

    /**
     * @param $user
     * @param array $credentials
     * @return null
     */
    function retrieveByCredentialsApi($user, array $credentials)
    {
        if (is_null($user)) {
            return null;
        }

        $user_api = new \SimplePack\Nsoft\v2\User();
        $response = $user_api->login($credentials['email'], $credentials['password']);

        // if not nsoft user
        if ($response->fails()) {
            return null;
        }

        if (!$this->validateCredentials($user, $credentials)) {
            $user->password = bcrypt($credentials['password']);
        }

        return $this->retrieveApiUser($user, $response->getData()->first());
    }

    /**
     * @param $user
     * @param $api_user_session
     * @return mixed
     */
    private function retrieveApiUser($user, $api_user_session)
    {
        $credit = (new Credit())->get($api_user_session['session_token']);

        // if not nsoft user in system yet
        if (is_null($user->api)) {
            $api_user_data = (new \SimplePack\Nsoft\v1\User())->getDetails(
                $api_user_session['session_token'],
                $api_user_session['sub_id']
            );

            $user->api = $user->api()->create($api_user_data);
            $user->api->user_id_api = $api_user_data['id'];
        }

        $user->api->session_token = $api_user_session['session_token'];
        $user->api->balance = $credit['balance'];
        $user->api->push();

        return $user;
    }
}