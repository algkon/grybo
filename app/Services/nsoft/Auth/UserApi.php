<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-27
 * Time: 17:34
 */

namespace SimplePack\Nsoft\Auth;


use Illuminate\Database\Eloquent\Model;

class UserApi extends Model
{
    protected $table = 'users_api';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "user_id",
        "email",
        "first_name",
        "last_name",
        "member_card_number",
        "leisure_card_number",
        "gender",
        "national_id",
        "phone",
        "mobile_phone",
        "email",
        "date_of_birth",
        "addr_country_code",
        "addr_town",
        "addr_street",
        "addr_house",
        "addr_flat",
        "addr_post_code",
        "balance",
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'activation_code', 'session_token',
    ];

    /**
     * @param $value
     * @return null
     */
//    public function getAddrTownAttribute($value)
//    {
//        dd($value == 0, $value);
//        return $value == 0 ? null : $value ;
//    }

    /**
     * @param $value
     * @return string
     */
    public function getBalanceFormatedAttribute($value)
    {
        return number_format($this->balance, 2, ',', '');
    }

    public function getGenderLabelAttribute()
    {
        return $this->gender == 1 ? 'Vyras' : 'Moteris';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }
}