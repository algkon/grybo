<?php
/**
 * Created by PhpStorm.
 * User: polki
 * Date: 2016-08-31
 * Time: 15:02
 */

namespace SimplePack\Nsoft\v2;


class Products extends Api
{
    /**
     * @return array|null
     */
    public function memberships()
    {
        $response = $this->call('memberships', 'GET');

        return !$response->fails() ? $response->getData()->all() : null;
    }
}