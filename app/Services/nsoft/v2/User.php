<?php
/**
 * Created by PhpStorm.
 * User: Povilas
 * Date: 2016-07-14
 * Time: 05:37
 */

namespace SimplePack\Nsoft\v2;

class User extends Api
{
    /**
     * @param $username
     * @param $password
     * @return $this
     */
    public function login($username, $password)
    {
        return $this->call("login", "POST", [
            'username' => $username,
            'password' => $password,
        ]);
    }

    /**
     * REQUIRED: (string) login, (string) password, (int) vart_tipas, (string) vardas, (string) pavarde,
     *           (string) kodas, (string) pvm_kodas, (string) el_pastas, (string) tel_num, (string) faksas,
     *           (string) salis, (string) miestas, (string) gatve, (string) namas, (string) butas, (string) pasto_kodas
     *
     * @param array $data
     */
    public function register(array $data)
    {
        // Default values
        $data['vart_tipas'] = 1;
        $data['login'] = $data['el_pastas'];
        $data['salis'] = "LT";

        // Optional data
        $optional = ['tel_num', 'miestas', 'gatve', 'namas', 'butas', 'pasto_kodas', 'pvm_kodas', 'faksas'];

        // Update optional data
        foreach ($optional as $item) {
            $data[$item] = isset($data[$item]) && strlen(trim($data[$item])) > 0 ? $data[$item] : "NULL";
        }
//        dd($this->call("register_user", "POST", $data));

        return $this->call("register_user", "POST", $data);
    }

    /**
     * @param $confirmation_code
     * @param null $new_password
     */
    public function activate(array $data)
    {
        return $this->call("activate_user", "POST", $data);
    }

    /**
     * @param $session_token
     * @param null $from
     * @param null $to
     * @return $this
     */
    public function get_sales_report($session_token, $from = null, $to = null)
    {
        $from = is_null($from) ? '2010-01-01' : $from ;
        $to = is_null($to) ? date('Y-m-d') : $to ;

        $response = $this->call('users/' . $session_token . '/sales', 'GET', [
            'from' => $from,
            'to' => date('Y-m-d', strtotime($to . '+ 10 years')),
        ]);

        return !$response->fails() ? $response->getData()->all() : null;
    }
}