<?php
/**
 * Created by PhpStorm.
 * User: Povilas
 * Date: 2016-07-13
 * Time: 16:13
 */

namespace SimplePack\Nsoft\v2;

use SimplePack\Nsoft\Api as ApiBuilder;

class Api extends ApiBuilder
{
    protected $api_base_url = "https://ws.grybo7.lt/api/v2/";
}