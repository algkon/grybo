<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevi�ius
 * Date: 2016-09-14
 * Time: 14:09
 */

namespace SimplePack\Nsoft\v2;


class Cart extends Api
{
    protected $id;

    protected $cart;

    /**
     * @return $this|null
     */
    public function create()
    {
        $response = $this->call('carts', 'POST');

        if ($response->fails()) {
            return null;
        }

        // get data from response
        $this->cart = $response->getData()->all();

        // set cart id
        $this->id = isset($this->cart['cart']['reservationCode']) ? $this->cart['cart']['reservationCode'] : null ;

        // return cart info
        return $this;
    }

    /**
     * @param $id
     */
    public function add_item($id)
    {
        if (is_null($this->id)) {
            return null;
        }

        $response = $this->call(sprintf('carts/%s/items/%s', $this->id, $id), 'POST');

        return !$response->fails() ? $response->getData()->all() : null;
    }

    /**
     * Add cash to account balance
     *
     * REQUIRED: (integer) subject, (float) amount
     *
     * @param $user_id
     * @param $amount
     */
    public function add_cash($user_id, $amount)
    {
        if (is_null($this->id)) {
            return null;
        }

        $response = $this->call(sprintf('carts/%s/account_cash', $this->id), 'POST', [
            'subject' => intval($user_id),
            'amount' => floatval($amount),
        ]);

        return !$response->fails() ? $response->getData()->all() : null;
    }

    /**
     * @param $discount_code
     * @return bool|null
     */
    public function add_discount($discount_code)
    {
        if (is_null($this->id)) {
            return null;
        }

        $response = $this->call(sprintf('carts/%s/discounts', $this->id), 'POST', [
            'discountCode' => $discount_code,
        ]);

        return !$response->fails() ? true : false;
    }

    /**
     * @param $id
     * @return $this
     */
    public function carts_state($id)
    {
        return $this->call(sprintf('carts/%s', $id), 'GET');
    }

    /**
     * @return mixed|null
     */
    public function items()
    {
        if (is_null($this->id)) {
            return null;
        }

        $response = $this->call(sprintf('carts/%s/items', $this->id), 'GET');

        return !$response->fails() ? $response->getData()->all() : null;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
}