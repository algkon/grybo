<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-14
 * Time: 14:24
 */

namespace SimplePack\Nsoft\v2;


class Payment extends Api
{
    /**
     * Make payment as not logged in user
     *
     * REQUIRED: (string) name, (string) surname, (string) phone, (string) email, (string) payment_method
     * OPTIONAL: (string) company_name, (string) company_code, (string) company_vat, (string) company_address,
     *           (string) country, (bool) invoice, (bool) email_ads, (bool) create_user
     *
     * @param $reservation_code
     * @param array $data
     * @param string $payment_method
     * @return $this
     */
    public function cart_order_anon($reservation_code, $data, $payment_method = 'paysera')
    {
        return $this->call('carts/' . $reservation_code . '/order', 'POST', [
            'name' => $data['first_name'],
            'surname' => $data['last_name'],
            'phone' => $data['phone'],
            'email' => $data['email'],
            'payment_method' => $payment_method,
        ]);
    }

    /**
     * Make payment as logged in user with custom payment processor
     *
     * REQUIRED: (string) session_token, (string) payment_method
     *
     * @param $reservation_code
     * @param $session_token
     * @param string $payment_method
     * @return $this
     */
    public function user_order_user($reservation_code, $session_token, $payment_method = 'paysera')
    {
        return $this->call('carts/' . $reservation_code . '/user_order', 'POST', [
            'session_token' => $session_token,
            'payment_method' => $payment_method,
        ]);
    }

    /**
     * Make payment as logged in user from account balance.
     *
     * REQUIRED: (string) session_token
     *
     * @param $reservation_code
     * @param $session_token
     * @return $this
     */
    public function user_account_order($reservation_code, $session_token)
    {
        return $this->call('carts/' . $reservation_code . '/user_account_order', 'POST', [
            'session_token' => $session_token,
        ]);
    }

    /**
     * Payment trigger
     *
     * @param $reservation_code
     * @param $session_token
     * @param array $data
     * @param $payment_method
     * @param bool|false $annon
     * @return Payment
     */
    public function order($reservation_code, $session_token, array $data, $payment_method, $annon = false)
    {
        if ($annon) {
            return $this->cart_order_anon($reservation_code, $data, $payment_method);
        }

        switch ($payment_method) {
            case 'account':
                return $this->user_account_order($reservation_code, $session_token);
                break;
            default:
                return $this->user_order_user($reservation_code, $session_token, $payment_method);
                break;
        }
    }
}