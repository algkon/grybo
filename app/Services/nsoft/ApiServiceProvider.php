<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-14
 * Time: 16:02
 */

namespace SimplePack\Nsoft;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;
use SimplePack\Nsoft\Auth\ApiUserProvider;
use SimplePack\Nsoft\Auth\NsoftSessionGuard;
use SimplePack\Nsoft\Auth\NsoftUserProvider;
use SimplePack\Nsoft\Auth\User;
use SimplePack\Nsoft\v1\Credit;

class ApiServiceProvider extends ServiceProvider
{
    /**
     * @param Request $request
     */
    public function boot(Request $request)
    {
        Auth::provider('eloquent_api_combined', function ($app, array $config) {
            // Return an instance of Illuminate\Contracts\Auth\UserProvider...

            return new ApiUserProvider($app['hash'], $config['model']);
        });
    }
}