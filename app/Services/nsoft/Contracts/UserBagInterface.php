<?php
/**
 * Created by PhpStorm.
 * User: Povilas
 * Date: 2016-07-14
 * Time: 06:00
 */

namespace SimplePack\Nsoft\Contracts;


interface UserBagInterface
{
    /**
     * @param $session_token
     * @param $id
     * @return mixed
     */
    public function getDetails($session_token, $id);

    /**
     * @param $session_token
     * @param $data
     * @return mixed
     */
    public function setDetails($session_token, $id, $email, array $data);

    /**
     * @param $email
     * @return mixed
     */
    public function forgotPassword($email);
}