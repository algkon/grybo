<?php
/**
 * Created by PhpStorm.
 * User: Povilas
 * Date: 2016-07-14
 * Time: 05:47
 */

namespace SimplePack\Nsoft\Contracts;


interface UserAuthFactoryInterface
{
    /**
     * @param $username
     * @param $password
     * @return mixed
     */
    public function login($username, $password);

    /**
     * @param array $data
     * @return mixed
     */
    public function register(array $data);

    /**
     * @param $session_token
     * @return mixed
     */
    public function logout($session_token);

    /**
     * @param $confirmation_code
     * @param null $new_password
     * @return mixed
     */
    public function activate(array $data);

    /**
     * @param $email
     * @return mixed
     */
    public function forgotPassword($email);

    /**
     * @return mixed
     */
    public function fails();

    /**
     * @return MessageBag $bag
     */
    public function errors();
}