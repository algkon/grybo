<?php

namespace SimplePack\Nsoft\Requests\Account;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'api.first_name' => 'required|max:255',
            'api.last_name' => 'required|max:255',
            'api.phone' => 'numeric',
            'api.addr_house' => 'numeric',
            'api.addr_flat' => 'numeric',
        ];
    }
}
