<?php

namespace App\Models;

use Admin\Extensions\Auth\Access\Accessible;
use Admin\Models\EmploymentUser;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use SimplePack\Nsoft\Auth\UserApi;

class User extends Authenticatable
{
    use Notifiable, Accessible;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function api()
    {
        return $this->hasOne(UserApi::class);
    }

    /**
     * @return mixed
     */
    public function employments()
    {
        return $this->hasMany(EmploymentUser::class)->orderBy('employment_date', 'desc');
    }

    /**
     * @return mixed
     */
    public function activeEmployments()
    {
        return $this->hasMany(EmploymentUser::class)->orderBy('employment_date', 'desc')->where('status', "1");
    }
}
