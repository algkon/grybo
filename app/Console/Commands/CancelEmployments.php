<?php

namespace App\Console\Commands;

use Admin\Models\EmploymentUser;
use App\Mail\Employments\Canceled;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class CancelEmployments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'employments:cancel';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cancel employments if they do not have enough registered members.';

    /**
     * @var DatabaseManager
     */
    protected $db;

    /**
     * @var EmploymentUser
     */
    protected $employment_user;

    /**
     * @param EmploymentUser $employment_user
     * @param DatabaseManager $db
     */
    public function __construct(EmploymentUser $employment_user, DatabaseManager $db)
    {
        $this->db = $db;
        $this->employment_user = $employment_user;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $employments_need_to_cancel_ids = $this->employmentsNeedToCancel()->pluck('id')->toArray();
        $registrations_need_to_cancel = $this->usersNeedToWarn($employments_need_to_cancel_ids);

        foreach ($registrations_need_to_cancel as $registration) {
            Mail::to($registration->email)->queue(new Canceled($registration, trans('messages.employment.cancel')));
        }

        $this->employmentsCancel($employments_need_to_cancel_ids);
        $this->usersRegistrationCancel($registrations_need_to_cancel->pluck('id')->toArray());
    }

    /**
     * @return mixed
     */
    private function employmentsNeedToCancel()
    {
        $cancel_time = Carbon::now()->addHours(data_get(settings('custom_settings'), 'front.employments.cancel_time', 24));

        return $this->db->query()
            ->selectRaw('employments_calendar.*')
            ->from(DB::raw("(
                SELECT employment_calendar_id, COUNT(id) AS total
                FROM employment_user
                WHERE status IN('1','2')
                GROUP BY employment_calendar_id) AS registered_users
            "))
            ->rightJoin('employments_calendar', 'employments_calendar.id', '=' , 'registered_users.employment_calendar_id')
            ->where('active', '1')
            ->whereRaw('(registered_users.total < employments_calendar.people_min || ISNULL(registered_users.total))')
            ->whereRaw("concat(date(employments_calendar.starting_date), ' ', time(employments_calendar.starting_time)) <= ?", [$cancel_time])
            ->get();
    }

    /**
     * @param array $employments
     * @return mixed
     */
    private function employmentsCancel(array $employments)
    {
        return $this->db->table('employments_calendar')
            ->whereIn('id', $employments)
            ->update(['active' => '0']);
    }

    /**
     * @param array $employments
     * @return mixed
     */
    private function usersNeedToWarn(array $employments)
    {
        return $this->employment_user
            ->whereIn('employment_calendar_id', $employments)
            ->whereIn('status', ['1', '2'])
            ->get();
    }

    /**
     * @param array $registrations
     * @return mixed
     */
    private function usersRegistrationCancel(array $registrations)
    {
        return $this->employment_user
            ->whereIn('id', $registrations)
            ->update(['status' => '0']);
    }
}
