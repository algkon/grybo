<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\DatabaseManager;

class DeleteLongTimePendingOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'orders:delete-pending';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete all long time pending orders.';

    /**
     * @var DatabaseManager
     */
    protected $db;

    /**
     * @param DatabaseManager $db
     */
    public function __construct(DatabaseManager $db)
    {
        $this->db = $db;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $delete_time = Carbon::now()->subHours(48);

        return $this->db->table('orders')
            ->where('status', '0')
            ->where("created_at", '<=', $delete_time)
            ->delete();
    }
}
