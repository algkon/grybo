<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\DatabaseManager;

class DeleteEndedEmployments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'employments:delete-ended';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete ended employments.';

    /**
     * @var DatabaseManager
     */
    protected $db;

    /**
     * @param DatabaseManager $db
     */
    public function __construct(DatabaseManager $db)
    {
        $this->db = $db;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $delete_time = Carbon::now()->subHours(48);

        return $this->db->table('employments_calendar')
            ->where('active', '0')
            ->whereRaw("concat(date(employments_calendar.starting_date), ' ', time(employments_calendar.starting_time)) <= ?", [$delete_time])
            ->delete();
    }
}
