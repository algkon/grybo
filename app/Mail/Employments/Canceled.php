<?php

namespace App\Mail\Employments;

use Admin\Models\EmploymentUser;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Canceled extends Mailable
{
    use Queueable, SerializesModels;


    /**
     * @var null|string
     */
    public $reason;

    /**
     * @var EmploymentUser
     */
    public $employment_user;

    /**
     * Create a new message instance.
     *
     * @param EmploymentUser $employment_user
     * @param null $message
     */
    public function __construct(EmploymentUser $employment_user, $reason = null)
    {
        $this->employment_user = $employment_user;
        $this->reason = !is_null($reason) ? $reason : 'nesusidarė reikiamas narių skaičius' ;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Pranešimas apie atšauktą užsiėmimą')->view('mail.employments.canceled');
    }
}
