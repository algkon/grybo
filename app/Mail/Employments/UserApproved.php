<?php

namespace App\Mail\Employments;

use Admin\Models\EmploymentUser;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserApproved extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var EmploymentUser
     */
    public $employment_user;

    /**
     * Create a new message instance.
     *
     * @param EmploymentUser $employment_user
     */
    public function __construct(EmploymentUser $employment_user)
    {
        $this->employment_user = $employment_user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Registracija į užsiėmimą patvirtinta')->view('mail.employments.user-approved');
    }
}
