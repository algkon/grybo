<?php

namespace App\Http\Middleware;

use Admin\Repositories\EmploymentCalendarRepository;
use Closure;

class EmploymentDateExists
{
    /**
     * @var EmploymentCalendarRepository
     */
    private $calendar;

    /**
     * EmploymentDateExists constructor.
     * @param EmploymentCalendarRepository $calendar
     */
    public function __construct(EmploymentCalendarRepository $calendar)
    {
        $this->calendar = $calendar;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $date = $this->calendar->find($request->route('id'));

        if (is_null($date)) {
            return redirect()->route('employments');
        }

        return $next($request);
    }
}
