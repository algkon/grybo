<?php

namespace App\Http\Controllers;

use Admin\Repositories\SubscriberRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Requests\SubscribeRequest;

use App\Http\Requests;

class SubscribersController extends Controller
{
    /**
     * @var SubscriberRepository
     */
    private $subscriber;

    /**
     * SubscribersController constructor.
     * @param SubscriberRepository $subscriber
     */
    public function __construct(SubscriberRepository $subscriber)
    {
        $this->subscriber = $subscriber;
    }

    /**
     * @param SubscribeRequest $request
     * @return mixed
     */
    public function subscribe(SubscribeRequest $request)
    {
        $this->subscriber->subscribe($request->get('email'));

        if ($request->expectsJson()) {
            return new JsonResponse([
                'reload' => false,
                'message' => 'Sveikiname sėkmingai užsiprenumeravus mūsų naujienlaiškį!'
            ]);
        }

        return redirect()->back() //$request->session()->previousUrl() . '#newsletter'
            ->withSuccess('Sveikiname sėkmingai užsiprenumeravus mūsų naujienlaiškį!');
    }

    /**
     * @param $unsubscribe_token
     * @return mixed
     */
    public function unsubscribe($unsubscribe_token)
    {
        $this->subscriber->unsubscribe($unsubscribe_token);

        return redirect()->home()
            ->withSuccess('Prenumerata sėkmingai nutraukta.');
    }
}
