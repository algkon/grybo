<?php

namespace App\Http\Controllers;

use Admin\Repositories\PostRepository;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PostRepository $post)
    {
        return view('home', [
            'posts_services' => $post->getPostsByIds([16, 17, 18]),
            'posts_first_column' => $post->getLastsPostsByCategoryId(3, 2),
            'posts_last_column' => $post->getLastsPostsByCategoryId(4, 2),
        ]);
    }
}
