<?php

namespace App\Http\Controllers;

use Admin\Repositories\CategoryRepositoy;
use Admin\Repositories\PostRepository;
use Illuminate\Http\Request;

use App\Http\Requests;

class CategoriesController extends Controller
{
    /**
     * @var CategoryRepositoy
     */
    private $category;

    /**
     * CategoriesController constructor.
     * @param CategoryRepositoy $category
     */
    public function __construct(CategoryRepositoy $category)
    {
        $this->category = $category;
    }

    /**
     * @param $category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($category)
    {
        return view('category', [
            'model' => $category,
            'posts' => $category->publishedPosts()->paginate(6)
        ]);
    }
}
