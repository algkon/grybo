<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class PostsController extends Controller
{
    /**
     * @param $post
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($post)
    {
        return view($this->getLayout("post", $post->layout_name), [
            'model' => $post
        ]);
    }
}
