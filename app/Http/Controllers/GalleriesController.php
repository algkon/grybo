<?php

namespace App\Http\Controllers;

use Admin\Repositories\GalleryRepository;
use Illuminate\Http\Request;

use App\Http\Requests;

class GalleriesController extends Controller
{
    /**
     * @var GalleryRepository
     */
    private $gallery;

    /**
     * GalleriesController constructor.
     * @param GalleryRepository $gallery
     */
    public function __construct(GalleryRepository $gallery)
    {
        $this->gallery = $gallery;
    }

    /**
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($slug)
    {
        $gallery = $this->gallery->active()->findBy('slug', $slug);

        return view('gallery', compact('gallery'));
    }
}
