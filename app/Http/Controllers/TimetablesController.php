<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-10-24
 * Time: 01:24
 */

namespace App\Http\Controllers;


use Admin\Repositories\TimetableCalendarRepository;
use Admin\Repositories\TimetableRepository;

class TimetablesController extends Controller
{
    /**
     * @var TimetableRepository
     */
    private $timetable;

    /**
     * GalleriesController constructor.
     * @param TimetableRepository $timetable
     */
    public function __construct(TimetableRepository $timetable)
    {
        $this->timetable = $timetable;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $timetables = $this->timetable->active()->paginateWithRelationships(6, ['*'], 'calendar');

        return view('timetables.index', compact('timetables'));
    }

    /**
     * @param TimetableCalendarRepository $calendar
     * @param $slug
     * @param null $start
     * @param null $end
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(TimetableCalendarRepository $calendar, $slug, $start = null, $end = null)
    {
        $timetable = $this->timetable->active()->findBy('slug', $slug);

        return view('timetables.show', [
            'timetable' => $timetable,
            'calendar' => $calendar->handsonTableData($timetable->id, $start, $end),
        ]);
    }
}