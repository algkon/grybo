<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Collection;
use SimplePack\Nsoft\Requests\Account\BuyCreditRequest;
use SimplePack\Nsoft\Requests\Account\ChangePasswordRequest;
use SimplePack\Nsoft\Requests\Account\UpdateProfileRequest;
use SimplePack\Nsoft\v2\Cart;
use SimplePack\Nsoft\v2\Payment;

class AccountController extends Controller
{
    /**
     * @var \App\Models\User
     */
    private $user;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function profile()
    {
        return view('account.profile.show');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showEditForm()
    {
        $model = $this->guard()->user();

        return view('account.profile.edit', compact('model'));
    }

    /**
     * @param UpdateProfileRequest $request
     * @return $this
     */
    public function edit(UpdateProfileRequest $request)
    {
        $user_data = $request->except('api.addr_country_code', 'api.email', 'api.national_id');

        $response = (new \SimplePack\Nsoft\v1\User)->setDetails(
            $this->guard()->user()->api->session_token,
            $this->guard()->user()->api->user_id_api,
            $this->guard()->user()->api->email,
            $user_data['api']
        );

        if ($response->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($response->errors());
        }

        $this->guard()->user()->name = $user_data['api']['first_name'] . ' ' . $user_data['api']['last_name'];
        $this->guard()->user()->api->fill($user_data['api']);
        $this->guard()->user()->push();

        // redirect back to  user profile and show success message
        return redirect()->route('account.profile')->withSuccess('Pakeitimai sėkmingai išsaugoti!');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showPasswordForm()
    {
        return view('account.password.change');
    }

    /**
     * @param ChangePasswordRequest $request
     * @return $this
     */
    public function changePassword(ChangePasswordRequest $request)
    {
        $response = (new \SimplePack\Nsoft\v1\User)->changePassword(
            $this->guard()->user()->api->session_token,
            $request->all()
        );

        if ($response->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($response->errors());
        }

        $this->guard()->user()->password = bcrypt($request->get('new_password'));
        $this->guard()->user()->save();

        // redirect back to user profile and show success message
        return redirect()->route('account.profile')->withSuccess('Pakeitimai sėkmingai išsaugoti!');
    }

    /**
     * @param \SimplePack\Nsoft\v2\User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function orders(\SimplePack\Nsoft\v2\User $user)
    {
        $orders = new Collection($user->get_sales_report($this->guard()->user()->api->session_token));
        $orders = $orders->sortByDesc('pard_laikas');

        return view('account.orders.index', compact('orders'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function employments()
    {
        $employments = $this->guard()->user()->employments;

        return view('account.employments.index', compact('employments'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showCreditForm()
    {
        return view('account.credit.form');
    }

    /**
     * @param BuyCreditRequest $request
     * @param Cart $cart
     * @param Payment $payment
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function buyCredit(BuyCreditRequest $request, Cart $cart, Payment $payment)
    {        
        if (is_null($new_cart = $cart->create())) {
            return redirect()->back()->withInput($request->all())->withErrors([
                'cart_error' => 'Apmokėjimas negalimas. Prašome pabandyti vėliau.'
            ]);
        }
    
        // add cache
        $new_cart->add_cash($this->guard()->user()->api->user_id_api, $request->get('amount'));

        // pay
        $response = $payment->user_order_user($new_cart->getId(), $this->guard()->user()->api->session_token);

        if ($response->fails()) {
            return redirect()->route('cart')->withErrors($response->errors());
        }

        // get response data
        $response_data = $response->all();

        // redirect back to user profile and show success message
        return redirect()->away($response_data['url']);
    }
}
