<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Cache;
use SimplePack\Nsoft\v2\Products;

class MembershipsController extends Controller
{
    /**
     * @param Products $products
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showMembershipsForm(Products $products)
    {
        $grouped_products = Cache::remember('nsoft_products', 15, function () use ($products) {
            $collection = collect($products->memberships());

            return $collection->keyBy('product_id')->groupBy('product_category_id');
        });

        return view('memberships.index', compact('grouped_products'));
    }
}
