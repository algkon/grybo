<?php

namespace App\Http\Controllers;

use Admin\Models\Order;
use Admin\Repositories\OrderRepository;
use App\Mail\Order\OrderComplete;
use App\Mail\Wallet\WalletEmpty;
use App\Mail\Wallet\WalletUpdated;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Mail;
use SimplePack\Nsoft\Requests\Payment\PaymentSubmitRequest;
use SimplePack\Nsoft\v1\Credit;
use SimplePack\Nsoft\v2\Cart;
use SimplePack\Nsoft\v2\Payment;
use Gloudemans\Shoppingcart\Facades\Cart as ShoppingCart;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PaymentController extends Controller
{
    /**
     * @var OrderRepository
     */
    private $order;

    /**
     * PaymentController constructor.
     * @param OrderRepository $order
     */
    public function __construct(OrderRepository $order)
    {
        $this->order = $order;
    }

    /**
     * @param PaymentSubmitRequest $request
     * @param Cart $cart
     * @param Payment $payment
     * @return $this|PaymentController|\Illuminate\Http\RedirectResponse
     */
    public function pay(Request $request, Payment $payment, $order_id)
    {
        $order = $this->order->getByOrderId($order_id);

        if (!$order->available) {
            return redirect()->back();
        }

        // Send order request to nPoint API
        $response = $payment->order(
            $order->order_id,
            ($this->guard()->check()) ? $this->guard()->user()->api->session_token : null,
            [
                'first_name' => $order->first_name,
                'last_name' => $order->last_name,
                'email' => $order->email,
                'phone' => $order->phone,
            ],
            $order->payment_method,
            $this->guard()->guest()
        );

        return $this->beforeRedirect($order, $response, $request);
    }

    /**
     * @param $order
     * @param $response
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    private function beforeRedirect($order, $response, $request)
    {
        // get response data form nPoint API
        $response_data = $response->all();

        if ($response->fails()) {

            if ($response_data['code'] == 98385 && $order->price == 0) {
                return $this->payed($order);
            }

            return redirect()->route('order.show', $order->order_id)->withErrors($response->errors());
        }

        // if selected "form balance"
        if ($order->payment_method == 'account' && $response_data === true) {
            $user = $this->guard()->user();
            $user->api->balance -= $order->price;
            $user->api->save();

            if ($user->api->balance < 1) {
                Mail::to($user->email)->queue(new WalletEmpty($user));
            }

            return $this->payed($order);
        }

        // if selected another payment method
        if (isset($response_data['url'])) {
            return redirect()->away($response_data['url']);
        }

        // redirect back to cart and show error message
        return redirect()->route('cart')->withErrors([
            'cart_error' => 'Apmokėjimas negalimas. Prašome pabandyti vėliau.'
        ]);
    }

    /**
     * @param Cart $cart
     * @param $order_id
     * @return mixed
     */
    public function success(Cart $cart, Request $request)
    {
        $params = [];
        parse_str(base64_decode(strtr($request->get('data'), array('-' => '+', '_' => '/'))), $params);
        $order_id = $params['orderid'];

        $response = $cart->carts_state($order_id);

        if ($response->fails()) {
            throw new NotFoundHttpException();
        }

        return $this->makeSuccess($order_id, $response->getData()->all()) ;
    }

    /**
     * @return $this
     */
    public function cancel()
    {
        return view('payments.canceled');
    }

    /**
     * @param $order_id
     * @param $cart
     * @return mixed
     */
    protected function makeSuccess($order_id, $cart)
    {
        if ($this->paymentType($cart) == 'order') {
            $order = $this->order->getByOrderId($order_id);

            if ($order->status != 1 && $cart['status']['status'] == 5) {
                return $this->payed($order);
            }

            if ($order->status != 1 && $cart['status']['status'] != 5) {
                return redirect()->route('order.show', $order->order_id)->withSuccess('Užsakymas sėkmingai gautas. Jis bus patvirtintas iš karto po gauto apmokėjimo.');
            }

            if ($order->status == 1) {
                return redirect()->route('order.show', $order->order_id);
            }
        }

        if ($this->paymentType($cart) == 'balance') {

            if ($cart['status']['status'] == 5) {
                $user   = $this->guard()->user();
                $credit = (new Credit())->get($user->api->session_token);

                $user->api->balance = $credit['balance'];
                $user->api->save();

                Mail::to($user->email)->queue(new WalletUpdated($user));

                return redirect()->route('account.credit')->withSuccess('Jūsų sąskaita sėkmingai papildyta.');
            }

            return redirect()->route('account.credit')->withSuccess('Jūsų sąskaita bus sėkmingai papildyta iš karto po gauto apmokėjimo.');
        }

        return redirect()->home();
    }

    /**
     * @param $order_id
     * @param $cart
     * @return $this
     */
//    protected function makeCancel($order_id, $cart)
//    {
//        if ($this->paymentType($cart) == 'order') {
//            return redirect()->route('order.show', $order_id)->withErrors(['payment_error' => 'Užsakymas atšauktas.']);
//        }
//
//        if ($this->paymentType($cart) == 'balance') {
//            return redirect()->route('account.credit')->withErrors(['payment_error' => 'Sąskaitos pildymas atšauktas.']);
//        }
//
//        throw new NotFoundHttpException();
//    }

    /**
     * @param $cart
     * @return bool|string
     */
    protected function paymentType($cart)
    {
        if (!empty($cart['inside']['items'])) {
            return 'order';
        }

        if (!empty($cart['inside']['account_cash'])) {
            return 'balance';
        }

        return false;
    }

    /**
     * @param Order $order
     * @return mixed
     */
    protected function payed(Order $order)
    {
        $this->order->setAsPayed($order->order_id);

        Mail::to($order->email)->queue(new OrderComplete($order));

        return redirect()->route('order.show', $order->order_id)->withSuccess('Apmokėjimas sėkmingai atliktas.');
    }
}
