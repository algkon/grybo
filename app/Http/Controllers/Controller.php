<?php

namespace App\Http\Controllers;

use Admin\Extensions\Layout\SetLayout;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, SetLayout;

    /**
     * @return mixed
     */
    protected function guard()
    {
        return Auth::guard();
    }
}
