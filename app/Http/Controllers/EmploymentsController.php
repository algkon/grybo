<?php

namespace App\Http\Controllers;

use Admin\Repositories\EmploymentCalendarRepository;
use App\Mail\Employments\UserRegistered;
use Illuminate\Http\Request;
use App\Http\Requests\EmploymentRegisterRequest;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class EmploymentsController extends Controller
{
    /**
     * @var EmploymentCalendarRepository
     */
    private $calendar;

    /**
     * EmploymentsController constructor.
     * @param EmploymentCalendarRepository $calendar
     */
    public function __construct(EmploymentCalendarRepository $calendar)
    {
        $this->middleware('employment.date.exists', ['only' => 'showRegisterForm']);

        $this->calendar = $calendar;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showCalendar(Request $request, $year = '', $month = '', $day = '')
    {
        $year = $year ?: date('Y');
        $month = $month ?: date('m');
        $day = $day ?: date('d');

        $events_in_month = $this->calendar->eventsByDate($year, $month);
        $events_in_day   = $events_in_month->where('day', $day);

        \Calendar::initialize([
            'start_day' => 'monday',
            'month_type' => 'long',
            'day_type' => 'long',
            'show_next_prev' => true,
            'segments' => true,
            'template' => view('parts.calendar')
        ]);

        // Generate a calendar for the current month and year
        $calendar = \Calendar::generate($year, $month, $events_in_month->pluck('route_with_scroll', 'day')->all());

        return view('employments.index', compact('events_in_day', 'calendar'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showRegisterForm()
    {
        return $this->guard()->check() ? view('employments.form-user') : view('employments.form');
    }

    /**
     * @param EmploymentRegisterRequest $request
     * @param $id
     * @return mixed
     */
    public function register(EmploymentRegisterRequest $request, $id)
    {
        $date = $this->calendar->find($id);

        if (! $date->available()) {
            return redirect()->route('employments');
        }

        $user = $this->currentUserData() ? $this->currentUserData() : $request->all() ;

        $employment_user = $date->users()->create([
            'user_id' => $this->guard()->check() ? $this->guard()->id() : null ,
            'employment_id' => $date->employment->id,
            'employment_name' => $date->employment->title,
            'employment_date' => $date->starting_date_time,
            'employment_duration' => $date->duration,
            'coach' => $date->employment->coach->name,
            'first_name' => $user['first_name'],
            'last_name' => $user['last_name'],
            'phone' => $request->get('phone'),
            'email' => $user['email'],
        ]);

        Mail::to($employment_user->email)->queue(new UserRegistered($employment_user));

        return redirect()->route('employments')->withSuccess('Jūsų užklausa sėkmingai pateikta! Apie registracijos patvirtinimą pranešime Jūsų nurodytais kontaktais.');
    }

    /**
     * @return bool
     */
    public function currentUserData()
    {
        if ($this->guard()->guest()) {
            return false;
        }

        return $this->guard()->user()->api->getAttributes();
    }
}
