<?php

namespace App\Http\Controllers;

use Admin\Repositories\OrderRepository;
use App\Mail\Order\OrderCreated;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Mail;
use SimplePack\Nsoft\Requests\Order\UserDetailsSubmitRequest;
use SimplePack\Nsoft\Requests\Order\SubmitRequest;
use SimplePack\Nsoft\v2\Cart;
use SimplePack\Nsoft\v2\Payment;
use Gloudemans\Shoppingcart\Facades\Cart as ShoppingCart;

class OrdersController extends Controller
{
    /**
     * @var OrderRepository
     */
    private $order;

    /**
     * OrdersController constructor.
     * @param OrderRepository $order
     */
    public function __construct(OrderRepository $order)
    {
        $this->middleware('cart.empty', ['only' => 'showUserDetailsForm']);

        $this->order = $order;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function showUserDetailsForm()
    {
        return $this->guard()->check() ? view('orders.form-user') : view('orders.form');
    }

    /**
     * @param UserDetailsSubmitRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function userDetailsFormSubmit(UserDetailsSubmitRequest $request, Cart $cart, $id = null)
    {
        if (is_null($new_cart = $cart->create())) {
            return redirect()
                ->route('cart')
                ->withErrors(['cart_error' => 'Užsakymas negalimas. Prašome pabandyti vėliau.']);
        }

        foreach (ShoppingCart::content() as $item) {
            $new_cart->add_item($item->id);
        }

        $discount_code = $this->setDiscount($request, $new_cart); //member_card_number, leisure_card_number
        $order         = $this->makeOrder($request, $new_cart, $discount_code);

        Mail::to($order->email)->queue(new OrderCreated($order));

        return redirect()->route('order.show', $order->order_id);
    }

    /**
     * @param $request
     * @param $cart
     * @return string
     */
    protected function setDiscount($request, $cart)
    {
        $discount_codes = [
            $request->get('member_card_number'),
            $request->get('leisure_card_number'),
        ];

        foreach ($discount_codes as $discount_code) {
            if (!is_null($discount_code) && $cart->add_discount($discount_code)) {
                $this->updateCartPrice($cart);

                return $discount_code;
            }
        }

        return '';
    }

    /**
     * @param $request
     * @param null $cart
     * @param $discount_code
     * @return mixed
     */
    protected function makeOrder($request, $cart = null, $discount_code)
    {
        // prepare customer data
        $customer = $request->only('first_name', 'last_name', 'email', 'phone');
        $customer['ip'] = $request->ip();

        $order = $this->order->make(
            $cart->getId(),
            ShoppingCart::content(),
            ShoppingCart::total(),
            $discount_code,
            '', //$request->get('payment_method')
            $customer,
            $this->guard()->check() ? $this->guard()->id() : null
        );

        // destroy cart
        ShoppingCart::destroy();

        // Insert order in to DB
        return $order;
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($order_id)
    {
        $order = $this->order->getByOrderId($order_id);

        return view('orders.show', compact('order'));
    }

    /**
     * @param SubmitRequest $request
     * @param null $order_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function submit(SubmitRequest $request, $order_id = null)
    {
        $this->order->update(['payment_method' => $request->get('payment_method')], $order_id, 'order_id');

        return redirect()->route('payment.submit', $order_id);
    }

    /**
     * @param $cart
     */
    protected function updateCartPrice($cart)
    {
        foreach ($cart->items() as $item) {

            ShoppingCart::search(function ($cartItem, $rowId) use ($item) {

                if ($cartItem->id === $item['product_id']) {
                    $price = $item['product_price'] / (100 + config('cart.tax')) * 100;
                    $price = floatval(number_format($price, 2));

                    ShoppingCart::update($cartItem->rowId, ['price' => $price]);
                }
            });
        }
    }
}
