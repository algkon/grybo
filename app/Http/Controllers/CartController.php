<?php

namespace App\Http\Controllers;

use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Cache;
use SimplePack\Nsoft\v2\Products;

class CartController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('cart.index');
    }

    /**
     * @param Request $request
     * @param Products $products
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function add(Request $request, Products $products)
    {
        $grouped_products = Cache::remember('nsoft_products', 15, function () use ($products) {
            $collection = collect($products->memberships());

            return $collection->keyBy('product_id')->groupBy('product_category_id');
        });

        // list all nsoft products
        $product_list = $grouped_products->collapse()->keyBy('product_id');

        // if not found in list
        if (is_null($product = $product_list->get($request->get('id')))) {
            Cache::forget('nsoft_products');

            return redirect()->route('memberships')->withErrors(['product_error' => 'Atsiprašome, bet pasirinktas produktas buvo išimtas iš prekybos.']);
        }

        $exists = Cart::search(function ($cartItem, $rowId) use ($product) {
            return $cartItem->id === $product['product_id'];
        });

        if ($exists->count() === 0) {
            $price = $product['price'] / (100 + config('cart.tax')) * 100;
            $price = number_format($price, 2);
            // Add item to cart
            Cart::add($product['product_id'], $product['product_name'], 1, $price, $product);
        }

        return redirect()->route('cart');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        Cart::remove($id);

        return redirect()->back();
    }
}
