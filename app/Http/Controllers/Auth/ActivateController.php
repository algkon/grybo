<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;

use SimplePack\Nsoft\Auth\Traits\ActivationUsers;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\User;

class ActivateController extends Controller
{
    use ActivationUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'confirmation_code' => 'required',
            'email' => 'required|email|max:255',
            'new_password' => 'required|confirmed|min:6'
        ]);
    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'active' => "1",
        ]);
    }
}
