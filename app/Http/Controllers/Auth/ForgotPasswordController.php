<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use SimplePack\Nsoft\Requests\Auth\ForgotPasswordRequest;
use SimplePack\Nsoft\v1\User;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

//    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLinkRequestForm()
    {
        return view('auth.passwords.email');
    }

    /**
     * @param ForgotPasswordRequest $request
     * @param User $user
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function sendResetLinkEmail(ForgotPasswordRequest $request, User $user)
    {
        $response = $user->forgotPassword($request->get('email'));

        if ($response->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($response->errors());
        }

        return redirect()->route('auth.passwords.reset')->withSuccess('Priminimas sėkmingai išsiųstas el. paštu!');
    }
}
