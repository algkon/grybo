<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-10-10
 * Time: 02:35
 */

namespace App\Http\ViewComposers;

use Admin\Repositories\MenuRepository;
use Illuminate\Contracts\View\View;

class NavComposer
{
    /**
     * @var MenuRepository
     */
    private $menu;

    /**
     * NavComposer constructor.
     * @param MenuRepository $menu
     */
    public function __construct(MenuRepository $menu)
    {
        $this->menu = $menu;
    }

    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $navs = [];

        foreach ($this->menu->allWithRelationships('links') as $nav) {
            $navs[$nav->id] = $nav->isActive() ? $nav : null;
        }

        $view->with('nav', $navs);
    }
}