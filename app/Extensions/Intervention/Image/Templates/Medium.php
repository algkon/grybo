<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-22
 * Time: 23:25
 */

namespace App\Extensions\Intervention\Image\Templates;


use Intervention\Image\Filters\FilterInterface;
use Intervention\Image\Image;
use Intervention\Image\Templates\Medium as InterventionMedium;

class Medium extends InterventionMedium implements FilterInterface
{
    /**
     * @param Image $image
     * @return Image
     */
    public function applyFilter(Image $image)
    {
        return $image->fit(180, 180);
    }
}