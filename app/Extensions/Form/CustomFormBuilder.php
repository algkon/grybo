<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-19
 * Time: 16:38
 */

namespace App\Extensions\Form;


use Collective\Html\FormBuilder;
use Illuminate\Support\Collection;

class CustomFormBuilder extends FormBuilder
{

    /**
     * @param $name
     * @param array $list
     * @param null $selected
     * @param array $options
     * @param array $list_attributes
     * @return \Illuminate\Support\HtmlString
     */
    public function memberships($name, $list = [], $selected = null, $options = [], $list_attributes = [])
    {
        $collection = new Collection($list_attributes);
        $list_attributes = $collection->collapse()->keyBy('product_id');

        $list_attributes = $list_attributes->map(function ($item) {
            return [
                'product_id' => $item['product_id'],
                'product_name' => $item['product_name'],
                'price' => $item['price'],
            ];
        });

        return $this->select2($name, $list, $selected, $options, $list_attributes);
    }
    /**
     * Create a select box field.
     *
     * @param  string $name
     * @param  array  $list
     * @param  string $selected
     * @param  array  $options
     *
     * @return \Illuminate\Support\HtmlString
     */
    public function select2($name, $list = [], $selected = null, $options = [], $list_attributes = [])
    {
        // When building a select box the "value" attribute is really the selected one
        // so we will use that when checking the model or session for a value which
        // should provide a convenient method of re-populating the forms on post.
        $selected = $this->getValueAttribute($name, $selected);

        $options['id'] = $this->getIdAttribute($name, $options);

        if (! isset($options['name'])) {
            $options['name'] = $name;
        }

        // We will simply loop through the options and build an HTML value for each of
        // them until we have an array of HTML declarations. Then we will join them
        // all together into one single HTML element that can be put on the form.
        $html = [];

        if (isset($options['placeholder'])) {
            $html[] = $this->placeholderOption($options['placeholder'], $selected);
            unset($options['placeholder']);
        }

        foreach ($list as $value => $display) {
            $attributes = isset($list_attributes[$value]) ? $list_attributes[$value] : null ;
            $html[] = $this->getSelectOptionWithAttributes($display, $value, $selected, $attributes);
        }

        // Once we have all of this HTML, we can join this into a single element after
        // formatting the attributes into an HTML "attributes" string, then we will
        // build out a final select statement, which will contain all the values.
        $options = $this->html->attributes($options);

        $list = implode('', $html);

        return $this->toHtmlString("<select{$options}>{$list}</select>");
    }

    /**
     * Get the select option for the given value.
     *
     * @param  string $display
     * @param  string $value
     * @param  string $selected
     *
     * @return \Illuminate\Support\HtmlString
     */
    public function getSelectOptionWithAttributes($display, $value, $selected, $attributes)
    {
        if (is_array($display)) {
            return $this->optionGroup($display, $value, $selected);
        }

        return $this->optionWithAttributes($display, $value, $selected, $attributes);
    }

    /**
     * Create a select element option.
     *
     * @param  string $display
     * @param  string $value
     * @param  string $selected
     *
     * @return \Illuminate\Support\HtmlString
     */
    protected function optionWithAttributes($display, $value, $selected, $attributes = [])
    {
        $selected = $this->getSelectedValue($value, $selected);

        $options = ['value' => $value, 'selected' => $selected];

        foreach ($attributes as $name => $attribute) {
            $attributes['data-' . $name] = $attribute;
            unset($attributes[$name]);
        }

        $options = array_merge($options, $attributes);

        return $this->toHtmlString('<option' . $this->html->attributes($options) . '>' . e($display) . '</option>');
    }
}