<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-19
 * Time: 17:02
 */

namespace App\Extensions\Form;


use Illuminate\Support\Facades\Facade;

class CustomFormFacade extends Facade
{

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'custom.form';
    }
}