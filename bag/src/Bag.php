<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-07
 * Time: 23:03
 */

namespace Simplepack\Bag;


use Simplepack\Bag\Contracts\Bag as BagContract;
use Illuminate\Support\Collection;

class Bag
{
    /**
     * @var Bag
     */
    protected static $instance;

    /**
     * @var array
     */
    private $instances = [];

    /**
     * Bag constructor.
     */
    public function __construct()
    {
        self::$instance = $this;
    }

    /**
     * @return array|Collection
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new static;
        }

        return self::$instance;
    }

    /**
     * @param $bag
     * @return Collection|BagContract
     * @throws BagContractException
     */
    public function make($bag)
    {
        // check bag type
        if (is_object($bag) && !($bag instanceof BagContract)) {
            throw new BagContractException($bag, __METHOD__, __FILE__, __LINE__);
        }

        // prepare bag name
        $key = is_object($bag) ? $bag->getName() : $bag;

        // return existing bag
        if (array_key_exists($key, $this->instances)) {
            return $this->instances[$key];
        }

        // create new bag
        return $this->instances[$key] = is_object($bag) ? $bag : new Collection();
    }
}