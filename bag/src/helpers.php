<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-07
 * Time: 23:05
 */

use Simplepack\Bag\Bag;

if (!function_exists('bag')) {
    function bag($make = null)
    {
        if (is_null($make)) {
            return Bag::getInstance();
        }

        return Bag::getInstance()->make($make);
    }
}