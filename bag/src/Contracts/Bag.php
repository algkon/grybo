<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-08
 * Time: 00:36
 */

namespace Simplepack\Bag\Contracts;

interface Bag
{
    /**
     * @return string
     */
    public function getName();

    /**
     * @return mixed
     */
    public function all();

    /**
     * @param $key
     * @param null $default
     * @return mixed
     */
    public function get($key, $default = null);

    /**
     * @param $key
     * @return mixed
     */
    public function has($key);

    /**
     * @param $value
     * @return mixed
     */
    public function push($value);

    /**
     * @param $key
     * @param $value
     * @return mixed
     */
    public function put($key, $value);
}
