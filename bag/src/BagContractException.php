<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-08
 * Time: 01:04
 */

namespace Simplepack\Bag;


class BagContractException extends \ErrorException
{

    /**
     * BagContractException constructor.
     */
    public function __construct($object, $method = __METHOD__, $filename = __FILE__, $lineno = __LINE__)
    {
        $message = sprintf(
            'Argument 1 passed to %s() must be an instance of %s, instance of %s given, %s %s',
            $method,
            'Simplepack\Bag\Contracts\Bag',
            get_class($object),
            $filename ? ' in ' . $filename : '', $lineno);

        parent::__construct($message, 0, 1, $filename, $lineno);
    }
}