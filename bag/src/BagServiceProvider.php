<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-08
 * Time: 00:06
 */

namespace Simplepack\Bag;


use Illuminate\Support\ServiceProvider;

class BagServiceProvider extends ServiceProvider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // bind Bag application
        $this->app->bind('bag', function () {
            return Bag::getInstance();
        });

        // share bag object
        $this->app->make('view')->share('bag', $this->app->make('bag'));
    }
}