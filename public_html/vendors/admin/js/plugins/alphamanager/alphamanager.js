/**
 * Created by Povilas Stankevi�ius on 2016-09-21.
 */

"use strict";
var AlphaManager = {
    instances: [], init: function (j, c, f) {
        if (j == null) {
            j = {};
        }
        AlphaManager.Utils.embedCss(typeof j["skin"] === "undefined" ? "flat" : j["skin"]);
        if (!f) {
            f = AlphaManager.Utils.getBaseUrl();
        }
        if (!("showDialogTitle" in j)) {
            j.showDialogTitle = true;
        }
        if (!("showDialogButtons" in j)) {
            j.showDialogButtons = true;
        }
        if (!("maxDialogHeight" in j)) {
            j.maxDialogHeight = 1000;
        }
        if (!("multiSelect" in j)) {
            j.multiSelect = false;
        }
        var h = {
            parameters: j,
            baseUrl: f,
            elBg: null,
            elContent: null,
            elDlg: null,
            elIframe: null,
            elTitle: null,
            elButtons: null,
            files: [],
            show: function (m) {
                if (this.elIframe) {
                    this.elIframe.parentNode.removeChild(this.elIframe);
                }
                this.elIframe = document.createElement("iframe");
                this.elContent.appendChild(this.elIframe);
                this.elIframe.className = "alphamanager_iframe";
                var p = window, i = "inner";
                if (!("innerWidth" in window)) {
                    i = "client";
                    p = document.documentElement || document.body;
                }
                window.alphamanager_params = this.parameters;
                this.elIframe.src = this.baseUrl + "index.html";
                AlphaManager.Utils.addClass(this.elBg, "alphamanager_show");
                AlphaManager.Utils.addClass(this.elDlg, "alphamanager_show");
                var n = p[i + "Height"];
                var o = j.maxDialogHeight;
                if (o + 90 > n) {
                    o = n - 90;
                }
                this.setHeight(o);
            },
            hide: function () {
                AlphaManager.Utils.removeClass(this.elBg, "alphamanager_show");
                AlphaManager.Utils.removeClass(this.elDlg, "alphamanager_show");
            },
            setHeight: function (i) {
                this.elDlg.style.height = i + "px";
                this.elDlg.style.marginTop = "-" + Math.floor(i / 2) + "px";
                var m = i;
                if (this.elTitle != null) {
                    m -= this.elTitle.offsetHeight;
                }
                if (this.elButtons != null) {
                    m -= this.elButtons.offsetHeight;
                }
                this.elContent.style.height = m + "px";
            },
            setWidth: function (i) {
                this.elDlg.style.width = i + "px";
                this.elDlg.style.marginLeft = "-" + Math.floor(i / 2) + "px";
            },
            onLoaded: function (i) {
                this.setWidth(i + 14);
            },
            onFileSelected: function (i) {
                this.files = i;
                var m = this.parameters.multiSelect === true ? i.length > 0 : i.length == 1;
                if (m) {
                    AlphaManager.Utils.removeClass(this.elBtnOk, "alphamanager_disabled");
                } else {
                    AlphaManager.Utils.addClass(this.elBtnOk, "alphamanager_disabled");
                }
            }
        };
        var a = h.parameters.onLoaded;
        h.parameters.onLoaded = (function () {
            var n = h;
            var m = a;
            return function (i) {
                n.onLoaded(i);
                if (m) {
                    m(i);
                }
            };
        })();
        a = h.parameters.onFileSelected;
        h.parameters.onFileSelected = (function () {
            var n = h;
            var m = a;
            return function (i) {
                n.onFileSelected(i);
                if (m) {
                    m(i);
                }
            };
        })();
        a = h.parameters.onFileSet;
        h.parameters.onFileSet = (function () {
            var n = h;
            var m = a;
            return function (i) {
                n.hide();
                if (m) {
                    m(i);
                }
            };
        })();
        if (c) {
            if (typeof c == "string") {
                c = document.getElementById(c);
            }
            c.onclick = (function () {
                var m = h;
                return function () {
                    m.show();
                };
            })();
        }
        h.elBg = document.createElement("div");
        h.elBg.className = "alphamanager_bg";
        h.elContent = document.createElement("div");
        h.elContent.className = "alphamanager_content";
        h.elDlg = document.createElement("div");
        var b = ["alphamanager_dlg"];
        if (typeof j["skinMod"] !== "undefined") {
            var k = j["skinMod"].split(",");
            for (var d = 0; d < k.length; d++) {
                b.push("alphamanager_mod_" + k[d]);
            }
        }
        h.elDlg.className = b.join(" ");
        h.elDlg.style.width = "1300px";
        h.elDlg.style.marginLeft = "-650px";
        h.setHeight(j.maxDialogHeight);
        var e = document.getElementsByTagName("body")[0];
        e.appendChild(h.elBg);
        e.appendChild(h.elDlg);
        if (j.showDialogTitle === true) {
            h.elTitle = document.createElement("div");
            h.elTitle.className = "alphamanager_title";
            h.elDlg.appendChild(h.elTitle);
            var l = document.createElement("div");
            l.className = "alphamanager_title_text";
            l.textContent = "Alpha Manager";
            h.elTitle.appendChild(l);
            var g = document.createElement("div");
            g.href = "javascript:void(0)";
            g.className = "alphamanager_x";
            g.textContent = "×";
            h.elTitle.appendChild(g);
            g.onclick = (function () {
                var m = h;
                return function () {
                    m.hide();
                };
            })();
        }
        h.elDlg.appendChild(h.elContent);
        if (j.showDialogButtons === true) {
            h.elButtons = document.createElement("div");
            h.elButtons.className = "alphamanager_buttons";
            h.elBtnOk = document.createElement("a");
            h.elBtnOk.textContent = "OK";
            h.elBtnOk.className = "alphamanager_btn alphamanager_btn_ok alphamanager_disabled";
            h.elButtons.appendChild(h.elBtnOk);
            h.elBtnOk.onclick = (function () {
                var m = h;
                return function () {
                    if (AlphaManager.Utils.hasClass(this, "alphamanager_disabled")) {
                        return;
                    }
                    m.parameters.onFileSet(m.files);
                };
            })();
            h.elBtnCancel = document.createElement("a");
            h.elBtnCancel.textContent = "Cancel";
            h.elBtnCancel.className = "alphamanager_btn";
            h.elButtons.appendChild(h.elBtnCancel);
            h.elBtnCancel.onclick = (function () {
                var m = h;
                return function () {
                    m.hide();
                };
            })();
            h.elDlg.appendChild(h.elButtons);
        }
        AlphaManager.instances.push(h);
        return h;
    }
};
AlphaManager.Utils = {
    addClass: function (b, a) {
        if (AlphaManager.Utils.hasClass(b, a)) {
            return;
        }
        b.className = b.className.length == 0 ? a : b.className + " " + a;
    }, removeClass: function (c, a) {
        var b = AlphaManager.Utils.getClasses(c);
        while (b.indexOf(a) > -1) {
            b.splice(b.indexOf(a), 1);
        }
        var d = b.join(" ").trim();
        if (d.length > 0) {
            c.className = d;
        } else {
            if (c.hasAttribute("class")) {
                c.removeAttribute("class");
            }
        }
    }, getClasses: function (a) {
        if (typeof(a.className) === "undefined" || a.className == null) {
            return [];
        }
        return a.className.split(/\s+/);
    }, hasClass: function (d, a) {
        var c = AlphaManager.Utils.getClasses(d);
        for (var b = 0; b < c.length; b++) {
            if (c[b].toLowerCase() == a.toLowerCase()) {
                return true;
            }
        }
        return false;
    }, embedCss: function (e) {
        var d = document.getElementsByTagName("head")[0].getElementsByTagName("style");
        for (var b = d.length - 1; b >= 0; b--) {
            if (d[b].getAttribute("data-alphamanager") === "true") {
                d[b].parentNode.removeChild(d[b]);
            }
        }
        var c = document.createElement("style");
        c.setAttribute("data-alphamanager", "true");
        document.getElementsByTagName("head")[0].appendChild(c);
        var a = [];
        a["business"] = ".alphamanager_bg { background-color: black; opacity: 0.5; position: fixed; left: 0; top: 0; width: 100%; height: 3000px; z-index: 11111; display: none; } " + ".alphamanager_dlg { width: 400px; margin-left: -200px; top: 50%; left: 50%; padding: 0; position: fixed; z-index: 11112; background-color: white; border: 1px solid #999; overflow:hidden; display: none; }" + ".alphamanager_show { display: block; }" + ".alphamanager_title { box-sizing: border-box; font-size: 18px; font-weight: bold; padding: 7px 0 3px 15px; color: #333; border-bottom: 1px solid #ccc; height: 35px; }" + ".alphamanager_title_text { float: left; width: 95%; }" + ".alphamanager_x { box-sizing: border-box; float: right; width: 5%; text-align: right; cursor: pointer; padding-right: 17px; color: #999; }" + ".alphamanager_x:hover { color: #444; }" + ".alphamanager_btn { padding: 4px 10px; font-size: 15px; border: 1px solid #AAA !important; display: inline-block; float: right; cursor: pointer; margin-left: 15px; min-width: 90px; text-align: center; background-color: white; }" + ".alphamanager_btn:hover { background-color: #f3f3f3; }" + ".alphamanager_btn.alphamanager_disabled, .alphamanager_btn.alphamanager_disabled:hover, .alphamanager_btn.alphamanager_disabled:focus { color: #DDD; border-color: #DDD; background-color: white; cursor: default; }" + ".alphamanager_iframe { border: none; width: 100%; height: 100%; }" + ".alphamanager_buttons { background: #FAFAFA; height: 60px; padding: 15px 25px 15px 0;}";
        a["mono"] = ".alphamanager_bg { background-color: black; opacity: 0.5; position: fixed; left: 0; top: 0; width: 100%; height: 3000px; z-index: 11111; display: none; } " + ".alphamanager_dlg { width: 400px; margin-left: -200px; top: 50%; left: 50%; padding: 0; position: fixed; z-index: 11112; background-color: white; border: 1px solid #999; border-radius: 10px; overflow:hidden; display: none; }" + ".alphamanager_show { display: block; }" + ".alphamanager_title { box-sizing: border-box; font-size: 18px; font-weight: bold; padding: 7px 0 3px 15px; color: #333; border-bottom: 1px solid #ccc; height: 35px; }" + ".alphamanager_title_text { float: left; width: 95%; }" + ".alphamanager_x { box-sizing: border-box; float: right; width: 5%; text-align: right; cursor: pointer; padding-right: 17px; color: #999; }" + ".alphamanager_x:hover { color: #444; }" + ".alphamanager_btn { padding: 4px 10px; font-size: 15px; border: 2px solid #AAA !important; border-radius: 5px; display: inline-block; float: right; cursor: pointer; margin-left: 15px; min-width: 90px; text-align: center; background-color: white; color: #555; }" + ".alphamanager_btn:hover { background-color: #555; color: white; border-color: #555; }" + ".alphamanager_btn.alphamanager_disabled, .alphamanager_btn.alphamanager_disabled:hover, .alphamanager_btn.alphamanager_disabled:focus { color: #DDD; border-color: #DDD; background-color: white; cursor: default; }" + ".alphamanager_iframe { border: none; width: 100%; height: 100%; }" + ".alphamanager_buttons { background: #FAFAFA; height: 60px; padding: 15px 25px 15px 0;}" + ".alphamanager_mod_blue .alphamanager_btn { border: 2px solid #09F !important; color: #09F; }" + ".alphamanager_mod_blue .alphamanager_btn:hover { background-color: #09F; border-color: #09F !important; color: white; }" + ".alphamanager_mod_blue .alphamanager_btn.alphamanager_disabled, .alphamanager_mod_blue .alphamanager_btn.alphamanager_disabled:hover, .alphamanager_mod_blue .alphamanager_btn.alphamanager_disabled:focus { color: #DDD; border-color: #DDD; background-color: white; cursor: default; }" + ".alphamanager_mod_violet .alphamanager_btn { border: 2px solid #6C538B !important; color: #6C538B !important; }" + ".alphamanager_mod_violet .alphamanager_btn:hover { background-color: #6C538B; border-color: #6C538B !important; color: white !important; }" + ".alphamanager_mod_violet .alphamanager_btn.alphamanager_disabled, .alphamanager_mod_violet .alphamanager_btn.alphamanager_disabled:hover, .alphamanager_mod_violet .alphamanager_btn.alphamanager_disabled:focus { color: #DDD !important; border-color: #DDD !important; background-color: white; cursor: default; }";
        a["flat"] = ".alphamanager_bg { background-color: black; opacity: 0.5; position: fixed; left: 0; top: 0; width: 100%; height: 3000px; z-index: 11111; display: none; } " + ".alphamanager_dlg { width: 400px; margin-left: -200px; top: 50%; left: 50%; padding: 0; position: fixed; z-index: 11112; background-color: white; border-radius: 5px; overflow:hidden; display: none; }" + ".alphamanager_show { display: block; }" + ".alphamanager_title { box-sizing: border-box; font-size: 18px; font-weight: bold; padding: 7px 0 3px 15px; color: #333; border-bottom: 1px solid #ccc; height: 35px; }" + ".alphamanager_title_text { float: left; width: 95%; }" + ".alphamanager_x { box-sizing: border-box; float: right; width: 5%; text-align: right; cursor: pointer; padding-right: 17px; color: #999; }" + ".alphamanager_x:hover { color: #444; }" + ".alphamanager_btn { margin: 0 10px; padding: 5px 0; display: inline-block; float: right; color: white !important; font-size: 15px; margin-bottom: 20px; width: 90px; text-align: center; border-radius: 3px; background-color: #F14444; cursor: pointer; border: none !important; }" + ".alphamanager_btn:hover { background-color: #D33232; border: none !important; }" + ".alphamanager_btn_ok { background-color: #00FFB8; }" + ".alphamanager_btn_ok:hover { background-color: #008661; }" + ".alphamanager_btn.alphamanager_disabled, .alphamanager_btn.alphamanager_disabled:hover, .alphamanager_btn.alphamanager_disabled:focus { cursor: default; opacity: 0.3; }" + ".alphamanager_iframe { border: none; width: 100%; height: 100%; }" + ".alphamanager_buttons { background: #FAFAFA; height:60px; padding: 15px 25px 15px 0;}";
        c.innerHTML = a[e];
    }, getBaseUrl: function () {
        var e = /(^|.*[\\\/])(alphamanager|gplugin)\.js(?:\?.*|;.*)?$/i;
        var d = null;
        var a = document.getElementsByTagName("script");
        for (var c = 0; c < a.length; c++) {
            var b = a[c].src.match(e);
            if (b) {
                d = b[1];
                break;
            }
        }
        if (d != null) {
            if (d.indexOf(":/") == -1 && d.slice(0, 2) != "//") {
                if (d.indexOf("/") === 0) {
                    d = location.href.match(/^.*?:\/\/[^\/]*/)[0] + d;
                } else {
                    d = location.href.match(/^[^\?]*\/(?:)/)[0] + d;
                }
            }
        }
        return d;
    }
};
//window.demoLoaded();
