/**
 * Created by Povilas Stankevičius on 2016-09-25.
 */

function employment_user_status_update(form, callback) {
    var formData = $(form).serialize(),
        method   = $(form).find('input[name="_method"]');
        requestMethod = method.length ? method.val() : $(form).attr('method') ;

    $.ajax({
        url: $(form).attr('action'),
        type: requestMethod,
        data: formData,
        dataType: 'json',
        success: function (response) {

            if (typeof callback == "function") {
                callback();
            }

            swal({
                title: response.message,
                confirmButtonColor: "#66BB6A",
                type: "success"
            });
        },
        error: function () {
            swal({
                title: "Ups...",
                text: "Kažkas atsitiko!",
                confirmButtonColor: "#EF5350",
                type: "error"
            });
        }
    });
}

function employment_user_accept(button) {
    var form = $(button).closest('li').find('form'),
        container = $(button).closest('td');

    employment_user_status_update(form, function () {
        container.html('<span class="label label-success">Užregistruotas</span>');
    });
}

function employment_user_cancel(button) {
    var form = $(button).closest('li').find('form'),
        container = $(button).closest('td');

    employment_user_status_update(form, function (row) {
        container.html('<span class="label label-danger">Atšaukta</span>');
    });
}