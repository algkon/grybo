/* ------------------------------------------------------------------------------
 *
 *  # Basic datatables
 *
 *  Specific JS code additions for datatable_basic.html page
 *
 *  Version: 1.0
 *  Latest update: Aug 1, 2015
 *
 * ---------------------------------------------------------------------------- */

$(function () {

    // Table setup
    // ------------------------------
    // Setting datatable defaults
    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        bLengthChange: false,
        iDisplayLength: 50,
        columnDefs: [
            {
                orderable: false,
                targets: "no-sort"
            },
            {
                width: '40px',
                targets: 'actions'
            }
        ],
        dom: '<"datatable-header"f<"datatable-buttons">l><"datatable-scroll"t><"datatable-footer"ip>',
        buttons : [],
        language: {
            search: '<span>Paieška:</span> _INPUT_',
            lengthMenu: '<span>Rodyti:</span> _MENU_',
            info: 'Rodoma nuo _START_ iki _END_ iš _TOTAL_ įrašų',
            infoEmpty: 'Rodoma nuo 0 iki 0 iš 0 įrašų',
            infoFiltered: "(rasta tarp _MAX_ įrašų)",
            emptyTable: "Įrašų nėra",
            paginate: {'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;'}
        }
    });

    if (typeof extend_datatable_defaults == "function") {
        extend_datatable_defaults();
    }

    //Basic datatable
    $.fn.currentDatatable = $('.datatable-basic').DataTable();

    // External table functions
    // ------------------------------

    // Extend datatable
    $.extend($.fn.currentDatatable, {
        sweetDelete: function (element) {
            var table = this,
                row = $(element).parents('tr'),
                form = $(element).parents('tr').find('form.destroy');

            sweet_delete(form, function () {
                table
                    .row(row)
                    .remove()
                    .draw();
            });

            return false;
        },
        addRow: function (newRow) {
            var table = this;

            table.row.add($(newRow)).draw();
        },
        updateRow: function (row, newRow) {
            var table = this;

            row.html($(newRow).html());

            table
                .row( row )
                .invalidate()
                .draw();
        }
    });


    // External table additions
    // ------------------------------

    // Enable Select2 select for the length option
    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        width: 'auto'
    });
});
