/* ------------------------------------------------------------------------------
 *
 *  # CKEditor editor
 *
 *  Specific JS code additions for editor_ckeditor.html page
 *
 *  Version: 1.0
 *  Latest update: Aug 1, 2015
 *
 * ---------------------------------------------------------------------------- */

$(function () {

    var editors = $(document).find('#editor-full');

    if (editors.length != 0) {

        // Full featured editor
        CKEDITOR.replace('editor-full', {
            height: '400px',
            extraPlugins: 'forms,alphamanager',
            alphamanager: {
                skin: 'business',
                skinMod: '',
                hideCopyright: true,
                showRootDir: true,
                fileToolbar: ["upload", "|", "preview", "rename", "del", "view", "order", "search"], //"preview", "download",
                dirToolbar: ["create", "rename", "del"],
                fileMenu: ["select", "-", "preview", "-", "paste", "cut", "copy", "-", "rename", "del"],
                dirMenu: ["paste", "cut", "copy", "create", "rename", "del"],
                //returnUrlPrefix: window.Laravel.base_url + '//'
                returnUrlPrefix: ''
            }
            //a = a.replace(/\/[^/]*\/\.\.\//g, "/");
            //a = a.replace(/^(\.\.\/)+/g, "/");
            //
            //filebrowserBrowseUrl : window.Laravel.asset + 'vendors/admin/filemanager/core/dialog.php?type=2&editor=ckeditor&fldr=',
            ////filebrowserUploadUrl : window.Laravel.asset + 'vendors/admin/filemanager/core/dialog.php?type=2&editor=ckeditor&fldr=',
            //filebrowserImageBrowseUrl : window.Laravel.asset + 'vendors/admin/filemanager/core/dialog.php?type=1&editor=ckeditor&fldr='
        });

    }

});
