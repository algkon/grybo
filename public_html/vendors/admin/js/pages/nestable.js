/**
 * Created by Povilas Stankevi�ius on 2016-09-20.
 */

$(function() {

    var createItem = function(data) {
        "use strict";

        var li = $('<li />', {
                'id'  : data.id,
                'class'      : 'dd-item',
                'data-id'   : data.id,
                'data-title' : data.title,
                'data-slug'  : data.slug,
                'data-rel'  : data.rel,
                'data-target'  : data.target,
                'data-sort' : ''
            }),
            title = $('<div />', {'class' : 'dd-handle'}).text(data.title),
            buttons = navItemActions();

        title.appendTo(li);
        $(buttons).appendTo(li);

        return li;

    };

    var navItemActions = function () {
        "use strict";
        var html  = '';
        html += '<a class="button edit"><i class="icon-pencil7"></i></a>';
        html += '<a class="button delete text-danger-600"><i class="icon-trash"></i></a>';

        return html;
    };

    var serializeMenu = function () {
        "use strict";
        var relationships = $('.dd').nestable('serialize');
        $('input[name="links"]').val(JSON.stringify(relationships));
    };

    var resortNavItemsList = function () {
        "use strict";
        var rows = $('.dd').find('.dd-item');

        rows.each(function (index, value) {
            $(this).attr('data-sort', index);
        });
        serializeMenu();
    };

    var save_form = function (form) {
        var title = form.find('input[name="name"]').val(),
            slug  = form.find('input[name="slug"]').val(),
            rel   = form.find('select[name="rel"]').val(),
            target = form.find('select[name="target"]').val(),
            id    = (form.find('input[name="id"]').val() != "") ? form.find('input[name="id"]').val() : Math.floor((Math.random() * 1000000) + 1),
            data = {
                'id'    : id,
                'title' : title,
                'slug'  : slug,
                'rel' : rel,
                'target' : target
            };

        if($('.dd-item#' + data.id).length != 0) {
            $('.dd-item#' + data.id).replaceWith(createItem(data));
        } else {
            $('div.dd > .dd-list').append(createItem(data));
        }

        form.find('input[name="name"]').val('');
        form.find('input[name="slug"]').val('');
        form.find('select[name="rel"]').val('');
        form.find('select[name="target"]').val('');
        form.find('input[name="id"]').val('');

        form.addClass('hide');
        resortNavItemsList();
    };

    $('.dd').nestable({
        maxDepth : 1,
        group : 1
    });

    $(document).on('click', '.dd-item a.button.delete', function (event) {
        event.preventDefault();
        $(this).closest('.dd-item').remove();
        resortNavItemsList();
    });

    $(document).on('click', '.dd-item a.button.edit', function (event) {
        event.preventDefault();

        add_menu_link(function (form) {
            save_form(form);
        });

        var item = $(this).closest('.dd-item').data(),
            form = $('form.add-edit-item');

        form.find('input[name="name"]').val(item.title);
        form.find('input[name="slug"]').val(item.slug);
        form.find('select[name="rel"]').val(item.rel);
        form.find('select[name="target"]').val(item.target);
        form.find('input[name="id"]').val(item.id);

        //form.removeClass('hide');
    });

    $(document).on('click', 'button.add-blank', function () {
        add_menu_link(function (form) {
            save_form(form);
        });
    });

    //$(document).on('click', '.add-edit-item button.save-item-data', function(event){
    //    event.preventDefault();
    //
    //});

    //$(document).on('click', 'button.add-blank', function (event) {
    //    event.preventDefault();
    //    $('.add-edit-item').removeClass('hide');
    //});

    $('.dd').on('change', function() {
        /* on change event */
        resortNavItemsList();
    });

    if($('.dd').length != 0) {
        serializeMenu();
    }

});
