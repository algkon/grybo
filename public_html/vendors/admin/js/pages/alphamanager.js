/**
 * Created by Povilas Stankevičius on 2016-09-21.
 */

$(function () {

    var alphamanager_path = window.Laravel.base_url + '/vendors/admin/js/plugins/alphamanager';

    if ($('#alpha_button').length != 0) {
        var browser = AlphaManager.init({
            returnUrlPrefix: window.Laravel.base_url + '//',
            skin: 'business',
            skinMod: '',
            hideCopyright: true,
            showRootDir: true,
            fileToolbar: ["upload", "|", "preview", "rename", "del", "view", "order", "search"], //"preview", "download",
            dirToolbar: ["create", "rename", "del"],
            fileMenu: ["select", "-", "preview", "-", "paste", "cut", "copy", "-", "rename", "del"],
            dirMenu: ["paste", "cut", "copy", "create", "rename", "del"],
            multiSelect: true,
            onFileSet: function (files) {

                if (files.length > 0) {
                    var html = '',
                        idsArray = [];

                    var ids = $('.a_files').find('input.image_id');
                    ids.each(function () {
                        idsArray.push($(this).val());
                    });

                    var maxId = idsArray.length != 0 ? Math.max.apply(Math, idsArray) : 0;

                    for (var i in files) {
                        //thumbnail = alphamanager_path + '/php/thumb.php?f=' + encodeURIComponent(filePath) + '&width=180&height=180';//window.Laravel.base_url + '/uploads/' + file.name;
                        var file = files[i],
                            path = file.path.replace(window.Laravel.base_url, '').replace(/(\/?\.\.\/)+/, ''),
                            thumbnail = file.path.replace('../../../', 'image/medium/'),
                            fullPath = file.path.replace('../../../', 'image/original/');

                        console.log(path);

                        if (file.image === true) {
                            maxId++;

                            html += '<div class="col-lg-2 col-sm-6">' +
                                '<div class="thumbnail">' +
                                '<input type="hidden" name="images[' + maxId + '][id]" value="' + maxId + '" class="image_id" />' +
                                '<input type="hidden" name="images[' + maxId + '][ordering]" value="" class="image_order" />' +
                                '<input type="hidden" name="images[' + maxId + '][name]" value="' + file.name + '" />' +
                                '<input type="hidden" name="images[' + maxId + '][path]" value="' + path + '" />' +
                                '<input type="hidden" name="images[' + maxId + '][full_path_medium]" value="' + thumbnail + '" />' +
                                '<input type="hidden" name="images[' + maxId + '][full_path_original]" value="' + fullPath + '" />' +
                                '<div class="thumb">' +
                                '<img src="' + thumbnail + '" alt="">' +
                                '<div class="caption-overflow">' +
                                '<span>' +
                                '<a href="' + fullPath + '" data-popup="lightbox" rel="gallery" class="btn border-white text-white btn-flat btn-icon btn-rounded">' +
                                '<i class="icon-plus3"></i>' +
                                '</a>' +
                                '<a href="javascript:void(0)" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5" onclick="gallery_image_delete(this);">' +
                                '<i class="icon-trash"></i>' +
                                '</a>' +
                                '</span>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>';
                        }
                    }
                }
                $('.a_files').append(html);
                gallery_images_resort();
            }
        }, 'alpha_button' );
    }
});