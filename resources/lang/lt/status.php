<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-10-13
 * Time: 13:21
 */

return [
    'employment' => [
        0 => 'Atšaukta',
        1 => 'Užregistruotas',
        2 => 'Laukia patvirtinimo',
    ]
];