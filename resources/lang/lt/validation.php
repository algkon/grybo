<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-09-14
 * Time: 12:49
 */

return [
    /*
    |--------------------------------------------------------------------------
    | Patvirtinimo kalbos eilutės
    |--------------------------------------------------------------------------
    |
    | Sekančios kalbos eilutėse yra numatyti klaidos pranešimai naudojami
    | patvirtinimo klasėje. Kai kurios iš šių eilučių turi keletą versijų
    | tokių kaip dydžio taisyklės. Galite laisvai pataisyti bet kuriuos pranešimus.
    |
    */
    'accepted' => 'Laukas ":attribute" turi būti priimtas.',
    'active_url' => 'Laukas ":attribute" nėra galiojantis internetinis adresas.',
    'after' => 'Lauko ":attribute" reikšmė turi būti po :date datos.',
    'alpha' => 'Laukas ":attribute" gali turėti tik raides.',
    'alpha_dash' => 'Laukas ":attribute" gali turėti tik raides, skaičius ir brūkšnelius.',
    'alpha_num' => 'Laukas ":attribute" gali turėti tik raides ir skaičius.',
    'array' => 'Laukas ":attribute" turi būti masyvas.',
    'before' => 'Laukas ":attribute" turi būti data prieš :date.',
    'between' => [
        'numeric' => 'Lauko ":attribute" reikšmė turi būti tarp :min ir :max.',
        'file' => 'Failo dydis lauke ":attribute" turi būti tarp :min ir :max kilobaitų.',
        'string' => 'Simbolių skaičius lauke ":attribute" turi būti tarp :min ir :max.',
        'array' => 'Elementų skaičius lauke ":attribute" turi turėti nuo :min iki :max.',
    ],
    'boolean' => 'Lauko reikšmė ":attribute" turi būti \'taip\' arba \'ne\'.',
    'confirmed' => 'Slaptažodžiai nesutampa',
    'date' => 'Lauko ":attribute" reikšmė nėra galiojanti data.',
    'date_format' => 'Lauko ":attribute" reikšmė neatitinka formato :format.',
    'different' => 'Laukų ":attribute" ir :other reikšmės turi skirtis.',
    'digits' => 'Laukas ":attribute" turi būti sudarytas iš :digits skaitmenų.',
    'digits_between' => 'Laukas ":attribute" tuti turėti nuo :min iki :max skaitmenų.',
    'dimensions' => 'Lauke ":attribute" įkeltas paveiksliukas neatitinka išmatavimų reikalavimo.',
    'distinct' => 'Laukas ":attribute" pasikartoja.',
    'email' => 'Lauko ":attribute" reikšmė turi būti galiojantis el. pašto adresas.',
    'file' => 'The ":attribute" must be a file.',
    'filled' => 'Laukas ":attribute" turi būti užpildytas.',
    'exists' => 'Pasirinkta negaliojanti ":attribute" reikšmė.',
    'image' => 'Lauko ":attribute" reikšmė turi būti paveikslėlis.',
    'in' => 'Pasirinkta negaliojanti ":attribute" reikšmė.',
    'in_array' => 'Laukas ":attribute" neegzistuoja :other lauke.',
    'integer' => 'Lauko ":attribute" reikšmė turi būti veikasis skaičius.',
    'ip' => 'Lauko ":attribute" reikšmė turi būti galiojantis IP adresas.',
    'json' => 'Lauko ":attribute" reikšmė turi būti JSON tekstas.',
    'max' => [
        'numeric' => 'Lauko ":attribute" reikšmė negali būti didesnė nei :max.',
        'file' => 'Failo dydis lauke ":attribute" reikšmė negali būti didesnė nei :max kilobaitų.',
        'string' => 'Simbolių kiekis lauke ":attribute" reikšmė negali būti didesnė nei :max simbolių.',
        'array' => 'Elementų kiekis lauke ":attribute" negali turėti daugiau nei :max elementų.',
    ],
    'mimes' => 'Lauko reikšmė ":attribute" turi būti failas vieno iš sekančių tipų: :values.',
    'min' => [
        'numeric' => 'Lauko ":attribute" reikšmė turi būti ne mažesnė nei :min.',
        'file' => 'Failo dydis lauke ":attribute" turi būti ne mažesnis nei :min kilobaitų.',
        'string' => 'Simbolių kiekis lauke ":attribute" turi būti ne mažiau nei :min.',
        'array' => 'Elementų kiekis lauke ":attribute" turi būti ne mažiau nei :min.',
    ],
    'not_in' => 'Pasirinkta negaliojanti reikšmė ":attribute".',
    'numeric' => 'Lauko ":attribute" reikšmė turi būti skaičius.',
    'present' => 'Laukas ":attribute" turi egzistuoti.',
    'regex' => 'Negaliojantis lauko ":attribute" formatas.',
    'required' => 'Privaloma užpildyti lauką ":attribute".',
    'required_if' => 'Privaloma užpildyti lauką ":attribute".',
    'required_unless' => 'Laukas ":attribute" yra privalomas, nebent :other yra tarp :values reikšmių.',
    'required_with' => 'Privaloma užpildyti lauką ":attribute".',
    'required_with_all' => 'Privaloma užpildyti lauką ":attribute" kai pateikta :values.',
    'required_without' => 'Privaloma užpildyti lauką ":attribute".',
    'required_without_all' => 'Privaloma užpildyti lauką ":attribute" kai nepateikta nei viena iš reikšmių :values.',
    'same' => 'Laukai ":attribute" ir :other turi sutapti.',
    'size' => [
        'numeric' => 'Lauko ":attribute" reikšmė turi būti :size.',
        'file' => 'Failo dydis lauke ":attribute" turi būti :size kilobaitai.',
        'string' => 'Simbolių skaičius lauke ":attribute" turi būti :size.',
        'array' => 'Elementų kiekis lauke ":attribute" turi būti :size.',
    ],
    'string' => 'Laukas ":attribute" turi būti tekstinis.',
    'timezone' => 'Lauko ":attribute" reikšmė turi būti galiojanti laiko zona.',
    'unique' => 'Tokia ":attribute" reikšmė jau pasirinkta.',
    'url' => 'Negaliojantis lauko ":attribute" formatas.',
    /*
    |--------------------------------------------------------------------------
    | Pasirinktiniai patvirtinimo kalbos eilutės
    |--------------------------------------------------------------------------
    |
    | Čia galite nurodyti pasirinktinius patvirtinimo pranešimus, naudodami
    | konvenciją "attribute.rule" eilučių pavadinimams. Tai leidžia greitai
    | nurodyti konkrečią pasirinktinę kalbos eilutę tam tikrai atributo taisyklei.
    |
    */
    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
        'confirmation_code' => [
            'required' => 'Privaloma nurodyti aktyvavimo kodą.',
        ],
        'new_password' => [
            'required' => 'Privaloma įvesti naują slaptažodį.',
        ],
        'old_password' => [
            'required' => 'Privaloma įvesti seną slaptažodį.'
        ]
    ],
    /*
    |--------------------------------------------------------------------------
    | Pasirinktiniai patvirtinimo atributai
    |--------------------------------------------------------------------------
    |
    | Sekančios kalbos eilutės naudojamos pakeisti vietos žymes
    | kuo nors labiau priimtinu skaitytojui (pvz. "El.Pašto Adresas" vietoj
    | "email". TTai tiesiog padeda mums padaryti žinutes truputi aiškesnėmis.
    |
    */
    'attributes' => [
        'amount' => 'Suma',
        'title' => 'Pavadinimas',
        'email' => 'El. paštas',
        'el_pastas' => 'El. paštas',
        'password' => 'Slaptažodis',
        'new_password' => 'Naujas slaptažodis',
        'password_confirmation' => 'Pakartoti slaptažodį',
        'new_password_confirmation' => 'Pakartoti slaptažodį',
        'first_name' => 'Vardas',
        'vardas' => 'Vardas',
        'name' => 'Vardas',
        'last_name' => 'Pavardė',
        'pavarde' => 'Pavardė',
        'personal_code' => 'Asmens kodas',
        'kodas' => 'Asmens kodas',
        'national_id' => 'Asmens kodas',
        'tel_num' => 'Telefono numeris',
        'phone' => 'Telefono numeris',
        'country' => 'Šalis',
        'miestas' => 'Miestas',
        'addr_town' => 'Miestas',
        'gatve' => 'Gatvė',
        'addr_street' => 'Gatvė',
        'namas' => 'Namo nr.',
        'addr_house' => 'Namo nr.',
        'butas' => 'Buto nr.',
        'addr_flat' => 'Buto nr.',
        'pasto_kodas' => 'Pašto kodas',
        'addr_post_code' => 'Pašto kodas',
        'confirmation_code' => "Patvirtinimo kodas",
        'subject' => "Tema",
        'api' => [
            'email' => 'El. paštas',
            'el_pastas' => 'El. paštas',
            'password' => 'Slaptažodis',
            'new_password' => 'Naujas slaptažodis',
            'password_confirmation' => 'Pakartoti slaptažodį',
            'new_password_confirmation' => 'Pakartoti slaptažodį',
            'first_name' => 'Vardas',
            'vardas' => 'Vardas',
            'last_name' => 'Pavardė',
            'pavarde' => 'Pavardė',
            'personal_code' => 'Asmens kodas',
            'kodas' => 'Asmens kodas',
            'national_id' => 'Asmens kodas',
            'tel_num' => 'Telefono numeris',
            'phone' => 'Telefono numeris',
            'country' => 'Šalis',
            'miestas' => 'Miestas',
            'addr_town' => 'Miestas',
            'gatve' => 'Gatvė',
            'addr_street' => 'Gatvė',
            'namas' => 'Namo nr.',
            'addr_house' => 'Namo nr.',
            'butas' => 'Buto nr.',
            'addr_flat' => 'Buto nr.',
            'pasto_kodas' => 'Pašto kodas',
            'addr_post_code' => 'Pašto kodas',
            'confirmation_code' => "Patvirtinimo kodas",
        ]
    ],

    'nsoft' => [
        'response' => [
            'code' => [
                1 => "Vartotojas nerastas.",
                8 => "Vartotojas dar neaktyvavęs savo narystės.",
                123 => "",
                3611 => "Vartotojas tokiu el. pašto adresu jau yra.",
                3612 => "Vartotojas tokiu el. pašto adresu jau yra.",
                3615 => "Vartotojas tokiu asmens kodu jau yra.",
                6421=> "Neturite reikiamų teisių atliktį šį veiksmą.",
                7602 => "Neteisingas vartotojo vardas arba slaptažodis.",
                74152 => "Blogai nurodytas senas slaptažodis.",
                41843 => "Nepakankamas sąskaitos likutis.",
            ],
            'sqlstate' => [
                "P0001" => "Vartotojas nerastas.",
            ]
        ]
    ]
];