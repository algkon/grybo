<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-10-12
 * Time: 15:32
 */

return [
    'of_month' => [
        'january' => 'sausio',
        'february' => 'vasario',
        'march' => 'kovo',
        'april' => 'balandžio',
        'may' => 'gegužės',
        'june' => 'birželio',
        'july' => 'liepos',
        'august' => 'rugpjūčio',
        'september' => 'rugsėjo',
        'october' => 'spalio',
        'november' => 'lapkričio',
        'december' => 'gruodžio',
    ]
];