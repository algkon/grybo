<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-10-22
 * Time: 00:44
 */

return [
    'weekdays' => [
        'monday' => 'pirmadienis',
        'tuesday' => 'antradienis',
        'wednesday' => 'trečiadienis',
        'thursday' => 'ketvirtadienis',
        'friday' => 'penktadienis',
        'saturday' => 'šeštadienis',
        'sunday' => 'sekmadienis',
    ],
    'weekdays_short' => [
        'monday' => 'pirm.',
        'tuesday' => 'antr.',
        'wednesday' => 'treč.',
        'thursday' => 'ketvirt.',
        'friday' => 'penkt.',
        'saturday' => 'šešt.',
        'sunday' => 'sekm.',
    ],
];