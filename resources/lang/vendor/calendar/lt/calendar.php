<?php
/**
 * Created by PhpStorm.
 * User: Povilas Stankevičius
 * Date: 2016-10-13
 * Time: 16:47
 */

return [
    'su'         => 'Su',
    'mo'         => 'Mo',
    'tu'         => 'Tu',
    'we'         => 'We',
    'th'         => 'Th',
    'fr'         => 'Fr',
    'sa'         => 'Sa',
    'sun'        => 'Sun',
    'mon'        => 'Mon',
    'tue'        => 'Tue',
    'wed'        => 'Wed',
    'thu'        => 'Thu',
    'fri'        => 'Fri',
    'sat'        => 'Sat',
    'sunday'     => 'Sekmadienis',
    'monday'     => 'Pirmadienis',
    'tuesday'    => 'Antradienis',
    'wednesday'  => 'Trečiadienis',
    'thursday'   => 'Ketvirtadienis',
    'friday'     => 'Penktadienis',
    'saturday'   => 'Šeštadienis',
    'jan'        => 'Jan',
    'feb'        => 'Feb',
    'mar'        => 'Mar',
    'apr'        => 'Apr',
    'may'        => 'May',
    'jun'        => 'Jun',
    'jul'        => 'Jul',
    'aug'        => 'Aug',
    'sep'        => 'Sep',
    'oct'        => 'Oct',
    'nov'        => 'Nov',
    'dec'        => 'Dec',
    'january'    => 'Sausis',
    'february'   => 'Vasaris',
    'march'      => 'Kovas',
    'april'      => 'Balandis',
    'mayl'       => 'Gegužė',
    'june'       => 'Birželis',
    'july'       => 'Liepa',
    'august'     => 'Rugpjūtis',
    'september'  => 'Rugsėjis',
    'october'    => 'Spalis',
    'november'   => 'Lapkritis',
    'december'   => 'Gruodis'
];