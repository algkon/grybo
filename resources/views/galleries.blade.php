@extends('layouts.app-inner')

@section('meta_title', $model->meta_title)
@section('meta_description', $model->meta_description)
@section('meta_keywords', $model->meta_keywords)

@section('content')

    <section class="gallery">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="page-header">
                        <h3 class="title-bg">{{ $model->title }}</h3>
                    </div>

                    @if ($model->publishedGalleries)
                        <ul class="galeries-list clearfix">
                            @foreach($model->publishedGalleries as $gallery)
                                <li>
                                    <a href="{{ route('gallery', ['slug' => $gallery->slug]) }}">
                                        <img src="{{ $gallery->thumbnail('gallery-thumbnail') }}" alt="{{ $gallery->title }}" title="{{ $gallery->title }}" />
                                        <span>{{ $gallery->title }}</span>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    @endif
                </div>
            </div> <!-- /row -->
        </div>
    </section>

@endsection


