@extends('layouts.app-inner')

@section('meta_title', $model->meta_title)
@section('meta_description', $model->meta_description)
@section('meta_keywords', $model->meta_keywords)

@section('content')

    <section class="page information">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="page-header">
                        <h3 class="title-bg">{{ $model->title }}</h3>
                    </div>

                    <div class="content">{!! $model->content !!}</div>
                </div>
            </div>
        </div>
    </section>

@endsection

@push('scripts.footer')
<script type="text/javascript">
    $(function () {
        var container = $('section.information .content'),
                tables    = container.find('table');

        if (tables.length > 0) {
            tables.each(function () {
                $(this).addClass('table');
            });
        }
    });
</script>
@endpush