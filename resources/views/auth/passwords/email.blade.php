@extends('layouts.app-inner')

@section('meta_title', 'Slaptažodžio priminimas')

<!-- Main Content -->
@section('content')

    <section class="forgot-password">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <div class="page-header">
                        <h3 class="title-bg">Slaptažodžio priminimas</h3>
                    </div>

                    @include('components.notification')

                    <div class="form-block">

                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/slaptazodzio-priminimas') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('email') ? ' has-error has-feedback' : '' }}">
                                <label for="email" class="col-md-3 control-label">El. paštas</label>

                                <div class="col-md-9">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                    @if ($errors->has('email'))
                                        <span class="glyphicon glyphicon-remove form-control-feedback"
                                              aria-hidden="true"></span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-9 col-sm-12 col-xs-12 pull-right text-left">
                                    <input type="submit" name="submit" class="btn btn-default text-center" value="Siųsti priminimą">
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
