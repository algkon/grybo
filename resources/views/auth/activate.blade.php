@extends('layouts.app-inner')

@section('meta_title', 'Vartotojo aktyvavimas')

@section('content')

    <section class="activate-user">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <div class="page-header">
                        <h3 class="title-bg">Vartotojo aktyvavimas</h3>
                    </div>

                    @include('components.notification')

                    <div class="form-block">

                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/vartotojo-aktyvavimas') }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="confirmation_code" value="{{ old("code", request("code")) }}">

                            <div class="form-group{{ $errors->has('email') ? ' has-error has-feedback' : '' }}">
                                <label for="email" class="col-md-3 control-label">El. paštas</label>

                                <div class="col-md-9">
                                    {{ Form::email('email', null, ['class' => 'form-control']) }}

                                    @if ($errors->has('email'))
                                        <span class="glyphicon glyphicon-remove form-control-feedback"
                                              aria-hidden="true"></span>
                                    @endif
                                </div>
                            </div>

                            <hr>

                            <div class="form-group{{ $errors->has('new_password') ? ' has-error has-feedback' : '' }}">
                                <label for="new_password" class="col-md-3 control-label">Naujas slaptažodis</label>

                                <div class="col-md-9">
                                    {{ Form::password('new_password', ['class' => 'form-control']) }}

                                    @if ($errors->has('new_password'))
                                        <span class="glyphicon glyphicon-remove form-control-feedback"
                                              aria-hidden="true"></span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('new_password') ? ' has-error has-feedback' : '' }}{{ $errors->has('new_password_confirmation') ? ' has-error has-feedback' : '' }}">
                                <label for="new_password_confirmation" class="col-md-3 control-label">Pakartoti naują slaptažodį</label>

                                <div class="col-md-9">
                                    {{ Form::password('new_password_confirmation', ['class' => 'form-control']) }}

                                    @if ($errors->has('new_password'))
                                        <span class="glyphicon glyphicon-remove form-control-feedback"
                                              aria-hidden="true"></span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-9 col-sm-12 col-xs-12 pull-right text-left">
                                    <input type="submit" name="submit" class="btn btn-default text-center" value="Aktyvuoti">
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </section>
    
@endsection
