@extends('layouts.app-inner')

@section('meta_title', 'Prisijungti')

@section('content')

    <section class="login">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <div class="page-header">
                        <h3 class="title-bg">Prisijungti</h3>
                    </div>

                    @include('components.notification')

                    <div class="form-block">

                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/prisijungimas') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('email') ? ' has-error has-feedback' : '' }}">
                                <label for="email" class="col-md-3 control-label">El. paštas</label>

                                <div class="col-md-9">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">
                                    @if ($errors->has('email'))
                                        <span class="glyphicon glyphicon-remove form-control-feedback"
                                              aria-hidden="true"></span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error has-feedback' : '' }}">
                                <label for="password" class="col-md-3 control-label">Slaptažodis</label>

                                <div class="col-md-9">
                                    <input id="password" type="password" class="form-control" name="password">
                                    @if ($errors->has('password'))
                                        <span class="glyphicon glyphicon-remove form-control-feedback"
                                              aria-hidden="true"></span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-9 col-md-offset-3">
                                    <input type="submit" name="submit" class="btn btn-default text-center" value="Prisijungti">
                                    <a href="{{ url('/slaptazodzio-priminimas') }}">
                                        Pamiršote slaptažodį?
                                    </a>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
