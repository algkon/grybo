@extends('layouts.app-inner')

@section('meta_title', 'Registracija')

@section('content')

    <section class="register">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-xs-12">
                    <div class="page-header">
                        <h3 class="title-bg">Registracija</h3>
                    </div>

                    @include('components.notification')

                    <div class="form-block">

                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/registracija') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('first_name') ?  ' has-error has-feedback' : '' }}">
                                <label for="first_name" class="col-md-4 control-label">Vardas</label>

                                <div class="col-md-8">
                                    <input id="first_name" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}">

                                    @if ($errors->has('first_name'))
                                        <span class="glyphicon glyphicon-remove form-control-feedback"
                                              aria-hidden="true"></span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('last_name') ?  ' has-error has-feedback' : '' }}">
                                <label for="last_name" class="col-md-4 control-label">Pavardė</label>

                                <div class="col-md-8">
                                    <input id="last_name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}">

                                    @if ($errors->has('last_name'))
                                        <span class="glyphicon glyphicon-remove form-control-feedback"
                                              aria-hidden="true"></span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="last_name" class="col-md-4 control-label">Lytis</label>

                                <div class="radio-list col-md-8">
                                    <div class="radio">
                                        <label class="{{ old('gender', 1) == 1 ? 'active' : ''}}">
                                            {{--<input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>Pasirinkimas 1--}}
                                            {{ Form::radio('gender', 1 ) }} Vyras
                                        </label>
                                    </div>

                                    <div class="radio">
                                        <label class="{{ old('gender') == 2 ? 'active' : ''}}">
                                            {{--<input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">--}}
                                            {{ Form::radio('gender', 2 ) }} Moteris
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('national_id') ?  ' has-error has-feedback' : '' }}">
                                <label for="national_id" class="col-md-4 control-label">Asmens kodas</label>

                                <div class="col-md-8">
                                    <input id="national_id" type="text" class="form-control" name="national_id" value="{{ old('national_id') }}">

                                    @if ($errors->has('national_id'))
                                        <span class="glyphicon glyphicon-remove form-control-feedback"
                                              aria-hidden="true"></span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ?  ' has-error has-feedback' : '' }}">
                                <label for="email" class="col-md-4 control-label">El. paštas</label>

                                <div class="col-md-8">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                    @if ($errors->has('email'))
                                        <span class="glyphicon glyphicon-remove form-control-feedback"
                                              aria-hidden="true"></span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('member_card_number') ?  ' has-error has-feedback' : '' }}">
                                <label for="member_card_number" class="col-md-4 control-label">"Grybo 7" kliento kortelės nr.</label>

                                <div class="col-md-8">
                                    <input id="member_card_number" type="text" class="form-control" name="member_card_number" value="{{ old('member_card_number') }}">

                                    @if ($errors->has('member_card_number'))
                                        <span class="glyphicon glyphicon-remove form-control-feedback"
                                              aria-hidden="true"></span>
                                    @endif
                                </div>
                            </div>

                            {{--<div class="form-group{{ $errors->has('leisure_card_number') ?  ' has-error has-feedback' : '' }}">--}}
                                {{--<label for="leisure_card_number" class="col-md-4 control-label">"Laisvalaikio" kortelės nr.</label>--}}

                                {{--<div class="col-md-8">--}}
                                    {{--<input id="leisure_card_number" type="text" class="form-control" name="leisure_card_number" value="{{ old('leisure_card_number') }}">--}}

                                    {{--@if ($errors->has('leisure_card_number'))--}}
                                        {{--<span class="glyphicon glyphicon-remove form-control-feedback"--}}
                                              {{--aria-hidden="true"></span>--}}
                                    {{--@endif--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            <hr>

                            <div class="form-group{{ $errors->has('phone') ?  ' has-error has-feedback' : '' }}">
                                <label for="phone" class="col-md-4 control-label">Telefono numeris</label>

                                <div class="col-md-8">
                                    <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}">

                                    @if ($errors->has('phone'))
                                        <span class="glyphicon glyphicon-remove form-control-feedback"
                                              aria-hidden="true"></span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('country') ?  ' has-error has-feedback' : '' }}">
                                <label for="country" class="col-md-4 control-label">Šalis</label>

                                <div class="col-md-8">
                                    <input type="text" name="country" value="LIETUVA" class="form-control" disabled>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('addr_town') ?  ' has-error has-feedback' : '' }}">
                                <label for="addr_town" class="col-md-4 control-label">Miestas</label>

                                <div class="col-md-8">
                                    <input id="addr_town" type="text" class="form-control" name="addr_town" value="{{ old('addr_town') }}">

                                    @if ($errors->has('addr_town'))
                                        <span class="glyphicon glyphicon-remove form-control-feedback"
                                              aria-hidden="true"></span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('addr_street') ?  ' has-error has-feedback' : '' }}">
                                <label for="addr_street" class="col-md-4 control-label">Gatvė</label>

                                <div class="col-md-8">
                                    <input id="addr_street" type="text" class="form-control" name="addr_street" value="{{ old('addr_street') }}">

                                    @if ($errors->has('addr_street'))
                                        <span class="glyphicon glyphicon-remove form-control-feedback"
                                              aria-hidden="true"></span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('addr_house') ?  ' has-error has-feedback' : '' }}">
                                <label for="addr_house" class="col-md-4 control-label">Namo nr.</label>

                                <div class="col-md-8">
                                    <input id="addr_house" type="text" class="form-control" name="addr_house" value="{{ old('addr_house') }}">

                                    @if ($errors->has('addr_house'))
                                        <span class="glyphicon glyphicon-remove form-control-feedback"
                                              aria-hidden="true"></span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('addr_flat') ?  ' has-error has-feedback' : '' }}">
                                <label for="addr_flat" class="col-md-4 control-label">Buto nr.</label>

                                <div class="col-md-8">
                                    <input id="addr_flat" type="text" class="form-control" name="addr_flat" value="{{ old('addr_flat') }}">

                                    @if ($errors->has('addr_flat'))
                                        <span class="glyphicon glyphicon-remove form-control-feedback"
                                              aria-hidden="true"></span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('addr_post_code') ?  ' has-error has-feedback' : '' }}">
                                <label for="addr_post_code" class="col-md-4 control-label">Pašto kodas</label>

                                <div class="col-md-8">
                                    <input id="addr_post_code" type="text" class="form-control" name="addr_post_code" value="{{ old('addr_post_code') }}">

                                    @if ($errors->has('addr_post_code'))
                                        <span class="glyphicon glyphicon-remove form-control-feedback"
                                              aria-hidden="true"></span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-sm-12 col-xs-12 pull-right text-left">
                                    <input type="submit" name="submit" class="btn btn-default text-center" value="Registruotis">
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
