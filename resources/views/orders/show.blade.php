@extends('layouts.app-inner')

@section('meta_title', 'Užsakymas')

@section('content')

    <section class="order">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="page-header">
                        <h3 class="title-bg">Užsakymas: #{{ $order->order_id }}</h3>
                    </div>

                    @include('components.notification')

                    @if (!session()->has('success') && !$order->available)
                        <div class="alert alert-info alert-styled-left alert-bordered">
                            <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                                        class="sr-only">Close</span></button>
                            <p>Užsakymas apmokėtas arba jo galiojimo laikas yra pasibaigęs.</p>
                        </div>
                    @endif

                    <form class="form-horizontal" role="form" method="POST"
                          action="{{ route('order.submit', $order->order_id) }}">
                        {{ csrf_field() }}

                        <table class="table my-info">
                            <tbody>
                            <tr>
                                <td width="120px">Vardas:</td>
                                <td data-title="Vardas">{{ $order->first_name }}</td>
                            </tr>

                            <tr>
                                <td width="120px">Pavardė:</td>
                                <td data-title="Pavardė">{{ $order->last_name }}</td>
                            </tr>

                            <tr>
                                <td width="120px">El. paštas:</td>
                                <td data-title="El. paštas">{{ $order->email }}</td>
                            </tr>

                            <tr>
                                <td width="120px">Telefonas:</td>
                                <td data-title="Telefonas">{{ $order->phone }}</td>
                            </tr>
                            </tbody>
                        </table>

                        <table class="table cart">
                            <thead>
                            <tr>
                                <th>Prekė</th>
                                <th class="text-center">Kiekis/Laikotarpis</th>
                                <th class="text-center">Kaina</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($order->cart()->products as $product)
                                <tr>
                                    <td data-title="Prekė">{{ $product->name }}</td>
                                    <td data-title="Kiekis/Laikotarpis" class="text-center">
                                        {{ $product->options->set_membership_name }}
                                    </td>
                                    <td data-title="Kaina" class="text-center">{{ $product->subtotal }} €</td>
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="3" style="padding: 15px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2" class="text-right">Suma(be PVM):</td>
                                <td data-title="Suma(be PVM):" class="text-center">{{ $order->cart()->subtotal }} €</td>
                            </tr>
                            <tr>
                                <td colspan="2" class="text-right">PVM (21%):</td>
                                <td data-title="PVM (21%):" class="text-center">{{ $order->cart()->tax }} €</td>
                            </tr>
                            <tr>
                                <td colspan="2" class="text-right"><strong>Viso:</strong></td>
                                <td data-title="Viso:" class="text-center">{{ $order->cart()->total }} €</td>
                            </tr>
                            </tbody>
                        </table>

                        <br/>

                        @if ($order->available)
                            <div class="row">
                                <div class="col-md-12 text-right">

                                    @if ($order->cart()->total == 0)
                                        <input type="hidden" name="payment_method" value="none">
                                    @else
                                        <div class="radio-list">
                                            <div class="radio">
                                                <label class="active">
                                                    <input type="radio" name="payment_method" id="payment_method1"
                                                           value="paysera" checked> Paysera
                                                </label>
                                            </div>
                                            @if(auth()->check())
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="payment_method" id="payment_method2"
                                                               value="account"> Iš
                                                        sąskaitos(likutis: {{ auth()->user()->api->balance }} €)
                                                    </label>
                                                </div>
                                            @endif
                                        </div>
                                    @endif

                                </div>
                            </div>

                            <br/>

                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <button type="submit" class="btn btn-default">Apmokėti</button>
                                </div>
                            </div>
                        @endif
                    </form>

                </div>
            </div>
        </div>
    </section>

@endsection
@push('scripts.footer')
<!-- Google Code for www.grybo7.lt/paslaugos Conversion Page -->
<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 1060208045;
    var google_conversion_language = "en";
    var google_conversion_format = "3";
    var google_conversion_color = "ffffff";
    var google_conversion_label = "_ZNNCIr6kHQQrfvF-QM";
    var google_remarketing_only = false;
    /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt=""
             src="//www.googleadservices.com/pagead/conversion/1060208045/?label=_ZNNCIr6kHQQrfvF-QM&amp;guid=ON&amp;script=0"/>
    </div>
</noscript>
@endpush