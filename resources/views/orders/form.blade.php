@extends('layouts.app-inner')

@section('meta_title', 'Pirkėjo informacija')

@section('content')

    <section class="order-form">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-xs-12">
                    <div class="page-header">
                        <h3 class="title-bg">Pirkėjo informacija</h3>
                    </div>

                    @include('components.notification')

                    <div class="form-block">

                        <form class="form-horizontal" role="form" method="POST" action="{{ request()->fullUrl() }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('first_name') ? ' has-error has-feedback' : '' }}">
                                <label for="first_name" class="col-md-4 control-label">Vardas</label>

                                <div class="col-md-8">
                                    <input id="first_name" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" autofocus>

                                    @if ($errors->has('first_name'))
                                        <span class="glyphicon glyphicon-remove form-control-feedback"
                                              aria-hidden="true"></span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('last_name') ? ' has-error has-feedback' : '' }}">
                                <label for="last_name" class="col-md-4 control-label">Pavardė</label>

                                <div class="col-md-8">
                                    <input id="last_name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" autofocus>

                                    @if ($errors->has('last_name'))
                                        <span class="glyphicon glyphicon-remove form-control-feedback"
                                              aria-hidden="true"></span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error has-feedback' : '' }}">
                                <label for="email" class="col-md-4 control-label">El. paštas</label>

                                <div class="col-md-8">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                    @if ($errors->has('email'))
                                        <span class="glyphicon glyphicon-remove form-control-feedback"
                                              aria-hidden="true"></span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('phone') ? ' has-error has-feedback' : '' }}">
                                <label for="phone" class="col-md-4 control-label">Telefono numeris</label>

                                <div class="col-md-8">
                                    <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}">

                                    @if ($errors->has('phone'))
                                        <span class="glyphicon glyphicon-remove form-control-feedback"
                                              aria-hidden="true"></span>
                                    @endif
                                </div>
                            </div>

                            <hr>

                            <div class="form-group{{ $errors->has('member_card_number') ?  ' has-error has-feedback' : '' }}">
                                <label for="member_card_number" class="col-md-4 control-label">Nuolaidos kodas</label>

                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="member_card_number" value="{{ old('member_card_number') }}">

                                    @if ($errors->has('member_card_number'))
                                        <span class="glyphicon glyphicon-remove form-control-feedback"
                                              aria-hidden="true"></span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-sm-12 col-xs-12 pull-right text-left">
                                    <input type="submit" name="submit" class="btn btn-default" value="Toliau ->">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
@endsection
