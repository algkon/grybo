@extends('layouts.app-inner')

@section('meta_title', 'Tvarkaraščiai')
{{--@section('meta_description', $model->meta_description)--}}
{{--@section('meta_keywords', $model->meta_keywords)--}}

@section('content')

    <section class="articles news">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="page-header">
                        <h3 class="title-bg">Tvarkaraščiai</h3>
                    </div>

                    @if($timetables)
                        <div class="articles-list clearfix">
                            @foreach($timetables as $timetable)
                                <article>
                                    <div class="pull-left">
                                        <a href="{{ route('timetables.show', $timetable->slug) }}">
                                            <img src="{{ $timetable->thumbnail('post-medium') }}" alt="{{ $timetable->title }}" title="{{ $timetable->title }}" />
                                        </a>
                                    </div>

                                    <div class="pull-right">
                                        <header class="title-sm">
                                            <a href="{{ route('timetables.show', $timetable->slug) }}">{{ $timetable->title }}</a>
                                        </header>

                                        <p>{{ $timetable->short_description }}</p>

                                        <footer class="clearfix">
                                            <a href="{{ route('timetables.show', $timetable->slug) }}" class="read-more">Skaityti daugiau</a>
                                            <time>{{ $timetable->createdDateAsString() }}</time>
                                        </footer>
                                    </div>

                                    <div class="clearfix"></div>
                                </article>
                            @endforeach
                        </div>

                        {{ $timetables->links('pagination::grybo') }}
                    @endif
                </div>
            </div> <!-- /row -->
        </div>
    </section>

@endsection


