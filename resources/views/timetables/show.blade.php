@extends('layouts.app-inner')

@section('meta_title', $timetable->meta_title)
@section('meta_description', $timetable->meta_description)
@section('meta_keywords', $timetable->meta_keywords)

@section('content')

    <section class="timetable information">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="page-header">
                        <h3 class="title-bg">{{ $timetable->title }}</h3>
                    </div>

                    {{--var timetable_id = {{ $model->id }};--}}
                    {{--var weekdays_dates = {!! ->toJson() !!};--}}
                    {{--                    var hot_headers = ['{!! $calendar->getHeaderColumns()->implode("','") !!}'];--}}
                    {{--                    var hot_checks_labels_data = {!! $calendar->getData()->toJson() !!};--}}

                    <div class="choose-date">
                        <div class="col-xs-3 col-md-1 col-md-offset-7 col-lg-offset-8">
                            @if ($calendar->showTitlePrevious())
                                <a href="{{ route('timetables.show', [
                                    $timetable->slug,
                                    $calendar->startOfPreviousWeek(),
                                    $calendar->endOfPreviousWeek(),
                                    ]) }}" class="btn btn-default prev-url">&lt;&lt;</a>

                            @endif
                        </div>
                        <div class="col-xs-6 col-md-3 col-lg-2 time">
                            <time class="text-center">{{ $calendar->showTitle() }}</time>
                        </div>
                        <div class="col-xs-3 col-md-1 text-right">
                            <a href="{{ route('timetables.show', [
                                $timetable->slug,
                                $calendar->startOfNextWeek(),
                                $calendar->endOfNextWeek(),
                                ]) }}" class="btn btn-default next-url">&gt;&gt;</a>
                        </div>
                        <div class="clearfix"></div>

                        <div class="table-responsive">
                            @if ($calendar->getData()->count())
                                <table class="table timetable">
                                    <thead>
                                    <tr>
                                        @foreach($calendar->getNamedDatesOfWeekDays() as $day => $date)
                                            <th>
                                            <span class="show">
                                                {{ trans('date.weekdays.' . $day) }}
                                            </span>
                                                {{--                                            <small>{{ $date }}</small>--}}
                                            </th>
                                        @endforeach
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @foreach($calendar->getData() as $line)
                                        <tr>
                                            @foreach($line as $col)
                                                {{--                                            {{ $col[''] }}--}}
                                                <td class="{{ $col['status'] === "0" ? 'inactive' : '' }}">
                                                    <p class="text-center">
                                                        {{ $col['starting_time'] }} - {{ $col['ending_time'] }}
                                                    </p>
                                                </td>
                                                {{--<td data-title="Data/laikas">{{ $event->starting_date }}--}}
                                                {{--/ {{ $event->time_range }}</td>--}}
                                                {{--<td data-title="Užsiėmimo pavadinimas">{{ $event->employment->title }}</td>--}}
                                                {{--<td data-title="Treneris">{{ $event->employment->coach->name }}</td>--}}
                                                {{--<td class="register">--}}
                                                {{--@if($event->available())--}}
                                                {{--<a href="{{ route('employments.register.form', [$event->id]) }}">Registruotis--}}
                                                {{--<span>Liko vietų: {{ $event->total_free_space }}</span></a>--}}
                                                {{--@else--}}
                                                {{--@if($event->total_free_space)--}}
                                                {{--<a href="#" class="inactive">Registruotis--}}
                                                {{--<span>Nebegalioja</span></a>--}}
                                                {{--@else--}}
                                                {{--<a href="#" class="inactive">Registruotis--}}
                                                {{--<span>Liko vietų: {{ $event->total_free_space }}</span></a>--}}
                                                {{--@endif--}}
                                                {{--@endif--}}
                                                {{--</td>--}}

                                            @endforeach
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @endif
                        </div>
                    </div>



                    <div class="content">{!! $timetable->content !!}</div>
                </div>
            </div>
        </div>
    </section>

@endsection

@push('scripts.footer')
<script type="text/javascript">
    $(function () {
        var container = $('section.information .content'),
                tables = container.find('table');

        if (tables.length > 0) {
            tables.each(function () {
                $(this).addClass('table');
            });
        }
    });
</script>
@endpush


