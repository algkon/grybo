@extends('layouts.app')

@section('content')
    <section class="hero">
        <div class="container">
            <div class="row">
                <div class="app-login hidden-lg hidden-md text-center clearfix">
                    <a href="#" class="clearfix">
                        <i class="svg"></i>

                        <div>Naujiena! G7 - bet kur ir betkada, tavo telefone. Prisijunk!</div>
                    </a>
                </div>

                <div class="col-md-10">
                    @include('parts.hero-nav')

                    <div class="clearfix"></div>

                    <h1 class="title-bg">
                       {!! data_get(settings('custom_settings'),
                            'front.metadata.home_page') !!}
                    </h1>
                </div>

                <div class="clearfix"></div>

                @if ($posts_services->count())
                    <div class="col-md-12">

                        <ul class="clearfix">
                            @foreach($posts_services as $post)

                                <li>
                                    <div class="image">
                                        <a href="{{ $post->url() }}">
                                            <img src="{{ $post->thumbnail('home-post-large') }}"
                                                 alt="{{ $post->title }}" title="{{ $post->title }}"/>
                                        </a>
                                    </div>

                                    <div class="text-block">
                                        <h2 class="title title-sm">{{ $post->title }}</h2>

                                        <div class="text">
                                            {{ $post->short_description }}
                                        </div>

                                        <a href="{{ $post->url() }}" class="btn btn-default arrow">Daugiau</a>
                                    </div>
                                </li>

                            @endforeach
                        </ul>
                    </div> <!-- /services-list -->
                @endif

            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </section>

    <section class="articles">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <h3 class="title-bg">Naujienos</h3>

                    @if ($posts_first_column->count())
                        @foreach($posts_first_column as $post)
                            <article>
                                <div class="pull-left">
                                    <a href="{{ $post->url() }}">
                                        <img src="{{ $post->thumbnail('post-medium') }}" alt="{{ $post->title }}"
                                             title="{{ $post->title }}"/>
                                    </a>
                                </div>

                                <div class="pull-right">
                                    <header class="title-sm"><a href="{{ $post->url() }}">{{ $post->title }}</a></header>

                                    <p>
                                        {{ $post->short_description }}
                                    </p>

                                    <footer class="clearfix">
                                        <a href="{{ $post->url() }}" class="read-more">Skaityti daugiau</a>
                                        <time><?php print substr($post->created_at,0,10); ?></time>
                                    </footer>
                                </div>

                                <div class="clearfix"></div>
                            </article>
                        @endforeach
                    @endif
                </div>

                <div class="col-md-6 col-xs-12 coach-advises">
                    <h3 class="title-bg">Treneris pataria</h3>

                    @if ($posts_last_column->count())
                        @foreach($posts_last_column as $post)
                            <article>
                                <div class="pull-left">
                                    <a href="{{ $post->url() }}">
                                        <img src="{{ $post->thumbnail('post-medium') }}" alt="{{ $post->title }}"
                                             title="{{ $post->title }}"/>
                                    </a>
                                </div>

                                <div class="pull-right">
                                    <header class="title-sm"><a href="{{ $post->url() }}">{{ $post->title }}</a></header>

                                    <p>
                                        {{ $post->short_description }}
                                    </p>

                                    <footer class="clearfix">
                                        <a href="{{ $post->url() }}" class="read-more">Skaityti daugiau</a>
                                        <time><?php print substr($post->created_at,0,10); ?></time>
                                    </footer>
                                </div>

                                <div class="clearfix"></div>
                            </article>
                        @endforeach
                    @endif
                </div>

                <div class="clearfix"></div>
            </div>
            <!-- /row -->
        </div>
    </section>
@endsection
