
{heading_title_cell}<div class="col-xs-6 col-md-3 col-lg-2 time"><time class="text-center">{heading}</time></div>{/heading_title_cell}

{heading_previous_cell}<div class="col-xs-3 col-md-1 col-md-offset-7 col-lg-offset-8"><a class="btn btn-default prev-url" href="{previous_url}">&lt;&lt;</a></div>{/heading_previous_cell}
{heading_next_cell}<div class="col-xs-3 col-md-1 text-right"><a class="btn btn-default next-url" href="{next_url}">&gt;&gt;</a></div>{/heading_next_cell}


    {table_open}<div class="clearfix"></div><div class="table-responsive"><table class="table month">{/table_open}

        {heading_row_start}{/heading_row_start}
        {heading_row_end}{/heading_row_end}

        {week_row_start}<thead><tr>{/week_row_start}
            {week_day_cell}<th>{week_day}</th>{/week_day_cell}
        {week_row_end}</tr></thead>{/week_row_end}

        {cal_row_start}<tr>{/cal_row_start}
            {cal_cell_start}{/cal_cell_start}

                {cal_cell_content}<td class="has_content"><a href="{content}">{day}</a></td>{/cal_cell_content}
                {cal_cell_content_today}<td class="active has_content"><a href="{content}">{day}</a></td>{/cal_cell_content_today}
                {cal_cell_selected_day}<td class="active has_content"><a href="{content}">{day}</a></td>{/cal_cell_selected_day}

                {cal_cell_no_content}<td>{day}</td>{/cal_cell_no_content}
                {cal_cell_no_content_today}<td class="active">{day}</td>{/cal_cell_no_content_today}

                {cal_cell_blank} <td>&nbsp;</td>{/cal_cell_blank}

            {cal_cell_end}{/cal_cell_end}
        {cal_row_end}</tr>{/cal_row_end}

    {table_close}</table></div>{/table_close}

