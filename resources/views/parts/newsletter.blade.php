<form action="{{ route('subscribe') }}" method="post" class="ajax-form">
    {{ csrf_field() }}
    <label>Užsiprenumeruok naujienlaiškį</label>

    <div class="form-group">
        <input type="email" name="email" placeholder="El. paštas"/>
    </div>

    <div class="form-group submit">
        <input type="submit" name="submit" class="btn btn-default" value="Prenumeruoti"/>
    </div>

    <p class="message"></p>
    <p class="errors"></p>
</form>