<nav>
    <ul class="clearfix">
        <li class="prices"><a href="{{ route('memberships') }}">Paslaugos</a></li>
        {{--<li class="prices"><a href="{{ url('kainos') }}">Kainos</a></li>--}}
        {{--<li class="user-zone"><a href="{{ route('account.profile') }}">Vartotojo zona</a></li>--}}
        <li class="calendar"><a href="{{ route('employments') }}">Užsiėmimų tvarkaraštis</a></li>
        <li class="discount"><a href="{{ url('akcijos') }}">Akcijos</a></li>
        <li class="event"><a href="{{ url('renginiai') }}">Renginiai</a></li>
    </ul>
</nav>