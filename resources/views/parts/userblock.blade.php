@if(auth()->guest())
    <div class="user-area">
        <form action="{{ route('auth.login') }}" method="post" class="ajax-form" id="user-login">
            {{ csrf_field() }}
            <div class="pull-left">
                <div class="form-group with-bg first">
                    <input type="email" name="email" value="{{ old('email') }}" class="form-control" placeholder="Vartotojas"/>
                </div>

                <div class="form-group with-bg">
                    <input type="password" name="password" class="form-control" placeholder="Slaptažodis"/>
                </div>
            </div>

            <div class="pull-right">
                <div class="form-group link">
                    <ul>
                        <li><a href="{{ route('auth.register') }}">Dar nesi narys?</a></li>
                        <li><a href="{{ route('auth.passwords.reset') }}">Pamiršai slaptažodį?</a></li>
                    </ul>
                </div>

                <div class="form-group">
                    <input type="submit" name="submit" class="btn btn-default text-center"
                           value="Prisijungti"/>
                </div>
            </div>

            <div class="clearfix"></div>

            <p class="errors"></p>
        </form>
    </div>
@else
    <div class="user-area logged">
        <div class="col-md-6 pull-left">
            <ul>
                <li><span class="glyphicon glyphicon-user" aria-hidden="true"></span>{{ auth()->user()->api->first_name }}</li>
                <li class="settings"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span><a href="{{ route('account.profile') }}">Nustatymai</a></li>
                <li><span class="glyphicon glyphicon-remove" aria-hidden="true"></span><a href="{{ route('auth.logout') }}">Atsijungti</a></li>
            </ul>
        </div>

        <div class="col-md-6 pull-right text-left">
            <span>Likutis: <strong>{{ auth()->user()->api->balance_formated }} EUR</strong></span>
            <a href="{{ route('account.credit') }}" class="btn btn-default">Pildyti sąskaitą</a>
        </div>

        <div class="clearfix"></div>
    </div>
@endif