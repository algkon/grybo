<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Užsakymas apmokėtas | Grybo7.lt</title>
</head>

<body>

<div link="#c6d4df" alink="#c6d4df" vlink="#c6d4df" text="#bba978" style="font-family:Helvetica,Arial,sans-serif;font-size:12px;color:#bba978">
    <table style="width: 100%;max-width:538px;background-color:#393836" align="center" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
            <td style="background-color:#303437;text-align:center;padding:10px 0">
                <img src="http://www.grybo7.lt/assets/images/g7-logo.png" alt="Sporto klubas G7 Sportas - Sveikos gyvensenos edukologinis centras" height="65">
            </td>
        </tr>
        <tr>
            <td bgcolor="#bba978" style="padding:6px 0;">
                <table border="0" align="center" cellpadding="0" cellspacing="0" style="background-color:#fff;width:100%">

                    <tbody>
                    <tr>
                        <td style="padding:25px 15px 10px;color:#151515;font-size:12px">
                            <span style="font-size:16px;font-weight:bold">Sveiki {{ $order->first_name }},</span>

                            <p style="padding-top:5px">Jūsų užsakymas sėkmingai apmokėtas. Ačiū, kad pirkote!</p>

                            <br><br>

                            <strong>Užsakymas <span style="text-decoration: underline;">#{{ $order->order_id }}</span>:</strong>
                            <table style="border: 1px solid #bba978;width: 100%;margin: 5px auto">
                                @foreach($order->cart()->products as $product)
                                    <tr>
                                        <td style="padding: 4px 5px">Prekė:</td>
                                        <td style="padding: 4px 5px">{{ $product->name }}</td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 4px 5px">Kiekis/Laikotarpis:</td>
                                        <td style="padding: 4px 5px">{{ $product->options->set_membership_name }}</td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 4px 5px;@if($loop->iteration < $order->cart()->count)border-bottom: 1px dashed #ccc;@endif">Kaina:</td>
                                        <td style="padding: 4px 5px;@if($loop->iteration < $order->cart()->count)border-bottom: 1px dashed #ccc;@endif">{{ $product->subtotal }} €</td>
                                    </tr>
                                @endforeach
                            </table>
                            <table style="width: 100%;margin: 5px auto;text-align: right;">
                                <tr>
                                    <td>Suma(be PVM):</td>
                                    <td style="width: 60px">{{ $order->cart()->subtotal }} €</td>
                                </tr>
                                <tr>
                                    <td>PVM (21%):</td>
                                    <td style="width: 60px">{{ $order->cart()->tax }} €</td>
                                </tr>
                                <tr>
                                    <td><strong>Viso mokėti:</strong></td>
                                    <td style="width: 60px"><strong>{{ $order->cart()->total }} €</strong></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>

        <tr>
            <td style="text-align:center">
                <p style="font-size:12px;color:#bba978;margin:0;padding:12px 0">2015 © GRYBO 7. Visos teisės saugomos.</p>
            </td>
        </tr>
        </tbody>
    </table>
    <table align="center" style="color:#151515">
        <tbody>
        <tr>
            <td align="center" style="padding:5px 0">
                <small style="font-size:10px">Šis laiškas sugeruotas ir išsiųstas automatiniu būdu, todėl į jį atsakyti nereikia.</small>
            </td>
        </tr>
        </tbody>
    </table>
</div>

</body>

</html>
