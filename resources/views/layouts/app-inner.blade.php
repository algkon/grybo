<!DOCTYPE html>
<html lang="lt">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>@yield('meta_title', data_get(settings('custom_settings'), 'front.metadata.title'))</title>
    <meta name="description"
          content="@yield('meta_description', data_get(settings('custom_settings'), 'front.metadata.description'))">
    <meta name="keywords"
          content="@yield('meta_keywords', data_get(settings('custom_settings'), 'front.metadata.keywords'))">

    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/lightbox.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/style.css?v=1.000002') }}" rel="stylesheet"/>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
                'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1728685904010577');
        fbq('track', 'PageView');
    </script>
    <noscript>
        <img height="1" width="1" style="display:none"
             src="https://www.facebook.com/tr?id=1728685904010577&ev=PageView&noscript=1"
        />
    </noscript>
    @stack('scripts.head')
</head>
<body class="inner-page">
<header id="header-block">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <a href="{{ url('/') }}" id="logo"><img
                            src="{{ asset('img/G7sportas.png') }}" alt="G7 sportas"/></a>

                <ul class="links hidden-xs clearfix">
                    {{--
                    <li class="friends"><a href="{{ url('draugai') }}">Draugai</a></li>
                    --}}
                    <li class="contacts"><a href="{{ url('kontaktai') }}">Kontaktai</a></li>
                    <li class="rules"><a href="{{ url('taisykles') }}">Taisyklės</a></li>
                    <li class="cart">
                        <a href="{{ route('cart') }}">
                            <i class="glyphicon glyphicon-shopping-cart"></i>
                            <span>Krepšelis ({{ Cart::count() }})</span>
                        </a>
                    </li>
                </ul>

                <div class="time hidden-sm hidden-xs">
                    <span class="title">Darbo laikas</span>

                    <ul>
                        <li><span>I - V</span> {{ data_get(settings('custom_settings'),
                            'front.working_hours.working_days') }}
                        </li>
                        <li><span>VI - VII</span> {{ data_get(settings('custom_settings'),
                            'front.working_hours.weekdays') }}
                        </li>
                    </ul>
                </div>

                @include('parts.userblock')

                {{--
                <div class="language-block">--}}
                    {{--
                    <div class="dropdown-btn">--}}
                        {{--
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" --}}
                                {{--aria-haspopup="true" aria-expanded="true">Lt--}}
                            {{--
                        </button>
                        --}}

                        {{--
                        <ul class="dropdown-menu">--}}
                            {{--
                            <li><a href="#">En</a></li>
                            --}}
                            {{--
                            <li><a href="#">Ru</a></li>
                            --}}
                            {{--
                        </ul>
                        --}}
                        {{--
                    </div>
                    --}}
                    {{--
                </div>
                --}}

                <div class="menu-toggle" style="display: none;"></div>
            </div>
        </div>
    </div>

    @include('navs.main')
</header>

<section class="hero">
    <div class="container">
        <div class="row">
            <div class="app-login hidden-lg hidden-md text-center clearfix">
                <a href="#" class="clearfix">
                    <i class="svg"></i>
                    <div>Naujiena! G7 - bet kur ir betkada, tavo telefone. Prisijunk!</div>
                </a>
            </div>

            <div class="col-md-10">
                @include('parts.hero-nav')

                <div class="clearfix"></div>

                <h1 class="title-bg">
                    {{ data_get(settings('custom_settings'),
                           'front.metadata.home_page') }}
                </h1>
            </div>
        </div> <!-- /row -->
    </div> <!-- /container -->
</section>

@yield('content')

<footer class="main-footer">
    <section class="logos">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <ul class="clearfix">
                        <li>
                            <img src="{{ asset('img/lpg-logo.png') }}" alt="LPG" title="LPG"/>
                        </li>
                        <li>
                            <img src="{{ asset('img/clinique-logo.png') }}" alt="Clinique" title="Clinique"/>
                        </li>
                        <li>
                            <img src="{{ asset('img/technogym-logo.png') }}" alt="Technogym" title="Technogym"/>
                        </li>
                        <li>
                            <img src="{{ asset('img/lieknejimo-akademija-logo.png') }}" alt="Lieknėjimo akademija"
                                 title="Lieknėjimo akademija"/>
                        </li>
                        <li>
                            <img src="{{ asset('img/g7-grynas-maistas-logo.png') }}" alt="G7 grynas maistas"
                                 title="G7 grynas maistas"/>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section class="info-block">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-sm-12 pull-right">
                    <div class="row">
                        <div class="col-md-6 contacts">
                            <ul>
                                <li class="email"><a href="mailto:info@grybo7.lt">info@grybo7.lt</a></li>
                                <li class="phone"><a href="javascript:;">8 620 37777</a></li>
                                <li class="address"><a href="javascript:;">V. Grybo g. 7, Vilnius, LT-10313</a></li>
                                <li class="fb"><a href="https://www.facebook.com/sportoklubas/" target="_blank">G7
                                        Sportas</a></li>
                            </ul>
                            @include('parts.newsletter')
                        </div>
                        <div class="col-md-6" id="map">

                        </div>
                    </div>
                </div>

                <div class="col-md-5 col-sm-12 pull-left">
                    @include('navs.footer')
                </div>
            </div>
        </div>
    </section>

    <div class="container-fluid text-center">
        <div class="col-md-12"><?php print date('Y');?> &copy; GRYBO 7. Visos teisės saugomos.</div>
    </div>
</footer>

<div id="nav-overlay" style="display: none;"></div>

<script type="text/javascript">
    (function (atm, doIt) {
        // atm.order_match = '546'; // OPTIONAL: Order MATCH
        // atm.order_value = '19.99'; // OPTIONAL: Order value
        // atm.product_id = 'product-id'; // OPTIONAL: Product name or product ID

        // Do not modify any of the code below.
        atm.client_id = "f79c065ea50d1394e5897d8021f3cb46"; // CLIENT: mikroosam@gmail.com
        doIt(atm);
    })({}, function (d) {
        var a = document.createElement("iframe");
        a.style.cssText = "width: 0; height: 0; border: 0; position: absolute; left: -5px;";
        a.src = "javascript:false";
        var c = function () {
            setTimeout(function () {
                var c = a.contentDocument || a.contentWindow.document, b = c.createElement("script");
                b.src = "//static-trackers.adtarget.me/javascripts/pixel.min.js";
                b.id = "GIHhtQfW-atm-pixel";
                b["data-pixel"] = d.client_id;
                b["allow-flash"] = d.allow_flash;
                b.settings_obj = d;
                c.body.appendChild(b)
            }, 0)
        };
        a.addEventListener ? a.addEventListener("load", c, !1) : a.attachEvent ? a.attachEvent("onload", c) : a.onload = c;
        document.body.appendChild(a)
    });

    //  After the pixel above is loaded, you can use custom AJAX calls to register custom events (like button clicks, form fillouts and etc. )
    //__AtmUrls = window.__AtmUrls || [];
    //__AtmUrls.push('add-to-cart');
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/lightbox.min.js') }}"></script>
<script src="{{ asset('js/functions.js') }}"></script>
<script>
    image = window.location.origin  + '/img/g7.png';

    function initMap() {
        var uluru = {lat: 54.701450, lng: 25.315342};

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 15,
            center: uluru
        });

        var icons = {
            icon: image
        };

        var markerMap = new google.maps.Marker({
            position: uluru,
            map: map,
            icon : icons.icon
        });

        if ($('#mapContact').length) {
            var mapContact = new google.maps.Map(document.getElementById('mapContact'), {
                zoom: 15,
                center: uluru
            });

            var markerMapContact = new google.maps.Marker({
                position: uluru,
                map: mapContact,
                icon : icons.icon
            });
        }
    }
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDUi4x_vzZQbq0sq4GeU4z76BobRHhFF-g&callback=initMap">
</script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-4091042-75', 'auto');
    ga('send', 'pageview');
</script>
<script type="text/javascript">
    /* <![CDATA[ */
      var google_conversion_id = 1060208045;
      var google_custom_params = window.google_tag_params;
      var google_remarketing_only = true;
    /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1060208045/?guid=ON&amp;script=0"/>
    </div>
</noscript>
@stack('scripts.footer')
</body>
</html>