@extends('layouts.app-inner')

@section('meta_title', 'Krepšelis')

@section('content')

    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="page-header">
                        <h3 class="title-bg">Krepšelis</h3>
                    </div>

                    @include('components.notification')

                    <table class="table cart">
                        <thead>
                        <tr>
                            <th>Prekė</th>
                            <th class="text-center">Kiekis/Laikotarpis</th>
                            <th class="hidden-xs hidden-sm"></th>
                            <th class="text-center last">Kaina</th>
                            <th class="visible-xs-block visible-sm-block"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @if (Cart::count())
                            @foreach(Cart::content() as $id => $product)
                                <tr>
                                    <td>{{ $product->name }}</td>
                                    <td class="text-center">{{ $product->options['set_membership_name'] }}</td>
                                    <td class="text-center delete hidden-xs hidden-sm">
                                        {{ Form::open(['route' => ['cart.delete', $id], 'method' => 'DELETE' ]) }}
                                            <button class="btn btn-default btn-sm" type="submit">
                                                <span class="glyphicon glyphicon-remove"></span>
                                            </button>
                                        {{ Form::close() }}
                                    </td>
                                    <td class="text-center last">{{ $product->price }} €</td>
                                    <td class="text-center delete visible-xs-block visible-sm-block">
                                        {{ Form::open(['route' => ['cart.delete', $id], 'method' => 'DELETE' ]) }}
                                        <button class="btn btn-default btn-sm" type="submit">
                                            Pašalinti <span class="glyphicon glyphicon-remove"></span>
                                        </button>
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="4" class="hidden-xs hidden-sm last" style="padding: 15px;">&nbsp;</td>
                                <td colspan="1" class="visible-xs-block visible-sm-block" style="padding: 15px;">&nbsp;</td>
                            </tr>
                            <tr class="total">
                                <td colspan="3" class="text-right hidden-xs hidden-sm">Suma(be PVM):</td>
                                <td class="text-center" data-title="Suma(be PVM):">{{ Cart::subtotal() }} €</td>
                            </tr>
                            <tr class="total">
                                <td colspan="3" class="text-right hidden-xs hidden-sm">PVM (21%):</td>
                                <td class="text-center" data-title="PVM (21%):">{{ Cart::tax() }} €</td>
                            </tr>
                            <tr class="total">
                                <td colspan="3" class="text-right hidden-xs hidden-sm"><strong>Viso:</strong></td>
                                <td class="text-center" data-title="Viso:">{{ Cart::total() }} €</td>
                            </tr>
                        @else
                            <tr>
                                <td colspan="4" class="empty">Krepšelis tuščias</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>

                    <div class="row cart-buttons">
                        <div class="col-md-6"><a href="{{ route('memberships') }}" class="btn btn-default"><- Tęsti apsipirkimą</a></div>
                        @if (Cart::count())
                            <div class="col-md-6 text-right">
                                <a href="{{ url('/uzsakymo-forma') }}" class="btn btn-default">Pirkti -></a>
                            </div>
                        @endif
                    </div>

                </div>
            </div>
        </div>
    </section>

@endsection