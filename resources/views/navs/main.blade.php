@if (isset($nav[4]) && $nav[4] && $nav[4]->links)
    <nav id="navigation">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="clearfix navigation-list">
                        @foreach($nav[4]->links as $link)
                            @if ($link->slug == '#')
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" role="button" data-toggle="dropdown"
                                       aria-haspopup="true" aria-expanded="false">{{ $link->title }}</a>
                                    <div class="container-fluid dropdown-menu">
                                        <div class="container clearfix">
                                            @if($link->title == 'Apie mus')
                                                @include('navs.dropdown-block', ['id' => 9])
                                            @else
                                                @include('navs.dropdown-block', ['id' => 6])
                                                @include('navs.dropdown-block', ['id' => 7])
                                            @endif
                                        </div>
                                    </div>
                                </li>
                            @else
                                <li>
                                    <a href="{{ url($link->slug) }}" title="{{ $link->title }}" rel="{{ $link->rel }}"
                                       target="{{ $link->target }}">
                                        {{ $link->title }}
                                    </a>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                    <span id="close-menu" style="display: none;"></span>
                </div>
            </div>
        </div>
    </nav>

@endif