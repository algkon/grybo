@if (isset($nav[$id]) && $nav[$id] && $nav[$id]->links)
    <div class="block">
        <h2 class="">{{ $nav[$id]->title }}</h2>

        <ul>
            @foreach($nav[$id]->links as $link)
                <li>
                    <a href="{{ url($link->slug) }}" title="{{ $link->title }}" rel="{{ $link->rel }}"
                       target="{{ $link->target }}">
                        {{ $link->title }}
                    </a>
                </li>
            @endforeach
        </ul>
    </div>
@endif