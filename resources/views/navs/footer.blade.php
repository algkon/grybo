@if (isset($nav[5]) && $nav[5] && $nav[5]->links)

    @foreach($nav[5]->links->split(2) as $links_group)
        <ul class="pull-left clearfix">
            @foreach($links_group as $link)
                <li>
                    <a href="{{ url($link->slug) }}" title="{{ $link->title }}" rel="{{ $link->rel }}"
                       target="{{ $link->target }}">
                        {{ $link->title }}
                    </a>
                </li>
            @endforeach
        </ul>
    @endforeach

@endif