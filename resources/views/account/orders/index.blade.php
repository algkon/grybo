@extends('layouts.app-inner')

@section('meta_title', 'Užsakymų istorija')

@section('content')

    <section class="orders">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="page-header">
                        <h3 class="title-bg">Užsakymų istorija</h3>
                    </div>

                    <div class="table-responsive">
                        <table class="table medium">
                        <thead>
                        <tr>
                            <th class="text-left">Prekės pavadinimas</th>
                            {{--<th>Pardavimo vieta</th>--}}
                            <th class="text-center">Prekės tipas</th>
                            <th class="text-center">Prekės kaina(EUR)</th>
                            <th class="text-center">Data</th>
                        </tr>
                        </thead>

                        <tbody>
                        @if ($orders->count())
                            @foreach($orders as $key => $order)
                                <tr>
                                    <td>{{ $order['pard_prekes_pavadinimas'] }}</td>
                                    {{--                        <td>{{ $order['pard_tasko_pavadinimas'] }}</td>--}}
                                    <td class="text-center">{{ $order['pard_prekes_rusies_pav'] }}</td>
                                    <td class="text-center">{{ $order['pard_kaina'] }}</td>
                                    <td class="text-center">{{ $order['pard_laikas'] }}</td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="4">Užsąkymų nėra</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                    </div>
                </div>
            </div> <!-- /row -->
        </div>
    </section>

@endsection