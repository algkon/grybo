@extends('layouts.app-inner')

@section('meta_title', 'Keisti slaptažodį')

@section('content')

    <section class="change-password">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <div class="page-header">
                        <h3 class="title-bg">Keisti slaptažodį</h3>
                    </div>

                    @include('components.notification')

                    <div class="form-block">

                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/keisti-slaptazodi') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('new_password') ? ' has-error has-feedback' : '' }}">
                                <label for="new_password" class="col-md-4 control-label">Naujas slaptažodis</label>

                                <div class="col-md-8">
                                    {{ Form::password('new_password', ['class' => 'form-control']) }}
                                    @if ($errors->has('new_password'))
                                        <span class="glyphicon glyphicon-remove form-control-feedback"
                                              aria-hidden="true"></span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('new_password') ? ' has-error has-feedback' : '' }}{{ $errors->has('new_password_confirmation') ? ' has-error' : '' }}">
                                <label for="new_password_confirmation" class="col-md-4 control-label">Pakartoti naują slaptažodį</label>

                                <div class="col-md-8">
                                    {{ Form::password('new_password_confirmation', ['class' => 'form-control']) }}
                                    @if ($errors->has('new_password'))
                                        <span class="glyphicon glyphicon-remove form-control-feedback"
                                              aria-hidden="true"></span>
                                    @endif
                                </div>
                            </div>

                            <hr>

                            <div class="form-group{{ $errors->has('old_password') ? ' has-error has-feedback' : '' }}">
                                <label for="old_password" class="col-md-4 control-label">Patvirtinimui įveskite seną slaptažodį</label>

                                <div class="col-md-8">
                                    {{ Form::password('old_password', ['class' => 'form-control']) }}
                                    @if ($errors->has('old_password'))
                                        <span class="glyphicon glyphicon-remove form-control-feedback"
                                              aria-hidden="true"></span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-sm-12 col-xs-12 pull-right text-left">
                                    <input type="submit" name="submit" class="btn btn-default text-center" value="Išsaugoti">
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
