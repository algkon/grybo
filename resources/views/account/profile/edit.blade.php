@extends('layouts.app-inner')

@section('meta_title', 'Redaguoti informaciją')

@section('content')

    <section class="edit-profile">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-xs-12">
                    <div class="page-header">
                        <h3 class="title-bg">Redaguoti informaciją</h3>
                    </div>

                    @include('components.notification')

                    <div class="form-block">
                        {{ Form::model($model, ['url' => '/redaguoti-informacija', 'class' => 'form-horizontal']) }}

                        <div class="form-group{{ $errors->has('api.first_name') ? ' has-error has-feedback' : '' }}">
                            <label for="first_name" class="col-md-4 control-label">Vardas</label>

                            <div class="col-md-8">
                                {{ Form::text('api[first_name]', null, ['class' => 'form-control', 'id' => 'first_name']) }}
                                @if ($errors->has('api.first_name'))
                                    <span class="glyphicon glyphicon-remove form-control-feedback"
                                          aria-hidden="true"></span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('api.last_name') ? ' has-error has-feedback' : '' }}">
                            <label for="last_name" class="col-md-4 control-label">Pavardė</label>

                            <div class="col-md-8">
                                {{ Form::text('api[last_name]', null, ['class' => 'form-control', 'id' => 'last_name']) }}
                                @if ($errors->has('api.last_name'))
                                    <span class="glyphicon glyphicon-remove form-control-feedback"
                                          aria-hidden="true"></span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="last_name" class="col-md-4 control-label">Lytis</label>

                            <div class="radio-list col-md-8">
                                <div class="radio">
                                    <label class="{{ old('api.gender', $model->api->gender) == 1 ? 'active' : ''}}">
                                        {{--<input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>Pasirinkimas 1--}}
                                        {{ Form::radio('api[gender]', 1 ) }} Vyras
                                    </label>
                                </div>

                                <div class="radio">
                                    <label class="{{ old('api.gender', $model->api->gender) == 2 ? 'active' : ''}}">
                                        {{--<input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">--}}
                                        {{ Form::radio('api[gender]', 2 ) }} Moteris
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('api.national_id') ? ' has-error has-feedback' : '' }}">
                            <label for="email" class="col-md-4 control-label">Asmens kodas</label>

                            <div class="col-md-8">
                                {{ Form::text('api[national_id]', null, ['class' => 'form-control', 'id' => 'national_id', 'disabled']) }}

                                @if ($errors->has('api.national_id'))
                                    <span class="glyphicon glyphicon-remove form-control-feedback"
                                          aria-hidden="true"></span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('api.email') ? ' has-error has-feedback' : '' }}">
                            <label for="email" class="col-md-4 control-label">El. paštas</label>

                            <div class="col-md-8">
                                {{ Form::email('api[email]', null, ['class' => 'form-control', 'id' => 'email', 'disabled']) }}

                                @if ($errors->has('api.email'))
                                    <span class="glyphicon glyphicon-remove form-control-feedback"
                                          aria-hidden="true"></span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('api.member_card_number') ?  ' has-error has-feedback' : '' }}">
                            <label for="member_card_number" class="col-md-4 control-label">"Grybo 7" kliento kortelės nr.</label>

                            <div class="col-md-8">
                                {{ Form::text('api[member_card_number]', null, ['class' => 'form-control', 'id' => 'member_card_number']) }}

                                @if ($errors->has('api.member_card_number'))
                                    <span class="glyphicon glyphicon-remove form-control-feedback"
                                          aria-hidden="true"></span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('api.leisure_card_number') ?  ' has-error has-feedback' : '' }}">
                            <label for="leisure_card_number" class="col-md-4 control-label">"Laisvalaikio" kortelės nr.</label>

                            <div class="col-md-8">
                                {{ Form::text('api[leisure_card_number]', null, ['class' => 'form-control', 'id' => 'leisure_card_number']) }}

                                @if ($errors->has('api.leisure_card_number'))
                                    <span class="glyphicon glyphicon-remove form-control-feedback"
                                          aria-hidden="true"></span>
                                @endif
                            </div>
                        </div>

                        <hr>

                        <div class="form-group{{ $errors->has('api.phone') ? ' has-error has-feedback' : '' }}">
                            <label for="phone" class="col-md-4 control-label">Telefono numeris</label>

                            <div class="col-md-8">
                                {{ Form::text('api[phone]', null, ['class' => 'form-control', 'id' => 'phone']) }}

                                @if ($errors->has('api.phone'))
                                    <span class="glyphicon glyphicon-remove form-control-feedback"
                                          aria-hidden="true"></span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('api.country') ? ' has-error has-feedback' : '' }}">
                            <label for="country" class="col-md-4 control-label">Šalis</label>

                            <div class="col-md-8">
                                {{ Form::text('api[country]', 'LIETUVA', ['class' => 'form-control', 'id' => 'country', 'disabled']) }}

                                @if ($errors->has('api.country'))
                                    <span class="glyphicon glyphicon-remove form-control-feedback"
                                          aria-hidden="true"></span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('api.addr_town') ? ' has-error has-feedback' : '' }}">
                            <label for="addr_town" class="col-md-4 control-label">Miestas</label>

                            <div class="col-md-8">
                                {{ Form::text('api[addr_town]', null, ['class' => 'form-control', 'id' => 'addr_town']) }}

                                @if ($errors->has('api.addr_town'))
                                    <span class="glyphicon glyphicon-remove form-control-feedback"
                                          aria-hidden="true"></span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('api.addr_street') ? ' has-error has-feedback' : '' }}">
                            <label for="addr_street" class="col-md-4 control-label">Gatvė</label>

                            <div class="col-md-8">
                                {{ Form::text('api[addr_street]', null, ['class' => 'form-control', 'id' => 'addr_street']) }}

                                @if ($errors->has('api.addr_street'))
                                    <span class="glyphicon glyphicon-remove form-control-feedback"
                                          aria-hidden="true"></span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('api.addr_house') ? ' has-error has-feedback' : '' }}">
                            <label for="addr_house" class="col-md-4 control-label">Namo nr.</label>

                            <div class="col-md-8">
                                {{ Form::text('api[addr_house]', null, ['class' => 'form-control', 'id' => 'addr_house']) }}

                                @if ($errors->has('api.addr_house'))
                                    <span class="glyphicon glyphicon-remove form-control-feedback"
                                          aria-hidden="true"></span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('api.addr_flat') ? ' has-error has-feedback' : '' }}">
                            <label for="addr_flat" class="col-md-4 control-label">Buto nr.</label>

                            <div class="col-md-8">
                                {{ Form::text('api[addr_flat]', null, ['class' => 'form-control', 'id' => 'addr_flat']) }}

                                @if ($errors->has('api.addr_flat'))
                                    <span class="glyphicon glyphicon-remove form-control-feedback"
                                          aria-hidden="true"></span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('api.addr_post_code') ? ' has-error has-feedback' : '' }}">
                            <label for="addr_post_code" class="col-md-4 control-label">Pašto kodas</label>

                            <div class="col-md-8">
                                {{ Form::text('api[addr_post_code]', null, ['class' => 'form-control', 'id' => 'addr_post_code']) }}

                                @if ($errors->has('api.addr_post_code'))
                                    <span class="glyphicon glyphicon-remove form-control-feedback"
                                          aria-hidden="true"></span>
                                @endif
                                {{--@if ($errors->has('addr_post_code'))--}}
                                {{--<span class="help-block">--}}
                                {{--<strong>{{ $errors->first('addr_post_code') }}</strong>--}}
                                {{--</span>--}}
                                {{--@endif--}}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-sm-12 col-xs-12 pull-right text-left">
                                <input type="submit" name="submit" class="btn btn-default text-center"
                                       value="Išsaugoti">
                            </div>
                        </div>

                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
