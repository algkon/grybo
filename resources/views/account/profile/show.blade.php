@extends('layouts.app-inner')

@section('meta_title', 'Vartotojo zona')

@section('content')

    <section class="user-zone">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="page-header">
                        <h3 class="title-bg">Vartotojo zona</h3>
                    </div>

                    <div class="user-info pull-left">
                        <div class="name">
                            <span>Sveiki, {{ auth()->user()->api->first_name }}</span>
                            <a href="{{ route('auth.logout') }}">Atsijungti</a>
                        </div>

                        <div class="account-balance clearfix">
                            <div class="wrapper"><span>Likutis</span> {{ auth()->user()->api->balance_formated }} EUR</div>
                            <a href="{{ route('account.credit') }}" class="btn btn-default">Pildyti sąskaitą</a>
                        </div>

                        <span class="table-title">Mano informacija</span>

                        <table class="table my-info">
                            <tbody>
                            <tr>
                                <td>Vardas</td>
                                <td data-title="Vardas">{{ auth()->user()->api->first_name }}</td>
                                <td><a href="{{ route('account.profile.change') }}">Redaguoti informaciją</a></td>
                            </tr>

                            <tr>
                                <td>Pavardė</td>
                                <td data-title="Pavardė">{{ auth()->user()->api->last_name }}</td>
                                <td><a href="{{ route('account.password.change') }}">Keisti slaptažodį</a></td>
                            </tr>

                            {{--<tr>--}}
                                {{--<td>Amžius</td>--}}
                                {{--<td data-title="Amžius">23 m.</td>--}}
                                {{--<td><a href="{{ route('account.orders') }}">Užsakymų istorija</a></td>--}}
                            {{--</tr>--}}

                            <tr>
                                <td>Miestas</td>
                                <td data-title="Miestas">{{ auth()->user()->api->addr_town }}</td>
                                <td><a href="{{ route('account.orders') }}">Užsakymų istorija</a></td>
                            </tr>

                            <tr>
                                <td>Lytis</td>
                                <td data-title="Lytis">{{ auth()->user()->api->gender_label }}</td>
                            </tr>
                            </tbody>
                        </table>

                        <a href="{{ route('employments') }}" class="btn btn-default">Registruotis į užsiėmimus</a>
                    </div>

                    <div class="user-activity pull-right">
                        <span class="table-title">Mano aktyvumas</span>

                        <table class="table my-activity">
                            <tbody>
                            @if(auth()->user()->activeEmployments->count())
                                @foreach(auth()->user()->activeEmployments->take(8) as $employment)
                                    <tr>
                                        <td data-title="Data / Laikas">
                                            {{ $employment->employment_date }} / {{ $employment->time_range }}
                                        </td>
                                        <td data-title="Treniruotė">{{ $employment->employment_name }}</td>
                                        <td data-title="Treneris">({{ $employment->coach }})</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="3">Įrašų nėra</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>

                        <a href="{{ route('account.employments') }}" class="pull-right">Visas aktyvumas</a>
                    </div>

                    <div class="clearfix"></div>
                </div>
            </div> <!-- /row -->
        </div>
    </section>

@endsection