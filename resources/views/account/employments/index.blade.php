@extends('layouts.app-inner')

@section('meta_title', 'Aktyvumas')

@section('content')

    <section class="employments">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="page-header">
                        <h3 class="title-bg">Aktyvumas</h3>
                    </div>

                    <div class="table-responsive">
                        <table class="table medium">
                            <thead>
                            <tr>
                                <th class="text-left">Data/laikas nuo - iki</th>
                                <th>Užsiėmimo pavadinimas</th>
                                <th>Treneris/-ė</th>
                                <th class="text-center">Būsena</th>
                            </tr>
                            </thead>

                            <tbody>
                            @if ($employments->count())
                                @foreach($employments as $employment)
                                    <tr>
                                        <td>
                                            {{ $employment->employment_date }} / {{ $employment->time_range }}
                                        </td>
                                        <td>{{ $employment->employment_name }}</td>
                                        <td>{{ $employment->coach }}</td>
                                        <td class="text-center">{{ trans('status.employment.' . $employment->status) }}</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="3">Įrašų nėra</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /row -->
        </div>
    </section>

@endsection