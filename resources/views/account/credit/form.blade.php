@extends('layouts.app-inner')

@section('meta_title', 'Balanso pildymas')

@section('content')

    <section class="credit">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <div class="page-header">
                        <h3 class="title-bg">Balanso pildymas</h3>
                    </div>

                    @include('components.notification')

                    <div class="form-block">

                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/pildyti-saskaita') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('amount') ? ' has-error has-feedback' : '' }}">
                                <label for="amount" class="col-md-3 control-label">Suma</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="basic-addon2">€</span>
                                        <input id="amount" type="text" class="form-control" name="amount" value="{{ old('amount') }}">
                                    </div>
                                    @if ($errors->has('amount'))
                                        <span class="glyphicon glyphicon-remove form-control-feedback"
                                              aria-hidden="true"></span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-9 col-sm-12 col-xs-12 pull-right text-left">
                                    <input type="submit" name="submit" class="btn btn-default text-center" value="Apmokėti">
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
