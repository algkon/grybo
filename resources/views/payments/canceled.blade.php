@extends('layouts.app-inner')

@section('meta_title', 'Apmokėjimas atšauktas')

@section('content')

    <section class="information">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-xs-12">

                    <div class="alert alert-danger alert-styled-left alert-bordered">
                        {{--<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>--}}
                        <p>Apmokėjimas atšauktas.</p>
                    </div>

                </div>
            </div>
        </div>
    </section>

@endsection


