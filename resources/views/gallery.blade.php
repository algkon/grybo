@extends('layouts.app-inner')

@section('meta_title', $gallery->meta_title)
@section('meta_description', $gallery->meta_description)
@section('meta_keywords', $gallery->meta_keywords)

@section('content')

    <section class="gallery single">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="page-header">
                        <h3 class="title-bg">{{ $gallery->title }}</h3>
                    </div>

                    <ul class="galeries-list preview clearfix">
                        @foreach($gallery->images as $image)
                            <li>
                                <a class="fancy-gallery" data-lightbox="image-gallery" href="{{ $image->path('gallery-large') }}">
                                    <img src="{{ $image->path('gallery-thumbnail') }}" alt="{{ $gallery->title }}" title="{{ $gallery->title }}" />
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </section>

@endsection