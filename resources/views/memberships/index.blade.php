@extends('layouts.app-inner')

@section('meta_title', 'Paslaugos')

@section('content')

    <section class="credit">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="page-header">
                        <h3 class="title-bg">Paslaugos</h3>
                    </div>

                    @include('components.notification')

                    <div class="content">
                        <p class="text-center">
                            <img src="{{ asset('uploads/Pranešimai/2017.png') }}" alt="kainos">
                        </p>
                    </div>
                    <table class="table memberships">

                        <thead>
                        <tr>
                            <th>Kategorija</th>
                            <th>Paslauga</th>
                            <th>Kaina</th>
                            <th></th>
                        </tr>
                        </thead>

                        <tbody>
                        @if ($grouped_products)
                            @foreach($grouped_products as $id => $group)
                                <tr>
                                    <td data-title="Data/laikas">{{ $group->first()['product_category_name'] }}</td>
                                    <td data-title="Paslauga" class="dropdown">
                                        <div class="form-block">
                                            {{ Form::open(['route' => 'cart.add', 'id' => 'form-' . $id ]) }}
                                                <div class="form-group">
                                                    {{ Form::memberships(
                                                        'id',
                                                        $group->pluck('product_name', 'product_id'),
                                                        null,
                                                        ['class' => 'form-control', 'onchange' => 'update_price(this);', 'memberships-' . $id],
                                                        $grouped_products
                                                    ) }}
                                                </div>
                                            {{ Form::close() }}
                                        </div>
                                    </td>
                                    <td data-title="Kaina" class="text-center">
                                        <span class="price">{{ $group->first()['price'] }}</span> €
                                    </td>
                                    <td class="text-center add-to-cart">
                                        <button class="btn btn-default" form="form-{{ $id }}">
                                            <span class="glyphicon glyphicon-shopping-cart"></span>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="5">Užsąkymų nėra</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

@endsection

@push('scripts.head')
<script type="text/javascript">
    function update_price(element) {
        var selected = $(element).find('option:selected'),
            price    = selected.data('price');

        selected.closest('tr').find('span.price').text(price);
    }
</script>
@endpush