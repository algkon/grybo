<div class="sidebar sidebar-main">
    <div class="sidebar-content">

        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">
                    <li>
                        <a href="{{ route('admin.orders.index') }}">
                            <i class="icon-coin-dollar"></i>
                            <span>
                                {{ trans('admin::routes.admin.orders.index') }}
                                @if ($new_orders_count)
                                    <span class="badge badge-warning">{{ $new_orders_count }}</span>
                                @endif
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('admin.employments.users.index') }}">
                            <i class="icon-calendar2"></i>
                            <span>
                                {{ trans('admin::routes.admin.employments.users.index') }}
                                @if ($employments_users_requests_count)
                                    <span class="badge badge-warning">{{ $employments_users_requests_count }}</span>
                                @endif
                            </span>
                        </a>
                    </li>
                    <li class="navigation-divider"></li>
                    <li>
                        <a href="{{ route('admin.pages.index') }}"><i class="icon-file-empty"></i>
                            <span>{{ trans('admin::routes.admin.pages.index') }}</span></a>
                        <ul>
                            <li class="{{ active_class(is_current_route('admin.pages.index')) }}">
                                <a href="{{ route('admin.pages.index') }}">
                                    {{ trans('admin::routes.admin.pages.index') }}
                                </a>
                            </li>
                            <li class="{{ active_class(is_current_route('admin.pages.create')) }}">
                                <a href="{{ route('admin.pages.create') }}">
                                    {{ trans('admin::routes.admin.pages.create') }}
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{ route('admin.posts.index') }}"><i class="icon-file-text2"></i>
                            <span>{{ trans('admin::routes.admin.posts.index') }}</span></a>
                        <ul>
                            <li class="{{ active_class(is_current_route('admin.posts.index') && !request()->has('filter.category_id')) }}">
                                <a href="{{ route('admin.posts.index') }}">
                                    {{ trans('admin::routes.admin.posts.index') }}
                                </a>
                            </li>
                            <li class="{{ active_class(is_current_route('admin.posts.create')) }}">
                                <a href="{{ route('admin.posts.create') }}">
                                    {{ trans('admin::routes.admin.posts.create') }}
                                </a>
                            </li>
                            @if ($admin_menu_categories->count())
                                <li class="navigation-divider"></li>

                                @foreach($admin_menu_categories as $category)
                                    <li class="{{ active_class(is_current_route('admin.posts.index') && request('filter.category_id') == $category->id) }}">
                                        <a href="{{ route('admin.posts.index', ['filter' => ['category_id' => $category->id]]) }}">
                                            {{ $category->title }}
                                        </a>
                                    </li>
                                @endforeach
                            @endif
                        </ul>
                    </li>
                    <li>
                        <a href="{{ route('admin.galleries.index') }}"><i class="icon-images2"></i>
                            <span>{{ trans('admin::routes.admin.galleries.index') }}</span></a>
                        <ul>
                            <li class="{{ active_class(is_current_route('admin.galleries.index')) }}">
                                <a href="{{ route('admin.galleries.index') }}">
                                    {{ trans('admin::routes.admin.galleries.index') }}
                                </a>
                            </li>
                            <li class="{{ active_class(is_current_route('admin.galleries.create')) }}">
                                <a href="{{ route('admin.galleries.create') }}">
                                    {{ trans('admin::routes.admin.galleries.create') }}
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{ route('admin.categories.index') }}"><i class="icon-indent-increase"></i>
                            <span>{{ trans('admin::routes.admin.categories.index') }}</span></a>
                        <ul>
                            <li class="{{ active_class(is_current_route('admin.categories.index')) }}">
                                <a href="{{ route('admin.categories.index') }}">
                                    {{ trans('admin::routes.admin.categories.index') }}
                                </a>
                            </li>
                            <li class="{{ active_class(is_current_route('admin.categories.create')) }}">
                                <a href="{{ route('admin.categories.create') }}">
                                    {{ trans('admin::routes.admin.categories.create') }}
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{ route('admin.menu.index') }}"><i class="icon-menu7"></i>
                            <span>{{ trans('admin::routes.admin.menu.index') }}</span></a>
                        <ul>
                            <li class="{{ active_class(is_current_route('admin.menu.index')) }}">
                                <a href="{{ route('admin.menu.index') }}">
                                    {{ trans('admin::routes.admin.menu.index') }}
                                </a>
                            </li>
                            <li class="{{ active_class(is_current_route('admin.menu.create')) }}">
                                <a href="{{ route('admin.menu.create') }}">
                                    {{ trans('admin::routes.admin.menu.create') }}
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="{{ active_class(is_current_route('admin.users.edit')) }}">
                        <a href="{{ route('admin.users.index') }}"><i class="icon-users"></i>
                            <span>{{ trans('admin::routes.admin.users.index') }}</span>
                        </a>
                        <ul>
                            <li class="{{ active_class(is_current_route('admin.users.index')) }}">
                                <a href="{{ route('admin.users.index') }}">{{ trans('admin::routes.admin.users.index') }}</a>
                            </li>
                            <li class="{{ active_class(is_current_route('admin.users.create')) }}">
                                <a href="{{ route('admin.users.create') }}">{{ trans('admin::routes.admin.users.create') }}</a>
                            </li>
                            <li class="navigation-divider"></li>
                            <li class="{{ active_class(is_current_route('admin.users.access.index')) }}">
                                <a href="{{ route('admin.users.access.index') }}">
                                    {{ trans('admin::routes.admin.users.access.index') }}
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{ route('admin.employments.group.index') }}">
                            <i class="icon-lifebuoy"></i>
                            <span>{{ trans('admin::routes.admin.employments.group.index') }}</span>
                        </a>
                        <ul>
                            <li class="{{ active_class(is_current_route('admin.employments.group.index') || is_current_route('admin.employments.group.dates.index')) }}">
                                <a href="{{ route('admin.employments.group.index') }}">
                                    {{ trans('admin::routes.admin.employments.group.index') }}
                                </a>
                            </li>
                            <li class="{{ active_class(is_current_route('admin.employments.group.create')) }}">
                                <a href="{{ route('admin.employments.group.create') }}">
                                    {{ trans('admin::routes.admin.employments.group.create') }}
                                </a>
                            </li>
                            <li class="navigation-divider"></li>
                            <li class="{{ active_class(is_current_route('admin.employments.group.calendar')) }}">
                                <a href="{{ route('admin.employments.group.calendar') }}">
                                    {{ trans('admin::routes.admin.employments.group.calendar') }}
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{ route('admin.timetables.index') }}"><i class="icon-calendar"></i>
                            <span>{{ trans('admin::routes.admin.timetables.index') }}</span></a>
                        <ul>
                            <li class="{{ active_class(is_current_route('admin.timetables.index')) }}">
                                <a href="{{ route('admin.timetables.index') }}">
                                    {{ trans('admin::routes.admin.timetables.index') }}
                                </a>
                            </li>
                            <li class="{{ active_class(is_current_route('admin.timetables.create')) }}">
                                <a href="{{ route('admin.timetables.create') }}">
                                    {{ trans('admin::routes.admin.timetables.create') }}
                                </a>
                            </li>
                            {{--<li class="navigation-divider"></li>--}}
                            {{--<li class="{{ active_class(is_current_route('admin.employments.calendar')) }}">--}}
                                {{--<a href="{{ route('admin.employments.calendar') }}">--}}
                                    {{--{{ trans('admin::routes.admin.employments.calendar') }}--}}
                                {{--</a>--}}
                            {{--</li>--}}
                        </ul>
                    </li>
                    <li>
                        <a href="{{ route('admin.subscribers.index') }}"><i class="icon-mail5"></i>
                            <span>{{ trans('admin::routes.admin.subscribers.index') }}</span></a>
                        <ul>
                            <li class="{{ active_class(is_current_route('admin.subscribers.index')) }}">
                                <a href="{{ route('admin.subscribers.index') }}">
                                    {{ trans('admin::routes.admin.subscribers.index') }}
                                </a>
                            </li>
                            <li class="{{ active_class(is_current_route('admin.subscribers.mail')) }}">
                                <a href="{{ route('admin.subscribers.mail') }}">
                                    {{ trans('admin::routes.admin.subscribers.mail') }}
                                </a>
                            </li>
                        </ul>
                    </li>

                    <!-- /main -->

                </ul>
            </div>
        </div>
        <!-- /main navigation -->

    </div>
</div>