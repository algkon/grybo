<div class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ route('admin.index') }}"><img src="{{ asset('img/svg/logo.svg') }}" alt=""></a>

        <ul class="nav navbar-nav visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>

        <div class="navbar-right">
            <ul class="nav navbar-nav">
                @if (! auth('admin')->guest())
                    <li>
                        <a href="{{ route('admin.settings.edit') }}"><i class="icon-gear"></i></a>
                    </li>
                    <li class="dropdown dropdown-user">
                        <a class="dropdown-toggle" data-toggle="dropdown">
{{--                            <img src="{{ asset('vendors/admin/images/demo/users/face11.jpg') }}" alt="">--}}
                            <span>{{ auth('admin')->user()->name }}</span>
                            <i class="caret"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="{{ route('admin.logout') }}"><i class="icon-switch2"></i> Atsijungti</a></li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</div>