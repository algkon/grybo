@if ($role->permissions->count())
    @foreach($role->permissions as $permission)
        @if ($loop->last)
            <span class="label label-empty text-grey-600">{{ $permission->label }}</span>
        @else
            <span class="label label-empty text-grey-600">{{ $permission->label }}</span>,
        @endif
    @endforeach
@else
    -----
@endif