@if ($model->status == 1)
    <span class="label label-success">Užregistruotas</span>
@elseif ($model->status == 2)
    @if (isset($pending_dropdown) && $pending_dropdown === true)
        <div class="btn-group">
            <a href="#" class="label bg-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Laukia patvirtinimo <span class="caret"></span></a>

            <ul class="dropdown-menu dropdown-menu-right">
                <li>
                    {{ Form::model($model, [
                                'url' => ['admin/employments/users', $model->id],
                                'method' => 'PUT',
                            ]) }}
                    {{ Form::hidden('status', 1) }}
                    {{ Form::close() }}
                    <a href="javascript:void(0);" onclick="employment_user_accept(this);" style="padding: 5px 15px;">
                        <span class="label label-success label-block label-icon" style="float: none;margin-right: 0;">
                            <i class="icon-checkmark" style="font-size: 10px;"></i> Patvirtinti
                        </span>
                    </a>
                </li>
                <li>
                    {{ Form::model($model, [
                                'url' => ['admin/employments/users', $model->id],
                                'method' => 'PUT',
                            ]) }}
                    {{ Form::hidden('status', 0) }}
                    {{ Form::close() }}
                    <a href="javascript:void(0);" onclick="employment_user_cancel(this);" style="padding: 5px 15px;">
                        <span class="label label-danger label-block label-icon" style="float: none;margin-right: 0;">
                            <i class="icon-cross2" style="font-size: 10px;"></i> Atmesti
                        </span>
                    </a>
                </li>
            </ul>
        </div>
    @else
        <span class="label label-primary">Laukia patvirtinimo</span>
    @endif
@else
    <span class="label label-danger">Atšaukta</span>
@endif