@if ($model->isActive() && $model->isNow())
    <span class="label label-primary">Vyksta dabar</span>
@elseif($model->isActive() && !$model->isEnded())
    <span class="label label-success">Galiojanti</span>
@else
    <span class="label label-danger">Nebegalioja</span>
@endif