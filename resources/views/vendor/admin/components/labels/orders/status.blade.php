@if ($order->status == 0)
    <span class="label label-default">Laukiama apmokėjimo</span>
    {{--<span class="label label-warning">Naujas</span>--}}
@elseif ($order->status == 1)
    <span class="label label-info">Apmokėta: {{ $order->payed_at }}</span>
@elseif ($order->status == 2)
    <span class="label label-success">Apmokėta ir patvirtinta</span>
@else
    <span class="label label-danger">Atšaukta</span>
@endif
