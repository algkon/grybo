@if ($errors->has($name))
    <label id="{{ $name }}-error" class="validation-error-label" for="{{ $name }}">{{ $errors->first($name) }}</label>
@endif