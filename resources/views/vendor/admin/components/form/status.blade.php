<div class="form-group">
    <label class="control-label col-lg-3">Rodoma</label>

    <div class="col-lg-9">
        <div class="checkbox checkbox-switch">
            {{ Form::hidden('active', 0) }}
            @if (isset($disabled) && $disabled)
                {{ Form::checkbox('active', 1, null, [
                    'class' => 'switch',
                    'data-on-color' => 'success',
                    'data-off-color' => 'danger',
                    'data-on-text' => 'Taip',
                    'data-off-text' => 'Ne',
                    'data-size' => 'small',
                    'readonly' => true
                ]) }}
            @else
                {{ Form::checkbox('active', 1, null, [
                    'class' => 'switch',
                    'data-on-color' => 'success',
                    'data-off-color' => 'danger',
                    'data-on-text' => 'Taip',
                    'data-off-text' => 'Ne',
                    'data-size' => 'small'
                ]) }}
            @endif
        </div>
    </div>
</div>