<fieldset>
    <legend class="text-semibold">
        <i class="icon-images2 position-left"></i>
        Galerijos
        <a class="control-arrow" data-toggle="collapse" data-target="#collapse-gallery">
            <i class="icon-circle-down2"></i>
        </a>
    </legend>

    <div class="collapse in" id="collapse-gallery">

        <div class="row">
            <div class="col-xs-12">
                <button type="button" id="gallery_add" class="btn btn-default">
                    Pridėti galeriją
                </button>
            </div>
        </div>

        <div class="row" id="galleries">
            @foreach(old('galleries', $model->galleries->toArray()) as $gallery)
                <div class="col-lg-3 col-sm-6">
                    <div class="thumbnail">
                        <input type="hidden" name="galleries[{{ $gallery['id'] }}][id]"
                               value="{{ $gallery['id'] }}" class="gallery_id"/>
                        <input type="hidden" name="galleries[{{ $gallery['id'] }}][title]"
                               value="{{ $gallery['title'] }}"/>
                        <input type="hidden" name="galleries[{{ $gallery['id'] }}][thumbnail_medium]"
                               value="{{ $gallery['thumbnail_medium'] }}"/>

                        <div class="thumb">
                            <img src="{{ $gallery['thumbnail_medium'] }}" alt="">

                            <div class="caption-overflow">
                                <span>
                                    <a href="{{ route('admin.galleries.edit', [$gallery['id']]) }}" target="_blank"
                                       class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5">
                                        <i class="icon-pencil"></i>
                                    </a>
                                    <a href="javascript:void(0)" onclick="gallery_delete(this);"
                                       class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5">
                                        <i class="icon-trash"></i>
                                    </a>
                                </span>
                            </div>
                        </div>

                        <div class="caption" data-popup="tooltip" data-placement="bottom"
                             data-original-title="{{ $gallery['title'] }}">
                            {{ $gallery['title'] }}
                        </div>

                    </div>
                </div>
            @endforeach
        </div>

    </div>
</fieldset>