<fieldset>
    <legend class="text-semibold">
        <i class="icon-price-tag2 position-left"></i>
        Metaduomenys
        <a class="control-arrow" data-toggle="collapse" data-target="#collapse-meta">
            <i class="icon-circle-down2"></i>
        </a>
    </legend>

    <div class="collapse in" id="collapse-meta">

        <div class="form-group">
            <label class="control-label col-lg-3">Pavadinimas</label>

            <div class="col-lg-9">
                {{ Form::text('metadata[title]', null, ['class' => 'form-control']) }}
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-lg-3">Aprašymas</label>

            <div class="col-lg-9">
                {{ Form::textarea('metadata[description]', null, ['class' => 'form-control', 'rows' => 5]) }}
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-lg-3">Raktažodžiai</label>

            <div class="col-lg-9">
                {{ Form::textarea('metadata[keywords]', null, ['class' => 'form-control', 'rows' => 5]) }}
            </div>
        </div>

    </div>
</fieldset>