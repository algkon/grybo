@if ($categories->count())
    <div class="panel panel-white widget widget-categories">
        <div class="panel-body">
            <fieldset>
                <legend class="text-semibold">
                    <i class="icon-indent-increase position-left"></i>
                    Kategorijos
                    <a class="control-arrow" data-toggle="collapse" data-target="#widget-categories">
                        <i class="icon-circle-down2"></i>
                    </a>
                </legend>
                <div class="collapse in" id="widget-categories">

                    @foreach($categories as $category)
                        <div class="checkbox">
                            <label>
                                {{ Form::checkbox('categories[]', $category->id, null, ['class' => 'styled', 'data-label' => $category->title]) }}
                                {{ $category->title }}
                            </label>
                        </div>

                        @if ($category->child)
                            @foreach($category->child as $child_category)
                                <div class="checkbox child">
                                    <label>
                                        {{ Form::checkbox('categories[]', $child_category->id, null, ['class' => 'styled', 'data-label' => $child_category->title]) }}
                                        {{ $child_category->title }}
                                    </label>
                                </div>
                            @endforeach
                        @endif
                    @endforeach
                    <hr>
                    <div class="col-xs-12">
                        <div class="form-group {{ has_error($errors, 'category_id') }}">
                            <label for="category_id">Pagrindinė kategorija <span class="text-danger">*</span></label>
                            {{ Form::select('category_id', dropdown_list($model->categories->pluck('title', 'id'), ["" => str_repeat('-', 15)]), null, ['class' => 'category form-control', 'id' => 'category_id']) }}
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
@endif