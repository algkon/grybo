<div class="panel panel-white widget widget-post-info">
    <div class="panel-body">
        <fieldset>
            <legend class="text-semibold">
                <i class="icon-info3 position-left"></i>
                Įrašo informacija
                <a class="control-arrow" data-toggle="collapse" data-target="#widget-post-info">
                    <i class="icon-circle-down2"></i>
                </a>
            </legend>
            <div class="collapse in" id="widget-post-info">
                <strong>Author: </strong> {{ $model->getAuthorName() }}
                <br>
                <strong>Created: </strong> {{ $model->created_at }}
                <br>
                <strong>Updated: </strong> {{ $model->updated_at }}
            </div>
        </fieldset>
    </div>
</div>