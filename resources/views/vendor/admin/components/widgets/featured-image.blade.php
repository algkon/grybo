<div class="panel panel-white widget widget-featured-image">
    <div class="panel-body">
        <fieldset>
            <legend class="text-semibold">
                <i class="icon-image2 position-left"></i>
                Prisegtas paveikslėlis
                <a class="control-arrow" data-toggle="collapse" data-target="#widget-featured-image">
                    <i class="icon-circle-down2"></i>
                </a>
            </legend>
            <div class="collapse in text-center" id="widget-featured-image">
                @if($attachment = old('attachment', $model->attachment))
                    <div class="thumbnail">
                        <input type="hidden" name="attachment[name]" value="{{ $attachment['name'] }}"/>
                        <input type="hidden" name="attachment[path]" value="{{ $attachment['path'] }}"/>
                        <input type="hidden" name="attachment[full_path_medium]" value="{{ $attachment['full_path_medium'] }}"/>
                        <input type="hidden" name="attachment[full_path_original]" value="{{ $attachment['full_path_original'] }}"/>

                        <div class="thumb"><img src="{{ $attachment['full_path_medium'] }}" alt=""></div>
                    </div>

                    <a href="javascript:void(0)" onclick="delete_attachment();" class="btn btn-danger" type="button">
                        Pašalinti paveikslėlį
                    </a>
                @else
                    <a href="javascript:void(0)" id="attach_image" class="btn btn-default" type="button">Pasirinkti paveikslėlį</a>
                @endif

            </div>
        </fieldset>
    </div>
</div>

@push('scripts.head')
<script type="text/javascript" src="{{ asset('vendors/admin/js/pages/attachment.js') }}" async></script>
@endpush