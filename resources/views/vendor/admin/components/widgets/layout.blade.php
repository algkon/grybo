@inject('layout', 'Admin\Extensions\Layout\Layout')

<div class="panel panel-white widget widget-categories">
    <div class="panel-body">
        <fieldset>
            <legend class="text-semibold">
                <i class="icon-insert-template position-left"></i>
                Atvaizdavimas
                <a class="control-arrow" data-toggle="collapse" data-target="#widget-layout">
                    <i class="icon-circle-down2"></i>
                </a>
            </legend>
            <div class="collapse in" id="widget-layout">
                <div class="col-xs-12">
                    <div class="form-group {{ has_error($errors, 'layout') }}">
                        <label>Puslapio tipas</label>
                        {{ Form::select('layout[name]', dropdown_list($layout->getList(), ["" => str_repeat('-', 15)]), null, ['class' => 'form-control']) }}
                    </div>
                </div>
            </div>
        </fieldset>
    </div>
</div>