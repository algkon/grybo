@if ($errors->hasBag())
    <div class="alert alert-danger alert-styled-left alert-bordered">
        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Uždaryti</span></button>
        <p>{{ trans('admin::validation.has_errors') }}</p>
    </div>
@endif