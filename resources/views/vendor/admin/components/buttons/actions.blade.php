<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <i class="icon-menu9"></i>
        </a>

        <ul class="dropdown-menu dropdown-menu-right">
            @if (!isset($view) || $view)
                <li>
                    <a href="{{ route('admin.' . (isset($routes['view']) ? $routes['view'] : $model->getTable() . '.show'), $model->id) }}">
                        <i class="icon-file-eye"></i>Peržiūrėti
                    </a>
                </li>
            @endif

            @if (!isset($edit) || $edit)
                <li>
                    <a href="{{ route('admin.' . (isset($routes['edit']) ? $routes['edit'] : $model->getTable() . '.edit'), $model->id) }}" title="Redaguoti">
                        <i class="icon-pencil7"></i>Redaguoti
                    </a>
                </li>
            @endif

            @if (!isset($delete) || $delete)
                <li>
                    {{ Form::open([
                        'route' => ['admin.' . (isset($routes['destroy']) ? $routes['destroy'] : $model->getTable() . '.destroy'), $model->id],
                        'method' => 'delete',
                        'class' => 'destroy',
                        'data-confirm' => 'Are you sure you want to delete?',
                    ]) }}
                    {{ Form::close() }}
                    <a href="#" onclick="return $.fn.currentDatatable.sweetDelete(this);" title="Ištrinti">
                        <i class="icon-trash"></i>Ištrinti
                    </a>
                </li>
            @endif
        </ul>
    </li>
</ul>