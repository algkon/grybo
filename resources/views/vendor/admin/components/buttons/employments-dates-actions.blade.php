<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <i class="icon-menu9"></i>
        </a>

        <ul class="dropdown-menu dropdown-menu-right">

            @if(!$date->isNow() && $date->isActive() && !$date->isEnded())
                @if ($date->users->count())
                    <li>
                        <a class="btn disabled" style="text-align: left;">
                            <i class="icon-pencil7"></i>Redaguoti
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        {{ Form::open([
                            'route' => ['admin.employments.group.dates.cancel', $date->employment_id, $date->id],
                            'method' => 'put',
                            'class' => 'cancel'
                        ]) }}
                        {{ Form::close() }}
                        <a href="#" onclick="employment_date_cancel(event, this);" title="Atšaukti ir siųsti pranešimą">
                            <i class="icon-cross3"></i>Atšaukti
                        </a>
                    </li>
                @else
                    <li>
                        <a href="{{ route('admin.employments.group.dates.edit', [$date->employment_id, $date->id]) }}"
                           onclick="employment_date_edit(event, this);">
                            <i class="icon-pencil7"></i>Redaguoti
                        </a>
                    </li>
                    <li class="divider">
                    <li>
                        {{ Form::open([
                            'route' => ['admin.employments.group.dates.destroy', $date->employment_id, $date->id],
                            'method' => 'delete',
                            'class' => 'destroy'
                        ]) }}
                        {{ Form::close() }}
                        <a href="#" onclick="return $.fn.currentDatatable.sweetDelete(this);" title="Ištrinti">
                            <i class="icon-trash"></i>Ištrinti
                        </a>
                    </li>
                    </li>
                @endif

            @elseif ($date->isEnded() || !$date->isActive())
                <li>
                    {{ Form::open([
                        'route' => ['admin.employments.group.dates.destroy', $date->employment_id, $date->id],
                        'method' => 'delete',
                        'class' => 'destroy'
                    ]) }}
                    {{ Form::close() }}
                    <a href="#" onclick="return $.fn.currentDatatable.sweetDelete(this);" title="Ištrinti">
                        <i class="icon-trash"></i>Ištrinti
                    </a>
                </li>
            @else
                <li><a>Veiksmus bus galima atlikti pasibaigus užsiėmimui</a></li>
            @endif

        </ul>
    </li>
</ul>