@extends('admin::layouts.crud.single')

@section('content')

    {{ Form::model($model, ['url' => ['admin/menu', $model->id], 'method' => $method, 'class' => 'form-horizontal']) }}
    {{ Form::hidden('user_id', auth('admin')->user()->getAuthIdentifier()) }}
    {{ Form::hidden('links', '') }}
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">

                    <fieldset>
                        <legend class="text-semibold">
                            <i class="icon-file-empty position-left"></i>
                            Pagrindinė meniu informacija
                            <a class="control-arrow" data-toggle="collapse" data-target="#collapse-main">
                                <i class="icon-circle-down2"></i>
                            </a>
                        </legend>

                        <div class="collapse in" id="collapse-main">
                            <div class="form-group {{ has_error($errors, 'title') }} {{ has_error($errors, 'slug') }}">
                                <label class="control-label col-lg-3">Pavadinimas <span
                                            class="text-danger">*</span></label>

                                <div class="col-lg-9">
                                    {{ Form::text('title', null, ['class' => 'form-control']) }}
                                </div>
                            </div>

                            @include('admin::components.form.status')
                        </div>
                    </fieldset>

                    <fieldset>
                        <legend class="text-semibold">
                            <i class="icon-indent-increase position-left"></i>
                            Nuorodos
                            <a class="control-arrow" data-toggle="collapse" data-target="#collapse-content">
                                <i class="icon-circle-down2"></i>
                            </a>
                        </legend>
                        <div class="collapse in" id="collapse-content">
                            <div class="row">
                                <div class="col-lg-3">
                                    <button type="button" class="btn btn-primary btn-labeled btn-sm add-blank bootbox_form">
                                        <b><i class="icon-plus3"></i></b> Pridėti nuorodą
                                    </button>
                                </div>
                                <div class="col-lg-9">
                                    {{--<div class="clear"></div>--}}
                                    <div class="dd">
                                        <ol class="dd-list">
                                            @if($model->links->count())
                                            @foreach($model->links as $link)
                                                <li class="dd-item" id="{{ $link->id }}"
                                                    data-id="{{ $link->id }}"
                                                    data-title="{{ $link->title }}"
                                                    data-slug="{{ $link->slug }}"
                                                    data-rel="{{ $link->rel }}"
                                                    data-target="{{ $link->target }}"
                                                    data-sort="{{ $link->sort }}">

                                                    <div class="dd-handle">{{ $link->title }}</div>
                                                    @if($link->child->count())
                                                        <ol class="dd-list">
                                                            @foreach($link->child as $child)
                                                                <li class="dd-item" id="{{ $child->id }}"
                                                                    data-id="{{ $child->id }}"
                                                                    data-title="{{ $child->title }}"
                                                                    data-slug="{{ $child->slug }}"
                                                                    data-rel="{{ $child->rel }}"
                                                                    data-target="{{ $child->target }}"
                                                                    data-sort="{{ $child->sort }}">

                                                                    <div class="dd-handle">{{ $child->title }}</div>
                                                                    <a class="button edit"><i class="icon-pencil7"></i></a>
                                                                    <a class="button delete text-danger-600"><i class="icon-trash"></i></a>
                                                                </li>
                                                            @endforeach
                                                        </ol>
                                                    @endif
                                                    <a class="button edit"><i class="icon-pencil7"></i></a>
                                                    <a class="button delete text-danger-600"><i class="icon-trash"></i></a>
                                                </li>
                                            @endforeach
                                            @endif
                                        </ol>
                                    </div>
                                </div>
                            </div>
                            {{--<div class="row">--}}
                                {{--<div class="col-lg-6">--}}

                                {{--</div>--}}

                                {{--<div class="col-lg-6">--}}
                                    {{--<div class="add-edit-item panel panel-default hide">--}}
                                        {{--<div class="panel-heading">--}}
                                            {{--<i class="fa fa-info-circle fa-fw"></i> Pridėti/Redaguoti nuorodą--}}
                                        {{--</div>--}}
                                        {{--<!-- /.panel-heading -->--}}
                                        {{--<div class="panel-body">--}}
                                            {{--<div class="col-xs-12">--}}
                                                {{--<div class="form-group">--}}
                                                    {{--{!! Form::label('name', 'Pavadinimas') !!}--}}
                                                    {{--{!! Form::text('name', '', array('class' => 'form-control')) !!}--}}
                                                {{--</div>--}}
                                                {{--<div class="form-group">--}}
                                                    {{--{!! Form::label('slug', 'Nuoroda') !!}--}}
                                                    {{--{!! Form::text('slug', '', array('class' => 'form-control')) !!}--}}
                                                {{--</div>--}}
                                                {{--<div class="form-group">--}}
                                                    {{--{!! Form::label('rel', 'Rel') !!}--}}
                                                    {{--{!! Form::select('rel', ['' => '------------'] + $anchor_relationships, null, ['class' => 'form-control']) !!}--}}
                                                {{--</div>--}}
                                                {{--{!! Form::hidden('id', '') !!}--}}
                                                {{--<div class="form-group" style="margin-bottom: 0">--}}
                                                    {{--<button type="button" class="btn btn-primary btn-labeled save-item-data">--}}
                                                        {{--<b><i class="icon-floppy-disk"></i></b> Išsaugoti--}}
                                                    {{--</button>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<!-- /.panel-body -->--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        </div>
                    </fieldset>

                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">
                    <div class="text-right">
                        <a href="{{ route('admin.menu.index') }}" class="btn btn-danger btn-sm btn-labeled"><b><i
                                        class="icon-close2"></i></b> Atšaukti
                        </a>
                        <button type="submit" class="btn btn-success btn-sm btn-labeled"><b><i
                                        class="icon-checkmark"></i></b> Išsaugoti
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{ Form::close() }}

@endsection

@push('scripts.head')
<script type="text/javascript">
    function add_menu_link(callback) {
        bootbox.dialog({
                    title: "Pridėti/Redaguoti nuorodą.",
                    message: '<div class="row">  ' +
                    '<div class="col-md-12">' +
                    '<form class="form-vertical add-edit-item">' +
                    '<div class="form-group">' +
                    '<label class="control-label">Pavadinimas</label>' +
                    '<input id="name" name="name" type="text" class="form-control">' +
                    '</div>' +
                    '<div class="form-group">' +
                    '<label class="control-label">Nuoroda</label>' +
                    '<input id="slug" name="slug" type="text" class="form-control">' +
                    '</div>' +
                    '<div class="form-group">' +
                    '<label class="control-label">Rel</label>' +
                    '{{ Form::select('rel', ['' => '------------'] + $anchor_relationships, null, ['class' => 'form-control']) }}' +
                    '</div>' +
                    '<div class="form-group">' +
                    '<label class="control-label">Kaip atidaryti nuoroda?</label>' +
                    '{{ Form::select('target', ['' => 'Tas pats langas'] + $anchor_target, null, ['class' => 'form-control']) }}' +
                    '</div>' +
                    '{{ Form::hidden('id', '') }}' +
                    '</form>' +
                    '</div>' +
                    '</div>',
                    buttons: {
                        success: {
                            label: '<b><i class="icon-floppy-disk"></i></b> Išsaugoti',
                            className: "btn-primary btn-labeled",
                            callback: function () {
                                var form  = $(this).find('form.add-edit-item');

                                if (typeof callback == "function") {
                                    callback(form);
                                }
                            }
                        }
                    }
                }
        ).addClass('add_menu_link');
    }
</script>
@endpush