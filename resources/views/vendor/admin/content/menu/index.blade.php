@extends('admin::layouts.crud.all')

@section('content')

    <div class="panel panel-flat">
        <table class="table datatable-basic table-striped">
            <thead>
            <tr>
                <th>Pavadinimas</th>
                <th>ID</th>
                <th>Būsena</th>
                <th class="text-center no-sort actions">Veiksmai</th>
            </tr>
            </thead>
            <tbody>
            @foreach($menus as $menu)
                <tr class="pointer">
                    <td>{{ $menu->title }}</td>
                    <td>{{ $menu->slug }}</td>
                    <td>@include('admin::components.labels.status', ['model' => $menu])</td>
                    <td class="text-center">
                        @include('admin::components.buttons.actions', ['model' => $menu, 'view' => false])
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection