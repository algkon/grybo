@extends('admin::layouts.crud.single')

@section('content')

    <div class="row">
        <div class="col-md-12">

            <!-- Invoice template -->
            <div class="panel panel-white">
                <div class="panel-body">
                    <h5 class="text-uppercase text-semibold" style="margin: 0;">Užsakymas #{{ $model->order_id }}</h5>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 col-md-6 content-group order-user-details">
                    {{--<h5 class="text-uppercase text-semibold">Pirkėjo kontaktinė informacija</h5>--}}
                    <div class="well well-sm well-row-sm">
                        <dl class="dl-horizontal">
                            <dt>Vardas/Pavardė:</dt>
                            <dd>{{ $model->first_name }} {{ $model->last_name }}</dd>
                            <dt>Tel. nr.:</dt>
                            <dd>{{ $model->phone }}</dd>
                            <dt>El. paštas:</dt>
                            <dd>{{ $model->email }}</dd>
                            <dt>Registruotas vartotojas:</dt>
                            <dd>
                                @if ($model->user)
                                    <span class="label label-success">Taip</span>
                                    <a href="{{ route('admin.users.edit', $model->user_id) }}" target="_blank">
                                        #{{ $model->user_id }}
                                    </a>
                                @else
                                    <span class="label label-danger">Ne</span>
                                @endif
                            </dd>
                            <dt>&nbsp;</dt>
                            <dd>&nbsp;</dd>
                        </dl>
                    </div>
                </div>

                <div class="col-sm-12 col-md-6 content-group">
                    <div class="invoice-details">
                        {{--<h5 class="text-uppercase text-semibold">Užsakymo numeris #{{ $model->order_id }}</h5>--}}
                        <div class="well well-sm well-row-sm">
                            <dl class="dl-horizontal">
                                <dt>Užsakymo numeris:</dt>
                                <dd>#{{ $model->order_id }}</dd>
                                <dt>Užsakymo data:</dt>
                                <dd>{{ $model->created_at }}</dd>
                                <dt>Apmokėjimo data:</dt>
                                <dd>{{ $model->status == 2 ? $model->payed_at : '---------' }}</dd>
                                <dt>Būsena:</dt>
                                <dd>@include('admin::components.labels.orders.status', ['order' => $model])</dd>
                                {{--<dd>Donec id elit non mi porta gravida at eget metus.</dd>--}}
                                <dt>Mokėjimo būdas:</dt>
                                <dd>{{ $model->payment_method }}</dd>

                                <dt>Pritaikyta nuolaida:</dt>
                                @if ($model->hasDiscount())
                                    <dd><span class="label label-success">Taip</span> (kodas: {{ $model->discount_code }})</dd>
                                @else
                                    <dd><span class="label label-danger">Ne</span></dd>
                                @endif

                            </dl>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-white">
                <div class="table-responsive">
                    <table class="table table-lg">
                        <thead>
                        <tr>
                            <th style="width: 40%;">Prekė/paslauga</th>
                            <th>Kiekis/laikotarpis</th>
                            <th style="width: 100px;">Kaina</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($model->cart()->products as $product)
                            <tr>
                                <td style="border-top-style: dashed;">{{ $product->name }}</td>
                                <td style="border-top-style: dashed;">{{ $product->options->set_membership_name }}</td>
                                <td style="border-top-style: dashed;"><span class="text-semibold">{{ $product->price }} €</span></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /invoice template -->

            <div class="panel panel-white col-sm-4 col-sm-offset-8">

                <div class="row">
                    <div class="table-responsive no-border">
                        <table class="table table-lg">
                            <tbody>
                            <tr>
                                <th style="border: none;">Suma (be PVM):</th>
                                <td style="border: none;" class="text-right">{{ $model->cart()->subtotal }} €</td>
                            </tr>
                            <tr>
                                <th style="border-top-style: dashed;">PVM ({{ config('cart.tax') }}%):</th>
                                <td style="border-top-style: dashed;" class="text-right">{{ $model->cart()->tax }}€
                                </td>
                            </tr>
                            <tr style="background-color: #fcfcfc;">
                                <th>Viso:</th>
                                <td class="text-right text-primary">
                                    <h5 class="text-semibold" style="margin: 0;">{{ $model->cart()->total }} €</h5>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection