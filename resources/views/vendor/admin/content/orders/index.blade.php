@extends('admin::layouts.crud.all')

@section('content')

    <div class="panel panel-flat">
        <table class="table datatable-basic">
            <thead>
            <tr>
                <th>Užsakymo ID</th>
                <th>Pirkėjas</th>
                <th>Užsakymo data</th>
                <th>Būsena</th>
                <th>Kaina</th>
                <th class="text-center no-sort actions">Veiksmai</th>
            </tr>
            </thead>
            <tbody>
            @foreach($orders as $order)
                <tr>
                    <td>#{{ $order->order_id }}</td>
                    <td>{{ $order->first_name }} {{ $order->last_name }}</td>
                    <td>{{ $order->created_at }}</td>
                    <td>@include('admin::components.labels.orders.status')</td>
                    <td>{{ $order->price_formatted }} EUR
                    </td>
                    <td class="text-center">
                        @include('admin::components.buttons.actions', ['model' => $order, 'edit' => false])
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

@push('scripts.head')
<script type="text/javascript">
    function extend_datatable_defaults() {
        $.extend($.fn.dataTable.defaults, {
            order: [[2, 'desc']]
        });
    }
</script>
@endpush