<table class="table table-striped datatable-basic">
    <thead style="display: none;">
    <tr>
        <th></th>
        <th>Paveikslėlis</th>
        <th>Pavadinimas</th>
        <th>Sukurta</th>
        <th>Būsena</th>
    </tr>
    </thead>
    <tbody>
    @foreach($galleries as $gallery)
        <tr class="pointer" data-thumbnail="{{ $gallery->thumbnail('medium') }}" data-title="{{ $gallery->title }}"
            data-edit_url="{{ route('admin.galleries.edit', [$gallery->id]) }}" data-id="{{ $gallery->id }}">
            <td width="20px"></td>
            <td width="40px" style="padding-left: 10px;padding-right: 0;">
                <a href="{{ asset($gallery->thumbnail('original')) }}" data-popup="lightbox">
                    <img src="{{ asset($gallery->thumbnail('very-small')) }}" alt="" class="img-rounded img-preview">
                </a>
            </td>
            <td>{{ $gallery->title }}</td>
            <td width="150px">{{ $gallery->created_at }}</td>
            <td width="60px">@include('admin::components.labels.status', ['model' => $gallery])</td>
        </tr>
    @endforeach
    </tbody>
</table>

<script type="text/javascript"
        src="{{ asset('vendors/admin/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script type="text/javascript"
        src="{{ asset('vendors/admin/js/plugins/tables/datatables/extensions/select.min.js') }}"></script>
<script type="text/javascript">
    $(function () {
        $('.datatable-basic').DataTable({
            autoWidth: false,
            scrollY: 300,
            paging: false,
            info: false,
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer">',
            language: {
                search: '<span>Paieška:</span> _INPUT_'
            },
            columnDefs: [
                {
                    orderable: false,
                    className: 'select-checkbox',
                    targets: 0
                }
            ],
            select: {
                style: 'multiple',
                selector: 'td:first-child'
            }
        });
    });
</script>