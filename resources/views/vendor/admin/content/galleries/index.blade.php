@extends('admin::layouts.crud.all')

@section('content')

    <div class="panel panel-flat">
        <table class="table datatable-basic table-striped">
            <thead>
            <tr>
                <th class="no-sorts"></th>
                <th>Pavadinimas</th>
                <th>Paveikslėliai</th>
                <th>Sukurta</th>
                <th>Būsena</th>
                <th class="text-center no-sort actions">Veiksmai</th>
            </tr>
            </thead>
            <tbody>
            @foreach($galleries as $gallery)
                <tr class="pointer">
                    <td width="90px">
                        <a href="{{ asset($gallery->thumbnail('original')) }}" data-popup="lightbox">
                            <img src="{{ asset($gallery->thumbnail()) }}" alt="" class="img-rounded img-preview">
                        </a>
                    </td>
                    <td width="40%">{{ $gallery->title }}</td>
                    <td>{{ $gallery->images->count() }}</td>
                    <td>{{ $gallery->created_at }}</td>
                    <td>@include('admin::components.labels.status', ['model' => $gallery])</td>
                    <td class="text-center">
                        @include('admin::components.buttons.actions', ['model' => $gallery, 'view' => false])
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

@push('scripts.head')
    <script type="text/javascript">
        $(function () {
            $('[data-popup="lightbox"]').fancybox({
                padding: 3
            });
        });
    </script>
@endpush