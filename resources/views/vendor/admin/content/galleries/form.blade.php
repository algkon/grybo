@extends('admin::layouts.crud.single')

@section('content')

    {{ Form::model($model, ['url' => ['admin/galleries', $model->id], 'method' => $method, 'class' => 'form-horizontal']) }}
    {{ Form::hidden('user_id', auth('admin')->user()->getAuthIdentifier()) }}

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">


                    <fieldset>
                        <legend class="text-semibold">
                            <i class="icon-file-empty position-left"></i>
                            Pagrindinė informacija
                            <a class="control-arrow" data-toggle="collapse" data-target="#collapse-main">
                                <i class="icon-circle-down2"></i>
                            </a>
                        </legend>

                        <div class="collapse in" id="collapse-main">
                            <div class="form-group {{ has_error($errors, 'title') }} {{ has_error($errors, 'slug') }}">
                                <label class="control-label col-lg-3">Pavadinimas <span
                                            class="text-danger">*</span></label>

                                <div class="col-lg-9">
                                    {{ Form::text('title', null, ['class' => 'form-control']) }}
                                </div>
                            </div>

                            @include('admin::components.form.status')
                        </div>
                    </fieldset>

                    <fieldset>
                        <legend class="text-semibold">
                            <i class="icon-images2 position-left"></i>
                            Nuotraukos
                            <a class="control-arrow" data-toggle="collapse" data-target="#images">
                                <i class="icon-circle-down2"></i>
                            </a>
                        </legend>
                        <div class="collapse in" id="images">
                            <div class="row">
                                <div class="col-xs-12">
                                    <button type="button" id="alpha_button" class="btn btn-default">
                                        Pridėti nuotraukas
                                    </button>
                                </div>
                            </div>
                            <div class="row a_files" id="gallery">
                                @foreach(old('images', $model->images->toArray()) as $image)
                                    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                        <div class="thumbnail">
                                            <input type="hidden" name="images[{{ $image['id'] }}][id]" value="{{ $image['id'] }}" class="image_id"/>
                                            <input type="hidden" name="images[{{ $image['id'] }}][ordering]" value="{{ $image['ordering'] }}" class="image_order"/>
                                            <input type="hidden" name="images[{{ $image['id'] }}][name]" value="{{ $image['name'] }}"/>
                                            <input type="hidden" name="images[{{ $image['id'] }}][path]" value="{{ $image['path'] }}"/>
                                            <input type="hidden" name="images[{{ $image['id'] }}][full_path_medium]" value="{{ $image['full_path_medium'] }}"/>
                                            <input type="hidden" name="images[{{ $image['id'] }}][full_path_original]" value="{{ $image['full_path_original'] }}"/>

                                            <div class="thumb">
                                                <img src="{{ $image['full_path_medium'] }}" alt="">

                                                <div class="caption-overflow">
                                                    <span>
                                                        <a href="{{ $image['full_path_original'] }}" data-popup="lightbox"
                                                           rel="gallery"
                                                           class="btn border-white text-white btn-flat btn-icon btn-rounded">
                                                            <i class="icon-plus3"></i>
                                                        </a>
                                                        <a href="javascript:void(0)"
                                                           class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5"
                                                           onclick="gallery_image_delete(this);">
                                                            <i class="icon-trash"></i>
                                                        </a>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </fieldset>

                </div>
            </div>
        </div>

        {{--@if($model->exists)--}}
        {{--<div class="col-md-3">--}}
        {{--@include('admin::components.widgets.info')--}}
        {{--</div>--}}
        {{--@endif--}}

        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">
                    <div class="text-right">
                        <a href="{{ route('admin.galleries.index') }}" class="btn btn-danger btn-sm btn-labeled"><b><i
                                        class="icon-close2"></i></b> Atšaukti
                        </a>
                        <button type="submit" class="btn btn-success btn-sm btn-labeled"><b><i
                                        class="icon-checkmark"></i></b> Išsaugoti
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{ Form::close() }}

@endsection

@push('scripts.head')
<script type="text/javascript">
    $(function() {
        $("#gallery").sortable({
            placeholder: "sortable-placeholder",
            start: function(e, ui){
                ui.placeholder.height(ui.item.outerHeight() - 20);
                ui.placeholder.width(ui.item.outerWidth() - 0.5);
            },
            update: function( event, ui ) {
                gallery_images_resort();
            }
        });
        $("#gallery").disableSelection();
    });
</script>
@endpush