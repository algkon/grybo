@extends('admin::layouts.crud.single')

@section('content')

    {{ Form::model($model, ['url' => request()->fullUrl(), 'method' => $method, 'class' => 'form-horizontal']) }}
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">


                    <fieldset>
                        <legend class="text-semibold">
                            <i class="icon-calendar position-left"></i>
                            {{ $model->title }}
                            <a class="control-arrow" data-toggle="collapse" data-target="#collapse-main">
                                <i class="icon-circle-down2"></i>
                            </a>
                        </legend>

                        <div class="collapse in" id="collapse-main">

                            <div class="content-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        @if ($table->showTitlePrevious())
                                            <a href="{{ route('admin.timetables.calendar.form', [
                                                    $model->id,
                                                    $table->startOfPreviousWeek(),
                                                    $table->endOfPreviousWeek(),
                                                ]) }}" class="btn btn-default btn-xs">

                                                <i class="icon-arrow-left5"></i>
                                                {{ $table->titlePrevious() }}

                                            </a>
                                        @endif
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <a href="{{ route('admin.timetables.calendar.form', [
                                                $model->id,
                                                $table->startOfNextWeek(),
                                                $table->endOfNextWeek(),
                                            ]) }}" class="btn btn-default btn-xs">

                                            {{ $table->titleNext() }}
                                            <i class="icon-arrow-right5"></i>

                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="content-group-lg">
                                <div class="hot-container">
                                    <div id="hot_checks_labels"></div>
                                </div>
                            </div>

                        </div>
                    </fieldset>

                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="checkbox">
                                <label>
                                    {{ Form::checkbox('store_as_default_all') }} Išsaugoti kaip numatytąjį
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="{{ route('admin.timetables.index') }}" class="btn btn-danger btn-sm btn-labeled">
                                <b><i class="icon-close2"></i></b> Atšaukti
                            </a>
                            <button type="submit" class="btn btn-success btn-sm btn-labeled">
                                <b><i class="icon-checkmark"></i></b> Išsaugoti
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{ Form::close() }}

@endsection

@push('scripts.head')
    <script type="text/javascript">
        var timetable_id = {{ $model->id }};
        var weekdays_dates = {!! $table->getNamedDatesOfWeekDays()->toJson() !!};
        var hot_headers = ['{!! $table->getHeaderColumns()->implode("','") !!}'];
        var hot_checks_labels_data = {!! $table->getData()->toJson() !!};
    </script>
    <script type="text/javascript" src="{{ asset('vendors/admin/js/plugins/tables/handsontable/handsontable.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors\admin\js\pages\handsontable_custom.js') }}"></script>
@endpush