@extends('admin::layouts.crud.all')

@section('content')

    <div class="panel panel-flat">
        <table class="table datatable-basic table-striped">
            <thead>
            <tr>
                <th>Pavadinimas</th>
                <th>Nuoroda</th>
                <th>Būsena</th>
                <th class="text-center no-sort">Užimtumo grafikas</th>
                <th class="text-center no-sort actions">Veiksmai</th>
            </tr>
            </thead>
            <tbody>
            @foreach($timetables as $timetable)
                <tr class="pointer">
                    <td>{{ $timetable->title }}</td>
                    <td>{{ $timetable->slug }}</td>
                    <td>@include('admin::components.labels.status', ['model' => $timetable])</td>
                    <td class="text-center">
                        <a href="{{ route('admin.timetables.calendar.form', [$timetable->id]) }}">
                            <i class="icon-calendar"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        @include('admin::components.buttons.actions', ['model' => $timetable, 'view' => false])
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection