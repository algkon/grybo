@extends('admin::layouts.crud.all')

@section('content')

    <div class="panel panel-flat">
        <table class="table datatable-basic table-striped">
            <thead>
            <tr>
                <th>Pavadinimas</th>
                <th>Nuoroda</th>
                @if(!request()->has('filter.category_id'))<th>Category</th>@endif
                {{--<th>Autorius</th>--}}
                <th>Sukurta</th>
                <th>Būsena</th>
                <th class="text-center no-sort actions">Veiksmai</th>
            </tr>
            </thead>
            <tbody>
            @foreach($posts as $post)
                <tr class="pointer">
                    <td>{{ $post->title }}</td>
                    <td>{{ $post->slug }}</td>
                    @if(!request()->has('filter.category_id'))<td>{{ $post->categoryTitle }}</td>@endif
                    <td>{{ $post->created_at }}</td>
{{--                    <td>{{ $post->getAuthorName() }}</td>--}}
                    <td>@include('admin::components.labels.status', ['model' => $post])</td>
                    <td class="text-center">
                        @include('admin::components.buttons.actions', ['model' => $post, 'view' => false])
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection