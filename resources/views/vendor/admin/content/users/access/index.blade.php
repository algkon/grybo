@extends('admin::layouts.crud.single')

@section('content')

    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Vartotojo tipų ir teisių valdymas</h5>

            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            Čia galite sukurti/redaguoti vartotojo tipus ir leidimus bei priskirti teises kiekvienam vartotojo tipui.
        </div>

        {{ Form::open(['url' => 'admin/users/access', 'method' => 'PUT']) }}

            <div class="table-responsive">
                <table class="table table-bordered table-lg">
                    <tbody>
                    @if($roles->count())
                        <tr class="active">
                            <th></th>
                            @foreach($roles as $role)
                                <th class="text-center">
                                    <span class="label label-{{ $role->style }}">{{ $role->getLabel() }}</span>
                                    {{ Form::hidden('roles[' . $role->id . ']') }}
                                </th>
                            @endforeach
                        </tr>
                        @if($permissions->count())
                            @foreach($permissions as $permission)
                                <tr>
                                    <td class="col-sm-4">
                                        {{ $permission->getLabel() }}
                                    </td>
                                    @foreach($roles as $role)
                                        <td class="text-center">
                                            {{ Form::checkbox('roles[' . $role->id . '][]', $permission->id,
                                                    old('roles[' . $role->id . '][]', $role->hasPermission($permission)),
                                                    ['class' => 'styled']) }}
                                        </td>
                                    @endforeach
                                </tr>
                            @endforeach
                        @endif
                    @endif
                    </tbody>
                </table>
            </div>

            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <a href="{{ route('admin.permissions.refresh') }}" class="btn btn-primary btn-sm btn-labeled">
                            <b><i class="icon-rotate-cw3"></i></b> Atnaujinti sąrašą
                        </a>
                    </div>
                    <div class="col-md-6 text-right">
                        <button type="submit" class="btn btn-success btn-sm btn-labeled">
                            <b><i class="icon-checkmark"></i></b> Išsaugoti
                        </button>
                    </div>
                </div>
            </div>
        {{ Form::close() }}
    </div>

@endsection