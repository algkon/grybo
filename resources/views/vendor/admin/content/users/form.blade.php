@extends('admin::layouts.crud.single')

@section('content')

    {{ Form::model($model, ['url' => ['admin/users', $model->id], 'method' => $method, 'class' => 'form-horizontal']) }}

        @if(!in_array(3, old('roles_ids', $model->roles_ids)))
            {{ Form::hidden('api_user', 1, ['disabled']) }}
        @else
            {{ Form::hidden('api_user', 1) }}
        @endif

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-white">
                    <div class="panel-body">

                        <fieldset>
                            <legend class="text-semibold">
                                <i class="icon-file-text2 position-left"></i>
                                Pagrindinė informacija
                                <a class="control-arrow" data-toggle="collapse" data-target="#collapse-main">
                                    <i class="icon-circle-down2"></i>
                                </a>
                            </legend>

                            <div class="collapse in" id="collapse-main">
                                @if(!in_array(3, old('roles_ids', $model->roles_ids)))
                                    <div id="standart">
                                        <div class="form-group {{ has_error($errors, 'name') }}">
                                            <label class="control-label col-lg-3">Vardas, Pavardė <span
                                                        class="text-danger">*</span></label>

                                            <div class="col-lg-9">
                                                {{ Form::text('name', null, ['class' => 'form-control']) }}
                                            </div>
                                        </div>

                                        <div class="form-group {{ has_error($errors, 'email') }}">
                                            <label class="control-label col-lg-3">El. paštas <span
                                                        class="text-danger">*</span></label>

                                            <div class="col-lg-9">
                                                {{ Form::email('email', null, ['class' => 'form-control']) }}
                                            </div>
                                        </div>
                                    </div>

                                @endif

                                @if (!$model->exists || in_array(3, $model->roles_ids))
                                    <div id="npoint" style=" @if(!$model->exists && !in_array(3, old('roles_ids', []))) display:none @endif">

                                            <div class="form-group {{ has_error($errors, 'api.first_name') }}">
                                                <label class="control-label col-lg-3">Vardas <span
                                                            class="text-danger">*</span></label>

                                                <div class="col-lg-9">
                                                    {{ Form::text('api[first_name]', null, ['class' => 'form-control']) }}
                                                </div>
                                            </div>

                                            <div class="form-group {{ has_error($errors, 'api.last_name') }}">
                                                <label class="control-label col-lg-3">Pavardė <span
                                                            class="text-danger">*</span></label>

                                                <div class="col-lg-9">
                                                    {{ Form::text('api[last_name]', null, ['class' => 'form-control']) }}
                                                </div>
                                            </div>

                                            <div class="form-group {{ has_error($errors, 'api.email') }}">
                                                <label class="control-label col-lg-3">El. paštas <span
                                                            class="text-danger">*</span></label>

                                                <div class="col-lg-9">
                                                    @if (!$model->exists)
                                                        {{ Form::text('api[email]', null, ['class' => 'form-control']) }}
                                                    @else
                                                        {{ Form::text('api[email]', null, ['class' => 'form-control', 'disabled']) }}
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group {{ has_error($errors, 'api.national_id') }}">
                                                <label class="control-label col-lg-3">Asmens kodas <span
                                                            class="text-danger">*</span></label>

                                                <div class="col-lg-9">
                                                    @if (!$model->exists)
                                                        {{ Form::text('api[national_id]', null, ['class' => 'form-control']) }}
                                                    @else
                                                        {{ Form::text('api[national_id]', null, ['class' => 'form-control', 'disabled']) }}
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group {{ has_error($errors, 'api.gender') }}">
                                                <label class="control-label col-lg-3">Lytis</label>

                                                <div class="col-lg-9">
                                                    {{ Form::select('api[gender]', [1 => 'Vyras', 2 => 'Moteris'], null, ['class' => 'form-control']) }}
                                                </div>
                                            </div>

                                            <div class="form-group {{ has_error($errors, 'api.phone') }}">
                                                <label class="control-label col-lg-3">Telefono numeris</label>

                                                <div class="col-lg-9">
                                                    {{ Form::text('api[phone]', null, ['class' => 'form-control']) }}
                                                </div>
                                            </div>

                                            <div class="form-group {{ has_error($errors, 'api.country') }}">
                                                <label class="control-label col-lg-3">Šalis</label>

                                                <div class="col-lg-9">
                                                    {{ Form::text('api[country]', 'LIETUVA', ['class' => 'form-control', 'disabled']) }}
                                                </div>
                                            </div>

                                            <div class="form-group {{ has_error($errors, 'api.addr_town') }}">
                                                <label class="control-label col-lg-3">Miestas</label>

                                                <div class="col-lg-9">
                                                    {{ Form::text('api[addr_town]', null, ['class' => 'form-control']) }}
                                                </div>
                                            </div>

                                            <div class="form-group {{ has_error($errors, 'api.addr_street') }}">
                                                <label class="control-label col-lg-3">Gatvė</label>

                                                <div class="col-lg-9">
                                                    {{ Form::text('api[addr_street]', null, ['class' => 'form-control']) }}
                                                </div>
                                            </div>

                                            <div class="form-group {{ has_error($errors, 'api.addr_house') }}">
                                                <label class="control-label col-lg-3">Namo nr.</label>

                                                <div class="col-lg-9">
                                                    {{ Form::text('api[addr_house]', null, ['class' => 'form-control']) }}
                                                </div>
                                            </div>

                                            <div class="form-group {{ has_error($errors, 'api.addr_flat') }}">
                                                <label class="control-label col-lg-3">Buto nr.</label>

                                                <div class="col-lg-9">
                                                    {{ Form::text('api[addr_flat]', null, ['class' => 'form-control']) }}
                                                </div>
                                            </div>

                                            <div class="form-group {{ has_error($errors, 'api.addr_post_code') }}">
                                                <label class="control-label col-lg-3">Pašto kodas</label>

                                                <div class="col-lg-9">
                                                    {{ Form::text('api[addr_post_code]', null, ['class' => 'form-control']) }}
                                                </div>
                                            </div>

                                            <div class="form-group {{ has_error($errors, 'api.member_card_number') }}">
                                                <label class="control-label col-lg-3">"Grybo 7" kliento kortelės nr.</label>

                                                <div class="col-lg-9">
                                                    {{ Form::text('api[member_card_number]', null, ['class' => 'form-control']) }}
                                                </div>
                                            </div>

                                            <div class="form-group {{ has_error($errors, 'api.leisure_card_number') }}">
                                                <label class="control-label col-lg-3">"Laisvalaikio" kortelės nr.</label>

                                                <div class="col-lg-9">
                                                    {{ Form::text('api[leisure_card_number]', null, ['class' => 'form-control']) }}
                                                </div>
                                            </div>
                                        </div>
                                @endif
                            </div>
                        </fieldset>

                        <fieldset>
                            <legend class="text-semibold">
                                <i class="icon-lock position-left"></i>
                                Saugumas ir leidimai
                                <a class="control-arrow" data-toggle="collapse" data-target="#collapse-safe">
                                    <i class="icon-circle-down2"></i>
                                </a>
                            </legend>

                            <div class="collapse in" id="collapse-safe">

                                @if(!in_array(3, old('roles_ids', $model->roles_ids)))
                                    <div id="password">
                                        @if($model->exists)
                                            <div class="alert alert-warning alert-styled-left">
                                                <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                                                            class="sr-only">Close</span></button>
                                                <span class="text-semibold">Dėmesio!</span> Jeigu nenorite keisti slaptažodžio,
                                                laukus palikite tuščius.
                                            </div>
                                        @endif

                                        <div class="form-group {{ has_error($errors, 'password') }}">
                                            <label class="control-label col-lg-3">
                                                Slaptažodis
                                                @if(!$model->exists)
                                                    <span class="text-danger">*</span>
                                                @endif
                                            </label>

                                            <div class="col-lg-9">
                                                {{ Form::password('password', ['class' => 'form-control']) }}
                                            </div>
                                        </div>

                                        <div class="form-group {{ has_error($errors, 'password') }}">
                                            <label class="control-label col-lg-3">
                                                Pakartoti slaptažodį
                                                @if(!$model->exists)
                                                    <span class="text-danger">*</span>
                                                @endif
                                            </label>

                                            <div class="col-lg-9">
                                                {{ Form::password('password_confirmation', ['class' => 'form-control']) }}
                                            </div>
                                        </div>

                                        <hr>
                                    </div>
                                @endif

                                <div class="form-group {{ has_error($errors, 'roles') }}">
                                    <label class="control-label col-lg-3">Vartotojo tipai <span
                                                class="text-danger">*</span></label>

                                    <div class="col-lg-9">
                                        <div class="multi-select-full">
                                            @if ($model->exists && in_array(3, old('roles_ids', $model->roles_ids)))
                                                <input type="hidden" name="roles_ids[]" value="3">
                                                {{ Form::select('roles_ids[]', $role->getRolesDropdownList([], ["" => ""]), null, ['class' => 'multiselect-select-roles', 'id' => 'multiselect-select-roles', 'multiple', 'disabled']) }}
                                            @elseif($model->exists)
                                                {{ Form::select('roles_ids[]', $role->getRolesDropdownList([], ["" => ""], [], [3]), null, ['class' => 'multiselect-select-roles', 'id' => 'multiselect-select-roles', 'multiple']) }}
                                            @else
                                                {{ Form::select('roles_ids[]', $role->getRolesDropdownList([], ["" => ""]), null, ['class' => 'multiselect-select-roles', 'id' => 'multiselect-select-roles', 'multiple']) }}
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-lg-3">Statusas</label>

                                    <div class="col-lg-9">
                                        <div class="checkbox checkbox-switch">
                                            <input type="hidden" name="active" value="0">
                                            <input type="checkbox" data-on-color="success" data-off-color="danger" data-on-text="Taip"
                                                   data-off-text="Ne" data-size="small" class="switch" value="1" name="active"
                                                   @if($model->isActive()) checked="checked" @endif>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </fieldset>

                    </div>
                </div>
            </div>

            {{--<div class="col-md-3">--}}
                {{--@include('admin::components.widgets.featured-image')--}}
            {{--</div>--}}

            <div class="col-md-12">
                <div class="panel panel-white">
                    <div class="panel-body">
                        <div class="text-right">
                            <a href="{{ route('admin.users.index') }}" class="btn btn-danger btn-sm btn-labeled"><b><i
                                            class="icon-close2"></i></b> Atšaukti
                            </a>
                            <button type="submit" class="btn btn-success btn-sm btn-labeled"><b><i
                                            class="icon-checkmark"></i></b> Išsaugoti
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    {{ Form::close() }}

@endsection

{{--@push('scripts.head')--}}
    {{--<script type="text/javascript">--}}
        {{--$(function () {--}}
            {{--$('#multiselect-select-roles').on('change', function () {--}}
                {{--var selectedData = $(this).val();--}}

                {{--if ($.inArray("3", selectedData) != -1) {--}}
                    {{--$('#standart, #password').hide();--}}
                    {{--$('#npoint').show();--}}
                {{--} else {--}}
                    {{--$('#npoint').hide();--}}
                    {{--$('#standart, #password').show();--}}
                {{--}--}}
            {{--});--}}
        {{--});--}}
    {{--</script>--}}
{{--@endpush--}}