@extends('admin::layouts.crud.all')

@section('content')

    <div class="panel panel-flat">
        <table class="table datatable-basic table-striped">
            <thead>
            <tr>
                <th>Vardas</th>
                <th>El. paštas</th>
                <th>Tipas</th>
                <th>Būsena</th>
                <th class="text-center no-sort actions">Veiksmai</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr class="pointer">
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>@include('admin::components.labels.users.role', ['user' => $user])</td>
                    <td>@include('admin::components.labels.status', ['model' => $user])</td>
                    <td class="text-center">
                        @include('admin::components.buttons.user-actions', ['model' => $user, 'view' => false])
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection