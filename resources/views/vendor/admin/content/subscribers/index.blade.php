@extends('admin::layouts.crud.all')

@section('content')

    <div class="panel panel-flat">
        <table class="table datatable-basic table-striped">
            <thead>
            <tr>
                <th>El. paštas</th>
                <th class="text-center no-sort actions">Veiksmai</th>
            </tr>
            </thead>
            <tbody>
            @foreach($subscribers as $subscriber)
                <tr class="pointer">
                    <td>{{ $subscriber->email }}</td>
                    <td class="text-center">
                        @include('admin::components.buttons.actions', ['model' => $subscriber, 'view' => false, 'edit' => false])
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection