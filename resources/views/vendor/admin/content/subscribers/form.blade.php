@extends('admin::layouts.crud.single')

@section('content')

    {{ Form::model($model, ['url' => ['admin/subscribers/mail'], 'method' => $method, 'class' => 'form-horizontal']) }}
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">


                    <fieldset>
                        <legend class="text-semibold">
                            <i class="icon-mail5 position-left"></i>
                            Laiško turinys
                            <a class="control-arrow" data-toggle="collapse" data-target="#collapse-main">
                                <i class="icon-circle-down2"></i>
                            </a>
                        </legend>

                        <div class="collapse in" id="collapse-main">
                            <div class="form-group {{ has_error($errors, 'subject') }}">
                                <label class="control-label col-lg-3">Tema <span
                                            class="text-danger">*</span></label>

                                <div class="col-lg-9">
                                    {{ Form::text('subject', null, ['class' => 'form-control']) }}
                                </div>
                            </div>
                            <div class="form-group {{ has_error($errors, 'content') }}">
                                <div class="col-lg-12">
                                    {{ Form::textarea('content', null, ['class' => 'form-control', 'id' => 'editor-full']) }}
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary btn-sm btn-labeled">
                            <b><i class="icon-mail5"></i></b> Siųsti
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{ Form::close() }}

@endsection