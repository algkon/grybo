@extends('admin::layouts.crud.all')

@section('content')

    <div class="panel panel-flat">
        <table class="table datatable-basic table-striped">
            <thead>
            <tr>
                <th>Pavadinimas</th>
                <th>Nuoroda</th>
                <th>Sukurta</th>
                <th>Būsena</th>
                <th class="text-center no-sort actions">Veiksmai</th>
            </tr>
            </thead>
            <tbody>
            @foreach($pages as $page)
                <tr class="pointer">
                    <td>{{ $page->title }}</td>
                    <td>{{ $page->slug }}</td>
                    <td>{{ $page->created_at }}</td>
                    <td>@include('admin::components.labels.status', ['model' => $page])</td>
                    <td class="text-center">
                        @include('admin::components.buttons.actions', ['model' => $page, 'view' => false])
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection