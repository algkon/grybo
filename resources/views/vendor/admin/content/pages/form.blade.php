@extends('admin::layouts.crud.single')

@section('content')

    {{ Form::model($model, ['url' => ['admin/pages', $model->id], 'method' => $method, 'class' => 'form-horizontal']) }}
    {{ Form::hidden('user_id', auth('admin')->user()->getAuthIdentifier()) }}
        <div class="row">
            <div class="col-md-9">
                <div class="panel panel-white">
                    <div class="panel-body">

                        <fieldset>
                            <legend class="text-semibold">
                                <i class="icon-file-empty position-left"></i>
                                Pagrindinė puslapio informacija
                                <a class="control-arrow" data-toggle="collapse" data-target="#collapse-main">
                                    <i class="icon-circle-down2"></i>
                                </a>
                            </legend>

                            <div class="collapse in" id="collapse-main">
                                <div class="form-group {{ has_error($errors, 'title') }} {{ has_error($errors, 'slug') }}">
                                    <label class="control-label col-lg-3">Pavadinimas <span
                                                class="text-danger">*</span></label>

                                    <div class="col-lg-9">
                                        {{ Form::text('title', null, ['class' => 'form-control']) }}
                                    </div>
                                </div>

                                @include('admin::components.form.status')
                            </div>
                        </fieldset>

                        <fieldset>
                            <legend class="text-semibold">
                                <i class="icon-file-text2 position-left"></i>
                                Puslapio turinys
                                <a class="control-arrow" data-toggle="collapse" data-target="#collapse-content">
                                    <i class="icon-circle-down2"></i>
                                </a>
                            </legend>
                            <div class="collapse in" id="collapse-content">
                                <div class="form-group {{ has_error($errors, 'description') }}">
                                    <div class="col-lg-12">
                                        {{ Form::textarea('content', null, ['class' => 'form-control', 'id' => 'editor-full']) }}
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        @include('admin::components.form.metadata')
                        @include('admin::components.form.galleries')

                    </div>
                </div>
            </div>

            <div class="col-md-3">
                @if($model->exists)
                    @include('admin::components.widgets.info')
                @endif

                @include('admin::components.widgets.featured-image')

                @include('admin::components.widgets.layout')
            </div>

            <div class="col-md-9">
                <div class="panel panel-white">
                    <div class="panel-body">
                        <div class="text-right">
                            <a href="{{ route('admin.pages.index') }}" class="btn btn-danger btn-sm btn-labeled">
                                <b><i class="icon-close2"></i></b> Atšaukti
                            </a>
                            <button type="submit" class="btn btn-success btn-sm btn-labeled">
                                <b><i class="icon-checkmark"></i></b> Išsaugoti
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    {{ Form::close() }}

@endsection