@extends('admin::layouts.crud.single')

@section('content')

    {{ Form::open(['url' => ['admin/settings'], 'class' => 'form-horizontal']) }}
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">

                    @if(!empty($schema))
                        @foreach($schema as $group_name => $group)

                            @if (!empty($group))
                                @foreach($group as $table_name => $table)

                                    <fieldset>
                                        <legend class="text-semibold">
                                            {{ $table['title'] }}
                                            <a class="control-arrow" data-toggle="collapse" data-target="#collapse-main-{{ $table_name }}">
                                                <i class="icon-circle-down2"></i>
                                            </a>
                                        </legend>

                                        <div class="collapse in" id="collapse-main-{{ $table_name }}">

                                            @if (isset($table['rows']) && !empty($table['rows']))
                                                @foreach($table['rows'] as $row_name => $row)

                                                    <div class="form-group">
                                                        <label class="control-label col-lg-3">{{ $row['title'] }}</label>

                                                        <div class="col-lg-9">
                                                            {!! Form::{$row['type']}('settings[' . $group_name . '][' . $table_name . '][' . $row_name . ']',
                                                            (data_get($settings, $group_name . '.' . $table_name . '.' . $row_name) ? data_get($settings, $group_name . '.' . $table_name . '.' . $row_name) : ''),
                                                            ['class' => 'form-control']) !!}
                                                        </div>
                                                    </div>

                                                @endforeach
                                            @endif

                                        </div>
                                    </fieldset>

                                @endforeach
                            @endif

                        @endforeach
                    @endif

                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">
                    <div class="text-right">
                        <button type="submit" class="btn btn-success btn-sm btn-labeled">
                            <b><i class="icon-checkmark"></i></b> Išsaugoti
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{ Form::close() }}

@endsection