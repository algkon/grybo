@extends('admin::layouts.crud.single')

@section('content')

    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Table components</h5>

            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            Table below contains different examples of components, that can be used in the table: form components,
            interface components, buttons etc. All of them are adapted for different cases, such as multiple elements in
            the same table cell. These components support all available sizes and styles. Also added a few examples of
            sparklines based on <code>sparklines.js</code> library. For more advanced sparklines <code>D3.js</code>
            library suits best
        </div>

        {{ Form::open(['url' => 'admin/permissions']) }}

        <div class="table-responsive">
            <table class="table table-bordered table-lg">
                <tbody>
                @if($roles->count())
                    <tr class="active">
                        <th></th>
                        @foreach($roles as $role)
                            <th class="text-center"><span
                                        class="label label-{{ $role->style }}">{{ $role->getLabel() }}</span></th>
                        @endforeach
                    </tr>
                    @if($permissions->count())
                        @foreach($permissions as $permission)
                            <tr>
                                <td class="col-sm-4">
                                    {{ $permission->getLabel() }}
                                </td>
                                @foreach($roles as $role)
                                    <td class="text-center">
                                        {{ Form::checkbox('permissions[' . $role->id . ']', $permission->id,
                                                old('permissions[' . $role->id . ']', $role->hasPermission($permission)),
                                                ['class' => 'styled']) }}
                                    </td>
                                @endforeach
                            </tr>
                        @endforeach
                    @endif
                @endif
                </tbody>
            </table>
        </div>

        <div class="panel-body">
            <div class="text-right">
                <button type="submit" class="btn btn-success btn-sm btn-labeled">
                    <b><i class="icon-checkmark"></i></b> Išsaugoti
                </button>
            </div>
        </div>
        {{ Form::close() }}
    </div>

@endsection