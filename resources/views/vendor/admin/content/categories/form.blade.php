@extends('admin::layouts.crud.single')

@section('content')

    {{ Form::model($model, ['url' => ['admin/categories', $model->id], 'method' => $method, 'class' => 'form-horizontal']) }}
    {{ Form::hidden('user_id', auth('admin')->user()->getAuthIdentifier()) }}

        <div class="row">
            <div class="col-md-9">
                <div class="panel panel-white">
                    <div class="panel-body">


                        <fieldset>
                            <legend class="text-semibold">
                                <i class="icon-file-empty position-left"></i>
                                Kategorijos informacija
                                <a class="control-arrow" data-toggle="collapse" data-target="#collapse-main">
                                    <i class="icon-circle-down2"></i>
                                </a>
                            </legend>

                            <div class="collapse in" id="collapse-main">
                                <div class="form-group {{ has_error($errors, 'title') }} {{ has_error($errors, 'slug') }}">
                                    <label class="control-label col-lg-3">Pavadinimas <span
                                                class="text-danger">*</span></label>

                                    <div class="col-lg-9">
                                        {{ Form::text('title', null, ['class' => 'form-control']) }}
                                    </div>
                                </div>

                                <div class="form-group {{ has_error($errors, 'parent_id') }}">
                                    <label class="control-label col-lg-3">Tėvinė kategorija</label>

                                    <div class="col-lg-9">
                                        {{ Form::select('parent_id', $repository->rootCategoriesDropdownList([0 => str_repeat('-', 20)], [], [$model->getKey()]), null, ['class' => 'form-control']) }}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-lg-3">Pridėti į admin meniu</label>

                                    <div class="col-lg-9">
                                        <div class="checkbox checkbox-switch">
                                            <input type="hidden" name="admin_menu" value="0">
                                            {{ Form::checkbox('admin_menu', 1, null, [
                                                'class' => 'switch',
                                                'data-on-color' => 'success',
                                                'data-off-color' => 'danger',
                                                'data-on-text' => 'Taip',
                                                'data-off-text' => 'Ne',
                                                'data-size' => 'small',
                                            ]) }}
                                        </div>
                                    </div>
                                </div>

                                @include('admin::components.form.status')
                            </div>
                        </fieldset>

                        <fieldset>
                            <legend class="text-semibold">
                                <i class="icon-file-text2 position-left"></i>
                                Kategorijos aprašymas
                                <a class="control-arrow" data-toggle="collapse" data-target="#collapse-content">
                                    <i class="icon-circle-down2"></i>
                                </a>
                            </legend>
                            <div class="collapse in" id="collapse-content">
                                <div class="form-group {{ has_error($errors, 'description') }}">
                                    <div class="col-lg-12">
                                        {{ Form::textarea('description', null, ['class' => 'form-control', 'id' => 'editor-full']) }}
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        @include('admin::components.form.metadata')

                    </div>
                </div>
            </div>

            @if($model->exists)
                <div class="col-md-3">
                    @include('admin::components.widgets.info')
                </div>
            @endif

            <div class="col-md-3">
                @include('admin::components.widgets.featured-image')
            </div>

            <div class="col-md-9">
                <div class="panel panel-white">
                    <div class="panel-body">
                        <div class="text-right">
                            <a href="{{ route('admin.categories.index') }}" class="btn btn-danger btn-sm btn-labeled"><b><i
                                            class="icon-close2"></i></b> Atšaukti
                            </a>
                            <button type="submit" class="btn btn-success btn-sm btn-labeled"><b><i
                                            class="icon-checkmark"></i></b> Išsaugoti
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    {{ Form::close() }}

@endsection