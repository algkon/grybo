@extends('admin::layouts.crud.all')

@section('content')

    <div class="panel panel-flat">
        <table class="table datatable-basic table-striped">
            <thead>
            <tr>
                <th>Pavadinimas</th>
                <th>Nuoroda</th>
                <th>Būsena</th>
                <th class="text-center no-sort actions">Veismai</th>
            </tr>
            </thead>
            <tbody>
            @foreach($categories as $category)
                <tr class="pointer">
                    <td>{{ $category->titleTree }}</td>
                    <td>{{ $category->slug }}</td>
                    <td>@include('admin::components.labels.status', ['model' => $category])</td>
                    <td class="text-center">
                        @include('admin::components.buttons.actions', ['model' => $category, 'view' => false])
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection