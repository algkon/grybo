<div class="breadcrumb-line breadcrumb-margin-bottom">
    <ul class="breadcrumb">
        <li>
            <a href="{{ route('admin.index') }}">
                <i class="icon-home2 position-left"></i> {{ trans('admin::routes.admin.index') }}
            </a>
        </li>

        @foreach(bag('breadcrumb')->all() as $route => $name)
            @if ($loop->last)
                <li class="active">{{ $name }}</li>
            @else
                <li><a href="{{ route($route) }}">{{ $name }}</a></li>
            @endif
        @endforeach
    </ul>
</div>