@extends('admin::layouts.crud.single')

@section('content')

    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Table components</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            Table below contains different examples of components, that can be used in the table: form components, interface components, buttons etc. All of them are adapted for different cases, such as multiple elements in the same table cell. These components support all available sizes and styles. Also added a few examples of sparklines based on <code>sparklines.js</code> library. For more advanced sparklines <code>D3.js</code> library suits best
        </div>

        <div class="table-responsive">
            <table class="table table-bordered table-lg">
                <tbody>
                @if($roles->count())
                    <tr class="active">
                        <th>Teisės</th>
                        @foreach($roles as $role)
                            <th class="text-center"><span class="label label-{{ $role->style }}">{{ $role->label }}</span></th>
                        @endforeach
                    </tr>
                    @foreach(app('router')->getRoutes() as $route)
                        @if($route->getName())
                            <tr>
                                <td class="col-sm-4">
                                    {{ str_replace('admin::routes.', '', trans('admin::routes.' . $route->getName())) }}
                                </td>
                                @foreach($roles as $role)
                                    <td class="text-center">
                                        <input type="checkbox" class="styled" checked="checked">
                                    </td>
                                @endforeach
                            </tr>
                        @endif
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>

@endsection