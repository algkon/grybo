@extends('admin::layouts.crud.single')

@section('content')

    <form class="form-horizontal" action="#">
        <div class="row">
            <div class="col-md-9">
                <div class="panel panel-white">
                    <div class="panel-body">

                        <fieldset>
                            <legend class="text-semibold">
                                <i class="icon-file-text2 position-left"></i>
                                Informacija
                                <a class="control-arrow" data-toggle="collapse" data-target="#collapse-main">
                                    <i class="icon-circle-down2"></i>
                                </a>
                            </legend>

                            <div class="collapse in" id="collapse-main">
                                <div class="form-group">
                                    <label class="control-label col-lg-3">Pavadinimas <span
                                                class="text-danger">*</span></label>

                                    <div class="col-lg-9">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-lg-3">Leidimai <span
                                                class="text-danger">*</span></label>

                                    <div class="col-lg-9">
                                        <div class="input-group">
                                            <select class="bootstrap-select select-all-values" multiple="multiple" data-width="100%">
                                                <option value="AK">Alaska</option>
                                                <option value="HI">Hawaii</option>
                                                <option value="CA">California</option>
                                                <option value="NV">Nevada</option>
                                                <option value="OR">Oregon</option>
                                                <option value="HI">Hawaii</option>
                                                <option value="CA">California</option>
                                                <option value="NV">Nevada</option>
                                                <option value="OR">Oregon</option>
                                                <option value="HI">Hawaii</option>
                                                <option value="CA">California</option>
                                                <option value="NV">Nevada</option>
                                                <option value="OR">Oregon</option>
                                            </select>

                                            <div class="input-group-btn">
                                                <button type="button" class="btn btn-info" id="select-all-values">Select all</button>
                                                <button type="button" class="btn btn-default" id="deselect-all-values">Deselect all</button>
                                            </div>
                                        </div>
                                        {{--<select multiple="multiple" data-placeholder="Pasirinkti"--}}
                                                {{--class="select-size-sm">--}}
                                            {{--<option></option>--}}
                                            {{--<option value="AZ">Create user</option>--}}
                                            {{--<option value="CO">Create page</option>--}}
                                            {{--<option value="ID">Create post</option>--}}
                                            {{--<option value="A">Create category</option>--}}
                                            {{--<option value="CO">Create page</option>--}}
                                            {{--<option value="ID">Create post</option>--}}
                                            {{--<option value="A">Create category</option>--}}
                                            {{--<option value="CO">Create page</option>--}}
                                            {{--<option value="ID">Create post</option>--}}
                                            {{--<option value="A">Create category</option>--}}
                                        {{--</select>--}}
                                    </div>
                                </div>

                            </div>
                        </fieldset>

                    </div>
                </div>
            </div>
        </div>
    </form>

@endsection