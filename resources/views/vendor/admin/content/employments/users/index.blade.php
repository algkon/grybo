@extends('admin::layouts.crud.all')

@section('content')

    <div class="panel panel-flat">
        <table class="table datatable-basic table-striped">
            <thead>
            <tr>
                <th>Klientas</th>
                <th>Užsiėmimas</th>
                <th>Užsiėmimo data/laikas</th>
                <th>Registracijos data</th>
                <th>Būsena</th>
                <th class="text-center no-sort actions">Veiksmai</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr class="pointer">
                    <td>{{ $user->full_name }}</td>
                    <td>
                        {{ $user->employment_name }}
                        <small class="display-block text-muted">Treneris: {{ $user->coach }}</small>
                    </td>
                    <td>{{ $user->employment_date }} / {{ $user->time_range }}</td>
                    <td>{{ $user->created_at }}</td>
                    <td style="padding-right: 0;">
                        @include('admin::components.labels.employments.users.status', [
                            'model' => $user,
                            'pending_dropdown' => true
                        ])
                    </td>
                    <td class="text-center">
                        @include('admin::components.buttons.actions', [
                            'model' => $user,
                            'view' => false,
                            'routes' => ['edit' => 'employments.users.edit', 'destroy' => 'employments.users.destroy']
                        ])
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

@push('scripts.head')
    <script type="text/javascript" src="{{ asset('vendors/admin/js/pages/employments_users.js') }}"></script>
    <script type="text/javascript">
        function extend_datatable_defaults() {
            $.extend($.fn.dataTable.defaults, {
                order: [[2, 'asc'], [3, 'desc']]
            });
        }
    </script>
@endpush