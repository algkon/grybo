@extends('admin::layouts.crud.single')

@section('content')

    <div class="row">
        <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">

            <!-- Invoice template -->
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h5 class="text-uppercase text-semibold">Registracijos informacija</h5>
                </div>

                <div class="panel-body employment-user-details no-padding-bottom">
                    <div class="well well-sm well-row-sm content-group">
                        <dl class="dl-horizontal">
                            <dt>Vardas/Pavardė:</dt>
                            <dd>{{ $model->first_name }} {{ $model->last_name }}</dd>
                            <dt>Registruotas vartotojas:</dt>
                            <dd>
                                @if ($model->user)
                                    <span class="label label-success">Taip</span>
                                    <a href="{{ route('admin.users.edit', $model->user_id) }}" target="_blank">
                                        #{{ $model->user_id }}
                                    </a>
                                @else
                                    <span class="label label-danger">Ne</span>
                                @endif
                            </dd>
                            <dt>Tel. nr.:</dt>
                            <dd>{{ $model->phone }}</dd>
                            {{--<dd>Donec id elit non mi porta gravida at eget metus.</dd>--}}
                            <dt>El. paštas:</dt>
                            <dd>{{ $model->email }}</dd>
                            <dt>Užsiėmimas:</dt>
                            <dd>{{ $model->employment_name }}</dd>
                            <dt>Treneris:</dt>
                            <dd>{{ $model->coach }}</dd>
                            <dt>Užsiėmimo data/laikas:</dt>
                            <dd>{{ $model->employment_date }} / {{ $model->time_range }}</dd>
                            <dt>Registracijos data/laikas:</dt>
                            <dd>{{ $model->created_at }}</dd>
                            <dt>Būsena:</dt>
                            <dd>@include('admin::components.labels.employments.users.status')</dd>
                        </dl>
                    </div>

                    <div class="text-right">

                        @if($model->status != 0)
                            {{ Form::model($model, [
                                'url' => ['admin/employments/users', $model->id],
                                'method' => $method,
                                'class' => 'form-inline content-group',
                                'style' => 'display:inline-block'
                            ]) }}
                            {{ Form::hidden('status', 0) }}
                            <button type="submit" class="btn btn-danger btn-sm btn-labeled">
                                <b><i class="icon-close2"></i></b> Atmesti registraciją
                            </button>
                            {{ Form::close() }}
                        @endif

                        @if($model->status != 1)
                            {{ Form::model($model, [
                                'url' => ['admin/employments/users', $model->id],
                                'method' => $method,
                                'class' => 'form-inline content-group',
                                'style' => 'display:inline-block'
                            ]) }}
                            {{ Form::hidden('status', 1) }}
                            <button type="submit" class="btn btn-success btn-sm btn-labeled">
                                <b><i class="icon-checkmark"></i></b> Patvirtinti registraciją
                            </button>
                            {{ Form::close() }}
                        @endif

                    </div>
                </div>
            </div>

            <!-- /invoice template -->
        </div>
    </div>

@endsection