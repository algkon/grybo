<div class="row">
    <div class="col-md-12">
        {{ Form::model($model, ['url' => $url, 'method' => $method, 'class' => 'form-vertical']) }}

        @if (!$model->exists && is_null($employment))
            <div class="form-group">
                <label class="control-label">Užsiėmimas</label>

                <div class="input-group">
                    <span class="input-group-addon"><i class="icon-lifebuoy"></i></span>
                    {{ Form::select('employment_id', $employments, null, ['class' => 'form-control']) }}
                </div>
            </div>
        @else
            {{ Form::hidden('employment_id', $employment->id) }}
        @endif

        <div class="form-group">
            <label class="control-label">Data</label>

            <div class="input-group">
                <span class="input-group-addon"><i class="icon-calendar3"></i></span>
                {{ Form::text('starting_date', null, ['class' => 'form-control pickadate']) }}
            </div>
        </div>
        <div class="form-group">
            <label class="control-label">Laikas</label>

            <div class="input-group">
                <span class="input-group-addon"><i class="icon-alarm"></i></span>
                {{ Form::text('starting_time', null, ['class' => 'form-control pickatime']) }}
            </div>
        </div>
        <div class="form-group">
            <label class="control-label">Trukmė</label>

            <div class="input-group">
                <span class="input-group-addon"><i class="icon-alarm"></i></span>
                {{ Form::text('duration', null, ['class' => 'form-control pickatime-limit']) }}
            </div>
        </div>
        <div class="form-group">
            <label class="control-label">Minimalus reikalingas narių skaičius</label>
            {{ Form::text('people_min', null, ['class' => 'form-control touchspin-set-value-min']) }}
        </div>
        <div class="form-group">
            <label class="control-label">Vietų skaičius</label>
            {{ Form::text('people_max', null, ['class' => 'form-control touchspin-set-value-max']) }}
        </div>
        </form>
    </div>
</div>