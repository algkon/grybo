@extends('admin::layouts.crud.all')

@section('content')

    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">{{ $employment->title }}</h5>
            <span>Treneris: <a href="{{ route('admin.users.edit', [$employment->coach->id]) }}">{{ $employment->coach->name }}</a></span>
        </div>

        <div class="panel-body no-padding"></div>

        <table class="table datatable-basic table-striped">
            <thead>
            <tr>
                <th>Data</th>
                <th class="text-center" style="width: 15%">Laikas</th>
                <th class="text-center" style="width: 15%">Trukmė</th>
                <th class="text-center" style="width: 15%">Min. narių skaičius</th>
                <th class="text-center" style="width: 15%">Užsiregistravę</th>
                <th class="text-center" style="width: 15%">Būsena</th>
                <th class="text-center no-sort">Veiksmai</th>
            </tr>
            </thead>
            <tbody>
            @foreach($employment->dates as $date)
                @include('admin::content.employments.group.dates.row')
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

@push('scripts.head')
<script type="text/javascript" src="{{ asset('vendors/admin/js/pages/employments_dates.js') }}"></script>
<script>
    function extend_datatable_defaults() {
        $.extend($.fn.dataTable.defaults, {
            order: [[0, 'asc'], [1, 'asc']],
            initComplete: function () {
                $('.datatable-buttons').html(
                        '<a href="{{ route("admin.employments.group.dates.create", [$employment->id]) }}" class="btn btn-default btn-labeled" ' +
                        'onclick="employment_date_add(event, this);"><b><i class="icon-calendar3"></i></b>Įterpti laiką</a>'
                ).addClass('pull-right')
            }
        });
    }
</script>
@endpush

