@if(isset($calendar) && $calendar == true)
    <tr class="pointer">
        <td class="hidden">{{ $date->starting_date_time }}</td>
        <td>{{ $date->employment->title }} ({{ $date->employment->coach->name }})</td>
        <td>{{ $date->starting_date }} / {{ $date->time_range }}</td>
        <td class="text-center">{{ $date->people_min }}</td>
        <td class="text-center">
            @if($date->users->count())
                <a href="{{ route('admin.employments.users.index', ['filter' => ['employment_calendar_id' => $date->id]]) }}"
                   target="_blank">
                    <i class="icon-people"></i> {{ $date->users->count() }}
                </a>/{{ $date->people_max }}
            @else
                {{ $date->users->count() }}/{{ $date->people_max }}
            @endif
        </td>
        <td class="text-center">@include('admin::components.labels.employments.dates.status', ['model' => $date])</td>
        <td class="text-center">@include('admin::components.buttons.employments-dates-actions')</td>
    </tr>
@else
    <tr class="pointer">
        <td>{{ $date->starting_date }}</td>
        <td class="text-center">{{ $date->starting_time }}</td>
        <td class="text-center">{{ $date->duration }}</td>
        <td class="text-center">{{ $date->people_min }}</td>
        <td class="text-center">
            @if($date->users->count())
                <a href="{{ route('admin.employments.users.index', ['filter' => ['employment_calendar_id' => $date->id]]) }}"
                   target="_blank">
                    <i class="icon-people"></i> {{ $date->users->count() }}
                </a>/{{ $date->people_max }}
            @else
                {{ $date->users->count() }}/{{ $date->people_max }}
            @endif
        </td>
        <td class="text-center" >@include('admin::components.labels.employments.dates.status', ['model' => $date])</td>
        <td class="text-center">@include('admin::components.buttons.employments-dates-actions')</td>
    </tr>
@endif