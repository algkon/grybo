@extends('admin::layouts.crud.all')

@section('content')

    <div class="panel panel-flat">
        <table class="table datatable-basic table-striped">
            <thead>
            <tr>
                <th style="width: 30%">Pavadinimas</th>
                <th style="width: 30%">Treneris</th>
                <th style="width: 15%">Būsena</th>
                <th class="text-center no-sort">Tvarkaraštis</th>
                <th class="text-center no-sort">Veiksmai</th>
            </tr>
            </thead>
            <tbody>
            @foreach($employments as $employment)
                <tr class="pointer">
                    <td>{{ $employment->title }}</td>
                    <td>{{ $employment->coach->name }}</td>
                    <td>@include('admin::components.labels.status', ['model' => $employment])</td>
                    <td class="text-center">
                        <a href="{{ route('admin.employments.group.dates.index', $employment->id) }}">
                            <i class="icon-calendar"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        @include('admin::components.buttons.actions', [
                            'model' => $employment,
                            'view' => false,
                            'routes' => [
                                'edit' => 'employments.group.edit',
                                'destroy' => 'employments.group.destroy',
                            ]
                        ])
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection