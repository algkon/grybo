@extends('admin::layouts.crud.single')

@section('content')

    {{ Form::model($model, ['url' => ['admin/employments/group', $model->id], 'method' => $method, 'class' => 'form-horizontal']) }}

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">


                    <fieldset>
                        <legend class="text-semibold">
                            <i class="icon-file-empty position-left"></i>
                            Pagrindinė informacija
                            <a class="control-arrow" data-toggle="collapse" data-target="#collapse-main">
                                <i class="icon-circle-down2"></i>
                            </a>
                        </legend>

                        <div class="collapse in" id="collapse-main">
                            <div class="form-group {{ has_error($errors, 'title') }} {{ has_error($errors, 'slug') }}">
                                <label class="control-label col-lg-3">Pavadinimas <span
                                            class="text-danger">*</span></label>

                                <div class="col-lg-9">
                                    {{ Form::text('title', null, ['class' => 'form-control']) }}
                                </div>
                            </div>

                            <div class="form-group {{ has_error($errors, 'user_id') }}">
                                <label class="control-label col-lg-3">Treneris <span
                                            class="text-danger">*</span></label>

                                <div class="col-lg-9">
                                    {{ Form::select('user_id', $coaches, null, ['class' => 'form-control']) }}
                                </div>
                            </div>

                            @include('admin::components.form.status', ['disabled' => ($model->users->count() ? true : false)])
                        </div>
                    </fieldset>

                </div>
            </div>

            {{--<div class="panel panel-white">--}}
                {{--<div class="panel-body">--}}

                    {{--<fieldset>--}}
                        {{--<legend class="text-semibold" style="margin-bottom: 0;">--}}
                            {{--<i class="icon-calendar position-left"></i>--}}
                            {{--Kalendorius--}}
                            {{--<a class="control-arrow" data-toggle="collapse" data-target="#collapse-calendar">--}}
                                {{--<i class="icon-circle-down2"></i>--}}
                            {{--</a>--}}
                        {{--</legend>--}}
                        {{--<div class="collapse in" id="collapse-calendar">--}}

                            {{--<table id="employment_calendar" class="table datatable-basic table-striped"--}}
                                   {{--style="margin-top: 0;">--}}
                                {{--<thead>--}}
                                {{--<tr>--}}
                                    {{--<th>Data</th>--}}
                                    {{--<th class="text-center" style="width: 18%">Laikas</th>--}}
                                    {{--<th class="text-center" style="width: 18%">Trukmė</th>--}}
                                    {{--<th class="text-center" style="width: 18%">Min. narių skaičius</th>--}}
                                    {{--<th class="text-center" style="width: 18%">Vietų skaičius</th>--}}
                                    {{--<th class="text-center" style="width: 18%">Būsena</th>--}}
                                    {{--<th class="no-sort"></th>--}}
                                {{--</tr>--}}
                                {{--</thead>--}}
                                {{--<tbody>--}}
                                {{--@foreach(old('dates', $model->dates->toArray()) as $date)--}}
                                    {{--<tr class="date" data-starting_date="{{ $date['starting_date'] }}"--}}
                                        {{--data-starting_time="{{ $date['starting_time'] }}"--}}
                                        {{--data-duration="{{ $date['duration'] }}"--}}
                                        {{--data-people_min="{{ $date['people_min'] }}"--}}
                                        {{--data-people_max="{{ $date['people_max'] }}"--}}
                                        {{--data-is_active="{{ $date['is_active'] ? 1 : 0 }}"--}}
                                        {{--data-is_now="{{ $date['is_now'] ? 1 : 0 }}"--}}
{{--                                        data-is_ended="{{ $date['is_ended'] ? 1 : 0 }}"--}}
                                        {{--data-is_new="0"--}}
                                        {{--data-id="{{ $date['id'] }}">--}}

                                        {{--<td>--}}
                                            {{--{{ $date['starting_date'] }}--}}
                                            {{--<input type="hidden" name="dates[{{ $date['id'] }}][starting_date]"--}}
                                                   {{--value="{{ $date['starting_date'] }}">--}}
                                            {{--<input type="hidden" name="dates[{{ $date['id'] }}][id]"--}}
                                                   {{--value="{{ $date['id'] }}">--}}
                                        {{--</td>--}}
                                        {{--<td class="text-center">--}}
                                            {{--{{ $date['starting_time'] }}--}}
                                            {{--<input type="hidden" name="dates[{{ $date['id'] }}][starting_time]"--}}
                                                   {{--value="{{ $date['starting_time'] }}">--}}
                                        {{--</td>--}}
                                        {{--<td class="text-center">--}}
                                            {{--{{ $date['duration'] }}--}}
                                            {{--<input type="hidden" name="dates[{{ $date['id'] }}][duration]"--}}
                                                   {{--value="{{ $date['duration'] }}">--}}
                                        {{--</td>--}}
                                        {{--<td class="text-center">--}}
                                            {{--{{ $date['people_min'] }}--}}
                                            {{--<input type="hidden" name="dates[{{ $date['id'] }}][people_min]"--}}
                                                   {{--value="{{ $date['people_min'] }}">--}}
                                        {{--</td>--}}
                                        {{--<td class="text-center">--}}
                                            {{--{{ $date['people_max'] }}--}}
                                            {{--<input type="hidden" name="dates[{{ $date['id'] }}][people_max]"--}}
                                                   {{--value="{{ $date['people_max'] }}">--}}
                                        {{--</td>--}}

                                        {{--<td class="text-center">--}}
                                            {{--<input type="hidden" name="dates[{{ $date['id'] }}][is_active]"--}}
                                                   {{--value="{{ $date['is_active'] }}">--}}
                                            {{--<input type="hidden" name="dates[{{ $date['id'] }}][is_now]"--}}
                                                   {{--value="{{ $date['is_now'] }}">--}}
                                            {{--<input type="hidden" name="dates[{{ $date['id'] }}][is_ended]"--}}
                                                   {{--value="{{ $date['is_ended'] }}">--}}

                                            {{--@if (!isset($date['is_new']))--}}
                                                {{----}}
                                            {{--@else--}}
                                                {{--<span class="label label-default">Įsigalios išsaugojus</span>--}}
                                            {{--@endif--}}
                                        {{--</td>--}}

                                        {{--<td class="text-center">--}}
                                            {{--<ul class="icons-list">--}}

                                                {{--<li class="dropdown">--}}
                                                    {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown"--}}
                                                       {{--aria-expanded="false">--}}
                                                        {{--<i class="icon-menu9"></i>--}}
                                                    {{--</a>--}}

                                                    {{--<ul class="dropdown-menu dropdown-menu-right">--}}
                                                        {{--@if (isset($date['is_now'], $date['is_active'], $date['is_ended']))--}}
                                                            {{--@if(!$date['is_now'] && $date['is_active'] && !$date['is_ended'])--}}
                                                                {{--<li>--}}
                                                                    {{--<a href="javascript:void(0)" onclick="employment_date_edit(this);">--}}
                                                                        {{--<i class="icon-pencil"></i> Redaguoti--}}
                                                                    {{--</a>--}}
                                                                {{--</li>--}}
                                                            {{--@elseif ($date['is_ended'] || !$date['is_active'])--}}
                                                                {{--<li>--}}
                                                                    {{--<a href="javascript:void(0)" onclick="return $.fn.currentDatatable.sweetDelete(this);">--}}
                                                                        {{--<i class="icon-trash"></i>Ištrinti--}}
                                                                    {{--</a>--}}
                                                                {{--</li>--}}
                                                            {{--@else--}}
                                                                {{--<li><a>Veiksmus bus galima atlikti pasibaigus užsiėmimui</a></li>--}}
                                                            {{--@endif--}}
                                                        {{--@else--}}
                                                            {{--<li>--}}
                                                                {{--<a href="javascript:void(0)" onclick="employment_date_edit(this);">--}}
                                                                    {{--<i class="icon-pencil"></i> Redaguoti--}}
                                                                {{--</a>--}}
                                                            {{--</li>--}}
                                                            {{--<li>--}}
                                                                {{--<a href="javascript:void(0)" onclick="return $.fn.currentDatatable.sweetDelete(this);">--}}
                                                                    {{--<i class="icon-trash"></i>Ištrinti--}}
                                                                {{--</a>--}}
                                                            {{--</li>--}}
                                                        {{--@endif--}}
                                                    {{--</ul>--}}
                                                {{--</li>--}}

                                            {{--</ul>--}}
                                        {{--</td>--}}

                                    {{--</tr>--}}
                                {{--@endforeach--}}
                                {{--</tbody>--}}
                            {{--</table>--}}

                            {{--<div class="row">--}}
                                {{--<div class="col-xs-12">--}}
                                    {{--<button type="button" class="btn btn-default btn-labeled" data-starting_date=""--}}
                                            {{--data-starting_time="" data-duration="" data-people_min="4" data-people_max="12"--}}
                                            {{--data-is_active="0"--}}
                                            {{--data-is_now="0"--}}
                                            {{--data-is_ended="0"--}}
                                            {{--data-is_new="1"--}}
                                            {{--onclick="employment_date_add(this);">--}}
                                        {{--<b><i class="icon-calendar3"></i></b>--}}
                                        {{--Įterpti laiką--}}
                                    {{--</button>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</fieldset>--}}

                    {{--@include('admin::components.form.metadata')--}}

                {{--</div>--}}
            {{--</div>--}}
        </div>

        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">
                    <div class="text-right">
                        <a href="{{ route('admin.employments.group.index') }}" class="btn btn-danger btn-sm btn-labeled">
                            <b><i class="icon-close2"></i></b> Atšaukti
                        </a>
                        <button type="submit" class="btn btn-success btn-sm btn-labeled">
                            <b><i class="icon-checkmark"></i></b> Išsaugoti
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{ Form::close() }}

@endsection

@push('scripts.head')
<script type="text/javascript" src="{{ asset('vendors/admin/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendors/admin/js/pages/employments.js') }}"></script>
@endpush