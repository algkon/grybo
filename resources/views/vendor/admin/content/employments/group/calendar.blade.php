@extends('admin::layouts.crud.all')

@section('content')

    <div class="panel panel-flat">
        <table class="table datatable-basic table-striped">
            <thead>
            <tr>
                <th class="hidden"></th>
                <th>Užsiėmimas</th>
                <th>Data/Laikas</th>
                <th class="text-center" style="width: 15%">Min. narių skaičius</th>
                <th class="text-center" style="width: 10%">Užsiregistravę</th>
                <th class="text-center" style="width: 10%">Būsena</th>
                <th class="text-center no-sort">Veiksmai</th>
            </tr>
            </thead>
            <tbody>
            @foreach($dates as $date)
                @include('admin::content.employments.group.dates.row', ['calendar' => true])
            @endforeach
            </tbody>
        </table>
    </div>

@endsection


@push('scripts.head')
<script type="text/javascript" src="{{ asset('vendors/admin/js/pages/employments_dates.js') }}"></script>
<script>
    function extend_datatable_defaults() {
        $.extend($.fn.dataTable.defaults, {
            order: [[0, 'asc']],
            columnDefs: [
                {
                    visible: false,
                    targets: 0
                },
                {
                    orderable: false,
                    width: '40px',
                    targets: "no-sort"
                }
            ],
            initComplete: function () {
                $('.datatable-buttons').html(
                    '<a href="{{ route("admin.employments.group.dates.create", [0]) }}" class="btn btn-default btn-labeled" ' +
                    'onclick="employment_date_add(event, this);"><b><i class="icon-calendar3"></i></b>Įterpti laiką</a>'
                ).addClass('pull-right')
            }
        });
    }
</script>
@endpush