<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ trans('admin::routes.' . request()->route()->getName()) }} | Admin dashboard</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{ asset('vendors/admin/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('vendors/admin/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('vendors/admin/css/core.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('vendors/admin/css/components.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('vendors/admin/css/colors.css') }}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="{{ asset('vendors/admin/js/plugins/loaders/pace.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/admin/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/admin/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/admin/js/plugins/loaders/blockui.min.js') }}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('vendors/admin/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/admin/js/core/libraries/jquery_ui/touch.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/admin/js/plugins/ui/moment/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/admin/js/plugins/pickers/anytime.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/admin/js/plugins/pickers/pickadate/picker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/admin/js/plugins/pickers/pickadate/picker.date.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/admin/js/plugins/pickers/pickadate/picker.time.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/admin/js/plugins/pickers/pickadate/legacy.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/admin/js/plugins/forms/inputs/touchspin.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('vendors/admin/js/plugins/media/fancybox.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/admin/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/admin/js/plugins/tables/datatables/extensions/buttons.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/admin/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/admin/js/plugins/notifications/sweet_alert.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/admin/js/plugins/notifications/bootbox.min.js') }}"></script>
    {{--<script type="text/javascript" src="{{ asset('vendors/admin/js/core/libraries/jquery_ui/touch.min.js') }}"></script>--}}
    {{--<script type="text/javascript" src="{{ asset('vendors/admin/js/plugins/ui/moment/moment.min.js') }}"></script>--}}
    {{--<script type="text/javascript" src="{{ asset('vendors/admin/js/plugins/pickers/anytime.min.js') }}"></script>--}}
    {{--<script type="text/javascript" src="{{ asset('vendors/admin/js/plugins/pickers/pickadate/picker.js') }}"></script>--}}
    {{--<script type="text/javascript" src="{{ asset('vendors/admin/js/plugins/pickers/pickadate/picker.date.js') }}"></script>--}}
    {{--<script type="text/javascript" src="{{ asset('vendors/admin/js/plugins/pickers/pickadate/picker.time.js') }}"></script>--}}
    {{--<script type="text/javascript" src="{{ asset('vendors/admin/js/plugins/pickers/pickadate/legacy.js') }}"></script>--}}
    {{--<script type="text/javascript" src="{{ asset('vendors/admin/js/plugins/forms/inputs/touchspin.min.js') }}"></script>--}}

    @stack('scripts.head')

    <script type="text/javascript" src="{{ asset('vendors/admin/js/core/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/admin/js/pages/components_modals.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/admin/js/pages/datatables_basic.js') }}"></script>
    <!-- /theme JS files -->

</head>

<body>

<!-- Main navbar -->
@include('admin::navigations.main')
<!-- /main navbar -->

<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        @include('admin::sidebar')
        <!-- /main sidebar -->

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            @include('admin::content.header')
            <!-- /page header -->

            <!-- Content area -->
            <div class="content">
                @include('admin::components.notice')

                @yield('content')
            </div>
            <!-- /content area -->

            <!-- Footer -->
            @include('admin::content.footer')
            <!-- /footer -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>

</html>
