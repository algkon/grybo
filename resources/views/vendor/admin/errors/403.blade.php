@extends('admin::layouts.error')

@section('content')

    <!-- Error title -->
    <div class="text-center content-group">
        <h1 class="error-title">403</h1>
        <h5>Jūsų teisės apribotos!</h5>
    </div>
    <!-- /error title -->

    <!-- Error content -->
    <div class="row">
        <div class="col-lg-4 col-lg-offset-4 col-sm-6 col-sm-offset-3">
            <div class="text-center">
                <a href="{{ route('admin.index') }}" class="btn btn-primary btn-block content-group"><i
                            class="icon-circle-left2 position-left"></i> Grįžti į pagrindinį puslapį</a>
            </div>
        </div>
    </div>
    <!-- /error wrapper -->

@endsection