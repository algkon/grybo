@if ($paginator->lastPage() > 1)
    <div class="Pagination" layout="v-center">
        @for ($i = 1; $i <= $paginator->lastPage(); $i++)

            @if (($paginator->currentPage() <= 5 && $i <= 7 && $i <= $paginator->lastPage()) || $i == $paginator->lastPage() || $i == 1)
                <a rel="nofollow" class="p-item {{ $paginator->currentPage() == $i ? '_active' : '' }}" href="{{ $paginator->url($i) }}" layout="v-center">
                    <span>{{ $i }}</span>
                </a>
            @elseif($paginator->currentPage() >= 5 && $paginator->currentPage() <= $paginator->lastPage() - 5 && ($i >= $paginator->currentPage() - 2 && $i <= $paginator->currentPage() + 2))
                <a rel="nofollow" class="p-item {{ $paginator->currentPage() == $i ? '_active' : '' }}" href="{{ $paginator->url($i) }}" layout="v-center">
                    <span>{{ $i }}</span>
                </a>
            @elseif($paginator->lastPage() > 7 && $i >= $paginator->lastPage() - 6 && $paginator->currentPage() > $paginator->lastPage() - 5)
                <a rel="nofollow" class="p-item {{ $paginator->currentPage() == $i ? '_active' : '' }}" href="{{ $paginator->url($i) }}" layout="v-center">
                    <span>{{ $i }}</span>
                </a>
            @endif

            @if (($paginator->currentPage() > 5 && $i == 1) || ($paginator->currentPage() <= $paginator->lastPage() - 5 && $i == $paginator->lastPage() - 1))
                <span class="p-gap" layout="v-end">
                    <span>...</span>
                </span>
            @endif
        @endfor
    </div>
@endif
