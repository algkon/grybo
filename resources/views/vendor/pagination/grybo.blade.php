@if ($paginator->lastPage() > 1)
    <div class="pagination">
        <ul class="clearfix">
            @for ($i = 1; $i <= $paginator->lastPage(); $i++)

                @if (($paginator->currentPage() <= 4 && $i <= 5 && $i <= $paginator->lastPage()) || $i == $paginator->lastPage() || $i == 1)
                    <li class="{{ $paginator->currentPage() == $i ? 'active' : '' }}">
                        <a href="{{ $paginator->url($i) }}">{{ $i }}</a>
                    </li>
                @elseif($paginator->currentPage() >= 4 && $paginator->currentPage() <= $paginator->lastPage() - 4 && ($i >= $paginator->currentPage() - 2 && $i <= $paginator->currentPage() + 2))
                    <li class="{{ $paginator->currentPage() == $i ? 'active' : '' }}">
                        <a href="{{ $paginator->url($i) }}">{{ $i }}</a>
                    </li>
                @elseif($paginator->lastPage() > 5 && $i >= $paginator->lastPage() - 4 && $paginator->currentPage() > $paginator->lastPage() - 4)
                    <li class="{{ $paginator->currentPage() == $i ? 'active' : '' }}">
                        <a href="{{ $paginator->url($i) }}">{{ $i }}</a>
                    </li>
                @endif

                @if (($paginator->currentPage() > 4 && $i == 1) || ($paginator->currentPage() <= $paginator->lastPage() - 4 && $i == $paginator->lastPage() - 1))
                    <li>...</li>
                @endif
            @endfor
        </ul>
    </div>
@endif
