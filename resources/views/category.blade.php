@extends('layouts.app-inner')

@section('meta_title', $model->meta_title)
@section('meta_description', $model->meta_description)
@section('meta_keywords', $model->meta_keywords)

@section('content')

    <section class="articles news">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="page-header">
                        <h3 class="title-bg">{{ $model->title }}</h3>
                    </div>

                    @if($posts)
                        <div class="articles-list clearfix">
                            @foreach($posts as $post)
                                <article>
                                    <div class="pull-left">
                                        <a href="{{ $post->url() }}">
                                            <img src="{{ $post->thumbnail('post-medium') }}" alt="{{ $post->title }}" title="{{ $post->title }}" />
                                        </a>
                                    </div>

                                    <div class="pull-right">
                                        <header class="title-sm">
                                            <a href="{{ $post->url() }}">{{ $post->title }}</a>
                                        </header>

                                        <p>{{ $post->short_description }}</p>

                                        <footer class="clearfix">
                                            <a href="{{ $post->url() }}" class="read-more">Skaityti daugiau</a>
                                            <time>{{ $post->createdDateAsString() }}</time>
                                        </footer>
                                    </div>

                                    <div class="clearfix"></div>
                                </article>
                            @endforeach
                        </div>

                        {{ $posts->links('pagination::grybo') }}
                    @endif
                </div>
            </div> <!-- /row -->
        </div>
    </section>

@endsection


