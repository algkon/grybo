@extends('layouts.app-inner')

@section('meta_title', 'Asmeninė informacija')

@section('content')

    <section class="order-form">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <div class="page-header">
                        <h3 class="title-bg">Asmeninė informacija</h3>
                    </div>

                    @include('components.notification')

                    <div class="form-block">

                        <form class="form-horizontal" role="form" method="POST" action="{{ request()->fullUrl() }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('first_name') ? ' has-error has-feedback' : '' }}">
                                <label for="first_name" class="col-md-3 control-label">Vardas</label>

                                <div class="col-md-9">
                                    <input type="text" name="first_name" value="{{ auth()->user()->api->first_name }}" class="form-control" disabled>
                                    <input type="hidden" name="first_name" value="{{ auth()->user()->api->first_name }}">
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('last_name') ? ' has-error has-feedback' : '' }}">
                                <label for="last_name" class="col-md-3 control-label">Pavardė</label>

                                <div class="col-md-9">
                                    <input type="text" name="last_name" value="{{ auth()->user()->api->last_name }}" class="form-control" disabled>
                                    <input type="hidden" name="last_name" value="{{ auth()->user()->api->last_name }}">
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error has-feedback' : '' }}">
                                <label for="email" class="col-md-3 control-label">El. paštas</label>

                                <div class="col-md-9">
                                    <input type="email" name="email" value="{{ auth()->user()->api->email }}" class="form-control" disabled>
                                    <input type="hidden" name="email" value="{{ auth()->user()->api->email }}">
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('phone') ? ' has-error has-feedback' : '' }}">
                                <label for="phone" class="col-md-3 control-label">Telefono numeris</label>

                                <div class="col-md-9">
                                    <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}">

                                    @if ($errors->has('phone'))
                                        <span class="glyphicon glyphicon-remove form-control-feedback"
                                              aria-hidden="true"></span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-9 col-sm-12 col-xs-12 pull-right text-left">
                                    <input type="submit" name="submit" class="btn btn-default" value="Toliau ->">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection