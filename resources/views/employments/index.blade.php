@extends('layouts.app-inner')

@section('meta_title', 'Užsiėmimų tvarkaraštis')

@section('content')

    <section class="sessions">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="page-header">
                        <h3 class="title-bg">Užsiėmimų tvarkaraštis</h3>
                    </div>

                    @include('components.notification')

                    <div class="choose-date">
                        <p class="info-text">Pasirinkite pageidaujamą užsiėmimų datą kalendoriuje...</p>

                        {!! $calendar !!}
                    </div>

                    <div id="day-events">
                        @if ($events_in_day->count())
                            <table class="table session-time">
                                <thead>
                                <tr>
                                    <th>Data/laikas nuo-iki</th>
                                    <th>Užsiėmimo pavadinimas</th>
                                    <th>Treneris</th>
                                    <th></th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($events_in_day as $event)
                                    <tr>
                                        <td data-title="Data/laikas">{{ $event->starting_date }} / {{ $event->time_range }}</td>
                                        <td data-title="Užsiėmimo pavadinimas">{{ $event->employment->title }}</td>
                                        <td data-title="Treneris">{{ $event->employment->coach->name }}</td>
                                        <td class="register">
                                            @if($event->available())
                                                <a href="{{ route('employments.register.form', [$event->id]) }}">Registruotis <span>Liko vietų: {{ $event->total_free_space }}</span></a>
                                            @else
                                                @if($event->total_free_space)
                                                    <a href="#" class="inactive">Registruotis <span>Nebegalioja</span></a>
                                                @else
                                                    <a href="#" class="inactive">Registruotis <span>Liko vietų: {{ $event->total_free_space }}</span></a>
                                                @endif
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
            </div> <!-- /row -->
        </div>
    </section>

@endsection