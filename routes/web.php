<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
use Admin\Repositories\PageRepository;
use Admin\Repositories\CategoryRepositoy;
use Admin\Repositories\PostRepository;

Route::get('/cron', function () {
    Artisan::call('schedule:run');
});

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);

//Auth::routes();

// login
Route::get('/prisijungimas', ['as' => 'auth.login', 'uses' => 'Auth\LoginController@showLoginForm']);
Route::post('/prisijungimas', 'Auth\LoginController@login');

// register
Route::get('/registracija', ['as' => 'auth.register', 'uses' => 'Auth\RegisterController@showRegistrationForm']);
Route::post('/registracija', 'Auth\RegisterController@register');

// logout
Route::get('/atsijungti', ['as' => 'auth.logout', 'uses' => 'Auth\LoginController@logout']);

// activate user
Route::get('/vartotojo-aktyvavimas', ['as' => 'auth.activate.form', 'uses' => 'Auth\ActivateController@showActivationForm']);
Route::post('/vartotojo-aktyvavimas', 'Auth\ActivateController@activate');

// forgot password
Route::get('/slaptazodzio-priminimas', [
    'as' => 'auth.passwords.reset',
    'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm'
]);
Route::post('/slaptazodzio-priminimas', 'Auth\ForgotPasswordController@sendResetLinkEmail');

// view profile
Route::get('/vartotojo-zona', ['as' => 'account.profile', 'uses' => 'AccountController@profile']);

// edit profile
Route::get('/redaguoti-informacija', ['as' => 'account.profile.change', 'uses' => 'AccountController@showEditForm']);
Route::post('/redaguoti-informacija', 'AccountController@edit');

// change password
Route::get('/keisti-slaptazodi', ['as' => 'account.password.change', 'uses' => 'AccountController@showPasswordForm']);
Route::post('/keisti-slaptazodi', 'AccountController@changePassword');

// orders history
Route::get('/uzsakymu-istorija', ['as' => 'account.orders', 'uses' => 'AccountController@orders']);

// activity
Route::get('aktyvumas', ['as' => 'account.employments', 'uses' => 'AccountController@employments']);

// credit
Route::get('/pildyti-saskaita', ['as' => 'account.credit', 'uses' => 'AccountController@showCreditForm']);
Route::post('/pildyti-saskaita', ['as' => 'account.credit.buy', 'uses' => 'AccountController@buyCredit']);

// memberships
Route::get('/paslaugos', ['as' => 'memberships', 'uses' => 'MembershipsController@showMembershipsForm']);

// employments
Route::get('/uzsiemimai/{year?}/{month?}/{day?}', ['as' => 'employments', 'uses' => 'EmploymentsController@showCalendar']);

Route::get('/registracija-i-uzsiemima/{id}', [
    'as' => 'employments.register.form',
    'uses' => 'EmploymentsController@showRegisterForm'
])->where('id', '[0-9]+');

Route::post('/registracija-i-uzsiemima/{id}', [
    'as' => 'employments.register.register',
    'uses' => 'EmploymentsController@register'
])->where('id', '[0-9]+');

// Cart
Route::get('/pirkiniu-krepselis', ['as' => 'cart', 'uses' => 'CartController@index']);
Route::post('/cart/add', ['as' => 'cart.add', 'uses' => 'CartController@add']);
Route::delete('/cart/delete/{id}', ['as' => 'cart.delete', 'uses' => 'CartController@delete']);

// Order
Route::get('/uzsakymo-forma', ['as' => 'order.form', 'uses' => 'OrdersController@showUserDetailsForm']);
Route::post('/uzsakymo-forma', ['as' => 'order.form.submit', 'uses' => 'OrdersController@userDetailsFormSubmit']);
Route::get('/uzsakymas/{order_id}', ['as' => 'order.show', 'uses' => 'OrdersController@show']);
Route::post('/uzsakymas/{order_id}', ['as' => 'order.submit', 'uses' => 'OrdersController@submit']);

// Payment
Route::get('/apmoketi/{order_id}', ['as' => 'payment.submit', 'uses' => 'PaymentController@pay']);
Route::get('/payment/success', ['as' => 'payment.success', 'uses' => 'PaymentController@success']);
Route::get('/payment/cancel', ['as' => 'payment.cancel', 'uses' => 'PaymentController@cancel']);

// Newsletter
Route::post('/naujienlaiskis', ['as' => 'subscribe', 'uses' => 'SubscribersController@subscribe']);
Route::get('/unsubscribe/{unsubscribe_token}', ['as' => 'unsubscribe', 'uses' => 'SubscribersController@unsubscribe']);

// Galleries
Route::get('/galerija/{slug}', ['as' => 'gallery', 'uses' => 'GalleriesController@show']);

Route::get('/tvarkarasciai', ['as' => 'timetables.index', 'uses' => 'TimetablesController@index']);
Route::get('/tvarkarasciai/{slug}/{start?}/{end?}', ['as' => 'timetables.show', 'uses' => 'TimetablesController@show']);

// Pages, Categories, Posts
Route::get('{slug}', function ($slug, PageRepository $page_repo, CategoryRepositoy $category_repo, PostRepository $post_repo) {

    // first check if page exist
    if ($page = $page_repo->active()->findBy('slug', $slug)) {
        return app('App\Http\Controllers\PagesController')->callAction('show', compact('page'));
    }

    // second check if category exist
    if ($category = $category_repo->active()->findBy('slug', $slug)) {
        return app('App\Http\Controllers\CategoriesController')->callAction('show', compact('category'));
    }

    // last check if post exists
    if ($post = $post_repo->active()->findBy('slug', $slug)) {
        return app('App\Http\Controllers\PostsController')->callAction('show', compact('post'));
    }

    return redirect('/', 301); // if has been not found any rows, redirect home
});

// only post route
Route::get('{category}/{slug}', function ($category, $slug, PostRepository $post_repo) {

    if ($post = $post_repo->active()->findBy('slug', $slug)) {
        return app('App\Http\Controllers\PostsController')->callAction('show', compact('post'));
    }

    return redirect('/', 301); // if has been not found any rows, redirect home
});
