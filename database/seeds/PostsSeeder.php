<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostsSeeder extends Seeder
{
    /**
     * @var \Admin\Repositories\PostRepository
     */
    private $post_repo;

    /**
     * PostsSeeder constructor.
     * @param \Admin\Repositories\PostRepository $post_repo
     * @param \Admin\Models\Post $post
     */
    public function __construct(\Admin\Repositories\PostRepository $post_repo)
    {
        $this->post_repo = $post_repo;
    }


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $articles = DB::connection('mysql2')->table('grybo7_mod_news2')->select(
            'visible',
            'title',
            'news_data',
            'text_short',
            'text',
            'img_mini',
            'edit_date',
            'sef_alias'
        )->where('img_mini', '!=', '')->get();

//        dd($articles->count());
        foreach ($articles as $article) {
            $post = $this->post_repo->makeModel();
            $post->fill([
                'user_id' => 31,
                'category_id' => 4,
                'title' => $article->title,
                'description' => $article->text_short,
                'content' => str_replace('<img src="/upl/', '<img src="/uploads/', $article->text),
                'slug' => $article->sef_alias,
                'active' => ($article->visible == 1) ? '1' : '0',
                'created_at' => $article->news_data,
                'updated_at' => $article->edit_date
            ]);
            $post->save();

            // set attachment
            $post->setAttachment([
                'name' => $article->img_mini,
                'path' => 'uploads/naujienos/' . $article->img_mini,
            ]);

            // set categories
            $post->categories()->sync([4]);
        }


//
//        dd($post);
    }
}
